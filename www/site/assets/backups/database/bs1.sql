# --- WireDatabaseBackup {"time":"2020-06-06 19:26:12","user":"admin","dbName":"db_boardschmiede","description":"","tables":[],"excludeTables":[],"excludeCreateTables":[],"excludeExportTables":[]}

DROP TABLE IF EXISTS `caches`;
CREATE TABLE `caches` (
  `name` varchar(250) NOT NULL,
  `data` mediumtext NOT NULL,
  `expires` datetime NOT NULL,
  PRIMARY KEY (`name`),
  KEY `expires` (`expires`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('Modules.wire/modules/', 'AdminTheme/AdminThemeDefault/AdminThemeDefault.module\nAdminTheme/AdminThemeReno/AdminThemeReno.module\nAdminTheme/AdminThemeUikit/AdminThemeUikit.module\nFieldtype/FieldtypeCache.module\nFieldtype/FieldtypeCheckbox.module\nFieldtype/FieldtypeComments/CommentFilterAkismet.module\nFieldtype/FieldtypeComments/FieldtypeComments.module\nFieldtype/FieldtypeComments/InputfieldCommentsAdmin.module\nFieldtype/FieldtypeDatetime.module\nFieldtype/FieldtypeEmail.module\nFieldtype/FieldtypeFieldsetClose.module\nFieldtype/FieldtypeFieldsetOpen.module\nFieldtype/FieldtypeFieldsetTabOpen.module\nFieldtype/FieldtypeFile.module\nFieldtype/FieldtypeFloat.module\nFieldtype/FieldtypeImage.module\nFieldtype/FieldtypeInteger.module\nFieldtype/FieldtypeModule.module\nFieldtype/FieldtypeOptions/FieldtypeOptions.module\nFieldtype/FieldtypePage.module\nFieldtype/FieldtypePageTable.module\nFieldtype/FieldtypePageTitle.module\nFieldtype/FieldtypePassword.module\nFieldtype/FieldtypeRepeater/FieldtypeFieldsetPage.module\nFieldtype/FieldtypeRepeater/FieldtypeRepeater.module\nFieldtype/FieldtypeRepeater/InputfieldRepeater.module\nFieldtype/FieldtypeSelector.module\nFieldtype/FieldtypeText.module\nFieldtype/FieldtypeTextarea.module\nFieldtype/FieldtypeToggle.module\nFieldtype/FieldtypeURL.module\nFileCompilerTags.module\nImage/ImageSizerEngineAnimatedGif/ImageSizerEngineAnimatedGif.module\nImage/ImageSizerEngineIMagick/ImageSizerEngineIMagick.module\nInputfield/InputfieldAsmSelect/InputfieldAsmSelect.module\nInputfield/InputfieldButton.module\nInputfield/InputfieldCheckbox.module\nInputfield/InputfieldCheckboxes/InputfieldCheckboxes.module\nInputfield/InputfieldCKEditor/InputfieldCKEditor.module\nInputfield/InputfieldDatetime/InputfieldDatetime.module\nInputfield/InputfieldEmail.module\nInputfield/InputfieldFieldset.module\nInputfield/InputfieldFile/InputfieldFile.module\nInputfield/InputfieldFloat.module\nInputfield/InputfieldForm.module\nInputfield/InputfieldHidden.module\nInputfield/InputfieldIcon/InputfieldIcon.module\nInputfield/InputfieldImage/InputfieldImage.module\nInputfield/InputfieldInteger.module\nInputfield/InputfieldMarkup.module\nInputfield/InputfieldName.module\nInputfield/InputfieldPage/InputfieldPage.module\nInputfield/InputfieldPageAutocomplete/InputfieldPageAutocomplete.module\nInputfield/InputfieldPageListSelect/InputfieldPageListSelect.module\nInputfield/InputfieldPageListSelect/InputfieldPageListSelectMultiple.module\nInputfield/InputfieldPageName/InputfieldPageName.module\nInputfield/InputfieldPageTable/InputfieldPageTable.module\nInputfield/InputfieldPageTitle/InputfieldPageTitle.module\nInputfield/InputfieldPassword/InputfieldPassword.module\nInputfield/InputfieldRadios/InputfieldRadios.module\nInputfield/InputfieldSelect.module\nInputfield/InputfieldSelectMultiple.module\nInputfield/InputfieldSelector/InputfieldSelector.module\nInputfield/InputfieldSubmit/InputfieldSubmit.module\nInputfield/InputfieldText.module\nInputfield/InputfieldTextarea.module\nInputfield/InputfieldToggle/InputfieldToggle.module\nInputfield/InputfieldURL.module\nJquery/JqueryCore/JqueryCore.module\nJquery/JqueryMagnific/JqueryMagnific.module\nJquery/JqueryTableSorter/JqueryTableSorter.module\nJquery/JqueryUI/JqueryUI.module\nJquery/JqueryWireTabs/JqueryWireTabs.module\nLanguageSupport/FieldtypePageTitleLanguage.module\nLanguageSupport/FieldtypeTextareaLanguage.module\nLanguageSupport/FieldtypeTextLanguage.module\nLanguageSupport/LanguageSupport.module\nLanguageSupport/LanguageSupportFields.module\nLanguageSupport/LanguageSupportPageNames.module\nLanguageSupport/LanguageTabs.module\nLanguageSupport/ProcessLanguage.module\nLanguageSupport/ProcessLanguageTranslator.module\nLazyCron.module\nMarkup/MarkupAdminDataTable/MarkupAdminDataTable.module\nMarkup/MarkupCache.module\nMarkup/MarkupHTMLPurifier/MarkupHTMLPurifier.module\nMarkup/MarkupPageArray.module\nMarkup/MarkupPageFields.module\nMarkup/MarkupPagerNav/MarkupPagerNav.module\nMarkup/MarkupRSS.module\nPage/PageFrontEdit/PageFrontEdit.module\nPagePathHistory.module\nPagePaths.module\nPagePermissions.module\nPageRender.module\nProcess/ProcessCommentsManager/ProcessCommentsManager.module\nProcess/ProcessField/ProcessField.module\nProcess/ProcessForgotPassword.module\nProcess/ProcessHome.module\nProcess/ProcessList.module\nProcess/ProcessLogger/ProcessLogger.module\nProcess/ProcessLogin/ProcessLogin.module\nProcess/ProcessModule/ProcessModule.module\nProcess/ProcessPageAdd/ProcessPageAdd.module\nProcess/ProcessPageClone.module\nProcess/ProcessPageEdit/ProcessPageEdit.module\nProcess/ProcessPageEditImageSelect/ProcessPageEditImageSelect.module\nProcess/ProcessPageEditLink/ProcessPageEditLink.module\nProcess/ProcessPageList/ProcessPageList.module\nProcess/ProcessPageLister/ProcessPageLister.module\nProcess/ProcessPageSearch/ProcessPageSearch.module\nProcess/ProcessPagesExportImport/ProcessPagesExportImport.module\nProcess/ProcessPageSort.module\nProcess/ProcessPageTrash.module\nProcess/ProcessPageType/ProcessPageType.module\nProcess/ProcessPageView.module\nProcess/ProcessPermission/ProcessPermission.module\nProcess/ProcessProfile/ProcessProfile.module\nProcess/ProcessRecentPages/ProcessRecentPages.module\nProcess/ProcessRole/ProcessRole.module\nProcess/ProcessTemplate/ProcessTemplate.module\nProcess/ProcessUser/ProcessUser.module\nSession/SessionHandlerDB/ProcessSessionDB.module\nSession/SessionHandlerDB/SessionHandlerDB.module\nSession/SessionLoginThrottle/SessionLoginThrottle.module\nSystem/SystemNotifications/FieldtypeNotifications.module\nSystem/SystemNotifications/SystemNotifications.module\nSystem/SystemUpdater/SystemUpdater.module\nTextformatter/TextformatterEntities.module\nTextformatter/TextformatterMarkdownExtra/TextformatterMarkdownExtra.module\nTextformatter/TextformatterNewlineBR.module\nTextformatter/TextformatterNewlineUL.module\nTextformatter/TextformatterPstripper.module\nTextformatter/TextformatterSmartypants/TextformatterSmartypants.module\nTextformatter/TextformatterStripTags.module', '2010-04-08 03:10:01');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('ModulesVerbose.info', '{\"148\":{\"summary\":\"Minimal admin theme that supports all ProcessWire features.\",\"core\":true,\"versionStr\":\"0.1.4\"},\"169\":{\"summary\":\"Uikit v3 admin theme\",\"core\":true,\"versionStr\":\"0.3.0\"},\"97\":{\"summary\":\"This Fieldtype stores an ON\\/OFF toggle via a single checkbox. The ON value is 1 and OFF value is 0.\",\"core\":true,\"versionStr\":\"1.0.1\"},\"28\":{\"summary\":\"Field that stores a date and optionally time\",\"core\":true,\"versionStr\":\"1.0.5\"},\"29\":{\"summary\":\"Field that stores an e-mail address\",\"core\":true,\"versionStr\":\"1.0.1\"},\"106\":{\"summary\":\"Close a fieldset opened by FieldsetOpen. \",\"core\":true,\"versionStr\":\"1.0.0\"},\"105\":{\"summary\":\"Open a fieldset to group fields. Should be followed by a Fieldset (Close) after one or more fields.\",\"core\":true,\"versionStr\":\"1.0.1\"},\"107\":{\"summary\":\"Open a fieldset to group fields. Same as Fieldset (Open) except that it displays in a tab instead.\",\"core\":true,\"versionStr\":\"1.0.0\"},\"6\":{\"summary\":\"Field that stores one or more files\",\"core\":true,\"versionStr\":\"1.0.6\"},\"89\":{\"summary\":\"Field that stores a floating point (decimal) number\",\"core\":true,\"versionStr\":\"1.0.5\"},\"57\":{\"summary\":\"Field that stores one or more GIF, JPG, or PNG images\",\"core\":true,\"versionStr\":\"1.0.2\"},\"84\":{\"summary\":\"Field that stores an integer\",\"core\":true,\"versionStr\":\"1.0.1\"},\"27\":{\"summary\":\"Field that stores a reference to another module\",\"core\":true,\"versionStr\":\"1.0.1\"},\"181\":{\"summary\":\"Field that stores single and multi select options.\",\"core\":true,\"versionStr\":\"0.0.1\"},\"4\":{\"summary\":\"Field that stores one or more references to ProcessWire pages\",\"core\":true,\"versionStr\":\"1.0.5\"},\"111\":{\"summary\":\"Field that stores a page title\",\"core\":true,\"versionStr\":\"1.0.0\"},\"133\":{\"summary\":\"Field that stores a hashed and salted password\",\"core\":true,\"versionStr\":\"1.0.1\"},\"172\":{\"summary\":\"Maintains a collection of fields that are repeated for any number of times.\",\"core\":true,\"versionStr\":\"1.0.6\"},\"173\":{\"summary\":\"Repeats fields from another template. Provides the input for FieldtypeRepeater.\",\"core\":true,\"versionStr\":\"1.0.6\"},\"3\":{\"summary\":\"Field that stores a single line of text\",\"core\":true,\"versionStr\":\"1.0.1\"},\"1\":{\"summary\":\"Field that stores multiple lines of text\",\"core\":true,\"versionStr\":\"1.0.7\"},\"135\":{\"summary\":\"Field that stores a URL\",\"core\":true,\"versionStr\":\"1.0.1\"},\"25\":{\"summary\":\"Multiple selection, progressive enhancement to select multiple\",\"core\":true,\"versionStr\":\"2.0.2\"},\"131\":{\"summary\":\"Form button element that you can optionally pass an href attribute to.\",\"core\":true,\"versionStr\":\"1.0.0\"},\"37\":{\"summary\":\"Single checkbox toggle\",\"core\":true,\"versionStr\":\"1.0.6\"},\"38\":{\"summary\":\"Multiple checkbox toggles\",\"core\":true,\"versionStr\":\"1.0.7\"},\"155\":{\"summary\":\"CKEditor textarea rich text editor.\",\"core\":true,\"versionStr\":\"1.6.3\"},\"94\":{\"summary\":\"Inputfield that accepts date and optionally time\",\"core\":true,\"versionStr\":\"1.0.7\"},\"80\":{\"summary\":\"E-Mail address in valid format\",\"core\":true,\"versionStr\":\"1.0.1\"},\"78\":{\"summary\":\"Groups one or more fields together in a container\",\"core\":true,\"versionStr\":\"1.0.1\"},\"55\":{\"summary\":\"One or more file uploads (sortable)\",\"core\":true,\"versionStr\":\"1.2.6\"},\"90\":{\"summary\":\"Floating point number with precision\",\"core\":true,\"versionStr\":\"1.0.3\"},\"30\":{\"summary\":\"Contains one or more fields in a form\",\"core\":true,\"versionStr\":\"1.0.7\"},\"40\":{\"summary\":\"Hidden value in a form\",\"core\":true,\"versionStr\":\"1.0.1\"},\"171\":{\"summary\":\"Select an icon\",\"core\":true,\"versionStr\":\"0.0.2\"},\"56\":{\"summary\":\"One or more image uploads (sortable)\",\"core\":true,\"versionStr\":\"1.2.3\"},\"85\":{\"summary\":\"Integer (positive or negative)\",\"core\":true,\"versionStr\":\"1.0.4\"},\"79\":{\"summary\":\"Contains any other markup and optionally child Inputfields\",\"core\":true,\"versionStr\":\"1.0.2\"},\"41\":{\"summary\":\"Text input validated as a ProcessWire name field\",\"core\":true,\"versionStr\":\"1.0.0\"},\"60\":{\"summary\":\"Select one or more pages\",\"core\":true,\"versionStr\":\"1.0.7\"},\"15\":{\"summary\":\"Selection of a single page from a ProcessWire page tree list\",\"core\":true,\"versionStr\":\"1.0.1\"},\"137\":{\"summary\":\"Selection of multiple pages from a ProcessWire page tree list\",\"core\":true,\"versionStr\":\"1.0.2\"},\"86\":{\"summary\":\"Text input validated as a ProcessWire Page name field\",\"core\":true,\"versionStr\":\"1.0.6\"},\"112\":{\"summary\":\"Handles input of Page Title and auto-generation of Page Name (when name is blank)\",\"core\":true,\"versionStr\":\"1.0.2\"},\"122\":{\"summary\":\"Password input with confirmation field that doesn&#039;t ever echo the input back.\",\"core\":true,\"versionStr\":\"1.0.2\"},\"39\":{\"summary\":\"Radio buttons for selection of a single item\",\"core\":true,\"versionStr\":\"1.0.5\"},\"36\":{\"summary\":\"Selection of a single value from a select pulldown\",\"core\":true,\"versionStr\":\"1.0.2\"},\"43\":{\"summary\":\"Select multiple items from a list\",\"core\":true,\"versionStr\":\"1.0.1\"},\"149\":{\"summary\":\"Build a page finding selector visually.\",\"author\":\"Avoine + ProcessWire\",\"core\":true,\"versionStr\":\"0.2.8\"},\"32\":{\"summary\":\"Form submit button\",\"core\":true,\"versionStr\":\"1.0.2\"},\"34\":{\"summary\":\"Single line of text\",\"core\":true,\"versionStr\":\"1.0.6\"},\"35\":{\"summary\":\"Multiple lines of text\",\"core\":true,\"versionStr\":\"1.0.3\"},\"108\":{\"summary\":\"URL in valid format\",\"core\":true,\"versionStr\":\"1.0.2\"},\"116\":{\"summary\":\"jQuery Core as required by ProcessWire Admin and plugins\",\"href\":\"http:\\/\\/jquery.com\",\"core\":true,\"versionStr\":\"1.8.3\"},\"151\":{\"summary\":\"Provides lightbox capability for image galleries. Replacement for FancyBox. Uses Magnific Popup by @dimsemenov.\",\"href\":\"http:\\/\\/dimsemenov.com\\/plugins\\/magnific-popup\\/\",\"core\":true,\"versionStr\":\"0.0.1\"},\"103\":{\"summary\":\"Provides a jQuery plugin for sorting tables.\",\"href\":\"http:\\/\\/mottie.github.io\\/tablesorter\\/\",\"core\":true,\"versionStr\":\"2.2.1\"},\"117\":{\"summary\":\"jQuery UI as required by ProcessWire and plugins\",\"href\":\"http:\\/\\/ui.jquery.com\",\"core\":true,\"versionStr\":\"1.9.6\"},\"45\":{\"summary\":\"Provides a jQuery plugin for generating tabs in ProcessWire.\",\"core\":true,\"versionStr\":\"1.0.9\"},\"163\":{\"summary\":\"Field that stores a page title in multiple languages. Use this only if you want title inputs created for ALL languages on ALL pages. Otherwise create separate languaged-named title fields, i.e. title_fr, title_es, title_fi, etc. \",\"author\":\"Ryan Cramer\",\"core\":true,\"versionStr\":\"1.0.0\"},\"164\":{\"summary\":\"Field that stores a multiple lines of text in multiple languages\",\"core\":true,\"versionStr\":\"1.0.0\"},\"162\":{\"summary\":\"Field that stores a single line of text in multiple languages\",\"core\":true,\"versionStr\":\"1.0.0\"},\"158\":{\"summary\":\"ProcessWire multi-language support.\",\"author\":\"Ryan Cramer\",\"core\":true,\"versionStr\":\"1.0.3\"},\"161\":{\"summary\":\"Required to use multi-language fields.\",\"author\":\"Ryan Cramer\",\"core\":true,\"versionStr\":\"1.0.0\"},\"165\":{\"summary\":\"Required to use multi-language page names.\",\"author\":\"Ryan Cramer\",\"core\":true,\"versionStr\":\"0.1.0\"},\"166\":{\"summary\":\"Organizes multi-language fields into tabs for a cleaner easier to use interface.\",\"author\":\"adamspruijt, ryan\",\"core\":true,\"versionStr\":\"1.1.4\"},\"159\":{\"summary\":\"Manage system languages\",\"author\":\"Ryan Cramer\",\"core\":true,\"versionStr\":\"1.0.3\",\"permissions\":{\"lang-edit\":\"Administer languages and static translation files\"}},\"160\":{\"summary\":\"Provides language translation capabilities for ProcessWire core and modules.\",\"author\":\"Ryan Cramer\",\"core\":true,\"versionStr\":\"1.0.1\"},\"67\":{\"summary\":\"Generates markup for data tables used by ProcessWire admin\",\"core\":true,\"versionStr\":\"1.0.7\"},\"156\":{\"summary\":\"Front-end to the HTML Purifier library.\",\"core\":true,\"versionStr\":\"4.9.5\"},\"113\":{\"summary\":\"Adds renderPager() method to all PaginatedArray types, for easy pagination output. Plus a render() method to PageArray instances.\",\"core\":true,\"versionStr\":\"1.0.0\"},\"98\":{\"summary\":\"Generates markup for pagination navigation\",\"core\":true,\"versionStr\":\"1.0.5\"},\"152\":{\"summary\":\"Keeps track of past URLs where pages have lived and automatically redirects (301 permament) to the new location whenever the past URL is accessed.\",\"core\":true,\"versionStr\":\"0.0.5\"},\"114\":{\"summary\":\"Adds various permission methods to Page objects that are used by Process modules.\",\"core\":true,\"versionStr\":\"1.0.5\"},\"115\":{\"summary\":\"Adds a render method to Page and caches page output.\",\"core\":true,\"versionStr\":\"1.0.5\"},\"48\":{\"summary\":\"Edit individual fields that hold page data\",\"core\":true,\"versionStr\":\"1.1.3\",\"searchable\":\"fields\"},\"87\":{\"summary\":\"Acts as a placeholder Process for the admin root. Ensures proper flow control after login.\",\"core\":true,\"versionStr\":\"1.0.1\"},\"76\":{\"summary\":\"Lists the Process assigned to each child page of the current\",\"core\":true,\"versionStr\":\"1.0.1\"},\"170\":{\"summary\":\"View and manage system logs.\",\"author\":\"Ryan Cramer\",\"core\":true,\"versionStr\":\"0.0.2\",\"permissions\":{\"logs-view\":\"Can view system logs\",\"logs-edit\":\"Can manage system logs\"},\"page\":{\"name\":\"logs\",\"parent\":\"setup\",\"title\":\"Logs\"}},\"10\":{\"summary\":\"Login to ProcessWire\",\"core\":true,\"versionStr\":\"1.0.8\"},\"50\":{\"summary\":\"List, edit or install\\/uninstall modules\",\"core\":true,\"versionStr\":\"1.1.8\"},\"17\":{\"summary\":\"Add a new page\",\"core\":true,\"versionStr\":\"1.0.8\"},\"7\":{\"summary\":\"Edit a Page\",\"core\":true,\"versionStr\":\"1.0.9\"},\"129\":{\"summary\":\"Provides image manipulation functions for image fields and rich text editors.\",\"core\":true,\"versionStr\":\"1.2.0\"},\"121\":{\"summary\":\"Provides a link capability as used by some Fieldtype modules (like rich text editors).\",\"core\":true,\"versionStr\":\"1.0.8\"},\"12\":{\"summary\":\"List pages in a hierarchical tree structure\",\"core\":true,\"versionStr\":\"1.2.2\"},\"150\":{\"summary\":\"Admin tool for finding and listing pages by any property.\",\"author\":\"Ryan Cramer\",\"core\":true,\"versionStr\":\"0.2.6\",\"permissions\":{\"page-lister\":\"Use Page Lister\"}},\"104\":{\"summary\":\"Provides a page search engine for admin use.\",\"core\":true,\"versionStr\":\"1.0.6\"},\"14\":{\"summary\":\"Handles page sorting and moving for PageList\",\"core\":true,\"versionStr\":\"1.0.0\"},\"109\":{\"summary\":\"Handles emptying of Page trash\",\"core\":true,\"versionStr\":\"1.0.3\"},\"134\":{\"summary\":\"List, Edit and Add pages of a specific type\",\"core\":true,\"versionStr\":\"1.0.1\"},\"83\":{\"summary\":\"All page views are routed through this Process\",\"core\":true,\"versionStr\":\"1.0.4\"},\"136\":{\"summary\":\"Manage system permissions\",\"core\":true,\"versionStr\":\"1.0.1\"},\"138\":{\"summary\":\"Enables user to change their password, email address and other settings that you define.\",\"core\":true,\"versionStr\":\"1.0.4\"},\"168\":{\"summary\":\"Shows a list of recently edited pages in your admin.\",\"author\":\"Ryan Cramer\",\"href\":\"http:\\/\\/modules.processwire.com\\/\",\"core\":true,\"versionStr\":\"0.0.2\",\"permissions\":{\"page-edit-recent\":\"Can see recently edited pages\"},\"page\":{\"name\":\"recent-pages\",\"parent\":\"page\",\"title\":\"Recent\"}},\"68\":{\"summary\":\"Manage user roles and what permissions are attached\",\"core\":true,\"versionStr\":\"1.0.4\"},\"47\":{\"summary\":\"List and edit the templates that control page output\",\"core\":true,\"versionStr\":\"1.1.4\",\"searchable\":\"templates\"},\"66\":{\"summary\":\"Manage system users\",\"core\":true,\"versionStr\":\"1.0.7\",\"searchable\":\"users\"},\"125\":{\"summary\":\"Throttles login attempts to help prevent dictionary attacks.\",\"core\":true,\"versionStr\":\"1.0.3\"},\"139\":{\"summary\":\"Manages system versions and upgrades.\",\"core\":true,\"versionStr\":\"0.1.7\"},\"61\":{\"summary\":\"Entity encode ampersands, quotes (single and double) and greater-than\\/less-than signs using htmlspecialchars(str, ENT_QUOTES). It is recommended that you use this on all text\\/textarea fields except those using a rich text editor or a markup language like Markdown.\",\"core\":true,\"versionStr\":\"1.0.0\"},\"175\":{\"summary\":\"One or more image uploads (optional predefined crops)\",\"author\":\"Horst Nogajski\",\"versionStr\":\"1.2.0\"},\"176\":{\"summary\":\"One or more image uploads (optional predefined crops)\",\"author\":\"Horst Nogajski\",\"versionStr\":\"1.2.0\"},\"177\":{\"summary\":\"One or more image uploads (optional predefined crops)\",\"author\":\"Horst Nogajski\",\"versionStr\":\"1.2.0\"},\"178\":{\"summary\":\"One or more image uploads (optional predefined crops)\",\"author\":\"Horst Nogajski\",\"versionStr\":\"1.2.0\",\"permissions\":{\"croppable-image-3\":\"Crop images with CroppableImage3\"}},\"179\":{\"summary\":\"Allows collection of image handling diagnostics\",\"author\":\"Horst\",\"versionStr\":\"0.0.1\"},\"180\":{\"summary\":\"Allows evaluation of run-time environment and detection of changes.\",\"author\":\"Stephen Dickinson, QBox\",\"versionStr\":\"0.2.3\"},\"174\":{\"summary\":\"Create and\\/or restore database backups from ProcessWire admin.\",\"author\":\"Ryan Cramer\",\"versionStr\":\"0.0.5\",\"permissions\":{\"db-backup\":\"Manage database backups (recommended for superuser only)\"},\"page\":{\"name\":\"db-backups\",\"parent\":\"setup\",\"title\":\"DB Backups\"}},\"186\":{\"summary\":\"Fieldtype which fetches taxes setting from SnipWire module config and builds a dropdown list.\",\"author\":\"Martin Gartner\",\"versionStr\":\"0.8.6\"},\"184\":{\"summary\":\"Snipcart dashboard integration for ProcessWire.\",\"author\":\"Martin Gartner\",\"versionStr\":\"0.8.6\",\"permissions\":{\"snipwire-dashboard\":\"Use the SnipWire Dashboard sections\"},\"page\":{\"name\":\"snipwire\",\"title\":\"SnipWire\",\"parent\":\"setup\"}},\"183\":{\"summary\":\"Full Snipcart shopping cart integration for ProcessWire.\",\"author\":\"Martin Gartner\",\"href\":\"https:\\/\\/processwire.com\\/talk\\/topic\\/21554-snipwire-snipcart-integration-for-processwire\\/\",\"versionStr\":\"0.8.6\"},\"185\":{\"summary\":\"Snipcart markup output for SnipWire.\",\"author\":\"Martin Gartner\",\"versionStr\":\"0.8.6\"},\"182\":{\"summary\":\"Enter a full YouTube or Vimeo URL by itself in any paragraph (example: http:\\/\\/www.youtube.com\\/watch?v=Wl4XiYadV_k) and this will automatically convert it to an embedded video. This formatter is intended to be run on trusted input. Recommended for use with TinyMCE textarea fields.\",\"author\":\"Ryan Cramer\",\"href\":\"https:\\/\\/modules.processwire.com\\/modules\\/textformatter-video-embed\\/\",\"versionStr\":\"1.1.1\"},\"187\":{\"summary\":\"Multiple Page selection using auto completion and sorting capability. Intended for use as an input field for Page reference fields.\",\"core\":true,\"versionStr\":\"1.1.2\"}}', '2010-04-08 03:10:01');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__421e3b7f4b74778748c0c30d586811e0', '{\"source\":{\"file\":\"D:\\/workspace\\/boardschmiede\\/site\\/modules\\/DiagnosePhp\\/DiagnoseWebserver.module\",\"hash\":\"dff2512a4382810bd7c6ce2123472fb1\",\"size\":4413,\"time\":1587556382,\"ns\":\"\\\\\"},\"target\":{\"file\":\"D:\\/workspace\\/boardschmiede\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/DiagnosePhp\\/DiagnoseWebserver.module\",\"hash\":\"eb203ddbb51e22987a643e21b5eb5482\",\"size\":4605,\"time\":1587556382}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__ae14da2f1639603ae9d2703c82d4e5fb', '{\"source\":{\"file\":\"D:\\/workspace\\/boardschmiede\\/site\\/modules\\/DiagnosePhp\\/ProcessDiagnostics.module\",\"hash\":\"4560b510543cc143dfd2d7d481cd7deb\",\"size\":10814,\"time\":1587556382,\"ns\":\"\\\\\"},\"target\":{\"file\":\"D:\\/workspace\\/boardschmiede\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/DiagnosePhp\\/ProcessDiagnostics.module\",\"hash\":\"e1612e7268b43349dcebe13459faf147\",\"size\":11021,\"time\":1587556382}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('Modules.site/modules/', 'CroppableImage3/CroppableImage3.module\nCroppableImage3/FieldtypeCroppableImage3/FieldtypeCroppableImage3.module\nCroppableImage3/InputfieldCroppableImage3/InputfieldCroppableImage3.module\nCroppableImage3/ProcessCroppableImage3/ProcessCroppableImage3.module\nDiagnosePhp/DiagnoseDatabase.module\nDiagnosePhp/DiagnoseExample.module\nDiagnosePhp/DiagnoseFiles.module\nDiagnosePhp/DiagnoseImagehandling.module\nDiagnosePhp/DiagnoseModules.module\nDiagnosePhp/DiagnosePhp.module\nDiagnosePhp/DiagnoseWebserver.module\nDiagnosePhp/ProcessDiagnostics.module\nHelloworld/Helloworld.module\nProcessDatabaseBackups/ProcessDatabaseBackups.module\nSnipWire/FieldtypeSnipWireTaxSelector/FieldtypeSnipWireTaxSelector.module.php\nSnipWire/MarkupSnipWire/MarkupSnipWire.module.php\nSnipWire/ProcessSnipWire/ProcessSnipWire.module.php\nSnipWire/SnipWire.module.php\nTextformatterVideoEmbed/TextformatterVideoEmbed.module', '2010-04-08 03:10:01');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__5ae34ac82eb8933d4f53f0884eb122a1', '{\"source\":{\"file\":\"D:\\/workspace\\/boardschmiede\\/site\\/templates\\/admin.php\",\"hash\":\"9636f854995462a4cb877cb1204bc2fe\",\"size\":467,\"time\":1577984776,\"ns\":\"ProcessWire\"},\"target\":{\"file\":\"D:\\/workspace\\/boardschmiede\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/admin.php\",\"hash\":\"9636f854995462a4cb877cb1204bc2fe\",\"size\":467,\"time\":1577984776}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__df6816735208d9d77e71d4a6321bb79b', '{\"source\":{\"file\":\"D:\\/workspace\\/boardschmiede\\/site\\/modules\\/DiagnosePhp\\/DiagnoseDatabase.module\",\"hash\":\"c896ad8a79fd6d265db22845c46189ea\",\"size\":9660,\"time\":1587556382,\"ns\":\"\\\\\"},\"target\":{\"file\":\"D:\\/workspace\\/boardschmiede\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/DiagnosePhp\\/DiagnoseDatabase.module\",\"hash\":\"b3d567b03954855a292489ff87499f72\",\"size\":9850,\"time\":1587556382}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__7dcd25b51b01ac74a219fc8ef52a56db', '{\"source\":{\"file\":\"D:\\/workspace\\/boardschmiede\\/site\\/templates\\/_init.php\",\"hash\":\"03d948499ec1c07d62a674daefd3ad67\",\"size\":1035,\"time\":1587762335,\"ns\":\"ProcessWire\"},\"target\":{\"file\":\"D:\\/workspace\\/boardschmiede\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/_init.php\",\"hash\":\"03d948499ec1c07d62a674daefd3ad67\",\"size\":1035,\"time\":1587762335}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__5959a832c0f8b39b1aa4d23d82ee12ec', '{\"source\":{\"file\":\"D:\\/workspace\\/boardschmiede\\/site\\/templates\\/_main.php\",\"hash\":\"d858f9dace62bdbfab0579f1a2b8edb5\",\"size\":2908,\"time\":1589622620,\"ns\":\"ProcessWire\"},\"target\":{\"file\":\"D:\\/workspace\\/boardschmiede\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/_main.php\",\"hash\":\"d858f9dace62bdbfab0579f1a2b8edb5\",\"size\":2908,\"time\":1589622620}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__e1018a841de9d71b642846de25c5ee36', '{\"source\":{\"file\":\"D:\\/workspace\\/boardschmiede\\/site\\/templates\\/home.php\",\"hash\":\"d907e145d2d589a8810e0c02334528b0\",\"size\":4516,\"time\":1587755630,\"ns\":\"ProcessWire\"},\"target\":{\"file\":\"D:\\/workspace\\/boardschmiede\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/home.php\",\"hash\":\"d907e145d2d589a8810e0c02334528b0\",\"size\":4516,\"time\":1587755630}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__90649f37a650d3a10bdc0274a1195ec5', '{\"source\":{\"file\":\"D:\\/workspace\\/boardschmiede\\/site\\/templates\\/basic-page.php\",\"hash\":\"343a5c586f1e72cafba78739f0acd525\",\"size\":969,\"time\":1587754342,\"ns\":\"ProcessWire\"},\"target\":{\"file\":\"D:\\/workspace\\/boardschmiede\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/basic-page.php\",\"hash\":\"343a5c586f1e72cafba78739f0acd525\",\"size\":969,\"time\":1587754342}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__b794072abe17c7ede759dbfec87990f3', '{\"source\":{\"file\":\"D:\\/workspace\\/boardschmiede\\/site\\/modules\\/DiagnosePhp\\/DiagnoseModules.module\",\"hash\":\"9541494d63373f3c5df290126ddf4359\",\"size\":6597,\"time\":1587556382,\"ns\":\"\\\\\"},\"target\":{\"file\":\"D:\\/workspace\\/boardschmiede\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/DiagnosePhp\\/DiagnoseModules.module\",\"hash\":\"385ac5bb67dfd52de1ef8e49971f170e\",\"size\":6889,\"time\":1587556382}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__e1107e52278cf85427af9a0d145c7a53', '{\"source\":{\"file\":\"D:\\/workspace\\/boardschmiede\\/site\\/modules\\/DiagnosePhp\\/DiagnosePhp.module\",\"hash\":\"b36f04d3ae3e0a63753a40f40d68035d\",\"size\":8319,\"time\":1587556382,\"ns\":\"\\\\\"},\"target\":{\"file\":\"D:\\/workspace\\/boardschmiede\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/DiagnosePhp\\/DiagnosePhp.module\",\"hash\":\"4b32a4ab728343fb7e71e26cd2240b78\",\"size\":8499,\"time\":1587556382}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__20977776d24cd3dea39e9ba8ce3d0246', '{\"source\":{\"file\":\"D:\\/workspace\\/boardschmiede\\/site\\/templates\\/board-overview-page.php\",\"hash\":\"dc09884f99c596788ae7ccb7c69db1cc\",\"size\":2002,\"time\":1587764973,\"ns\":\"ProcessWire\"},\"target\":{\"file\":\"D:\\/workspace\\/boardschmiede\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/board-overview-page.php\",\"hash\":\"dc09884f99c596788ae7ccb7c69db1cc\",\"size\":2002,\"time\":1587764973}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__c5c7ce541a2ab620a161df216a462e7b', '{\"source\":{\"file\":\"D:\\/workspace\\/boardschmiede\\/site\\/modules\\/DiagnosePhp\\/DiagnoseExample.module\",\"hash\":\"cdaa285c8c566fe75265d4b47d337ffc\",\"size\":1476,\"time\":1587556382,\"ns\":\"\\\\\"},\"target\":{\"file\":\"D:\\/workspace\\/boardschmiede\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/DiagnosePhp\\/DiagnoseExample.module\",\"hash\":\"4096eb00a3c2570f06fb3239363809f4\",\"size\":1664,\"time\":1587556382}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__f3cca45a1191a9540ac8152dd3f20051', '{\"source\":{\"file\":\"D:\\/workspace\\/boardschmiede\\/site\\/modules\\/DiagnosePhp\\/DiagnoseFiles.module\",\"hash\":\"c0bbb0d9ddb27f412461f26cd1a1ef11\",\"size\":6110,\"time\":1587556382,\"ns\":\"\\\\\"},\"target\":{\"file\":\"D:\\/workspace\\/boardschmiede\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/DiagnosePhp\\/DiagnoseFiles.module\",\"hash\":\"96a1b363de5e0cccb7ac0940f3faf344\",\"size\":6294,\"time\":1587556382}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__c82002e99d4ffa99e968da946c80c8b1', '{\"source\":{\"file\":\"D:\\/workspace\\/boardschmiede\\/site\\/modules\\/DiagnosePhp\\/DiagnoseImagehandling.module\",\"hash\":\"7dd8fa804558f80a390773c9ba82aa29\",\"size\":6577,\"time\":1587556382,\"ns\":\"\\\\\"},\"target\":{\"file\":\"D:\\/workspace\\/boardschmiede\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/DiagnosePhp\\/DiagnoseImagehandling.module\",\"hash\":\"9f23df0ea1a1b64624700726868fcca0\",\"size\":6777,\"time\":1587556382}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__baffbabf1acbc47fe13788735b158c1e', '{\"source\":{\"file\":\"D:\\/workspace\\/boardschmiede\\/site\\/templates\\/board-page.php\",\"hash\":\"62efedd3f9a105fd78f3ba98bf7bf6b9\",\"size\":2337,\"time\":1587763911,\"ns\":\"ProcessWire\"},\"target\":{\"file\":\"D:\\/workspace\\/boardschmiede\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/board-page.php\",\"hash\":\"62efedd3f9a105fd78f3ba98bf7bf6b9\",\"size\":2337,\"time\":1587763911}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__08d2d0b637039fcf971af74cf221aa14', '{\"source\":{\"file\":\"\\/var\\/www\\/html\\/site\\/modules\\/DiagnosePhp\\/DiagnoseImagehandling.module\",\"hash\":\"7dd8fa804558f80a390773c9ba82aa29\",\"size\":6577,\"time\":1590404552,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/var\\/www\\/html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/DiagnosePhp\\/DiagnoseImagehandling.module\",\"hash\":\"f68ae20a082c18ebc0062dac893912c3\",\"size\":6751,\"time\":1590404552}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__dc7cea36f468e9fca8e2f46ffcc15a77', '{\"source\":{\"file\":\"\\/var\\/www\\/html\\/site\\/modules\\/DiagnosePhp\\/DiagnoseDatabase.module\",\"hash\":\"c896ad8a79fd6d265db22845c46189ea\",\"size\":9660,\"time\":1590404552,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/var\\/www\\/html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/DiagnosePhp\\/DiagnoseDatabase.module\",\"hash\":\"b8997138e09eea4cd8fd97e0bb9cb7bd\",\"size\":9824,\"time\":1590404552}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__28e37a78fe9a06a874481ff7dbe21dae', '{\"source\":{\"file\":\"\\/var\\/www\\/html\\/site\\/modules\\/DiagnosePhp\\/DiagnoseExample.module\",\"hash\":\"cdaa285c8c566fe75265d4b47d337ffc\",\"size\":1476,\"time\":1590404552,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/var\\/www\\/html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/DiagnosePhp\\/DiagnoseExample.module\",\"hash\":\"ffe1a583f9658c72297f796883fced66\",\"size\":1638,\"time\":1590404552}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__beee426143b91013fd0aa871bfca1609', '{\"source\":{\"file\":\"\\/var\\/www\\/html\\/site\\/modules\\/DiagnosePhp\\/DiagnoseFiles.module\",\"hash\":\"c0bbb0d9ddb27f412461f26cd1a1ef11\",\"size\":6110,\"time\":1590404552,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/var\\/www\\/html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/DiagnosePhp\\/DiagnoseFiles.module\",\"hash\":\"73d2184b1128f17fddaeca02017f6cd5\",\"size\":6268,\"time\":1590404552}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__16122b191f15b11469d30519ad824976', '{\"source\":{\"file\":\"\\/var\\/www\\/html\\/site\\/modules\\/DiagnosePhp\\/DiagnoseModules.module\",\"hash\":\"9541494d63373f3c5df290126ddf4359\",\"size\":6597,\"time\":1590404552,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/var\\/www\\/html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/DiagnosePhp\\/DiagnoseModules.module\",\"hash\":\"b87343fbee5f9cce6ea2ce91f741e6ac\",\"size\":6863,\"time\":1590404552}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__598541cb13523f3b812773211c09272a', '{\"source\":{\"file\":\"\\/var\\/www\\/html\\/site\\/modules\\/DiagnosePhp\\/DiagnosePhp.module\",\"hash\":\"b36f04d3ae3e0a63753a40f40d68035d\",\"size\":8319,\"time\":1590404552,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/var\\/www\\/html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/DiagnosePhp\\/DiagnosePhp.module\",\"hash\":\"571729220ee48a626b5fa323d08e2694\",\"size\":8473,\"time\":1590404552}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__94db96b2d46b7521f4bbb88cdf429b1e', '{\"source\":{\"file\":\"\\/var\\/www\\/html\\/site\\/modules\\/DiagnosePhp\\/DiagnoseWebserver.module\",\"hash\":\"dff2512a4382810bd7c6ce2123472fb1\",\"size\":4413,\"time\":1590404552,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/var\\/www\\/html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/DiagnosePhp\\/DiagnoseWebserver.module\",\"hash\":\"9c0878817a4cbeb492ddfd29e923e628\",\"size\":4579,\"time\":1590404552}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('Modules.info', '{\"148\":{\"name\":\"AdminThemeDefault\",\"title\":\"Default\",\"version\":14,\"autoload\":\"template=admin\",\"created\":1580889073,\"configurable\":19,\"namespace\":\"ProcessWire\\\\\"},\"169\":{\"name\":\"AdminThemeUikit\",\"title\":\"Uikit\",\"version\":30,\"requiresVersions\":{\"ProcessWire\":[\">=\",\"3.0.100\"]},\"autoload\":\"template=admin\",\"created\":1580889090,\"configurable\":4,\"namespace\":\"ProcessWire\\\\\"},\"97\":{\"name\":\"FieldtypeCheckbox\",\"title\":\"Checkbox\",\"version\":101,\"singular\":true,\"created\":1580889073,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"28\":{\"name\":\"FieldtypeDatetime\",\"title\":\"Datetime\",\"version\":105,\"created\":1580889073,\"namespace\":\"ProcessWire\\\\\"},\"29\":{\"name\":\"FieldtypeEmail\",\"title\":\"E-Mail\",\"version\":101,\"singular\":true,\"created\":1580889073,\"namespace\":\"ProcessWire\\\\\"},\"106\":{\"name\":\"FieldtypeFieldsetClose\",\"title\":\"Fieldset (Close)\",\"version\":100,\"singular\":true,\"created\":1580889073,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"105\":{\"name\":\"FieldtypeFieldsetOpen\",\"title\":\"Fieldset (Open)\",\"version\":101,\"singular\":true,\"created\":1580889073,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"107\":{\"name\":\"FieldtypeFieldsetTabOpen\",\"title\":\"Fieldset in Tab (Open)\",\"version\":100,\"singular\":1,\"created\":1580889073,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"6\":{\"name\":\"FieldtypeFile\",\"title\":\"Files\",\"version\":106,\"singular\":true,\"created\":1580889073,\"configurable\":4,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"89\":{\"name\":\"FieldtypeFloat\",\"title\":\"Float\",\"version\":105,\"singular\":1,\"created\":1580889073,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"57\":{\"name\":\"FieldtypeImage\",\"title\":\"Images\",\"version\":102,\"singular\":true,\"created\":1580889073,\"configurable\":4,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"84\":{\"name\":\"FieldtypeInteger\",\"title\":\"Integer\",\"version\":101,\"created\":1580889073,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"27\":{\"name\":\"FieldtypeModule\",\"title\":\"Module Reference\",\"version\":101,\"singular\":true,\"created\":1580889073,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"181\":{\"name\":\"FieldtypeOptions\",\"title\":\"Select Options\",\"version\":1,\"singular\":true,\"created\":1587570341,\"namespace\":\"ProcessWire\\\\\"},\"4\":{\"name\":\"FieldtypePage\",\"title\":\"Page Reference\",\"version\":105,\"autoload\":true,\"singular\":true,\"created\":1580889073,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"111\":{\"name\":\"FieldtypePageTitle\",\"title\":\"Page Title\",\"version\":100,\"singular\":1,\"created\":1580889073,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"133\":{\"name\":\"FieldtypePassword\",\"title\":\"Password\",\"version\":101,\"singular\":true,\"created\":1580889073,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"172\":{\"name\":\"FieldtypeRepeater\",\"title\":\"Repeater\",\"version\":106,\"installs\":[\"InputfieldRepeater\"],\"autoload\":true,\"singular\":true,\"created\":1587357503,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\"},\"173\":{\"name\":\"InputfieldRepeater\",\"title\":\"Repeater\",\"version\":106,\"requiresVersions\":{\"FieldtypeRepeater\":[\">=\",0]},\"created\":1587357503,\"namespace\":\"ProcessWire\\\\\"},\"3\":{\"name\":\"FieldtypeText\",\"title\":\"Text\",\"version\":101,\"singular\":true,\"created\":1580889073,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"1\":{\"name\":\"FieldtypeTextarea\",\"title\":\"Textarea\",\"version\":107,\"created\":1580889073,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"135\":{\"name\":\"FieldtypeURL\",\"title\":\"URL\",\"version\":101,\"singular\":true,\"created\":1580889073,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"25\":{\"name\":\"InputfieldAsmSelect\",\"title\":\"asmSelect\",\"version\":202,\"created\":1580889073,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"131\":{\"name\":\"InputfieldButton\",\"title\":\"Button\",\"version\":100,\"created\":1580889073,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"37\":{\"name\":\"InputfieldCheckbox\",\"title\":\"Checkbox\",\"version\":106,\"created\":1580889073,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"38\":{\"name\":\"InputfieldCheckboxes\",\"title\":\"Checkboxes\",\"version\":107,\"created\":1580889073,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"155\":{\"name\":\"InputfieldCKEditor\",\"title\":\"CKEditor\",\"version\":163,\"installs\":[\"MarkupHTMLPurifier\"],\"created\":1580889073,\"namespace\":\"ProcessWire\\\\\"},\"94\":{\"name\":\"InputfieldDatetime\",\"title\":\"Datetime\",\"version\":107,\"created\":1580889073,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"80\":{\"name\":\"InputfieldEmail\",\"title\":\"Email\",\"version\":101,\"created\":1580889073,\"namespace\":\"ProcessWire\\\\\"},\"78\":{\"name\":\"InputfieldFieldset\",\"title\":\"Fieldset\",\"version\":101,\"created\":1580889073,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"55\":{\"name\":\"InputfieldFile\",\"title\":\"Files\",\"version\":126,\"created\":1580889073,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"90\":{\"name\":\"InputfieldFloat\",\"title\":\"Float\",\"version\":103,\"created\":1580889073,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"30\":{\"name\":\"InputfieldForm\",\"title\":\"Form\",\"version\":107,\"created\":1580889073,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"40\":{\"name\":\"InputfieldHidden\",\"title\":\"Hidden\",\"version\":101,\"created\":1580889073,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"171\":{\"name\":\"InputfieldIcon\",\"title\":\"Icon\",\"version\":2,\"created\":1580889095,\"namespace\":\"ProcessWire\\\\\"},\"56\":{\"name\":\"InputfieldImage\",\"title\":\"Images\",\"version\":123,\"created\":1580889073,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"85\":{\"name\":\"InputfieldInteger\",\"title\":\"Integer\",\"version\":104,\"created\":1580889073,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"79\":{\"name\":\"InputfieldMarkup\",\"title\":\"Markup\",\"version\":102,\"created\":1580889073,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"41\":{\"name\":\"InputfieldName\",\"title\":\"Name\",\"version\":100,\"created\":1580889073,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"60\":{\"name\":\"InputfieldPage\",\"title\":\"Page\",\"version\":107,\"created\":1580889073,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"15\":{\"name\":\"InputfieldPageListSelect\",\"title\":\"Page List Select\",\"version\":101,\"created\":1580889073,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"137\":{\"name\":\"InputfieldPageListSelectMultiple\",\"title\":\"Page List Select Multiple\",\"version\":102,\"created\":1580889073,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"86\":{\"name\":\"InputfieldPageName\",\"title\":\"Page Name\",\"version\":106,\"created\":1580889073,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"112\":{\"name\":\"InputfieldPageTitle\",\"title\":\"Page Title\",\"version\":102,\"created\":1580889073,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"122\":{\"name\":\"InputfieldPassword\",\"title\":\"Password\",\"version\":102,\"created\":1580889073,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"39\":{\"name\":\"InputfieldRadios\",\"title\":\"Radio Buttons\",\"version\":105,\"created\":1580889073,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"36\":{\"name\":\"InputfieldSelect\",\"title\":\"Select\",\"version\":102,\"created\":1580889073,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"43\":{\"name\":\"InputfieldSelectMultiple\",\"title\":\"Select Multiple\",\"version\":101,\"created\":1580889073,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"149\":{\"name\":\"InputfieldSelector\",\"title\":\"Selector\",\"version\":28,\"autoload\":\"template=admin\",\"created\":1580889073,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"addFlag\":32},\"32\":{\"name\":\"InputfieldSubmit\",\"title\":\"Submit\",\"version\":102,\"created\":1580889073,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"34\":{\"name\":\"InputfieldText\",\"title\":\"Text\",\"version\":106,\"created\":1580889073,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"35\":{\"name\":\"InputfieldTextarea\",\"title\":\"Textarea\",\"version\":103,\"created\":1580889073,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"108\":{\"name\":\"InputfieldURL\",\"title\":\"URL\",\"version\":102,\"created\":1580889073,\"namespace\":\"ProcessWire\\\\\"},\"116\":{\"name\":\"JqueryCore\",\"title\":\"jQuery Core\",\"version\":183,\"singular\":true,\"created\":1580889073,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"151\":{\"name\":\"JqueryMagnific\",\"title\":\"jQuery Magnific Popup\",\"version\":1,\"singular\":1,\"created\":1580889073,\"namespace\":\"ProcessWire\\\\\"},\"103\":{\"name\":\"JqueryTableSorter\",\"title\":\"jQuery Table Sorter Plugin\",\"version\":221,\"singular\":1,\"created\":1580889073,\"namespace\":\"ProcessWire\\\\\"},\"117\":{\"name\":\"JqueryUI\",\"title\":\"jQuery UI\",\"version\":196,\"singular\":true,\"created\":1580889073,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"45\":{\"name\":\"JqueryWireTabs\",\"title\":\"jQuery Wire Tabs Plugin\",\"version\":109,\"created\":1580889073,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"163\":{\"name\":\"FieldtypePageTitleLanguage\",\"title\":\"Page Title (Multi-Language)\",\"version\":100,\"requiresVersions\":{\"LanguageSupportFields\":[\">=\",0],\"FieldtypeTextLanguage\":[\">=\",0]},\"singular\":true,\"created\":1580889073,\"namespace\":\"ProcessWire\\\\\"},\"164\":{\"name\":\"FieldtypeTextareaLanguage\",\"title\":\"Textarea (Multi-language)\",\"version\":100,\"requiresVersions\":{\"LanguageSupportFields\":[\">=\",0]},\"singular\":true,\"created\":1580889073,\"namespace\":\"ProcessWire\\\\\"},\"162\":{\"name\":\"FieldtypeTextLanguage\",\"title\":\"Text (Multi-language)\",\"version\":100,\"requiresVersions\":{\"LanguageSupportFields\":[\">=\",0]},\"singular\":true,\"created\":1580889073,\"namespace\":\"ProcessWire\\\\\"},\"158\":{\"name\":\"LanguageSupport\",\"title\":\"Languages Support\",\"version\":103,\"installs\":[\"ProcessLanguage\",\"ProcessLanguageTranslator\"],\"autoload\":true,\"singular\":true,\"created\":1580889073,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"addFlag\":32},\"161\":{\"name\":\"LanguageSupportFields\",\"title\":\"Languages Support - Fields\",\"version\":100,\"requiresVersions\":{\"LanguageSupport\":[\">=\",0]},\"installs\":[\"FieldtypePageTitleLanguage\",\"FieldtypeTextareaLanguage\",\"FieldtypeTextLanguage\"],\"autoload\":true,\"singular\":true,\"created\":1580889073,\"namespace\":\"ProcessWire\\\\\"},\"165\":{\"name\":\"LanguageSupportPageNames\",\"title\":\"Languages Support - Page Names\",\"version\":10,\"requiresVersions\":{\"LanguageSupport\":[\">=\",0],\"LanguageSupportFields\":[\">=\",0]},\"autoload\":true,\"singular\":true,\"created\":1580889073,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\"},\"166\":{\"name\":\"LanguageTabs\",\"title\":\"Languages Support - Tabs\",\"version\":114,\"requiresVersions\":{\"LanguageSupport\":[\">=\",0]},\"autoload\":\"template=admin\",\"singular\":true,\"created\":1580889073,\"configurable\":4,\"namespace\":\"ProcessWire\\\\\"},\"159\":{\"name\":\"ProcessLanguage\",\"title\":\"Languages\",\"version\":103,\"icon\":\"language\",\"requiresVersions\":{\"LanguageSupport\":[\">=\",0]},\"permission\":\"lang-edit\",\"singular\":1,\"created\":1580889073,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"useNavJSON\":true},\"160\":{\"name\":\"ProcessLanguageTranslator\",\"title\":\"Language Translator\",\"version\":101,\"requiresVersions\":{\"LanguageSupport\":[\">=\",0]},\"permission\":\"lang-edit\",\"singular\":1,\"created\":1580889073,\"namespace\":\"ProcessWire\\\\\"},\"67\":{\"name\":\"MarkupAdminDataTable\",\"title\":\"Admin Data Table\",\"version\":107,\"created\":1580889073,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"156\":{\"name\":\"MarkupHTMLPurifier\",\"title\":\"HTML Purifier\",\"version\":495,\"created\":1580889073,\"namespace\":\"ProcessWire\\\\\"},\"113\":{\"name\":\"MarkupPageArray\",\"title\":\"PageArray Markup\",\"version\":100,\"autoload\":true,\"singular\":true,\"created\":1580889073,\"namespace\":\"ProcessWire\\\\\"},\"98\":{\"name\":\"MarkupPagerNav\",\"title\":\"Pager (Pagination) Navigation\",\"version\":105,\"created\":1580889073,\"namespace\":\"ProcessWire\\\\\"},\"152\":{\"name\":\"PagePathHistory\",\"title\":\"Page Path History\",\"version\":5,\"autoload\":true,\"singular\":true,\"created\":1580889073,\"configurable\":4,\"namespace\":\"ProcessWire\\\\\"},\"114\":{\"name\":\"PagePermissions\",\"title\":\"Page Permissions\",\"version\":105,\"autoload\":true,\"singular\":true,\"created\":1580889073,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"115\":{\"name\":\"PageRender\",\"title\":\"Page Render\",\"version\":105,\"autoload\":true,\"singular\":true,\"created\":1580889073,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"48\":{\"name\":\"ProcessField\",\"title\":\"Fields\",\"version\":113,\"icon\":\"cube\",\"permission\":\"field-admin\",\"created\":1580889073,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true,\"useNavJSON\":true,\"addFlag\":32},\"87\":{\"name\":\"ProcessHome\",\"title\":\"Admin Home\",\"version\":101,\"permission\":\"page-view\",\"created\":1580889073,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"76\":{\"name\":\"ProcessList\",\"title\":\"List\",\"version\":101,\"permission\":\"page-view\",\"created\":1580889073,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"170\":{\"name\":\"ProcessLogger\",\"title\":\"Logs\",\"version\":2,\"icon\":\"tree\",\"permission\":\"logs-view\",\"singular\":1,\"created\":1580889095,\"namespace\":\"ProcessWire\\\\\",\"useNavJSON\":true},\"10\":{\"name\":\"ProcessLogin\",\"title\":\"Login\",\"version\":108,\"permission\":\"page-view\",\"created\":1580889073,\"configurable\":4,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"50\":{\"name\":\"ProcessModule\",\"title\":\"Modules\",\"version\":118,\"permission\":\"module-admin\",\"created\":1580889073,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true,\"useNavJSON\":true,\"nav\":[{\"url\":\"?site#tab_site_modules\",\"label\":\"Site\",\"icon\":\"plug\",\"navJSON\":\"navJSON\\/?site=1\"},{\"url\":\"?core#tab_core_modules\",\"label\":\"Core\",\"icon\":\"plug\",\"navJSON\":\"navJSON\\/?core=1\"},{\"url\":\"?configurable#tab_configurable_modules\",\"label\":\"Configure\",\"icon\":\"gear\",\"navJSON\":\"navJSON\\/?configurable=1\"},{\"url\":\"?install#tab_install_modules\",\"label\":\"Install\",\"icon\":\"sign-in\",\"navJSON\":\"navJSON\\/?install=1\"},{\"url\":\"?new#tab_new_modules\",\"label\":\"New\",\"icon\":\"plus\"},{\"url\":\"?reset=1\",\"label\":\"Refresh\",\"icon\":\"refresh\"}]},\"17\":{\"name\":\"ProcessPageAdd\",\"title\":\"Page Add\",\"version\":108,\"icon\":\"plus-circle\",\"permission\":\"page-edit\",\"created\":1580889073,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true,\"useNavJSON\":true},\"7\":{\"name\":\"ProcessPageEdit\",\"title\":\"Page Edit\",\"version\":109,\"icon\":\"edit\",\"permission\":\"page-edit\",\"singular\":1,\"created\":1580889073,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true,\"useNavJSON\":true},\"129\":{\"name\":\"ProcessPageEditImageSelect\",\"title\":\"Page Edit Image\",\"version\":120,\"permission\":\"page-edit\",\"singular\":1,\"created\":1580889073,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"121\":{\"name\":\"ProcessPageEditLink\",\"title\":\"Page Edit Link\",\"version\":108,\"icon\":\"link\",\"permission\":\"page-edit\",\"singular\":true,\"created\":1580889073,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"12\":{\"name\":\"ProcessPageList\",\"title\":\"Page List\",\"version\":122,\"icon\":\"sitemap\",\"permission\":\"page-edit\",\"created\":1580889073,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true,\"useNavJSON\":true},\"150\":{\"name\":\"ProcessPageLister\",\"title\":\"Lister\",\"version\":26,\"icon\":\"search\",\"permission\":\"page-lister\",\"created\":1580889073,\"configurable\":true,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true,\"useNavJSON\":true,\"addFlag\":32},\"104\":{\"name\":\"ProcessPageSearch\",\"title\":\"Page Search\",\"version\":106,\"permission\":\"page-edit\",\"singular\":1,\"created\":1580889073,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"14\":{\"name\":\"ProcessPageSort\",\"title\":\"Page Sort and Move\",\"version\":100,\"permission\":\"page-edit\",\"created\":1580889073,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"109\":{\"name\":\"ProcessPageTrash\",\"title\":\"Page Trash\",\"version\":103,\"singular\":1,\"created\":1580889073,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"134\":{\"name\":\"ProcessPageType\",\"title\":\"Page Type\",\"version\":101,\"singular\":1,\"created\":1580889073,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true,\"useNavJSON\":true,\"addFlag\":32},\"83\":{\"name\":\"ProcessPageView\",\"title\":\"Page View\",\"version\":104,\"permission\":\"page-view\",\"created\":1580889073,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"136\":{\"name\":\"ProcessPermission\",\"title\":\"Permissions\",\"version\":101,\"icon\":\"gear\",\"permission\":\"permission-admin\",\"singular\":1,\"created\":1580889073,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true,\"useNavJSON\":true},\"138\":{\"name\":\"ProcessProfile\",\"title\":\"User Profile\",\"version\":104,\"permission\":\"profile-edit\",\"singular\":1,\"created\":1580889073,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"168\":{\"name\":\"ProcessRecentPages\",\"title\":\"Recent Pages\",\"version\":2,\"icon\":\"clock-o\",\"permission\":\"page-edit-recent\",\"singular\":1,\"created\":1580889089,\"namespace\":\"ProcessWire\\\\\",\"useNavJSON\":true,\"nav\":[{\"url\":\"?edited=1\",\"label\":\"Edited\",\"icon\":\"users\",\"navJSON\":\"navJSON\\/?edited=1\"},{\"url\":\"?added=1\",\"label\":\"Created\",\"icon\":\"users\",\"navJSON\":\"navJSON\\/?added=1\"},{\"url\":\"?edited=1&me=1\",\"label\":\"Edited by me\",\"icon\":\"user\",\"navJSON\":\"navJSON\\/?edited=1&me=1\"},{\"url\":\"?added=1&me=1\",\"label\":\"Created by me\",\"icon\":\"user\",\"navJSON\":\"navJSON\\/?added=1&me=1\"},{\"url\":\"another\\/\",\"label\":\"Add another\",\"icon\":\"plus-circle\",\"navJSON\":\"anotherNavJSON\\/\"}]},\"68\":{\"name\":\"ProcessRole\",\"title\":\"Roles\",\"version\":104,\"icon\":\"gears\",\"permission\":\"role-admin\",\"created\":1580889073,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true,\"useNavJSON\":true},\"47\":{\"name\":\"ProcessTemplate\",\"title\":\"Templates\",\"version\":114,\"icon\":\"cubes\",\"permission\":\"template-admin\",\"created\":1580889073,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true,\"useNavJSON\":true},\"66\":{\"name\":\"ProcessUser\",\"title\":\"Users\",\"version\":107,\"icon\":\"group\",\"permission\":\"user-admin\",\"created\":1580889073,\"configurable\":\"ProcessUserConfig.php\",\"namespace\":\"ProcessWire\\\\\",\"permanent\":true,\"useNavJSON\":true},\"125\":{\"name\":\"SessionLoginThrottle\",\"title\":\"Session Login Throttle\",\"version\":103,\"autoload\":\"function\",\"singular\":true,\"created\":1580889073,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\"},\"139\":{\"name\":\"SystemUpdater\",\"title\":\"System Updater\",\"version\":17,\"singular\":true,\"created\":1580889073,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"61\":{\"name\":\"TextformatterEntities\",\"title\":\"HTML Entity Encoder (htmlspecialchars)\",\"version\":100,\"created\":1580889073,\"namespace\":\"ProcessWire\\\\\"},\"175\":{\"name\":\"CroppableImage3\",\"title\":\"Croppable Image 3 (Wrapper-Module)\",\"version\":\"1.2.0\",\"icon\":\"crop\",\"requiresVersions\":{\"ProcessWire\":[\">=\",\"3.0.20\"],\"PHP\":[\">=\",\"5.3.8\"]},\"installs\":[\"FieldtypeCroppableImage3\"],\"created\":1587552640},\"176\":{\"name\":\"FieldtypeCroppableImage3\",\"title\":\"Croppable Image 3 (Fieldtype & Main-Module)\",\"version\":\"1.2.0\",\"icon\":\"crop\",\"requiresVersions\":{\"ProcessWire\":[\">=\",\"3.0.20\"],\"PHP\":[\">=\",\"5.3.8\"],\"CroppableImage3\":[\">=\",\"1.2.0\"]},\"installs\":[\"InputfieldCroppableImage3\",\"ProcessCroppableImage3\"],\"singular\":1,\"created\":1587552640,\"configurable\":true},\"177\":{\"name\":\"InputfieldCroppableImage3\",\"title\":\"Croppable Image 3 (Inputfield)\",\"version\":\"1.2.0\",\"icon\":\"crop\",\"requiresVersions\":{\"ProcessWire\":[\">=\",\"3.0.20\"],\"PHP\":[\">=\",\"5.3.8\"],\"FieldtypeCroppableImage3\":[\">=\",\"1.2.0\"]},\"created\":1587552640},\"178\":{\"name\":\"ProcessCroppableImage3\",\"title\":\"Croppable Images 3 (Process)\",\"version\":\"1.2.0\",\"icon\":\"crop\",\"requiresVersions\":{\"ProcessWire\":[\">=\",\"3.0.20\"],\"PHP\":[\">=\",\"5.3.8\"],\"FieldtypeCroppableImage3\":[\">=\",\"1.2.0\"]},\"permission\":\"croppable-image-3\",\"singular\":1,\"created\":1587552640},\"179\":{\"name\":\"DiagnoseImagehandling\",\"title\":\"Image Handling\",\"version\":1,\"requiresVersions\":{\"ProcessDiagnostics\":[\">=\",0]},\"installs\":[\"ProcessDiagnostics\"],\"singular\":true,\"created\":1587552793,\"namespace\":\"\\\\\"},\"180\":{\"name\":\"ProcessDiagnostics\",\"title\":\"Diagnostics Page\",\"version\":23,\"singular\":true,\"created\":1587552793,\"namespace\":\"\\\\\"},\"174\":{\"name\":\"ProcessDatabaseBackups\",\"title\":\"Database Backups\",\"version\":5,\"icon\":\"database\",\"requiresVersions\":{\"ProcessWire\":[\">=\",\"3.0.62\"]},\"permission\":\"db-backup\",\"singular\":1,\"created\":1587368842,\"nav\":[{\"url\":\".\\/\",\"label\":\"View\",\"icon\":\"list\"},{\"url\":\"backup\\/\",\"label\":\"Backup\",\"icon\":\"plus-circle\"},{\"url\":\"upload\\/\",\"label\":\"Upload\",\"icon\":\"cloud-upload\"}]},\"186\":{\"name\":\"FieldtypeSnipWireTaxSelector\",\"title\":\"SnipWire TaxSelector\",\"version\":\"0.8.6\",\"icon\":\"shopping-cart\",\"requiresVersions\":{\"ProcessWire\":[\">=\",\"3.0.148\"],\"SnipWire\":[\">=\",0],\"InputfieldSelect\":[\">=\",0],\"PHP\":[\">=\",\"7.0.0\"]},\"singular\":true,\"created\":1587758638},\"184\":{\"name\":\"ProcessSnipWire\",\"title\":\"SnipWire Dashboard\",\"version\":\"0.8.6\",\"icon\":\"shopping-cart\",\"requiresVersions\":{\"ProcessWire\":[\">=\",\"3.0.148\"],\"SnipWire\":[\">=\",0],\"PHP\":[\">=\",\"7.0.0\"]},\"permission\":\"snipwire-dashboard\",\"singular\":1,\"created\":1587758638,\"nav\":[{\"url\":\"orders\\/\",\"label\":\"Orders\",\"icon\":\"file-text\"},{\"url\":\"subscriptions\\/\",\"label\":\"Subscriptions\",\"icon\":\"calendar\"},{\"url\":\"abandoned-carts\\/\",\"label\":\"Abandoned Carts\",\"icon\":\"shopping-cart\"},{\"url\":\"customers\\/\",\"label\":\"Customers\",\"icon\":\"user\"},{\"url\":\"products\\/\",\"label\":\"Products\",\"icon\":\"tag\"},{\"url\":\"discounts\\/\",\"label\":\"Discounts\",\"icon\":\"scissors\"},{\"url\":\"settings\\/\",\"label\":\"Settings\",\"icon\":\"cog\"}]},\"183\":{\"name\":\"SnipWire\",\"title\":\"SnipWire\",\"version\":\"0.8.6\",\"icon\":\"shopping-cart\",\"requiresVersions\":{\"ProcessWire\":[\">=\",\"3.0.148\"],\"PHP\":[\">=\",\"7.0.0\"]},\"installs\":[\"ProcessSnipWire\",\"MarkupSnipWire\",\"FieldtypeSnipWireTaxSelector\",\"FieldtypeOptions\"],\"autoload\":true,\"singular\":true,\"created\":1587758638,\"configurable\":\"SnipWireConfig.php\"},\"185\":{\"name\":\"MarkupSnipWire\",\"title\":\"SnipWire Markup\",\"version\":\"0.8.6\",\"icon\":\"shopping-cart\",\"requiresVersions\":{\"ProcessWire\":[\">=\",\"3.0.148\"],\"SnipWire\":[\">=\",0],\"PHP\":[\">=\",\"7.0.0\"]},\"autoload\":true,\"singular\":true,\"created\":1587758638},\"182\":{\"name\":\"TextformatterVideoEmbed\",\"title\":\"Video embed for YouTube\\/Vimeo\",\"version\":111,\"singular\":1,\"created\":1587637683,\"configurable\":3},\"187\":{\"name\":\"InputfieldPageAutocomplete\",\"title\":\"Page Auto Complete\",\"version\":112,\"namespace\":\"ProcessWire\\\\\"}}', '2010-04-08 03:10:01');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('Permissions.names', '{\"croppable-image-3\":1068,\"db-backup\":1049,\"logs-edit\":1019,\"logs-view\":1018,\"page-delete\":34,\"page-edit\":32,\"page-edit-recent\":1016,\"page-lister\":1006,\"page-lock\":54,\"page-move\":35,\"page-sort\":50,\"page-template\":51,\"page-view\":36,\"profile-edit\":53,\"snipwire-dashboard\":1075,\"user-admin\":52}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('ModulesUninstalled.info', '{\"AdminThemeReno\":{\"name\":\"AdminThemeReno\",\"title\":\"Reno\",\"version\":17,\"versionStr\":\"0.1.7\",\"author\":\"Tom Reno (Renobird)\",\"summary\":\"Admin theme for ProcessWire 2.5+ by Tom Reno (Renobird)\",\"requiresVersions\":{\"AdminThemeDefault\":[\">=\",0]},\"autoload\":\"template=admin\",\"created\":1590404553,\"installed\":false,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"FieldtypeCache\":{\"name\":\"FieldtypeCache\",\"title\":\"Cache\",\"version\":102,\"versionStr\":\"1.0.2\",\"summary\":\"Caches the values of other fields for fewer runtime queries. Can also be used to combine multiple text fields and have them all be searchable under the cached field name.\",\"created\":1590404553,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"CommentFilterAkismet\":{\"name\":\"CommentFilterAkismet\",\"title\":\"Comment Filter: Akismet\",\"version\":200,\"versionStr\":\"2.0.0\",\"summary\":\"Uses the Akismet service to identify comment spam. Module plugin for the Comments Fieldtype.\",\"requiresVersions\":{\"FieldtypeComments\":[\">=\",0]},\"created\":1590404553,\"installed\":false,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"FieldtypeComments\":{\"name\":\"FieldtypeComments\",\"title\":\"Comments\",\"version\":107,\"versionStr\":\"1.0.7\",\"summary\":\"Field that stores user posted comments for a single Page\",\"installs\":[\"InputfieldCommentsAdmin\"],\"created\":1590404553,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"InputfieldCommentsAdmin\":{\"name\":\"InputfieldCommentsAdmin\",\"title\":\"Comments Admin\",\"version\":104,\"versionStr\":\"1.0.4\",\"summary\":\"Provides an administrative interface for working with comments\",\"requiresVersions\":{\"FieldtypeComments\":[\">=\",0]},\"created\":1590404553,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"FieldtypePageTable\":{\"name\":\"FieldtypePageTable\",\"title\":\"ProFields: Page Table\",\"version\":8,\"versionStr\":\"0.0.8\",\"summary\":\"A fieldtype containing a group of editable pages.\",\"installs\":[\"InputfieldPageTable\"],\"autoload\":true,\"created\":1590404553,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"FieldtypeFieldsetPage\":{\"name\":\"FieldtypeFieldsetPage\",\"title\":\"Fieldset (Page)\",\"version\":1,\"versionStr\":\"0.0.1\",\"summary\":\"Fieldset with fields isolated to separate namespace (page), enabling re-use of fields.\",\"requiresVersions\":{\"FieldtypeRepeater\":[\">=\",0]},\"autoload\":true,\"created\":1590404553,\"installed\":false,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"FieldtypeSelector\":{\"name\":\"FieldtypeSelector\",\"title\":\"Selector\",\"version\":13,\"versionStr\":\"0.1.3\",\"author\":\"Avoine + ProcessWire\",\"summary\":\"Build a page finding selector visually.\",\"created\":1590404553,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"FieldtypeToggle\":{\"name\":\"FieldtypeToggle\",\"title\":\"Toggle (Yes\\/No)\",\"version\":1,\"versionStr\":\"0.0.1\",\"summary\":\"Configurable yes\\/no, on\\/off toggle alternative to a checkbox, plus optional \\u201cother\\u201d option.\",\"requiresVersions\":{\"InputfieldToggle\":[\">=\",0]},\"created\":1590404553,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"FileCompilerTags\":{\"name\":\"FileCompilerTags\",\"title\":\"Tags File Compiler\",\"version\":1,\"versionStr\":\"0.0.1\",\"summary\":\"Enables {var} or {var.property} variables in markup sections of a file. Can be used with any API variable.\",\"created\":1590404553,\"installed\":false,\"configurable\":4,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"ImageSizerEngineAnimatedGif\":{\"name\":\"ImageSizerEngineAnimatedGif\",\"title\":\"Animated GIF Image Sizer\",\"version\":1,\"versionStr\":\"0.0.1\",\"author\":\"Horst Nogajski\",\"summary\":\"Upgrades image manipulations for animated GIFs.\",\"created\":1590404553,\"installed\":false,\"configurable\":4,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"ImageSizerEngineIMagick\":{\"name\":\"ImageSizerEngineIMagick\",\"title\":\"IMagick Image Sizer\",\"version\":3,\"versionStr\":\"0.0.3\",\"author\":\"Horst Nogajski\",\"summary\":\"Upgrades image manipulations to use PHP\'s ImageMagick library when possible.\",\"created\":1590404553,\"installed\":false,\"configurable\":4,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"InputfieldPageTable\":{\"name\":\"InputfieldPageTable\",\"title\":\"ProFields: Page Table\",\"version\":13,\"versionStr\":\"0.1.3\",\"summary\":\"Inputfield to accompany FieldtypePageTable\",\"requiresVersions\":{\"FieldtypePageTable\":[\">=\",0]},\"created\":1590404553,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"InputfieldToggle\":{\"name\":\"InputfieldToggle\",\"title\":\"Toggle\",\"version\":1,\"versionStr\":\"0.0.1\",\"summary\":\"A toggle providing similar input capability to a checkbox but much more configurable.\",\"created\":1590404553,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"LazyCron\":{\"name\":\"LazyCron\",\"title\":\"Lazy Cron\",\"version\":102,\"versionStr\":\"1.0.2\",\"summary\":\"Provides hooks that are automatically executed at various intervals. It is called \'lazy\' because it\'s triggered by a pageview, so the interval is guaranteed to be at least the time requested, rather than exactly the time requested. This is fine for most cases, but you can make it not lazy by connecting this to a real CRON job. See the module file for details. \",\"href\":\"https:\\/\\/processwire.com\\/api\\/modules\\/lazy-cron\\/\",\"autoload\":true,\"singular\":true,\"created\":1590404553,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"MarkupCache\":{\"name\":\"MarkupCache\",\"title\":\"Markup Cache\",\"version\":101,\"versionStr\":\"1.0.1\",\"summary\":\"A simple way to cache segments of markup in your templates. \",\"href\":\"https:\\/\\/processwire.com\\/api\\/modules\\/markupcache\\/\",\"autoload\":true,\"singular\":true,\"created\":1590404553,\"installed\":false,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"MarkupPageFields\":{\"name\":\"MarkupPageFields\",\"title\":\"Markup Page Fields\",\"version\":100,\"versionStr\":\"1.0.0\",\"summary\":\"Adds $page->renderFields() and $page->images->render() methods that return basic markup for output during development and debugging.\",\"autoload\":true,\"singular\":true,\"created\":1590404553,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true,\"permanent\":true},\"MarkupRSS\":{\"name\":\"MarkupRSS\",\"title\":\"Markup RSS Feed\",\"version\":102,\"versionStr\":\"1.0.2\",\"summary\":\"Renders an RSS feed. Given a PageArray, renders an RSS feed of them.\",\"created\":1590404553,\"installed\":false,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"PageFrontEdit\":{\"name\":\"PageFrontEdit\",\"title\":\"Front-End Page Editor\",\"version\":3,\"versionStr\":\"0.0.3\",\"author\":\"Ryan Cramer\",\"summary\":\"Enables front-end editing of page fields.\",\"icon\":\"cube\",\"permissions\":{\"page-edit-front\":\"Use the front-end page editor\"},\"autoload\":true,\"created\":1590404553,\"installed\":false,\"configurable\":\"PageFrontEditConfig.php\",\"namespace\":\"ProcessWire\\\\\",\"core\":true,\"license\":\"MPL 2.0\"},\"PagePaths\":{\"name\":\"PagePaths\",\"title\":\"Page Paths\",\"version\":1,\"versionStr\":\"0.0.1\",\"summary\":\"Enables page paths\\/urls to be queryable by selectors. Also offers potential for improved load performance. Builds an index at install (may take time on a large site). Currently supports only single languages sites.\",\"autoload\":true,\"singular\":true,\"created\":1590404553,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"ProcessCommentsManager\":{\"name\":\"ProcessCommentsManager\",\"title\":\"Comments\",\"version\":10,\"versionStr\":\"0.1.0\",\"author\":\"Ryan Cramer\",\"summary\":\"Manage comments in your site outside of the page editor.\",\"icon\":\"comments\",\"requiresVersions\":{\"FieldtypeComments\":[\">=\",0]},\"permission\":\"comments-manager\",\"permissions\":{\"comments-manager\":\"Use the comments manager\"},\"created\":1590404553,\"installed\":false,\"searchable\":\"comments\",\"namespace\":\"ProcessWire\\\\\",\"core\":true,\"page\":{\"name\":\"comments\",\"parent\":\"setup\",\"title\":\"Comments\"},\"nav\":[{\"url\":\"?go=approved\",\"label\":\"Approved\"},{\"url\":\"?go=pending\",\"label\":\"Pending\"},{\"url\":\"?go=spam\",\"label\":\"Spam\"},{\"url\":\"?go=all\",\"label\":\"All\"}]},\"ProcessForgotPassword\":{\"name\":\"ProcessForgotPassword\",\"title\":\"Forgot Password\",\"version\":103,\"versionStr\":\"1.0.3\",\"summary\":\"Provides password reset\\/email capability for the Login process.\",\"permission\":\"page-view\",\"created\":1590404553,\"installed\":false,\"configurable\":4,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"ProcessPageClone\":{\"name\":\"ProcessPageClone\",\"title\":\"Page Clone\",\"version\":104,\"versionStr\":\"1.0.4\",\"summary\":\"Provides ability to clone\\/copy\\/duplicate pages in the admin. Adds a &quot;copy&quot; option to all applicable pages in the PageList.\",\"permission\":\"page-clone\",\"permissions\":{\"page-clone\":\"Clone a page\",\"page-clone-tree\":\"Clone a tree of pages\"},\"autoload\":\"template=admin\",\"created\":1590404553,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true,\"page\":{\"name\":\"clone\",\"title\":\"Clone\",\"parent\":\"page\",\"status\":1024}},\"ProcessPagesExportImport\":{\"name\":\"ProcessPagesExportImport\",\"title\":\"Pages Export\\/Import\",\"version\":1,\"versionStr\":\"0.0.1\",\"author\":\"Ryan Cramer\",\"summary\":\"Enables exporting and importing of pages. Development version, not yet recommended for production use.\",\"icon\":\"paper-plane-o\",\"permission\":\"page-edit-export\",\"created\":1590404553,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true,\"page\":{\"name\":\"export-import\",\"parent\":\"page\",\"title\":\"Export\\/Import\"}},\"ProcessSessionDB\":{\"name\":\"ProcessSessionDB\",\"title\":\"Sessions\",\"version\":4,\"versionStr\":\"0.0.4\",\"summary\":\"Enables you to browse active database sessions.\",\"icon\":\"dashboard\",\"requiresVersions\":{\"SessionHandlerDB\":[\">=\",0]},\"created\":1590404554,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true,\"page\":{\"name\":\"sessions-db\",\"parent\":\"access\",\"title\":\"Sessions\"}},\"SessionHandlerDB\":{\"name\":\"SessionHandlerDB\",\"title\":\"Session Handler Database\",\"version\":5,\"versionStr\":\"0.0.5\",\"summary\":\"Installing this module makes ProcessWire store sessions in the database rather than the file system. Note that this module will log you out after install or uninstall.\",\"installs\":[\"ProcessSessionDB\"],\"created\":1590404554,\"installed\":false,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"FieldtypeNotifications\":{\"name\":\"FieldtypeNotifications\",\"title\":\"Notifications\",\"version\":4,\"versionStr\":\"0.0.4\",\"summary\":\"Field that stores user notifications.\",\"requiresVersions\":{\"SystemNotifications\":[\">=\",0]},\"created\":1590404554,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"SystemNotifications\":{\"name\":\"SystemNotifications\",\"title\":\"System Notifications\",\"version\":12,\"versionStr\":\"0.1.2\",\"summary\":\"Adds support for notifications in ProcessWire (currently in development)\",\"icon\":\"bell\",\"installs\":[\"FieldtypeNotifications\"],\"autoload\":true,\"created\":1590404554,\"installed\":false,\"configurable\":\"SystemNotificationsConfig.php\",\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"TextformatterMarkdownExtra\":{\"name\":\"TextformatterMarkdownExtra\",\"title\":\"Markdown\\/Parsedown Extra\",\"version\":130,\"versionStr\":\"1.3.0\",\"summary\":\"Markdown\\/Parsedown extra lightweight markup language by Emanuil Rusev. Based on Markdown by John Gruber.\",\"created\":1590404554,\"installed\":false,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"TextformatterNewlineBR\":{\"name\":\"TextformatterNewlineBR\",\"title\":\"Newlines to XHTML Line Breaks\",\"version\":100,\"versionStr\":\"1.0.0\",\"summary\":\"Converts newlines to XHTML line break <br \\/> tags. \",\"created\":1590404554,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"TextformatterNewlineUL\":{\"name\":\"TextformatterNewlineUL\",\"title\":\"Newlines to Unordered List\",\"version\":100,\"versionStr\":\"1.0.0\",\"summary\":\"Converts newlines to <li> list items and surrounds in an <ul> unordered list. \",\"created\":1590404554,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"TextformatterPstripper\":{\"name\":\"TextformatterPstripper\",\"title\":\"Paragraph Stripper\",\"version\":100,\"versionStr\":\"1.0.0\",\"summary\":\"Strips paragraph <p> tags that may have been applied by other text formatters before it. \",\"created\":1590404554,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"TextformatterSmartypants\":{\"name\":\"TextformatterSmartypants\",\"title\":\"SmartyPants Typographer\",\"version\":171,\"versionStr\":\"1.7.1\",\"summary\":\"Smart typography for web sites, by Michel Fortin based on SmartyPants by John Gruber. If combined with Markdown, it should be applied AFTER Markdown.\",\"created\":1590404554,\"installed\":false,\"configurable\":4,\"namespace\":\"ProcessWire\\\\\",\"core\":true,\"url\":\"https:\\/\\/github.com\\/michelf\\/php-smartypants\"},\"TextformatterStripTags\":{\"name\":\"TextformatterStripTags\",\"title\":\"Strip Markup Tags\",\"version\":100,\"versionStr\":\"1.0.0\",\"summary\":\"Strips HTML\\/XHTML Markup Tags\",\"created\":1590404554,\"installed\":false,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"DiagnoseDatabase\":{\"name\":\"DiagnoseDatabase\",\"title\":\"Database Diagnostics\",\"version\":3,\"versionStr\":\"0.0.3\",\"author\":\"Stephen Dickinson, QBox\",\"summary\":\"Allows collection of database diagnostics\",\"requiresVersions\":{\"ProcessDiagnostics\":[\">=\",0]},\"installs\":[\"ProcessDiagnostics\"],\"singular\":true,\"created\":1590404552,\"installed\":false,\"namespace\":\"\\\\\"},\"DiagnoseExample\":{\"name\":\"DiagnoseExample\",\"title\":\"Example Diagnostics Provider\",\"version\":1,\"versionStr\":\"0.0.1\",\"author\":\"Stephen Dickinson, QBox\",\"summary\":\"Demonstrates how to write a simple diagnostics provider module\",\"requiresVersions\":{\"ProcessDiagnostics\":[\">=\",0]},\"installs\":[\"ProcessDiagnostics\"],\"singular\":true,\"created\":1590404552,\"installed\":false,\"namespace\":\"\\\\\"},\"DiagnoseFiles\":{\"name\":\"DiagnoseFiles\",\"title\":\"Filesystem Diagnostics\",\"version\":1,\"versionStr\":\"0.0.1\",\"author\":\"Stephen Dickinson, QBox.co\",\"summary\":\"Allows collection of file and directory diagnostics\",\"requiresVersions\":{\"ProcessDiagnostics\":[\">=\",0]},\"installs\":[\"ProcessDiagnostics\"],\"singular\":true,\"created\":1590404552,\"installed\":false,\"namespace\":\"\\\\\"},\"DiagnoseModules\":{\"name\":\"DiagnoseModules\",\"title\":\"Module version checking\",\"version\":101,\"versionStr\":\"1.0.1\",\"author\":\"Nico Knoll\",\"summary\":\"Allows collection of module version diagnostics\",\"requiresVersions\":{\"ProcessDiagnostics\":[\">=\",0]},\"installs\":[\"ProcessDiagnostics\"],\"singular\":true,\"created\":1590404552,\"installed\":false,\"configurable\":true,\"namespace\":\"\\\\\"},\"DiagnosePhp\":{\"name\":\"DiagnosePhp\",\"title\":\"PHP Diagnostics\",\"version\":7,\"versionStr\":\"0.0.7\",\"author\":\"Stephen Dickinson, QBox\",\"summary\":\"Allows collection of PHP diagnostics\",\"requiresVersions\":{\"ProcessDiagnostics\":[\">=\",0]},\"installs\":[\"ProcessDiagnostics\"],\"singular\":true,\"created\":1590404552,\"installed\":false,\"namespace\":\"\\\\\"},\"DiagnoseWebserver\":{\"name\":\"DiagnoseWebserver\",\"title\":\"Webserver Diagnostics\",\"version\":2,\"versionStr\":\"0.0.2\",\"author\":\"Netcarver\",\"summary\":\"Allows collection of webserver diagnostics\",\"requiresVersions\":{\"ProcessDiagnostics\":[\">=\",0]},\"installs\":[\"ProcessDiagnostics\"],\"singular\":true,\"created\":1590404552,\"installed\":false,\"namespace\":\"\\\\\"},\"Helloworld\":{\"name\":\"Helloworld\",\"title\":\"Hello World\",\"version\":3,\"versionStr\":\"0.0.3\",\"summary\":\"An example module used for demonstration purposes.\",\"href\":\"https:\\/\\/processwire.com\",\"icon\":\"smile-o\",\"autoload\":true,\"singular\":true,\"created\":1590404552,\"installed\":false}}', '2010-04-08 03:10:01');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('SnipWire__Settings', '{\"currencies\":[{\"currency\":\"eur\",\"precision\":2,\"decimalSeparator\":\".\",\"thousandSeparator\":\".\",\"negativeNumberFormat\":\"- %s%v\",\"numberFormat\":\"%s%v\",\"currencySymbol\":\"\\u20ac\",\"id\":\"a355c52e-eb1c-45ab-b136-b43978dddd1a\"}],\"features\":{\"inventoryManagement\":false},\"onlyAllowGuests\":true,\"allowExportAsPdf\":false,\"allowShareByEmail\":false,\"useLegacyForCsvExport\":false,\"emailSignature\":\"\",\"customerName\":\"\",\"websiteUrl\":\"\",\"emailFrom\":\"\",\"businessAddressLine1\":\"\",\"businessAddressLine2\":\"\",\"businessCity\":\"\",\"businessCountryCode\":\"\",\"businessStateCode\":\"\",\"businessVatNumber\":\"\",\"businessPostalCode\":\"\",\"domain\":null,\"timeZone\":\"(UTC+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna\",\"enabledCountries\":[\"AX\",\"AL\",\"AD\",\"AT\",\"BY\",\"BE\",\"BA\",\"BG\",\"HR\",\"CY\",\"CZ\",\"DK\",\"EE\",\"FO\",\"FI\",\"FR\",\"DE\",\"GI\",\"GR\",\"GG\",\"HU\",\"IS\",\"IE\",\"IM\",\"IT\",\"JE\",\"XK\",\"LV\",\"LI\",\"LT\",\"LU\",\"MK\",\"MT\",\"MD\",\"MC\",\"ME\",\"NL\",\"NO\",\"PL\",\"PT\",\"RO\",\"RU\",\"SM\",\"RS\",\"SK\",\"SI\",\"ES\",\"SJ\",\"SE\",\"CH\",\"TR\",\"UA\",\"GB\",\"VA\"],\"notifyWhenNewOrderOccurs\":false,\"currency\":\"eur\",\"currencyChargedWith\":\"USD\",\"allowDeferredPayment\":false,\"webhooksUrls\":[],\"invoicePrefix\":\"\",\"invoiceStartNumber\":null,\"includeProductImagesInInvoice\":false,\"currencySymbol\":\"\\u20ac\",\"numberFormat\":\"%s%v\",\"negativeNumberFormat\":\"- %s%v\",\"thousandSeparator\":\".\",\"decimalSeparator\":\".\",\"precision\":2,\"onlyAllowDeferredPayment\":false,\"protocol\":\"\",\"logoUrl\":\"\",\"eCommerceAnalyticsEnabled\":false,\"expressCheckoutEnabled\":false,\"customGatewayEnabled\":false,\"sendInvoiceCopyInBcc\":false,\"notificationEmails\":\"\",\"paymentConfiguration\":{\"useAuthorizeAndCapture\":false},\"ordersHistoryUrl\":\"\",\"allowedDomains\":[],\"automaticallySendInvoices\":true,\"clientVersion\":\"2.1\",\"backendVersion\":\"1.0\",\"addressSpecified\":false,\"emailsToNotify\":[],\"businessAddressCompleted\":false,\"businessAddress\":{\"fullName\":null,\"firstName\":null,\"name\":null,\"company\":null,\"address1\":\"\",\"address2\":\"\",\"fullAddress\":\"\",\"city\":\"\",\"country\":\"\",\"postalCode\":\"\",\"province\":\"\",\"phone\":null,\"vatNumber\":\"\"},\"notificationPreferences\":{\"subscriptionCancelation\":true}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__10d1c7948d07eb592745401399e256b5', '{\"source\":{\"file\":\"D:\\/workspace\\/boardschmiede\\/site\\/templates\\/snipcart-shop.php\",\"hash\":\"acbe7e49e5692d7e62b7bcce191aec1c\",\"size\":4804,\"time\":1587764648,\"ns\":\"ProcessWire\"},\"target\":{\"file\":\"D:\\/workspace\\/boardschmiede\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/snipcart-shop.php\",\"hash\":\"acbe7e49e5692d7e62b7bcce191aec1c\",\"size\":4804,\"time\":1587764648}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__d08a355181dd83464397bec6cad4f0d9', '{\"source\":{\"file\":\"D:\\/workspace\\/boardschmiede\\/site\\/templates\\/snipcart-product.php\",\"hash\":\"960e57fb2e0621050bd5012f17b2c759\",\"size\":3608,\"time\":1589314380,\"ns\":\"ProcessWire\"},\"target\":{\"file\":\"D:\\/workspace\\/boardschmiede\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/snipcart-product.php\",\"hash\":\"960e57fb2e0621050bd5012f17b2c759\",\"size\":3608,\"time\":1589314380}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__9c2671026bf8475e299ca16b66fff0b0', '{\"source\":{\"file\":\"\\/var\\/www\\/vhosts\\/hosting105330.a2fa5.netcup.net\\/boardschmiede.startuppages.de\\/site\\/templates\\/admin.php\",\"hash\":\"9636f854995462a4cb877cb1204bc2fe\",\"size\":467,\"time\":1581147920,\"ns\":\"ProcessWire\"},\"target\":{\"file\":\"\\/var\\/www\\/vhosts\\/hosting105330.a2fa5.netcup.net\\/boardschmiede.startuppages.de\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/admin.php\",\"hash\":\"9636f854995462a4cb877cb1204bc2fe\",\"size\":467,\"time\":1581147920}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__719b1c92c0d71287cf49f0cb9ce72722', '{\"source\":{\"file\":\"\\/var\\/www\\/vhosts\\/hosting105330.a2fa5.netcup.net\\/boardschmiede.startuppages.de\\/site\\/templates\\/_init.php\",\"hash\":\"03d948499ec1c07d62a674daefd3ad67\",\"size\":1035,\"time\":1588086579,\"ns\":\"ProcessWire\"},\"target\":{\"file\":\"\\/var\\/www\\/vhosts\\/hosting105330.a2fa5.netcup.net\\/boardschmiede.startuppages.de\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/_init.php\",\"hash\":\"03d948499ec1c07d62a674daefd3ad67\",\"size\":1035,\"time\":1588086579}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__71e1d45129dd660f2bbe9d08e08eaabe', '{\"source\":{\"file\":\"\\/var\\/www\\/vhosts\\/hosting105330.a2fa5.netcup.net\\/boardschmiede.startuppages.de\\/site\\/templates\\/_main.php\",\"hash\":\"d858f9dace62bdbfab0579f1a2b8edb5\",\"size\":2908,\"time\":1589623322,\"ns\":\"ProcessWire\"},\"target\":{\"file\":\"\\/var\\/www\\/vhosts\\/hosting105330.a2fa5.netcup.net\\/boardschmiede.startuppages.de\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/_main.php\",\"hash\":\"d858f9dace62bdbfab0579f1a2b8edb5\",\"size\":2908,\"time\":1589623322}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__0637f534f5e599f4fa52bf1b2ce45e14', '{\"source\":{\"file\":\"\\/var\\/www\\/vhosts\\/hosting105330.a2fa5.netcup.net\\/boardschmiede.startuppages.de\\/site\\/templates\\/home.php\",\"hash\":\"d907e145d2d589a8810e0c02334528b0\",\"size\":4516,\"time\":1587755746,\"ns\":\"ProcessWire\"},\"target\":{\"file\":\"\\/var\\/www\\/vhosts\\/hosting105330.a2fa5.netcup.net\\/boardschmiede.startuppages.de\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/home.php\",\"hash\":\"d907e145d2d589a8810e0c02334528b0\",\"size\":4516,\"time\":1587755746}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__6934b55f6941570c3c11ba3bb30d0eb5', '{\"source\":{\"file\":\"\\/var\\/www\\/vhosts\\/hosting105330.a2fa5.netcup.net\\/boardschmiede.startuppages.de\\/site\\/templates\\/basic-page.php\",\"hash\":\"343a5c586f1e72cafba78739f0acd525\",\"size\":969,\"time\":1587755264,\"ns\":\"ProcessWire\"},\"target\":{\"file\":\"\\/var\\/www\\/vhosts\\/hosting105330.a2fa5.netcup.net\\/boardschmiede.startuppages.de\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/basic-page.php\",\"hash\":\"343a5c586f1e72cafba78739f0acd525\",\"size\":969,\"time\":1587755264}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__77e612c9318513cbfd0f58e50db92fe2', '{\"source\":{\"file\":\"\\/var\\/www\\/vhosts\\/hosting105330.a2fa5.netcup.net\\/boardschmiede.startuppages.de\\/site\\/templates\\/board-overview-page.php\",\"hash\":\"dc09884f99c596788ae7ccb7c69db1cc\",\"size\":2002,\"time\":1588086579,\"ns\":\"ProcessWire\"},\"target\":{\"file\":\"\\/var\\/www\\/vhosts\\/hosting105330.a2fa5.netcup.net\\/boardschmiede.startuppages.de\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/board-overview-page.php\",\"hash\":\"dc09884f99c596788ae7ccb7c69db1cc\",\"size\":2002,\"time\":1588086579}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__ed274b250801bc6c0a06ead473f2aa92', '{\"source\":{\"file\":\"\\/var\\/www\\/vhosts\\/hosting105330.a2fa5.netcup.net\\/boardschmiede.startuppages.de\\/site\\/templates\\/snipcart-product.php\",\"hash\":\"960e57fb2e0621050bd5012f17b2c759\",\"size\":3608,\"time\":1589623322,\"ns\":\"ProcessWire\"},\"target\":{\"file\":\"\\/var\\/www\\/vhosts\\/hosting105330.a2fa5.netcup.net\\/boardschmiede.startuppages.de\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/snipcart-product.php\",\"hash\":\"960e57fb2e0621050bd5012f17b2c759\",\"size\":3608,\"time\":1589623322}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__bf144429e626acd79085252c7d0628ad', '{\"source\":{\"file\":\"\\/var\\/www\\/html\\/site\\/templates\\/_init.php\",\"hash\":\"03d948499ec1c07d62a674daefd3ad67\",\"size\":1035,\"time\":1590404553,\"ns\":\"ProcessWire\"},\"target\":{\"file\":\"\\/var\\/www\\/html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/_init.php\",\"hash\":\"03d948499ec1c07d62a674daefd3ad67\",\"size\":1035,\"time\":1590404553}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__c9c7e241b39e7b8c09a07cb000306242', '{\"source\":{\"file\":\"\\/var\\/www\\/html\\/site\\/templates\\/_main.php\",\"hash\":\"63696b89db0432571dfee20d3d71259c\",\"size\":4730,\"time\":1591463882,\"ns\":\"ProcessWire\"},\"target\":{\"file\":\"\\/var\\/www\\/html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/_main.php\",\"hash\":\"63696b89db0432571dfee20d3d71259c\",\"size\":4730,\"time\":1591463882}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__874910242a2375010bfe6ba44dda8f7d', '{\"source\":{\"file\":\"\\/var\\/www\\/html\\/site\\/templates\\/home.php\",\"hash\":\"ffe39a23d734e9c83747987ac2f14266\",\"size\":4183,\"time\":1590562913,\"ns\":\"ProcessWire\"},\"target\":{\"file\":\"\\/var\\/www\\/html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/home.php\",\"hash\":\"ffe39a23d734e9c83747987ac2f14266\",\"size\":4183,\"time\":1590562913}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__edf85e782ce9741d852681e74e29205e', '{\"source\":{\"file\":\"\\/var\\/www\\/html\\/site\\/templates\\/admin.php\",\"hash\":\"9636f854995462a4cb877cb1204bc2fe\",\"size\":467,\"time\":1590404553,\"ns\":\"ProcessWire\"},\"target\":{\"file\":\"\\/var\\/www\\/html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/admin.php\",\"hash\":\"9636f854995462a4cb877cb1204bc2fe\",\"size\":467,\"time\":1590404553}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__2df9314273afdeed8cb4f684610e2cd5', '{\"source\":{\"file\":\"\\/var\\/www\\/html\\/site\\/templates\\/basic-page.php\",\"hash\":\"343a5c586f1e72cafba78739f0acd525\",\"size\":969,\"time\":1590404553,\"ns\":\"ProcessWire\"},\"target\":{\"file\":\"\\/var\\/www\\/html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/basic-page.php\",\"hash\":\"343a5c586f1e72cafba78739f0acd525\",\"size\":969,\"time\":1590404553}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__be292a7a1566a2c9a5f504b4d767d32b', '{\"source\":{\"file\":\"\\/var\\/www\\/html\\/site\\/templates\\/snipcart-shop.php\",\"hash\":\"e0c46fd9c59c14bc031a2e87032c7629\",\"size\":3826,\"time\":1590566146,\"ns\":\"ProcessWire\"},\"target\":{\"file\":\"\\/var\\/www\\/html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/snipcart-shop.php\",\"hash\":\"e0c46fd9c59c14bc031a2e87032c7629\",\"size\":3826,\"time\":1590566146}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__dbb91cc358d7cba73b127b80e3224c38', '{\"source\":{\"file\":\"\\/var\\/www\\/html\\/site\\/templates\\/board-overview-page.php\",\"hash\":\"228da90ebb07876259c207b8acde13f2\",\"size\":969,\"time\":1590565816,\"ns\":\"ProcessWire\"},\"target\":{\"file\":\"\\/var\\/www\\/html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/board-overview-page.php\",\"hash\":\"228da90ebb07876259c207b8acde13f2\",\"size\":969,\"time\":1590565816}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__e18d6d706f7cc3fd46f16657b1515d05', '{\"source\":{\"file\":\"\\/var\\/www\\/html\\/site\\/templates\\/snipcart-product.php\",\"hash\":\"0869ba09c3ecec5488ac110b9ae1ef80\",\"size\":3542,\"time\":1591464236,\"ns\":\"ProcessWire\"},\"target\":{\"file\":\"\\/var\\/www\\/html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/snipcart-product.php\",\"hash\":\"0869ba09c3ecec5488ac110b9ae1ef80\",\"size\":3542,\"time\":1591464236}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__fb488b2a5cdaed9adaa824e46d8928bd', '{\"source\":{\"file\":\"\\/var\\/www\\/html\\/site\\/modules\\/DiagnosePhp\\/ProcessDiagnostics.module\",\"hash\":\"4560b510543cc143dfd2d7d481cd7deb\",\"size\":10814,\"time\":1590404552,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/var\\/www\\/html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/DiagnosePhp\\/ProcessDiagnostics.module\",\"hash\":\"f8d637f56c389feadc1d06516d54ba97\",\"size\":10995,\"time\":1590404552}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__78d9f32bd441b87dc4cee5fcb33d9e14', '{\"source\":{\"file\":\"\\/var\\/www\\/html\\/site\\/templates\\/basic-page-twoes.php\",\"hash\":\"95d5126652f84841675220e35d2465e7\",\"size\":1482,\"time\":1591459406,\"ns\":\"ProcessWire\"},\"target\":{\"file\":\"\\/var\\/www\\/html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/basic-page-twoes.php\",\"hash\":\"95d5126652f84841675220e35d2465e7\",\"size\":1482,\"time\":1591459406}}', '2010-04-08 03:10:10');

DROP TABLE IF EXISTS `field_admin_theme`;
CREATE TABLE `field_admin_theme` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` int(11) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `field_board_features`;
CREATE TABLE `field_board_features` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` mediumtext NOT NULL,
  `data1012` mediumtext,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`),
  FULLTEXT KEY `data1012` (`data1012`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

INSERT INTO `field_board_features` (`pages_id`, `data`, `data1012`) VALUES('1052', '<p><strong>Sizes:</strong></p>\n\n<p>136x42<br />\n140x50</p>', '');
INSERT INTO `field_board_features` (`pages_id`, `data`, `data1012`) VALUES('1058', '', '');
INSERT INTO `field_board_features` (`pages_id`, `data`, `data1012`) VALUES('1060', '', '');
INSERT INTO `field_board_features` (`pages_id`, `data`, `data1012`) VALUES('1063', '', '');
INSERT INTO `field_board_features` (`pages_id`, `data`, `data1012`) VALUES('1065', '', '');
INSERT INTO `field_board_features` (`pages_id`, `data`, `data1012`) VALUES('1077', '<p><strong>Sizes:</strong></p>\n\n<p>136x40cm<br />\n138x42cm</p>', '');

DROP TABLE IF EXISTS `field_board_images`;
CREATE TABLE `field_board_images` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` varchar(250) NOT NULL,
  `sort` int(10) unsigned NOT NULL,
  `description` text NOT NULL,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `filedata` mediumtext,
  `filesize` int(11) DEFAULT NULL,
  `created_users_id` int(10) unsigned NOT NULL DEFAULT '0',
  `modified_users_id` int(10) unsigned NOT NULL DEFAULT '0',
  `width` int(11) DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  `ratio` decimal(4,2) DEFAULT NULL,
  PRIMARY KEY (`pages_id`,`sort`),
  KEY `data` (`data`),
  KEY `modified` (`modified`),
  KEY `created` (`created`),
  KEY `filesize` (`filesize`),
  KEY `width` (`width`),
  KEY `height` (`height`),
  KEY `ratio` (`ratio`),
  FULLTEXT KEY `description` (`description`),
  FULLTEXT KEY `filedata` (`filedata`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

INSERT INTO `field_board_images` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`, `filesize`, `created_users_id`, `modified_users_id`, `width`, `height`, `ratio`) VALUES('1052', 'twintip_front_cutout_edited-v1.png', '0', '[\"\"]', '2020-04-23 13:59:28', '2020-04-23 13:59:28', '', '3718553', '41', '41', '1160', '3968', '0.29');
INSERT INTO `field_board_images` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`, `filesize`, `created_users_id`, `modified_users_id`, `width`, `height`, `ratio`) VALUES('1065', 'lw_back.png', '0', '[\"\"]', '2020-04-22 19:24:55', '2020-04-22 19:24:55', '', '13654724', '41', '41', '3128', '5568', '0.56');
INSERT INTO `field_board_images` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`, `filesize`, `created_users_id`, `modified_users_id`, `width`, `height`, `ratio`) VALUES('1063', 'ladys_front.png', '0', '[\"\"]', '2020-04-22 19:24:08', '2020-04-22 19:24:08', '', '14450572', '41', '41', '3128', '5568', '0.56');

DROP TABLE IF EXISTS `field_board_style`;
CREATE TABLE `field_board_style` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  `data1012` text,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  KEY `data_exact1012` (`data1012`(250)),
  FULLTEXT KEY `data` (`data`),
  FULLTEXT KEY `data1012` (`data1012`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_board_style` (`pages_id`, `data`, `data1012`) VALUES('1077', 'Freeride / Freestyle', '');
INSERT INTO `field_board_style` (`pages_id`, `data`, `data1012`) VALUES('1083', 'Wakestyle / Freestyle', '');

DROP TABLE IF EXISTS `field_body`;
CREATE TABLE `field_body` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` mediumtext NOT NULL,
  `data1012` mediumtext,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`),
  FULLTEXT KEY `data1012` (`data1012`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

INSERT INTO `field_body` (`pages_id`, `data`, `data1012`) VALUES('27', '<h3>The page you were looking for is not found.</h3>\n\n<p>Please use our search engine or navigation above to find the page.</p>', '<h3>Die Seite, die Sie suchen, wurde nicht gefunden. </h3>\n\n<p>Bitte verwenden Sie die Suchmaschine oder die Navigation oben, um die Seite zu finden.</p>');
INSERT INTO `field_body` (`pages_id`, `data`, `data1012`) VALUES('1', '<h1 style=\"text-align:center;\">THIS IS JUST THE BEGINNING OF YOUR JOURNEY</h1>\n\n<p style=\"text-align:center;\"><em>We are a small manufacturer located at the popular kite spot Workum in the Netherlands, who have set ourselves the goal to \"forge\" the ultimate board. Years of honing our craft, combined with a real passion for perfection, ensures every board is made to the highest possible standard.</em></p>', '<h2>Was ist ProcessWire?</h2>\n\n<p>ProcessWire gibt Ihnen volle Kontrolle über Ihre Felder, Vorlagen und Markup. Es bietet ein mächtiges Templating-System, das sich ganz nach Ihren richtet. Mit dem ProcessWire API bearbeiten Sie Inhalte spielend einfach und bequem. <a href=\"http://de.processwire.com\">Mehr erfahren</a></p>\n\n<h3>Über dieses Webseiten-Profil</h3>\n\n<p>Dieses Demo-Profil ist eine einfache Webseite, die Sie als Grundlage für die Entwicklung Ihrer eigenen Webseiten verwenden können oder um sich mit dem System vertraut zu machen. Die Seiten dienen lediglich als Beispiele und erheben nicht den Anspruch, alle ProcessWire Features demonstrieren zu wollen. Wenn Sie Ihre eigene Webseiten bauen, ist dieses Profil ein guter Ausgangspunkt. Sie können die vorhandenen Vorlagen und das Design verwenden wie sie sind, oder nach Belieben austauschen.</p>\n\n<h3>Diese Seite durchsuchen</h3>');
INSERT INTO `field_body` (`pages_id`, `data`, `data1012`) VALUES('1021', '<h2 style=\"text-align:center;\">THE NEW BOARDSCHMIEDE BOARDS</h2>\n\n<p style=\"text-align:center;\">odylorem ipsum. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.</p>', '');
INSERT INTO `field_body` (`pages_id`, `data`, `data1012`) VALUES('1022', '<p>Perfect boards for every rider</p>', '');
INSERT INTO `field_body` (`pages_id`, `data`, `data1012`) VALUES('1023', '', '');
INSERT INTO `field_body` (`pages_id`, `data`, `data1012`) VALUES('1024', '<h1 style=\"text-align:center;\">MEET THE TEAM OF BOARDSCHMIEDE</h1>', '');
INSERT INTO `field_body` (`pages_id`, `data`, `data1012`) VALUES('1025', '', '');
INSERT INTO `field_body` (`pages_id`, `data`, `data1012`) VALUES('1026', '<h3>ADDRESS</h3>\n\n<p>Suderseleane 7<br />\n8711GX Workum<br />\nThe Netherlands</p>\n\n<p>Phone: +49 151 510 10 236</p>\n\n<p>Email: <a href=\"mailto:info@boardschmiede.com\">info@boardschmiede.com</a></p>\n\n<h3>SOCIAL MEDIA</h3>\n\n<p><a href=\"https://www.facebook.com/customboards.de/\" target=\"_blank\" rel=\"noreferrer noopener\">Facebook</a></p>\n\n<p><a href=\"https://www.instagram.com/boardschmiede\" target=\"_blank\" rel=\"noreferrer noopener\">Instagram</a></p>', '');
INSERT INTO `field_body` (`pages_id`, `data`, `data1012`) VALUES('1050', '', '');
INSERT INTO `field_body` (`pages_id`, `data`, `data1012`) VALUES('1052', '<p>Bodylorem ipsum. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus.</p>', '');
INSERT INTO `field_body` (`pages_id`, `data`, `data1012`) VALUES('1002', '<h2>Ut capio feugiat saepius torqueo olim</h2>\n\n<h3>In utinam facilisi eum vicis feugait nimis</h3>\n\n<p>Iusto incassum appellatio cui macto genitus vel. Lobortis aliquam luctus, roto enim, imputo wisi tamen. Ratis odio, genitus acsi, neo illum consequat consectetuer ut.</p>\n\n<blockquote>\n<p>Wisi fere virtus cogo, ex ut vel nullus similis vel iusto. Tation incassum adsum in, quibus capto premo diam suscipere facilisi. Uxor laoreet mos capio premo feugait ille et. Pecus abigo immitto epulae duis vel. Neque causa, indoles verto, decet ingenium dignissim.</p>\n</blockquote>\n\n<p>Patria iriure vel vel autem proprius indoles ille sit. Tation blandit refoveo, accumsan ut ulciscor lucidus inhibeo capto aptent opes, foras.</p>\n\n<h3>Dolore ea valde refero feugait utinam luctus</h3>\n\n<p>Usitas, nostrud transverbero, in, amet, nostrud ad. Ex feugiat opto diam os aliquam regula lobortis dolore ut ut quadrum. Esse eu quis nunc jugis iriure volutpat wisi, fere blandit inhibeo melior, hendrerit, saluto velit. Eu bene ideo dignissim delenit accumsan nunc. Usitas ille autem camur consequat typicus feugait elit ex accumsan nutus accumsan nimis pagus, occuro. Immitto populus, qui feugiat opto pneum letalis paratus. Mara conventio torqueo nibh caecus abigo sit eum brevitas. Populus, duis ex quae exerci hendrerit, si antehabeo nobis, consequat ea praemitto zelus.</p>\n\n<p>Immitto os ratis euismod conventio erat jus caecus sudo. code test Appellatio consequat, et ibidem ludus nulla dolor augue abdo tego euismod plaga lenis. Sit at nimis venio venio tego os et pecus enim pneum magna nobis ad pneum. Saepius turpis probo refero molior nonummy aliquam neque appellatio jus luctus acsi. Ulciscor refero pagus imputo eu refoveo valetudo duis dolore usitas. Consequat suscipere quod torqueo ratis ullamcorper, dolore lenis, letalis quia quadrum plaga minim.</p>', '');
INSERT INTO `field_body` (`pages_id`, `data`, `data1012`) VALUES('1001', '<h2 style=\"text-align:center;\">ABOUT BOARDSCHMIEDE</h2>\n\n<p style=\"text-align:center;\">Dolore ad nunc, mos accumsan paratus duis suscipit luptatum facilisis macto uxor iaceo quadrum. Demoveo, appellatio elit neque ad commodo ea. Wisi, iaceo, tincidunt at commoveo rusticus et, ludus. Feugait at blandit bene blandit suscipere abdo duis ideo bis commoveo pagus ex, velit. Consequat commodo roto accumsan, duis transverbero.</p>', '<h2>Hinter den Wortbergen</h2>\n\n<p>Weit hinten, hinter den Wortbergen, fern der Länder Vokalien und Konsonantien leben die Blindtexte. Abgeschieden wohnen sie in Buchstabhausen an der Küste des Semantik, eines großen Sprachozeans. Ein kleines Bächlein namens Duden fließt durch ihren Ort und versorgt sie mit den nötigen Regelialien. Es ist ein paradiesmatisches Land, in dem einem gebratene Satzteile in den Mund fliegen. Nicht einmal von der allmächtigen Interpunktion werden die Blindtexte beherrscht – ein geradezu unorthographisches Leben.</p>');
INSERT INTO `field_body` (`pages_id`, `data`, `data1012`) VALUES('1004', '<h2>Pertineo vel dignissim, natu letalis fere odio</h2>\n\n<p>Magna in gemino, gilvus iusto capto jugis abdo mos aptent acsi qui. Utrum inhibeo humo humo duis quae. Lucidus paulatim facilisi scisco quibus hendrerit conventio adsum.</p>\n\n<h3>Si lobortis singularis genitus ibidem saluto</h3>\n\n<ul>\n	<li>Feugiat eligo foras ex elit sed indoles hos elit ex antehabeo defui et nostrud.</li>\n	<li>Letatio valetudo multo consequat inhibeo ille dignissim pagus et in quadrum eum eu.</li>\n	<li>Aliquam si consequat, ut nulla amet et turpis exerci, adsum luctus ne decet, delenit.</li>\n	<li>Commoveo nunc diam valetudo cui, aptent commoveo at obruo uxor nulla aliquip augue.</li>\n</ul>\n\n<p>Iriure, ex velit, praesent vulpes delenit capio vero gilvus inhibeo letatio aliquip metuo qui eros. Transverbero demoveo euismod letatio torqueo melior. Ut odio in suscipit paulatim amet huic letalis suscipere eros causa, letalis magna.</p>\n\n<ol>\n	<li>Feugiat eligo foras ex elit sed indoles hos elit ex antehabeo defui et nostrud.</li>\n	<li>Letatio valetudo multo consequat inhibeo ille dignissim pagus et in quadrum eum eu.</li>\n	<li>Aliquam si consequat, ut nulla amet et turpis exerci, adsum luctus ne decet, delenit.</li>\n	<li>Commoveo nunc diam valetudo cui, aptent commoveo at obruo uxor nulla aliquip augue.</li>\n</ol>', '');
INSERT INTO `field_body` (`pages_id`, `data`, `data1012`) VALUES('1020', '', '');
INSERT INTO `field_body` (`pages_id`, `data`, `data1012`) VALUES('1054', '', '');
INSERT INTO `field_body` (`pages_id`, `data`, `data1012`) VALUES('1058', '', '');
INSERT INTO `field_body` (`pages_id`, `data`, `data1012`) VALUES('1060', '', '');
INSERT INTO `field_body` (`pages_id`, `data`, `data1012`) VALUES('1063', '', '');
INSERT INTO `field_body` (`pages_id`, `data`, `data1012`) VALUES('1065', '', '');
INSERT INTO `field_body` (`pages_id`, `data`, `data1012`) VALUES('1081', '', '');
INSERT INTO `field_body` (`pages_id`, `data`, `data1012`) VALUES('1116', '<h2>Impressum</h2>\n\n<p>Angaben gemäß § 5 TMG</p>\n\n<p>Fa.Boardschmiede<br />\nSuderseleane 7<br />\n8711 GX Workum</p>\n\n<p><strong>Inhaber:</strong></p>\n\n<p>Thilo Schwemlein</p>\n\n<p><strong>Kontakt:</strong><br />\nTelefon: +49 / 15151010236<br />\nE-Mail: <a href=\"mailto:kontakt@boardschmiede.de\">kontakt@boardschmiede.de</a></p>\n\n<p>KVK 65509242</p>\n\n<p>BTW NL291671378B01</p>\n\n<p><strong>Haftungsausschluss:</strong></p>\n\n<p><strong>Haftung für Inhalte</strong></p>\n\n<p>Die Inhalte unserer Seiten wurden mit größter Sorgfalt erstellt. Für die Richtigkeit, Vollständigkeit und Aktualität der Inhalte können wir jedoch keine Gewähr übernehmen. Als Diensteanbieter sind wir gemäß § 7 Abs.1 TMG für eigene Inhalte auf diesen Seiten nach den allgemeinen Gesetzen verantwortlich. Nach §§ 8 bis 10 TMG sind wir als Diensteanbieter jedoch nicht verpflichtet, übermittelte oder gespeicherte fremde Informationen zu überwachen oder nach Umständen zu forschen, die auf eine rechtswidrige Tätigkeit hinweisen. Verpflichtungen zur Entfernung oder Sperrung der Nutzung von Informationen nach den allgemeinen Gesetzen bleiben hiervon unberührt. Eine diesbezügliche Haftung ist jedoch erst ab dem Zeitpunkt der Kenntnis einer konkreten Rechtsverletzung möglich. Bei Bekanntwerden von entsprechenden Rechtsverletzungen werden wir diese Inhalte umgehend entfernen.</p>\n\n<p><strong>Haftung für Links</strong></p>\n\n<p>Unser Angebot enthält Links zu externen Webseiten Dritter, auf deren Inhalte wir keinen Einfluss haben. Deshalb können wir für diese fremden Inhalte auch keine Gewähr übernehmen. Für die Inhalte der verlinkten Seiten ist stets der jeweilige Anbieter oder Betreiber der Seiten verantwortlich. Die verlinkten Seiten wurden zum Zeitpunkt der Verlinkung auf mögliche Rechtsverstöße überprüft. Rechtswidrige Inhalte waren zum Zeitpunkt der Verlinkung nicht erkennbar. Eine permanente inhaltliche Kontrolle der verlinkten Seiten ist jedoch ohne konkrete Anhaltspunkte einer Rechtsverletzung nicht zumutbar. Bei Bekanntwerden von Rechtsverletzungen werden wir derartige Links umgehend entfernen.</p>\n\n<p><strong>Urheberrecht</strong></p>\n\n<p>Die durch die Seitenbetreiber erstellten Inhalte und Werke auf diesen Seiten unterliegen dem deutschen Urheberrecht. Die Vervielfältigung, Bearbeitung, Verbreitung und jede Art der Verwertung außerhalb der Grenzen des Urheberrechtes bedürfen der schriftlichen Zustimmung des jeweiligen Autors bzw. Erstellers. Downloads und Kopien dieser Seite sind nur für den privaten, nicht kommerziellen Gebrauch gestattet. Soweit die Inhalte auf dieser Seite nicht vom Betreiber erstellt wurden, werden die Urheberrechte Dritter beachtet. Insbesondere werden Inhalte Dritter als solche gekennzeichnet. Sollten Sie trotzdem auf eine Urheberrechtsverletzung aufmerksam werden, bitten wir um einen entsprechenden Hinweis. Bei Bekanntwerden von Rechtsverletzungen werden wir derartige Inhalte umgehend entfernen.</p>\n\n<p><strong>Datenschutz</strong></p>\n\n<p>Die Nutzung unserer Webseite ist in der Regel ohne Angabe personenbezogener Daten möglich. Soweit auf unseren Seiten personenbezogene Daten (beispielsweise Name, Anschrift oder eMail-Adressen) erhoben werden, erfolgt dies, soweit möglich, stets auf freiwilliger Basis. Diese Daten werden ohne Ihre ausdrückliche Zustimmung nicht an Dritte weitergegeben.<br />\nWir weisen darauf hin, dass die Datenübertragung im Internet (z.B. bei der Kommunikation per E-Mail) Sicherheitslücken aufweisen kann. Ein lückenloser Schutz der Daten vor dem Zugriff durch Dritte ist nicht möglich.<br />\nDer Nutzung von im Rahmen der Impressumspflicht veröffentlichten Kontaktdaten durch Dritte zur Übersendung von nicht ausdrücklich angeforderter Werbung und Informationsmaterialien wird hiermit ausdrücklich widersprochen. Die Betreiber der Seiten behalten sich ausdrücklich rechtliche Schritte im Falle der unverlangten Zusendung von Werbeinformationen, etwa durch Spam-Mails, vor.</p>', '<h2>Impressum</h2>\n\n<p>Angaben gemäß § 5 TMG</p>\n\n<p>Fa.Boardschmiede<br />\nSuderseleane 7<br />\n8711 GX Workum</p>\n\n<p><strong>Inhaber:</strong></p>\n\n<p>Thilo Schwemlein</p>\n\n<p><strong>Kontakt:</strong><br />\nTelefon: +49 / 15151010236<br />\nE-Mail: <a href=\"mailto:kontakt@boardschmiede.de\">kontakt@boardschmiede.de</a></p>\n\n<p>KVK 65509242</p>\n\n<p>BTW NL291671378B01</p>\n\n<p><strong>Haftungsausschluss:</strong></p>\n\n<p><strong>Haftung für Inhalte</strong></p>\n\n<p>Die Inhalte unserer Seiten wurden mit größter Sorgfalt erstellt. Für die Richtigkeit, Vollständigkeit und Aktualität der Inhalte können wir jedoch keine Gewähr übernehmen. Als Diensteanbieter sind wir gemäß § 7 Abs.1 TMG für eigene Inhalte auf diesen Seiten nach den allgemeinen Gesetzen verantwortlich. Nach §§ 8 bis 10 TMG sind wir als Diensteanbieter jedoch nicht verpflichtet, übermittelte oder gespeicherte fremde Informationen zu überwachen oder nach Umständen zu forschen, die auf eine rechtswidrige Tätigkeit hinweisen. Verpflichtungen zur Entfernung oder Sperrung der Nutzung von Informationen nach den allgemeinen Gesetzen bleiben hiervon unberührt. Eine diesbezügliche Haftung ist jedoch erst ab dem Zeitpunkt der Kenntnis einer konkreten Rechtsverletzung möglich. Bei Bekanntwerden von entsprechenden Rechtsverletzungen werden wir diese Inhalte umgehend entfernen.</p>\n\n<p><strong>Haftung für Links</strong></p>\n\n<p>Unser Angebot enthält Links zu externen Webseiten Dritter, auf deren Inhalte wir keinen Einfluss haben. Deshalb können wir für diese fremden Inhalte auch keine Gewähr übernehmen. Für die Inhalte der verlinkten Seiten ist stets der jeweilige Anbieter oder Betreiber der Seiten verantwortlich. Die verlinkten Seiten wurden zum Zeitpunkt der Verlinkung auf mögliche Rechtsverstöße überprüft. Rechtswidrige Inhalte waren zum Zeitpunkt der Verlinkung nicht erkennbar. Eine permanente inhaltliche Kontrolle der verlinkten Seiten ist jedoch ohne konkrete Anhaltspunkte einer Rechtsverletzung nicht zumutbar. Bei Bekanntwerden von Rechtsverletzungen werden wir derartige Links umgehend entfernen.</p>\n\n<p><strong>Urheberrecht</strong></p>\n\n<p>Die durch die Seitenbetreiber erstellten Inhalte und Werke auf diesen Seiten unterliegen dem deutschen Urheberrecht. Die Vervielfältigung, Bearbeitung, Verbreitung und jede Art der Verwertung außerhalb der Grenzen des Urheberrechtes bedürfen der schriftlichen Zustimmung des jeweiligen Autors bzw. Erstellers. Downloads und Kopien dieser Seite sind nur für den privaten, nicht kommerziellen Gebrauch gestattet. Soweit die Inhalte auf dieser Seite nicht vom Betreiber erstellt wurden, werden die Urheberrechte Dritter beachtet. Insbesondere werden Inhalte Dritter als solche gekennzeichnet. Sollten Sie trotzdem auf eine Urheberrechtsverletzung aufmerksam werden, bitten wir um einen entsprechenden Hinweis. Bei Bekanntwerden von Rechtsverletzungen werden wir derartige Inhalte umgehend entfernen.</p>\n\n<p><strong>Datenschutz</strong></p>\n\n<p>Die Nutzung unserer Webseite ist in der Regel ohne Angabe personenbezogener Daten möglich. Soweit auf unseren Seiten personenbezogene Daten (beispielsweise Name, Anschrift oder eMail-Adressen) erhoben werden, erfolgt dies, soweit möglich, stets auf freiwilliger Basis. Diese Daten werden ohne Ihre ausdrückliche Zustimmung nicht an Dritte weitergegeben.<br />\nWir weisen darauf hin, dass die Datenübertragung im Internet (z.B. bei der Kommunikation per E-Mail) Sicherheitslücken aufweisen kann. Ein lückenloser Schutz der Daten vor dem Zugriff durch Dritte ist nicht möglich.<br />\nDer Nutzung von im Rahmen der Impressumspflicht veröffentlichten Kontaktdaten durch Dritte zur Übersendung von nicht ausdrücklich angeforderter Werbung und Informationsmaterialien wird hiermit ausdrücklich widersprochen. Die Betreiber der Seiten behalten sich ausdrücklich rechtliche Schritte im Falle der unverlangten Zusendung von Werbeinformationen, etwa durch Spam-Mails, vor.</p>');

DROP TABLE IF EXISTS `field_body_2`;
CREATE TABLE `field_body_2` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` mediumtext NOT NULL,
  `data1012` mediumtext,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`),
  FULLTEXT KEY `data1012` (`data1012`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_coverimage`;
CREATE TABLE `field_coverimage` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` varchar(250) NOT NULL,
  `sort` int(10) unsigned NOT NULL,
  `description` text NOT NULL,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `filedata` mediumtext,
  `filesize` int(11) DEFAULT NULL,
  `created_users_id` int(10) unsigned NOT NULL DEFAULT '0',
  `modified_users_id` int(10) unsigned NOT NULL DEFAULT '0',
  `width` int(11) DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  `ratio` decimal(4,2) DEFAULT NULL,
  PRIMARY KEY (`pages_id`,`sort`),
  KEY `data` (`data`),
  KEY `modified` (`modified`),
  KEY `created` (`created`),
  KEY `filesize` (`filesize`),
  KEY `width` (`width`),
  KEY `height` (`height`),
  KEY `ratio` (`ratio`),
  FULLTEXT KEY `description` (`description`),
  FULLTEXT KEY `filedata` (`filedata`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

INSERT INTO `field_coverimage` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`, `filesize`, `created_users_id`, `modified_users_id`, `width`, `height`, `ratio`) VALUES('1050', 'img_0278.jpeg', '0', '[\"\"]', '2020-04-20 11:04:37', '2020-04-20 11:04:37', '', NULL, '0', '0', NULL, NULL, NULL);
INSERT INTO `field_coverimage` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`, `filesize`, `created_users_id`, `modified_users_id`, `width`, `height`, `ratio`) VALUES('1', 'img_8948.jpg', '0', '[\"\"]', '2020-04-20 09:07:31', '2020-04-20 09:07:31', '{\"focus\":{\"top\":13.4,\"left\":52.5,\"zoom\":0}}', NULL, '0', '0', NULL, NULL, NULL);
INSERT INTO `field_coverimage` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`, `filesize`, `created_users_id`, `modified_users_id`, `width`, `height`, `ratio`) VALUES('1021', 'p1340624.jpg', '0', '[\"\"]', '2020-04-22 19:08:57', '2020-04-22 19:08:57', '', '554082', '41', '41', '1500', '1000', '1.50');
INSERT INTO `field_coverimage` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`, `filesize`, `created_users_id`, `modified_users_id`, `width`, `height`, `ratio`) VALUES('1001', 'p1020733.jpg', '0', '[\"\"]', '0000-00-00 00:00:00', '2020-04-22 19:15:44', '', '440466', '41', '41', '1999', '622', '3.21');

DROP TABLE IF EXISTS `field_email`;
CREATE TABLE `field_email` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` varchar(250) NOT NULL DEFAULT '',
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

INSERT INTO `field_email` (`pages_id`, `data`) VALUES('41', 'barkow86@gmail.com');

DROP TABLE IF EXISTS `field_fieldset_teaser`;
CREATE TABLE `field_fieldset_teaser` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` int(11) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `field_fieldset_teaser_end`;
CREATE TABLE `field_fieldset_teaser_end` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` int(11) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `field_fieldset_why`;
CREATE TABLE `field_fieldset_why` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` int(11) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `field_fieldset_why_end`;
CREATE TABLE `field_fieldset_why_end` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` int(11) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `field_headline`;
CREATE TABLE `field_headline` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  `data1012` text,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  KEY `data_exact1012` (`data1012`(250)),
  FULLTEXT KEY `data` (`data`),
  FULLTEXT KEY `data1012` (`data1012`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

INSERT INTO `field_headline` (`pages_id`, `data`, `data1012`) VALUES('1', 'THIS IS JUST THE BEGINNING OF YOUR JOURNEY', 'Kleine Demo-Webseite');
INSERT INTO `field_headline` (`pages_id`, `data`, `data1012`) VALUES('1030', '', '');
INSERT INTO `field_headline` (`pages_id`, `data`, `data1012`) VALUES('1035', 'What we do', '');
INSERT INTO `field_headline` (`pages_id`, `data`, `data1012`) VALUES('1036', 'Who we are', '');
INSERT INTO `field_headline` (`pages_id`, `data`, `data1012`) VALUES('1037', 'Where we are', '');
INSERT INTO `field_headline` (`pages_id`, `data`, `data1012`) VALUES('1038', 'Service', '');
INSERT INTO `field_headline` (`pages_id`, `data`, `data1012`) VALUES('1039', 'Unique Lab', '');
INSERT INTO `field_headline` (`pages_id`, `data`, `data1012`) VALUES('1040', '2', '');
INSERT INTO `field_headline` (`pages_id`, `data`, `data1012`) VALUES('1041', '3', '');
INSERT INTO `field_headline` (`pages_id`, `data`, `data1012`) VALUES('1042', '4', '');
INSERT INTO `field_headline` (`pages_id`, `data`, `data1012`) VALUES('1045', 'Manufactur', '');
INSERT INTO `field_headline` (`pages_id`, `data`, `data1012`) VALUES('1046', 'Team', '');
INSERT INTO `field_headline` (`pages_id`, `data`, `data1012`) VALUES('1047', '', '');
INSERT INTO `field_headline` (`pages_id`, `data`, `data1012`) VALUES('1070', 'Lightweight', '');
INSERT INTO `field_headline` (`pages_id`, `data`, `data1012`) VALUES('1071', 'Bulletproof', '');
INSERT INTO `field_headline` (`pages_id`, `data`, `data1012`) VALUES('1114', 'Wha a Boardschmiede?', '');
INSERT INTO `field_headline` (`pages_id`, `data`, `data1012`) VALUES('1120', 'TOBIAS BURG', '');
INSERT INTO `field_headline` (`pages_id`, `data`, `data1012`) VALUES('1121', 'THILO SCHWEMLEIN', '');

DROP TABLE IF EXISTS `field_image`;
CREATE TABLE `field_image` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` varchar(250) NOT NULL,
  `sort` int(10) unsigned NOT NULL,
  `description` text NOT NULL,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `filedata` mediumtext,
  `filesize` int(11) DEFAULT NULL,
  `created_users_id` int(10) unsigned NOT NULL DEFAULT '0',
  `modified_users_id` int(10) unsigned NOT NULL DEFAULT '0',
  `width` int(11) DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  `ratio` decimal(4,2) DEFAULT NULL,
  PRIMARY KEY (`pages_id`,`sort`),
  KEY `data` (`data`),
  KEY `modified` (`modified`),
  KEY `created` (`created`),
  KEY `filesize` (`filesize`),
  KEY `width` (`width`),
  KEY `height` (`height`),
  KEY `ratio` (`ratio`),
  FULLTEXT KEY `description` (`description`),
  FULLTEXT KEY `filedata` (`filedata`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

INSERT INTO `field_image` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`, `filesize`, `created_users_id`, `modified_users_id`, `width`, `height`, `ratio`) VALUES('1045', 'img_8948.jpg', '0', '[\"\"]', '2020-04-20 10:04:24', '2020-04-20 10:04:24', '', NULL, '0', '0', NULL, NULL, NULL);
INSERT INTO `field_image` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`, `filesize`, `created_users_id`, `modified_users_id`, `width`, `height`, `ratio`) VALUES('1046', 'img_8947_2.jpg', '0', '[\"\"]', '2020-04-20 10:13:30', '2020-04-20 10:13:30', '', NULL, '0', '0', NULL, NULL, NULL);
INSERT INTO `field_image` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`, `filesize`, `created_users_id`, `modified_users_id`, `width`, `height`, `ratio`) VALUES('1070', 'ladys_front.png', '0', '[\"\"]', '0000-00-00 00:00:00', '2020-04-22 19:27:21', '', '39912', '41', '41', '189', '336', '0.56');
INSERT INTO `field_image` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`, `filesize`, `created_users_id`, `modified_users_id`, `width`, `height`, `ratio`) VALUES('1071', 'beaver_2020_v2_front_1.png', '0', '[\"\"]', '0000-00-00 00:00:00', '2020-04-22 19:28:51', '', '35509', '41', '41', '189', '252', '0.75');
INSERT INTO `field_image` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`, `filesize`, `created_users_id`, `modified_users_id`, `width`, `height`, `ratio`) VALUES('1120', '8b9c9043-39fc-4866-9806-0c79fb4db863.jpg', '0', '[\"\"]', '2020-06-06 18:25:26', '2020-06-06 18:25:26', '', '259626', '41', '41', '1080', '1079', '1.00');
INSERT INTO `field_image` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`, `filesize`, `created_users_id`, `modified_users_id`, `width`, `height`, `ratio`) VALUES('1121', '4a1cee7b-a6d4-408c-9e8b-632c38ded7c7.jpg', '0', '[\"\"]', '2020-06-06 18:25:37', '2020-06-06 18:25:37', '', '61899', '41', '41', '1600', '1066', '1.50');

DROP TABLE IF EXISTS `field_image_alignment`;
CREATE TABLE `field_image_alignment` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` tinyint(4) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `field_image_why`;
CREATE TABLE `field_image_why` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` varchar(250) NOT NULL,
  `sort` int(10) unsigned NOT NULL,
  `description` text NOT NULL,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `filedata` mediumtext,
  `filesize` int(11) DEFAULT NULL,
  `created_users_id` int(10) unsigned NOT NULL DEFAULT '0',
  `modified_users_id` int(10) unsigned NOT NULL DEFAULT '0',
  `width` int(11) DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  `ratio` decimal(4,2) DEFAULT NULL,
  PRIMARY KEY (`pages_id`,`sort`),
  KEY `data` (`data`),
  KEY `modified` (`modified`),
  KEY `created` (`created`),
  KEY `filesize` (`filesize`),
  KEY `width` (`width`),
  KEY `height` (`height`),
  KEY `ratio` (`ratio`),
  FULLTEXT KEY `description` (`description`),
  FULLTEXT KEY `filedata` (`filedata`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

INSERT INTO `field_image_why` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`, `filesize`, `created_users_id`, `modified_users_id`, `width`, `height`, `ratio`) VALUES('1', 'beaver_2020_v2_front_1.png', '0', '[\"\"]', '2020-04-22 19:14:08', '2020-04-22 19:14:08', '', '4516205', '41', '41', '3024', '4032', '0.75');

DROP TABLE IF EXISTS `field_images`;
CREATE TABLE `field_images` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` varchar(250) NOT NULL,
  `sort` int(10) unsigned NOT NULL,
  `description` text NOT NULL,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `filedata` mediumtext,
  `filesize` int(11) DEFAULT NULL,
  `created_users_id` int(10) unsigned NOT NULL DEFAULT '0',
  `modified_users_id` int(10) unsigned NOT NULL DEFAULT '0',
  `width` int(11) DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  `ratio` decimal(4,2) DEFAULT NULL,
  PRIMARY KEY (`pages_id`,`sort`),
  KEY `data` (`data`),
  KEY `modified` (`modified`),
  KEY `created` (`created`),
  KEY `filesize` (`filesize`),
  KEY `width` (`width`),
  KEY `height` (`height`),
  KEY `ratio` (`ratio`),
  FULLTEXT KEY `description` (`description`),
  FULLTEXT KEY `filedata` (`filedata`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

INSERT INTO `field_images` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`, `filesize`, `created_users_id`, `modified_users_id`, `width`, `height`, `ratio`) VALUES('1', 'p1020733.jpg', '0', '[\"\"]', '2020-04-24 20:56:42', '2020-04-24 20:56:42', '', '1036566', '41', '41', '2000', '1333', '1.50');
INSERT INTO `field_images` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`, `filesize`, `created_users_id`, `modified_users_id`, `width`, `height`, `ratio`) VALUES('1', 'maxresdefault_2.jpg', '1', '[\"\"]', '2020-04-24 20:56:43', '2020-04-24 20:56:43', '', '178143', '41', '41', '1280', '720', '1.78');
INSERT INTO `field_images` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`, `filesize`, `created_users_id`, `modified_users_id`, `width`, `height`, `ratio`) VALUES('1', 'p1340624-1.jpg', '2', '[\"\"]', '2020-04-24 20:56:44', '2020-04-24 20:56:44', '', '554082', '41', '41', '1500', '1000', '1.50');
INSERT INTO `field_images` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`, `filesize`, `created_users_id`, `modified_users_id`, `width`, `height`, `ratio`) VALUES('1077', '56da4514-ddab-4f1f-8252-f28a60302e2f.jpg', '2', '[\"\"]', '2020-05-27 10:29:01', '2020-05-27 10:29:01', '', '182508', '41', '41', '1198', '1600', '0.75');
INSERT INTO `field_images` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`, `filesize`, `created_users_id`, `modified_users_id`, `width`, `height`, `ratio`) VALUES('1077', '83408820-a384-4e56-8cbb-08ffd5e17893.jpg', '1', '[\"\"]', '2020-05-27 11:01:34', '2020-05-27 11:01:34', '', '109986', '41', '41', '750', '1334', '0.56');
INSERT INTO `field_images` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`, `filesize`, `created_users_id`, `modified_users_id`, `width`, `height`, `ratio`) VALUES('1077', 'twintip-front-standard-450x600-min_1.png', '0', '[\"\"]', '2020-05-27 10:29:03', '2020-05-27 10:29:03', '', '57363', '41', '41', '450', '600', '0.75');

DROP TABLE IF EXISTS `field_language`;
CREATE TABLE `field_language` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` int(11) NOT NULL,
  `sort` int(10) unsigned NOT NULL,
  PRIMARY KEY (`pages_id`,`sort`),
  KEY `data` (`data`,`pages_id`,`sort`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

INSERT INTO `field_language` (`pages_id`, `data`, `sort`) VALUES('40', '1010', '0');
INSERT INTO `field_language` (`pages_id`, `data`, `sort`) VALUES('41', '1010', '0');

DROP TABLE IF EXISTS `field_language_files`;
CREATE TABLE `field_language_files` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` varchar(250) NOT NULL,
  `sort` int(10) unsigned NOT NULL,
  `description` text NOT NULL,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `filesize` int(11) DEFAULT NULL,
  `created_users_id` int(10) unsigned NOT NULL DEFAULT '0',
  `modified_users_id` int(10) unsigned NOT NULL DEFAULT '0',
  `filedata` mediumtext,
  PRIMARY KEY (`pages_id`,`sort`),
  KEY `data` (`data`),
  KEY `modified` (`modified`),
  KEY `created` (`created`),
  KEY `filesize` (`filesize`),
  FULLTEXT KEY `description` (`description`),
  FULLTEXT KEY `filedata` (`filedata`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--templates-admin--debug-inc.json', '117', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--textformatter--textformatterentities-module.json', '116', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--system--systemupdater--systemupdater-module.json', '115', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--session--sessionloginthrottle--sessionloginthrottle-module.json', '114', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--session--sessionhandlerdb--sessionhandlerdb-module.json', '113', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--session--sessionhandlerdb--processsessiondb-module.json', '112', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--process--processuser--processuser-module.json', '111', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--process--processtemplate--processtemplateexportimport-php.json', '110', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--process--processtemplate--processtemplate-module.json', '109', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--process--processprofile--processprofile-module.json', '107', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--process--processrole--processrole-module.json', '108', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--process--processpermission--processpermission-module.json', '106', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--process--processpageview-module.json', '105', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--process--processpagetype--processpagetype-module.json', '104', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--process--processpagetrash-module.json', '103', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--process--processpagesort-module.json', '102', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--process--processpagesearch--processpagesearch-module.json', '101', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--process--processpagelister--processpagelister-module.json', '100', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--process--processpagelist--processpagelist-module.json', '99', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--process--processpageedit--processpageedit-module.json', '96', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--process--processpageeditlink--processpageeditlink-module.json', '98', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--process--processpageeditimageselect--processpageeditimageselect-module.json', '97', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--process--processpageclone-module.json', '95', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--process--processpageadd--processpageadd-module.json', '94', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--process--processmodule--processmoduleinstall-php.json', '93', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--process--processlogin--processlogin-module.json', '91', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--process--processmodule--processmodule-module.json', '92', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--process--processlist-module.json', '90', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--process--processhome-module.json', '89', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--process--processforgotpassword-module.json', '88', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--process--processfield--processfield-module.json', '86', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--process--processfield--processfieldexportimport-php.json', '87', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--pagerender-module.json', '85', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--pagepaths-module.json', '84', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--languagesupport--languagetabs-module.json', '80', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--languagesupport--processlanguage-module.json', '81', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--markup--markuppagefields-module.json', '82', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--markup--markuppagernav--markuppagernav-module.json', '83', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--languagesupport--languagesupportpagenames-module.json', '79', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--languagesupport--languageparser-php.json', '76', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--languagesupport--languagesupport-module.json', '77', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--languagesupport--languagesupportfields-module.json', '78', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--inputfield--inputfieldurl-module.json', '74', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--jquery--jquerywiretabs--jquerywiretabs-module.json', '75', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--inputfield--inputfieldtinymce--inputfieldtinymce-module.json', '73', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--inputfield--inputfieldtextarea-module.json', '72', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--inputfield--inputfieldselectmultiple-module.json', '68', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--inputfield--inputfieldselector--inputfieldselector-module.json', '69', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--inputfield--inputfieldtext-module.json', '71', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--inputfield--inputfieldsubmit--inputfieldsubmit-module.json', '70', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--inputfield--inputfieldselect-module.json', '67', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--inputfield--inputfieldradios--inputfieldradios-module.json', '66', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--inputfield--inputfieldpassword-module.json', '65', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--inputfield--inputfieldpagename--inputfieldpagename-module.json', '61', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--inputfield--inputfieldpagetable--inputfieldpagetable-module.json', '62', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--inputfield--inputfieldpagetable--inputfieldpagetableajax-php.json', '63', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--inputfield--inputfieldpagetitle--inputfieldpagetitle-module.json', '64', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--inputfield--inputfieldpagelistselect--inputfieldpagelistselectmultiple-module.json', '60', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--inputfield--inputfieldpagelistselect--inputfieldpagelistselect-module.json', '59', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--inputfield--inputfieldpageautocomplete--inputfieldpageautocomplete-module.json', '58', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--inputfield--inputfieldpage--inputfieldpage-module.json', '57', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--inputfield--inputfieldname-module.json', '56', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--inputfield--inputfieldmarkup-module.json', '55', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--inputfield--inputfieldinteger-module.json', '54', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--inputfield--inputfieldfile--inputfieldfile-module.json', '49', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--inputfield--inputfieldfloat-module.json', '50', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--inputfield--inputfieldform-module.json', '51', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--inputfield--inputfieldhidden-module.json', '52', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--inputfield--inputfieldimage--inputfieldimage-module.json', '53', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--inputfield--inputfieldfieldset-module.json', '48', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--inputfield--inputfieldemail-module.json', '47', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--inputfield--inputfielddatetime--inputfielddatetime-module.json', '46', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--inputfield--inputfieldckeditor--inputfieldckeditor-module.json', '45', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--inputfield--inputfieldcheckboxes--inputfieldcheckboxes-module.json', '44', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--inputfield--inputfieldbutton-module.json', '42', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--inputfield--inputfieldcheckbox-module.json', '43', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--fieldtype--fieldtypetextarea-module.json', '39', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--fieldtype--fieldtypeurl-module.json', '40', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--inputfield--inputfieldasmselect--inputfieldasmselect-module.json', '41', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--fieldtype--fieldtypetext-module.json', '38', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--fieldtype--fieldtypeselector-module.json', '37', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--fieldtype--fieldtyperepeater--inputfieldrepeater-module.json', '36', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--fieldtype--fieldtyperepeater--fieldtyperepeater-module.json', '35', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--fieldtype--fieldtypepagetable-module.json', '34', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--fieldtype--fieldtypepage-module.json', '33', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--fieldtype--fieldtypemodule-module.json', '32', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--fieldtype--fieldtypefloat-module.json', '31', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--fieldtype--fieldtypefile-module.json', '30', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--fieldtype--fieldtypedatetime-module.json', '29', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--fieldtype--fieldtypecomments--inputfieldcommentsadmin-module.json', '28', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--fieldtype--fieldtypecomments--fieldtypecomments-module.json', '27', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--fieldtype--fieldtypecomments--commentfilterakismet-module.json', '24', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--fieldtype--fieldtypecomments--commentform-php.json', '25', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--fieldtype--fieldtypecomments--commentlist-php.json', '26', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--admintheme--adminthemedefault--adminthemedefault-module.json', '23', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--core--wireupload-php.json', '22', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--core--wiretempdir-php.json', '21', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--core--sessioncsrf-php.json', '18', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--core--wirecache-php.json', '19', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--core--wirehttp-php.json', '20', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--core--session-php.json', '17', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--core--pages-php.json', '13', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--core--password-php.json', '14', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--core--process-php.json', '15', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--core--sanitizer-php.json', '16', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--core--pagefile-php.json', '11', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--core--pageimage-php.json', '12', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--core--modules-php.json', '10', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--core--inputfieldwrapper-php.json', '9', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--core--inputfield-php.json', '8', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--core--functions-php.json', '7', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--core--fieldtypemulti-php.json', '6', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--core--fieldtype-php.json', '5', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--core--fieldselectorinfo-php.json', '4', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--core--fields-php.json', '2', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--core--admintheme-php.json', '3', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--core--fieldgroups-php.json', '1', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--core--field-php.json', '0', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--templates-admin--default-php.json', '118', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);

DROP TABLE IF EXISTS `field_language_files_site`;
CREATE TABLE `field_language_files_site` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` varchar(250) NOT NULL,
  `sort` int(10) unsigned NOT NULL,
  `description` text NOT NULL,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `filesize` int(11) DEFAULT NULL,
  `created_users_id` int(10) unsigned NOT NULL DEFAULT '0',
  `modified_users_id` int(10) unsigned NOT NULL DEFAULT '0',
  `filedata` mediumtext,
  PRIMARY KEY (`pages_id`,`sort`),
  KEY `data` (`data`),
  KEY `modified` (`modified`),
  KEY `created` (`created`),
  KEY `filesize` (`filesize`),
  FULLTEXT KEY `description` (`description`),
  FULLTEXT KEY `filedata` (`filedata`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

INSERT INTO `field_language_files_site` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'site--templates--_main-php.json', '0', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files_site` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'site--templates--search-php.json', '1', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);

DROP TABLE IF EXISTS `field_page_color`;
CREATE TABLE `field_page_color` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` int(10) unsigned NOT NULL,
  `sort` int(10) unsigned NOT NULL,
  PRIMARY KEY (`pages_id`,`sort`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

INSERT INTO `field_page_color` (`pages_id`, `data`, `sort`) VALUES('1021', '2', '0');
INSERT INTO `field_page_color` (`pages_id`, `data`, `sort`) VALUES('1052', '2', '0');
INSERT INTO `field_page_color` (`pages_id`, `data`, `sort`) VALUES('1020', '2', '0');
INSERT INTO `field_page_color` (`pages_id`, `data`, `sort`) VALUES('1077', '2', '0');
INSERT INTO `field_page_color` (`pages_id`, `data`, `sort`) VALUES('1026', '2', '0');

DROP TABLE IF EXISTS `field_pass`;
CREATE TABLE `field_pass` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` char(40) NOT NULL,
  `salt` char(32) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=ascii;

INSERT INTO `field_pass` (`pages_id`, `data`, `salt`) VALUES('41', 'ZgJzxkuuAojqEZIPdf.X9QLCFqm.WIu', '$2y$11$vK8ngaSqtGmb0PMlfIqXbO');
INSERT INTO `field_pass` (`pages_id`, `data`, `salt`) VALUES('40', '', '');

DROP TABLE IF EXISTS `field_permissions`;
CREATE TABLE `field_permissions` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` int(11) NOT NULL,
  `sort` int(10) unsigned NOT NULL,
  PRIMARY KEY (`pages_id`,`sort`),
  KEY `data` (`data`,`pages_id`,`sort`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

INSERT INTO `field_permissions` (`pages_id`, `data`, `sort`) VALUES('38', '32', '1');
INSERT INTO `field_permissions` (`pages_id`, `data`, `sort`) VALUES('38', '34', '2');
INSERT INTO `field_permissions` (`pages_id`, `data`, `sort`) VALUES('38', '35', '3');
INSERT INTO `field_permissions` (`pages_id`, `data`, `sort`) VALUES('37', '36', '0');
INSERT INTO `field_permissions` (`pages_id`, `data`, `sort`) VALUES('38', '36', '0');
INSERT INTO `field_permissions` (`pages_id`, `data`, `sort`) VALUES('38', '50', '4');
INSERT INTO `field_permissions` (`pages_id`, `data`, `sort`) VALUES('38', '51', '5');
INSERT INTO `field_permissions` (`pages_id`, `data`, `sort`) VALUES('38', '52', '7');
INSERT INTO `field_permissions` (`pages_id`, `data`, `sort`) VALUES('38', '53', '8');
INSERT INTO `field_permissions` (`pages_id`, `data`, `sort`) VALUES('38', '54', '6');

DROP TABLE IF EXISTS `field_process`;
CREATE TABLE `field_process` (
  `pages_id` int(11) NOT NULL DEFAULT '0',
  `data` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

INSERT INTO `field_process` (`pages_id`, `data`) VALUES('6', '17');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('3', '12');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('8', '12');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('9', '14');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('10', '7');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('11', '47');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('16', '48');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('300', '104');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('21', '50');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('29', '66');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('23', '10');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('304', '138');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('31', '136');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('22', '76');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('30', '68');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('303', '129');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('2', '87');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('302', '121');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('301', '109');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('28', '76');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('1007', '150');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('1009', '159');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('1011', '160');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('1015', '168');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('1017', '170');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('1048', '174');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('1067', '178');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('1069', '180');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('1073', '184');

DROP TABLE IF EXISTS `field_prod_variant_item`;
CREATE TABLE `field_prod_variant_item` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  `count` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(1)),
  KEY `count` (`count`,`pages_id`),
  KEY `parent_id` (`parent_id`,`pages_id`),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

INSERT INTO `field_prod_variant_item` (`pages_id`, `data`, `count`, `parent_id`) VALUES('1088', '1090,1091,1092', '3', '1089');
INSERT INTO `field_prod_variant_item` (`pages_id`, `data`, `count`, `parent_id`) VALUES('1093', '1095,1096', '2', '1094');
INSERT INTO `field_prod_variant_item` (`pages_id`, `data`, `count`, `parent_id`) VALUES('1097', '1099,1100', '2', '1098');
INSERT INTO `field_prod_variant_item` (`pages_id`, `data`, `count`, `parent_id`) VALUES('1102', '1104,1105', '2', '1103');
INSERT INTO `field_prod_variant_item` (`pages_id`, `data`, `count`, `parent_id`) VALUES('1106', '1108,1109', '2', '1107');

DROP TABLE IF EXISTS `field_prod_variants`;
CREATE TABLE `field_prod_variants` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  `count` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(1)),
  KEY `count` (`count`,`pages_id`),
  KEY `parent_id` (`parent_id`,`pages_id`),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

INSERT INTO `field_prod_variants` (`pages_id`, `data`, `count`, `parent_id`) VALUES('1077', '1088,1093,1097', '3', '1087');
INSERT INTO `field_prod_variants` (`pages_id`, `data`, `count`, `parent_id`) VALUES('1076', '1102,1106', '2', '1101');
INSERT INTO `field_prod_variants` (`pages_id`, `data`, `count`, `parent_id`) VALUES('1110', '', '0', '1111');
INSERT INTO `field_prod_variants` (`pages_id`, `data`, `count`, `parent_id`) VALUES('1112', '', '0', '1113');

DROP TABLE IF EXISTS `field_repeater_why`;
CREATE TABLE `field_repeater_why` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  `count` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(1)),
  KEY `count` (`count`,`pages_id`),
  KEY `parent_id` (`parent_id`,`pages_id`),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

INSERT INTO `field_repeater_why` (`pages_id`, `data`, `count`, `parent_id`) VALUES('1', '1039,1040,1041,1042', '4', '1034');

DROP TABLE IF EXISTS `field_roles`;
CREATE TABLE `field_roles` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` int(11) NOT NULL,
  `sort` int(10) unsigned NOT NULL,
  PRIMARY KEY (`pages_id`,`sort`),
  KEY `data` (`data`,`pages_id`,`sort`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

INSERT INTO `field_roles` (`pages_id`, `data`, `sort`) VALUES('40', '37', '0');
INSERT INTO `field_roles` (`pages_id`, `data`, `sort`) VALUES('41', '37', '0');
INSERT INTO `field_roles` (`pages_id`, `data`, `sort`) VALUES('41', '38', '2');

DROP TABLE IF EXISTS `field_shop_coverimage`;
CREATE TABLE `field_shop_coverimage` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` varchar(250) NOT NULL,
  `sort` int(10) unsigned NOT NULL,
  `description` text NOT NULL,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `filedata` mediumtext,
  `filesize` int(11) DEFAULT NULL,
  `created_users_id` int(10) unsigned NOT NULL DEFAULT '0',
  `modified_users_id` int(10) unsigned NOT NULL DEFAULT '0',
  `width` int(11) DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  `ratio` decimal(4,2) DEFAULT NULL,
  PRIMARY KEY (`pages_id`,`sort`),
  KEY `data` (`data`),
  KEY `modified` (`modified`),
  KEY `created` (`created`),
  KEY `filesize` (`filesize`),
  KEY `width` (`width`),
  KEY `height` (`height`),
  KEY `ratio` (`ratio`),
  FULLTEXT KEY `description` (`description`),
  FULLTEXT KEY `filedata` (`filedata`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_shop_coverimage` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`, `filesize`, `created_users_id`, `modified_users_id`, `width`, `height`, `ratio`) VALUES('1076', '83408820-a384-4e56-8cbb-08ffd5e17893.jpg', '0', '[\"\"]', '2020-05-27 09:01:18', '2020-05-27 09:01:18', '', '109986', '41', '41', '750', '1334', '0.56');
INSERT INTO `field_shop_coverimage` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`, `filesize`, `created_users_id`, `modified_users_id`, `width`, `height`, `ratio`) VALUES('1110', 'img_7463.jpeg', '0', '[\"\"]', '2020-05-27 09:01:30', '2020-05-27 09:01:30', '', '1866190', '41', '41', '4032', '3024', '1.33');
INSERT INTO `field_shop_coverimage` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`, `filesize`, `created_users_id`, `modified_users_id`, `width`, `height`, `ratio`) VALUES('1112', '56da4514-ddab-4f1f-8252-f28a60302e2f.jpg', '0', '[\"\"]', '2020-05-27 09:01:34', '2020-05-27 09:01:34', '', '182508', '41', '41', '1198', '1600', '0.75');

DROP TABLE IF EXISTS `field_simple_textblocks`;
CREATE TABLE `field_simple_textblocks` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  `count` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(1)),
  KEY `count` (`count`,`pages_id`),
  KEY `parent_id` (`parent_id`,`pages_id`),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

INSERT INTO `field_simple_textblocks` (`pages_id`, `data`, `count`, `parent_id`) VALUES('1', '1035,1036,1037,1038,1114', '5', '1032');

DROP TABLE IF EXISTS `field_snipcart_cart_custom_fields`;
CREATE TABLE `field_snipcart_cart_custom_fields` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` mediumtext NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

INSERT INTO `field_snipcart_cart_custom_fields` (`pages_id`, `data`) VALUES('1074', 'data-cart-custom1-name=\"By checking this box, I have read and agree to the <a href=\'https://www.domain.com/terms-and-conditions\' class=\'js-real-link\' target=\'_blank\'>Terms &amp; Conditions</a>\"\r\ndata-cart-custom1-options=\"true|false\"\r\ndata-cart-custom1-required=\"true\"');

DROP TABLE IF EXISTS `field_snipcart_item_categories`;
CREATE TABLE `field_snipcart_item_categories` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` int(11) NOT NULL,
  `sort` int(10) unsigned NOT NULL,
  PRIMARY KEY (`pages_id`,`sort`),
  KEY `data` (`data`,`pages_id`,`sort`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `field_snipcart_item_custom_fields`;
CREATE TABLE `field_snipcart_item_custom_fields` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` mediumtext NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `field_snipcart_item_description`;
CREATE TABLE `field_snipcart_item_description` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` mediumtext NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

INSERT INTO `field_snipcart_item_description` (`pages_id`, `data`) VALUES('1077', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.');
INSERT INTO `field_snipcart_item_description` (`pages_id`, `data`) VALUES('1078', 'Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.');
INSERT INTO `field_snipcart_item_description` (`pages_id`, `data`) VALUES('1079', 'Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.');

DROP TABLE IF EXISTS `field_snipcart_item_height`;
CREATE TABLE `field_snipcart_item_height` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` int(11) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `field_snipcart_item_id`;
CREATE TABLE `field_snipcart_item_id` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

INSERT INTO `field_snipcart_item_id` (`pages_id`, `data`) VALUES('1077', 'FREE-10001');
INSERT INTO `field_snipcart_item_id` (`pages_id`, `data`) VALUES('1078', 'BEER-10002');
INSERT INTO `field_snipcart_item_id` (`pages_id`, `data`) VALUES('1079', 'BEER-10003');
INSERT INTO `field_snipcart_item_id` (`pages_id`, `data`) VALUES('1080', '1080');
INSERT INTO `field_snipcart_item_id` (`pages_id`, `data`) VALUES('1083', '123123');

DROP TABLE IF EXISTS `field_snipcart_item_image`;
CREATE TABLE `field_snipcart_item_image` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` varchar(250) NOT NULL,
  `sort` int(10) unsigned NOT NULL,
  `description` text NOT NULL,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `filedata` mediumtext,
  `filesize` int(11) DEFAULT NULL,
  `created_users_id` int(10) unsigned NOT NULL DEFAULT '0',
  `modified_users_id` int(10) unsigned NOT NULL DEFAULT '0',
  `width` int(11) DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  `ratio` decimal(4,2) DEFAULT NULL,
  PRIMARY KEY (`pages_id`,`sort`),
  KEY `data` (`data`),
  KEY `modified` (`modified`),
  KEY `created` (`created`),
  KEY `filesize` (`filesize`),
  KEY `width` (`width`),
  KEY `height` (`height`),
  KEY `ratio` (`ratio`),
  FULLTEXT KEY `description` (`description`),
  FULLTEXT KEY `filedata` (`filedata`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

INSERT INTO `field_snipcart_item_image` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`, `filesize`, `created_users_id`, `modified_users_id`, `width`, `height`, `ratio`) VALUES('1083', 'beaver_2020_v2_front_1.png', '0', '[\"\"]', '2020-04-24 23:55:16', '2020-04-24 23:55:16', '', '4516205', '41', '41', '3024', '4032', '0.75');
INSERT INTO `field_snipcart_item_image` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`, `filesize`, `created_users_id`, `modified_users_id`, `width`, `height`, `ratio`) VALUES('1078', 'beer2.jpg', '0', '[\"\"]', '2020-04-24 23:04:40', '2020-04-24 23:04:40', '', '499786', '41', '41', '1600', '1200', '1.33');
INSERT INTO `field_snipcart_item_image` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`, `filesize`, `created_users_id`, `modified_users_id`, `width`, `height`, `ratio`) VALUES('1079', 'beer3.jpg', '0', '[\"\"]', '2020-04-24 23:04:40', '2020-04-24 23:04:40', '', '487606', '41', '41', '1600', '1200', '1.33');
INSERT INTO `field_snipcart_item_image` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`, `filesize`, `created_users_id`, `modified_users_id`, `width`, `height`, `ratio`) VALUES('1077', 'ladys_front.png', '0', '[\"\"]', '2020-04-24 23:34:49', '2020-04-24 23:34:49', '', '14450572', '41', '41', '3128', '5568', '0.56');

DROP TABLE IF EXISTS `field_snipcart_item_length`;
CREATE TABLE `field_snipcart_item_length` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` int(11) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `field_snipcart_item_max_quantity`;
CREATE TABLE `field_snipcart_item_max_quantity` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` int(11) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `field_snipcart_item_min_quantity`;
CREATE TABLE `field_snipcart_item_min_quantity` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` int(11) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `field_snipcart_item_payment_interval`;
CREATE TABLE `field_snipcart_item_payment_interval` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` int(10) unsigned NOT NULL,
  `sort` int(10) unsigned NOT NULL,
  PRIMARY KEY (`pages_id`,`sort`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `field_snipcart_item_payment_interval_count`;
CREATE TABLE `field_snipcart_item_payment_interval_count` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` int(11) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `field_snipcart_item_payment_trial`;
CREATE TABLE `field_snipcart_item_payment_trial` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` int(11) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `field_snipcart_item_price_eur`;
CREATE TABLE `field_snipcart_item_price_eur` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

INSERT INTO `field_snipcart_item_price_eur` (`pages_id`, `data`) VALUES('1077', '1099');
INSERT INTO `field_snipcart_item_price_eur` (`pages_id`, `data`) VALUES('1078', '19.90');
INSERT INTO `field_snipcart_item_price_eur` (`pages_id`, `data`) VALUES('1079', '1199');
INSERT INTO `field_snipcart_item_price_eur` (`pages_id`, `data`) VALUES('1083', '1099');
INSERT INTO `field_snipcart_item_price_eur` (`pages_id`, `data`) VALUES('1090', '25');
INSERT INTO `field_snipcart_item_price_eur` (`pages_id`, `data`) VALUES('1091', '25');
INSERT INTO `field_snipcart_item_price_eur` (`pages_id`, `data`) VALUES('1092', '0');
INSERT INTO `field_snipcart_item_price_eur` (`pages_id`, `data`) VALUES('1095', '0');
INSERT INTO `field_snipcart_item_price_eur` (`pages_id`, `data`) VALUES('1096', '250');
INSERT INTO `field_snipcart_item_price_eur` (`pages_id`, `data`) VALUES('1099', '0');
INSERT INTO `field_snipcart_item_price_eur` (`pages_id`, `data`) VALUES('1100', '5');
INSERT INTO `field_snipcart_item_price_eur` (`pages_id`, `data`) VALUES('1104', '10');
INSERT INTO `field_snipcart_item_price_eur` (`pages_id`, `data`) VALUES('1105', '15');
INSERT INTO `field_snipcart_item_price_eur` (`pages_id`, `data`) VALUES('1108', '0');
INSERT INTO `field_snipcart_item_price_eur` (`pages_id`, `data`) VALUES('1109', '25');

DROP TABLE IF EXISTS `field_snipcart_item_quantity`;
CREATE TABLE `field_snipcart_item_quantity` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` int(11) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

INSERT INTO `field_snipcart_item_quantity` (`pages_id`, `data`) VALUES('1077', '1');
INSERT INTO `field_snipcart_item_quantity` (`pages_id`, `data`) VALUES('1083', '1');

DROP TABLE IF EXISTS `field_snipcart_item_quantity_step`;
CREATE TABLE `field_snipcart_item_quantity_step` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` int(11) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

INSERT INTO `field_snipcart_item_quantity_step` (`pages_id`, `data`) VALUES('1077', '1');
INSERT INTO `field_snipcart_item_quantity_step` (`pages_id`, `data`) VALUES('1083', '1');

DROP TABLE IF EXISTS `field_snipcart_item_recurring_shipping`;
CREATE TABLE `field_snipcart_item_recurring_shipping` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` tinyint(4) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `field_snipcart_item_shippable`;
CREATE TABLE `field_snipcart_item_shippable` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` tinyint(4) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

INSERT INTO `field_snipcart_item_shippable` (`pages_id`, `data`) VALUES('1077', '1');
INSERT INTO `field_snipcart_item_shippable` (`pages_id`, `data`) VALUES('1078', '1');
INSERT INTO `field_snipcart_item_shippable` (`pages_id`, `data`) VALUES('1079', '1');
INSERT INTO `field_snipcart_item_shippable` (`pages_id`, `data`) VALUES('1080', '1');

DROP TABLE IF EXISTS `field_snipcart_item_stackable`;
CREATE TABLE `field_snipcart_item_stackable` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` tinyint(4) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

INSERT INTO `field_snipcart_item_stackable` (`pages_id`, `data`) VALUES('1078', '1');
INSERT INTO `field_snipcart_item_stackable` (`pages_id`, `data`) VALUES('1079', '1');
INSERT INTO `field_snipcart_item_stackable` (`pages_id`, `data`) VALUES('1080', '1');

DROP TABLE IF EXISTS `field_snipcart_item_taxable`;
CREATE TABLE `field_snipcart_item_taxable` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` tinyint(4) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

INSERT INTO `field_snipcart_item_taxable` (`pages_id`, `data`) VALUES('1077', '1');
INSERT INTO `field_snipcart_item_taxable` (`pages_id`, `data`) VALUES('1078', '1');
INSERT INTO `field_snipcart_item_taxable` (`pages_id`, `data`) VALUES('1079', '1');
INSERT INTO `field_snipcart_item_taxable` (`pages_id`, `data`) VALUES('1080', '1');

DROP TABLE IF EXISTS `field_snipcart_item_taxes`;
CREATE TABLE `field_snipcart_item_taxes` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

INSERT INTO `field_snipcart_item_taxes` (`pages_id`, `data`) VALUES('1080', '20% VAT');
INSERT INTO `field_snipcart_item_taxes` (`pages_id`, `data`) VALUES('1077', '20% VAT');

DROP TABLE IF EXISTS `field_snipcart_item_weight`;
CREATE TABLE `field_snipcart_item_weight` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` int(11) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `field_snipcart_item_width`;
CREATE TABLE `field_snipcart_item_width` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` int(11) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `field_subheadline`;
CREATE TABLE `field_subheadline` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  `data1012` text,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  KEY `data_exact1012` (`data1012`(250)),
  FULLTEXT KEY `data` (`data`),
  FULLTEXT KEY `data1012` (`data1012`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `field_summary`;
CREATE TABLE `field_summary` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` mediumtext NOT NULL,
  `data1012` mediumtext,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`),
  FULLTEXT KEY `data1012` (`data1012`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

INSERT INTO `field_summary` (`pages_id`, `data`, `data1012`) VALUES('1002', 'Dolore ea valde refero feugait utinam luctus. Probo velit commoveo et, delenit praesent, suscipit zelus, hendrerit zelus illum facilisi, regula. ', '');
INSERT INTO `field_summary` (`pages_id`, `data`, `data1012`) VALUES('1001', 'This is a placeholder page with two child pages to serve as an example. ', 'Dies ist eine Beispiel-Seite mit zwei Unterseiten.');
INSERT INTO `field_summary` (`pages_id`, `data`, `data1012`) VALUES('1005', 'View this template\'s source for a demonstration of how to create a basic site map. ', 'Schauen Sie sich den Quell-Code dieser Musterseite an, um zu sehen, wie man einfache Sitemaps erstellt.');
INSERT INTO `field_summary` (`pages_id`, `data`, `data1012`) VALUES('1004', 'Mos erat reprobo in praesent, mara premo, obruo iustum pecus velit lobortis te sagaciter populus.', '');
INSERT INTO `field_summary` (`pages_id`, `data`, `data1012`) VALUES('1', 'We are a small manufacturer located at the popular kite spot Workum in the Netherlands, who have set ourselves the goal to \"forge\" the ultimate board. Years of honing our craft, combined with a real passion for perfection, ensures every board is made to the highest possible standard.', 'ProcessWire ist ein Open-Source-CMS und Web-Applikations-Framework, das sich ganz den Anforderungen von Designern, Entwicklern und deren Kunden anpaßt.');
INSERT INTO `field_summary` (`pages_id`, `data`, `data1012`) VALUES('1020', '', '');
INSERT INTO `field_summary` (`pages_id`, `data`, `data1012`) VALUES('1021', '', '');
INSERT INTO `field_summary` (`pages_id`, `data`, `data1012`) VALUES('1022', '', '');
INSERT INTO `field_summary` (`pages_id`, `data`, `data1012`) VALUES('1023', '', '');
INSERT INTO `field_summary` (`pages_id`, `data`, `data1012`) VALUES('1024', '', '');
INSERT INTO `field_summary` (`pages_id`, `data`, `data1012`) VALUES('1025', '', '');
INSERT INTO `field_summary` (`pages_id`, `data`, `data1012`) VALUES('1026', '', '');
INSERT INTO `field_summary` (`pages_id`, `data`, `data1012`) VALUES('1050', '', '');
INSERT INTO `field_summary` (`pages_id`, `data`, `data1012`) VALUES('1052', 'Perfect Freestyle Board for riders up to 65kg', '');
INSERT INTO `field_summary` (`pages_id`, `data`, `data1012`) VALUES('1054', '', '');
INSERT INTO `field_summary` (`pages_id`, `data`, `data1012`) VALUES('1058', 'Iam stylish', '');
INSERT INTO `field_summary` (`pages_id`, `data`, `data1012`) VALUES('1060', '', '');
INSERT INTO `field_summary` (`pages_id`, `data`, `data1012`) VALUES('1063', '', '');
INSERT INTO `field_summary` (`pages_id`, `data`, `data1012`) VALUES('1065', '', '');
INSERT INTO `field_summary` (`pages_id`, `data`, `data1012`) VALUES('1081', '', '');
INSERT INTO `field_summary` (`pages_id`, `data`, `data1012`) VALUES('1116', '', '');

DROP TABLE IF EXISTS `field_teaer_image`;
CREATE TABLE `field_teaer_image` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` varchar(250) NOT NULL,
  `sort` int(10) unsigned NOT NULL,
  `description` text NOT NULL,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `filedata` mediumtext,
  `filesize` int(11) DEFAULT NULL,
  `created_users_id` int(10) unsigned NOT NULL DEFAULT '0',
  `modified_users_id` int(10) unsigned NOT NULL DEFAULT '0',
  `width` int(11) DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  `ratio` decimal(4,2) DEFAULT NULL,
  PRIMARY KEY (`pages_id`,`sort`),
  KEY `data` (`data`),
  KEY `modified` (`modified`),
  KEY `created` (`created`),
  KEY `filesize` (`filesize`),
  KEY `width` (`width`),
  KEY `height` (`height`),
  KEY `ratio` (`ratio`),
  FULLTEXT KEY `description` (`description`),
  FULLTEXT KEY `filedata` (`filedata`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

INSERT INTO `field_teaer_image` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`, `filesize`, `created_users_id`, `modified_users_id`, `width`, `height`, `ratio`) VALUES('1001', 'morning-1935072_1920.jpg', '0', '[\"\"]', '2020-04-20 10:19:24', '2020-04-20 10:19:24', '', NULL, '0', '0', NULL, NULL, NULL);
INSERT INTO `field_teaer_image` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`, `filesize`, `created_users_id`, `modified_users_id`, `width`, `height`, `ratio`) VALUES('1', 'img_6565.jpg', '0', '[\"\"]', '0000-00-00 00:00:00', '2020-05-26 15:39:23', '', '266747', '41', '41', '1616', '757', '2.13');

DROP TABLE IF EXISTS `field_teaser_headline`;
CREATE TABLE `field_teaser_headline` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  `data1012` text,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  KEY `data_exact1012` (`data1012`(250)),
  FULLTEXT KEY `data` (`data`),
  FULLTEXT KEY `data1012` (`data1012`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

INSERT INTO `field_teaser_headline` (`pages_id`, `data`, `data1012`) VALUES('1', 'Just a container', '');
INSERT INTO `field_teaser_headline` (`pages_id`, `data`, `data1012`) VALUES('1001', 'TEaser?', '');
INSERT INTO `field_teaser_headline` (`pages_id`, `data`, `data1012`) VALUES('1050', '', '');
INSERT INTO `field_teaser_headline` (`pages_id`, `data`, `data1012`) VALUES('1052', '', '');
INSERT INTO `field_teaser_headline` (`pages_id`, `data`, `data1012`) VALUES('1054', '', '');
INSERT INTO `field_teaser_headline` (`pages_id`, `data`, `data1012`) VALUES('1058', '', '');
INSERT INTO `field_teaser_headline` (`pages_id`, `data`, `data1012`) VALUES('1060', '', '');
INSERT INTO `field_teaser_headline` (`pages_id`, `data`, `data1012`) VALUES('1063', '', '');
INSERT INTO `field_teaser_headline` (`pages_id`, `data`, `data1012`) VALUES('1065', '', '');
INSERT INTO `field_teaser_headline` (`pages_id`, `data`, `data1012`) VALUES('1081', '', '');
INSERT INTO `field_teaser_headline` (`pages_id`, `data`, `data1012`) VALUES('1116', '', '');

DROP TABLE IF EXISTS `field_teaser_link`;
CREATE TABLE `field_teaser_link` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

INSERT INTO `field_teaser_link` (`pages_id`, `data`) VALUES('1', '/boards');

DROP TABLE IF EXISTS `field_teaser_linktext`;
CREATE TABLE `field_teaser_linktext` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  `data1012` text,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  KEY `data_exact1012` (`data1012`(250)),
  FULLTEXT KEY `data` (`data`),
  FULLTEXT KEY `data1012` (`data1012`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

INSERT INTO `field_teaser_linktext` (`pages_id`, `data`, `data1012`) VALUES('1', 'Shop now!', '');
INSERT INTO `field_teaser_linktext` (`pages_id`, `data`, `data1012`) VALUES('1050', '', '');
INSERT INTO `field_teaser_linktext` (`pages_id`, `data`, `data1012`) VALUES('1052', '', '');
INSERT INTO `field_teaser_linktext` (`pages_id`, `data`, `data1012`) VALUES('1054', '', '');
INSERT INTO `field_teaser_linktext` (`pages_id`, `data`, `data1012`) VALUES('1058', '', '');
INSERT INTO `field_teaser_linktext` (`pages_id`, `data`, `data1012`) VALUES('1060', '', '');
INSERT INTO `field_teaser_linktext` (`pages_id`, `data`, `data1012`) VALUES('1063', '', '');
INSERT INTO `field_teaser_linktext` (`pages_id`, `data`, `data1012`) VALUES('1065', '', '');
INSERT INTO `field_teaser_linktext` (`pages_id`, `data`, `data1012`) VALUES('1081', '', '');
INSERT INTO `field_teaser_linktext` (`pages_id`, `data`, `data1012`) VALUES('1116', '', '');

DROP TABLE IF EXISTS `field_teaser_text`;
CREATE TABLE `field_teaser_text` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` mediumtext NOT NULL,
  `data1012` mediumtext,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`),
  FULLTEXT KEY `data1012` (`data1012`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

INSERT INTO `field_teaser_text` (`pages_id`, `data`, `data1012`) VALUES('1', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Atque facere, nobis assumenda neque illum aliquid magnam alias libero similique necessitatibus rerum nesciunt non ipsum est magni rem! Id, necessitatibus nisi.', '');
INSERT INTO `field_teaser_text` (`pages_id`, `data`, `data1012`) VALUES('1001', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem', '');
INSERT INTO `field_teaser_text` (`pages_id`, `data`, `data1012`) VALUES('1050', '', '');
INSERT INTO `field_teaser_text` (`pages_id`, `data`, `data1012`) VALUES('1052', '', '');
INSERT INTO `field_teaser_text` (`pages_id`, `data`, `data1012`) VALUES('1054', '', '');
INSERT INTO `field_teaser_text` (`pages_id`, `data`, `data1012`) VALUES('1058', '', '');
INSERT INTO `field_teaser_text` (`pages_id`, `data`, `data1012`) VALUES('1060', '', '');
INSERT INTO `field_teaser_text` (`pages_id`, `data`, `data1012`) VALUES('1063', '', '');
INSERT INTO `field_teaser_text` (`pages_id`, `data`, `data1012`) VALUES('1065', '', '');
INSERT INTO `field_teaser_text` (`pages_id`, `data`, `data1012`) VALUES('1081', '', '');
INSERT INTO `field_teaser_text` (`pages_id`, `data`, `data1012`) VALUES('1116', '', '');

DROP TABLE IF EXISTS `field_text_fields`;
CREATE TABLE `field_text_fields` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  `count` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(1)),
  KEY `count` (`count`,`pages_id`),
  KEY `parent_id` (`parent_id`,`pages_id`),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

INSERT INTO `field_text_fields` (`pages_id`, `data`, `count`, `parent_id`) VALUES('1001', '1045,1046', '2', '1044');
INSERT INTO `field_text_fields` (`pages_id`, `data`, `count`, `parent_id`) VALUES('1050', '', '0', '1051');
INSERT INTO `field_text_fields` (`pages_id`, `data`, `count`, `parent_id`) VALUES('1052', '', '0', '1053');
INSERT INTO `field_text_fields` (`pages_id`, `data`, `count`, `parent_id`) VALUES('1054', '', '0', '1055');
INSERT INTO `field_text_fields` (`pages_id`, `data`, `count`, `parent_id`) VALUES('1058', '', '0', '1059');
INSERT INTO `field_text_fields` (`pages_id`, `data`, `count`, `parent_id`) VALUES('1060', '', '0', '1061');
INSERT INTO `field_text_fields` (`pages_id`, `data`, `count`, `parent_id`) VALUES('1063', '', '0', '1064');
INSERT INTO `field_text_fields` (`pages_id`, `data`, `count`, `parent_id`) VALUES('1065', '', '0', '1066');
INSERT INTO `field_text_fields` (`pages_id`, `data`, `count`, `parent_id`) VALUES('1021', '1070,1071', '2', '1062');
INSERT INTO `field_text_fields` (`pages_id`, `data`, `count`, `parent_id`) VALUES('1081', '', '0', '1082');
INSERT INTO `field_text_fields` (`pages_id`, `data`, `count`, `parent_id`) VALUES('1116', '', '0', '1117');
INSERT INTO `field_text_fields` (`pages_id`, `data`, `count`, `parent_id`) VALUES('1024', '1120,1121', '2', '1119');

DROP TABLE IF EXISTS `field_textblock`;
CREATE TABLE `field_textblock` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` mediumtext NOT NULL,
  `data1012` mediumtext,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`),
  FULLTEXT KEY `data1012` (`data1012`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

INSERT INTO `field_textblock` (`pages_id`, `data`, `data1012`) VALUES('1030', '', '');
INSERT INTO `field_textblock` (`pages_id`, `data`, `data1012`) VALUES('1035', 'From small series to custom made, we will be happy to shape the kitesurf board of your choice that fits your needs.', '');
INSERT INTO `field_textblock` (`pages_id`, `data`, `data1012`) VALUES('1036', 'We are a small team of craftsmen, with a lot of love for the sport and passion for our work.', '');
INSERT INTO `field_textblock` (`pages_id`, `data`, `data1012`) VALUES('1037', 'We are located together with surfshop Core ‘n More, close to the beautiful kite spot Workum in the Netherlands.', '');
INSERT INTO `field_textblock` (`pages_id`, `data`, `data1012`) VALUES('1038', 'Looking for a custom-made board that fits your needs, than come for expertise and personal advice to our shop, located at surfshop “Core ‘n More” in Workum, the Netherlands.', '');
INSERT INTO `field_textblock` (`pages_id`, `data`, `data1012`) VALUES('1039', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec elementum quis dolor vehicula suscipit', '');
INSERT INTO `field_textblock` (`pages_id`, `data`, `data1012`) VALUES('1040', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec elementum quis dolor vehicula suscipit', '');
INSERT INTO `field_textblock` (`pages_id`, `data`, `data1012`) VALUES('1041', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec elementum quis dolor vehicula suscipit', '');
INSERT INTO `field_textblock` (`pages_id`, `data`, `data1012`) VALUES('1042', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec elementum quis dolor vehicula suscipit', '');
INSERT INTO `field_textblock` (`pages_id`, `data`, `data1012`) VALUES('1045', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem', '');
INSERT INTO `field_textblock` (`pages_id`, `data`, `data1012`) VALUES('1046', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem \n\nLorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem', '');
INSERT INTO `field_textblock` (`pages_id`, `data`, `data1012`) VALUES('1047', '', '');
INSERT INTO `field_textblock` (`pages_id`, `data`, `data1012`) VALUES('1070', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem.', '');
INSERT INTO `field_textblock` (`pages_id`, `data`, `data1012`) VALUES('1071', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem.', '');
INSERT INTO `field_textblock` (`pages_id`, `data`, `data1012`) VALUES('1114', 'Don’t ask, try!\nYou’re welcome to come visit us and see for yourself...\nBecause in the end, we will let our boards do the talking!', '');
INSERT INTO `field_textblock` (`pages_id`, `data`, `data1012`) VALUES('1120', '<p><em>Shaper Boardschmiede, Teamrider</em></p>\n\n<p>Nationalitity: German, living in the Netherlands for the last 6 years.<br />\nBorn: 1990<br />\nLanguages: German, Dutch, English<br />\nSports: Kitesurfing, SUP, Mountainbiking, Crossfit<br />\nFavorite Boards: 135x40 Freeride Twintip, Wave-Skim, Surfboard Beaver<br />\nFavorite Kites: Duotone Rebel 13, Duotone Neo 7m for wave<br />\nKiting Style: I love to switch between Oldschool/Airstyle and skim<br />\nFavorite Trick: Board offs<br />\nShaping Kiteboards since: 2017<br />\nMain Responsibilities: Manufactur Twintip Serie, Dutch customer service</p>', '');
INSERT INTO `field_textblock` (`pages_id`, `data`, `data1012`) VALUES('1121', '<p><em>Owner Boardschmiede, head shaper</em></p>\n\n<p>Nationality: German<br />\nBorn: 1977<br />\nLanguages: German, English and very little Dutch<br />\nSports: Kitesurfing, windsurfing, fitness<br />\nFavorite Boards: I love all our boards<br />\nFavorite Kites: Duotone Rebel,Duotone Neo<br />\nKiting Style: Oldsschool/airstyle, strapless, wave<br />\nFavorite Trick: Rodeo<br />\nShaping since: 2012<br />\nMainjob before: Since 1999 composite specialist</p>', '');

DROP TABLE IF EXISTS `field_title`;
CREATE TABLE `field_title` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  `data1012` text,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  KEY `data_exact1012` (`data1012`(250)),
  FULLTEXT KEY `data` (`data`),
  FULLTEXT KEY `data1012` (`data1012`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('11', 'Templates', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('16', 'Fields', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('22', 'Setup', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('3', 'Pages', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('6', 'Add Page', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('8', 'Tree', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('9', 'Save Sort', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('10', 'Edit', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('21', 'Modules', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('29', 'Users', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('30', 'Roles', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('2', 'Admin', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('7', 'Trash', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('27', '404 Page', '404 Seite');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('302', 'Insert Link', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('23', 'Login', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('304', 'Profile', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('301', 'Empty Trash', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('300', 'Search', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('303', 'Insert Image', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('28', 'Access', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('31', 'Permissions', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('32', 'Edit pages', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('34', 'Delete pages', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('35', 'Move pages (change parent)', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('36', 'View pages', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('50', 'Sort child pages', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('51', 'Change templates on pages', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('52', 'Administer users', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('53', 'User can update profile/password', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('54', 'Lock or unlock a page', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1', 'Home', 'Startseite');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1001', 'About', 'Über');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1002', 'Manufactur', 'Manufaktur');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1000', 'Search', 'Suche');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1004', 'History', 'Geschichte');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1005', 'Site Map', 'Sitemap');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1006', 'Use Page Lister', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1007', 'Find', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1009', 'Languages', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1010', 'EN', 'EN');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1011', 'Language Translator', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1012', 'GER', 'DE');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1015', 'Recent', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1016', 'Can see recently edited pages', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1017', 'Logs', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1018', 'Can view system logs', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1019', 'Can manage system logs', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1020', 'Boards', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1021', 'Boards', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1022', 'Twin-Tips', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1023', 'Skimboards', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1024', 'Team', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1025', 'Blog', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1026', 'Contact', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1027', 'Repeaters', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1028', 'text_fields', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1029', 'en', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1031', 'simple_textblocks', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1032', 'en', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1033', 'repeater_why', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1034', 'en', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1043', 'blog', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1044', 'about', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1048', 'DB Backups', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1049', 'Manage database backups (recommended for superuser only)', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1050', 'Boards', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1051', 'boards', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1052', 'Freestyler 135', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1053', 'freestyler', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1054', 'Waveboards', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1055', 'waveboards', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1056', 'twin-tips', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1057', 'skimboards', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1058', 'Testboard', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1059', 'testboard', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1060', 'Beaver', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1061', 'beaver', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1062', 'boards', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1063', 'Rote Rakete', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1064', 'rote-rakete', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1065', 'Flitzer', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1066', 'flitzer', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1067', 'Croppable Images 3', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1068', 'Crop images with CroppableImage3', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1069', 'Diagnostics', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1072', 'shop', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1073', 'SnipWire', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1074', 'Custom Cart Fields', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1075', 'Use the SnipWire Dashboard sections', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1076', 'Freestyler', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1077', 'Freestyler 135', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1078', 'Festish Wet Warmer', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1079', 'Axolotl Juicer', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1080', 'Freestyler', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1081', 'Shoping', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1082', 'shoping', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1083', 'Freestyler 140', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1085', 'prod_variants', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1086', 'prod_variants', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1087', 'freestyler-135', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1089', '1588051372-1268-1', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1090', 'Red', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1091', 'Blue', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1092', 'White', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1088', 'Rail color', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1093', 'Binding', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1094', '1588052086-4136-1', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1095', 'Boardschmiede Binding', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1096', 'LF Boots', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1097', 'Fins', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1098', '1588052239-1408-1', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1099', 'Freestyle fins', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1100', 'Freeride Fins', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1101', 'freestyler', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1102', 'Globale Varianten', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1103', '1588826993-8588-1', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1104', 'Variante 1', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1105', 'Variante 2', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1106', 'Variants 2', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1107', '1588827044-6278-1', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1108', 'No', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1109', 'Yes', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1110', 'Skimboards', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1111', 'skimboards', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1112', 'Waveboards', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1113', 'waveboards', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1115', 'freestyler-140', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1116', 'Impressum', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1117', 'impressum', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1118', 'contact', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1119', 'team', '');

DROP TABLE IF EXISTS `field_video_url`;
CREATE TABLE `field_video_url` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` mediumtext NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

INSERT INTO `field_video_url` (`pages_id`, `data`) VALUES('1052', '<p>http://www.youtube.com/watch?v=16evq_vySAc</p>');
INSERT INTO `field_video_url` (`pages_id`, `data`) VALUES('1077', '<p>http://www.youtube.com/watch?v=16evq_vySAc</p>');

DROP TABLE IF EXISTS `field_why_headline`;
CREATE TABLE `field_why_headline` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

INSERT INTO `field_why_headline` (`pages_id`, `data`) VALUES('1', 'WHY YOU SHOULD CHOOSE BOARDSCHMIEDE');

DROP TABLE IF EXISTS `field_why_subheadline`;
CREATE TABLE `field_why_subheadline` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

INSERT INTO `field_why_subheadline` (`pages_id`, `data`) VALUES('1', 'Our boards are the best...');

DROP TABLE IF EXISTS `fieldgroups`;
CREATE TABLE `fieldgroups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(250) CHARACTER SET ascii NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=109 DEFAULT CHARSET=utf8mb4;

INSERT INTO `fieldgroups` (`id`, `name`) VALUES('2', 'admin');
INSERT INTO `fieldgroups` (`id`, `name`) VALUES('3', 'user');
INSERT INTO `fieldgroups` (`id`, `name`) VALUES('4', 'role');
INSERT INTO `fieldgroups` (`id`, `name`) VALUES('5', 'permission');
INSERT INTO `fieldgroups` (`id`, `name`) VALUES('1', 'home');
INSERT INTO `fieldgroups` (`id`, `name`) VALUES('88', 'sitemap');
INSERT INTO `fieldgroups` (`id`, `name`) VALUES('83', 'basic-page');
INSERT INTO `fieldgroups` (`id`, `name`) VALUES('80', 'search');
INSERT INTO `fieldgroups` (`id`, `name`) VALUES('97', 'language');
INSERT INTO `fieldgroups` (`id`, `name`) VALUES('98', 'repeater_text_fields');
INSERT INTO `fieldgroups` (`id`, `name`) VALUES('99', 'repeater_simple_textblocks');
INSERT INTO `fieldgroups` (`id`, `name`) VALUES('100', 'repeater_repeater_why');
INSERT INTO `fieldgroups` (`id`, `name`) VALUES('101', 'board-page');
INSERT INTO `fieldgroups` (`id`, `name`) VALUES('102', 'board-overview-page');
INSERT INTO `fieldgroups` (`id`, `name`) VALUES('103', 'snipcart-cart');
INSERT INTO `fieldgroups` (`id`, `name`) VALUES('104', 'snipcart-shop');
INSERT INTO `fieldgroups` (`id`, `name`) VALUES('105', 'snipcart-product');
INSERT INTO `fieldgroups` (`id`, `name`) VALUES('106', 'repeater_prod_variant_item');
INSERT INTO `fieldgroups` (`id`, `name`) VALUES('107', 'repeater_prod_variants');
INSERT INTO `fieldgroups` (`id`, `name`) VALUES('108', 'basic-page-twoes');

DROP TABLE IF EXISTS `fieldgroups_fields`;
CREATE TABLE `fieldgroups_fields` (
  `fieldgroups_id` int(10) unsigned NOT NULL DEFAULT '0',
  `fields_id` int(10) unsigned NOT NULL DEFAULT '0',
  `sort` int(11) unsigned NOT NULL DEFAULT '0',
  `data` text,
  PRIMARY KEY (`fieldgroups_id`,`fields_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('2', '2', '1', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('3', '98', '3', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('4', '5', '0', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('5', '1', '0', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('3', '92', '1', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '118', '14', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('80', '1', '0', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('98', '106', '1', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '109', '7', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('88', '1', '0', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('99', '106', '1', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('88', '79', '1', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('83', '76', '4', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('97', '100', '1', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('3', '4', '2', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('83', '122', '8', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('97', '97', '2', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '78', '3', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('3', '103', '4', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('2', '1', '0', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('83', '121', '11', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('97', '1', '0', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('3', '3', '0', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('98', '105', '2', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('83', '104', '5', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '79', '4', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('83', '116', '6', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '76', '5', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('83', '1', '1', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('99', '78', '0', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '123', '2', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('100', '78', '0', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '108', '6', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '116', '13', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '110', '12', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '114', '8', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '115', '9', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '112', '10', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '111', '11', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '1', '1', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('83', '127', '0', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '122', '15', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('83', '123', '2', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('102', '116', '6', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('101', '127', '0', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('101', '1', '1', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('101', '155', '2', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('101', '79', '3', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('101', '76', '4', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('101', '124', '5', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('101', '125', '6', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('101', '128', '7', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('102', '127', '0', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('102', '1', '1', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('102', '123', '2', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('102', '79', '3', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('83', '120', '10', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('102', '76', '4', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('101', '122', '11', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '127', '0', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('100', '106', '1', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('83', '118', '7', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('102', '118', '7', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('101', '117', '15', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('103', '129', '1', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('104', '154', '2', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('105', '138', '14', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('105', '139', '15', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('105', '140', '16', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('105', '141', '17', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('105', '142', '18', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('105', '143', '19', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('105', '144', '20', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('105', '145', '21', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('105', '146', '22', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('105', '151', '23', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('105', '127', '0', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('105', '1', '1', '{\"columnWidth\":70,\"label\":\"Product Name (Title)\",\"notes\":\"Name of the product to be used in catalogue and cart.\"}');
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('105', '155', '2', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('105', '132', '3', '{\"columnWidth\":30}');
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('105', '131', '4', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('105', '133', '5', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('105', '125', '6', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('106', '131', '1', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('107', '152', '1', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('104', '1', '0', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('104', '153', '1', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('83', '119', '9', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('102', '122', '8', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('102', '119', '9', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('102', '120', '10', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('101', '104', '8', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('101', '116', '9', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('101', '118', '10', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('98', '78', '0', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('103', '1', '0', '{\"collapsed\":4}');
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('83', '79', '3', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('102', '104', '5', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('101', '121', '14', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('101', '120', '13', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('101', '119', '12', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('107', '1', '0', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('106', '1', '0', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('105', '153', '7', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('105', '44', '8', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('105', '128', '9', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('105', '134', '10', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('105', '135', '11', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('105', '136', '12', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('105', '137', '13', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('108', '127', '0', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('108', '1', '1', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('108', '116', '7', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('108', '104', '6', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('108', '156', '5', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('108', '123', '2', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('108', '76', '4', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('108', '79', '3', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('83', '117', '12', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('83', '44', '13', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('108', '118', '8', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('108', '122', '9', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('108', '119', '10', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('108', '120', '11', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('108', '121', '12', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('108', '117', '13', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('108', '44', '14', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('102', '121', '11', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('102', '117', '12', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('102', '44', '13', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('101', '44', '16', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '119', '16', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '120', '17', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '121', '18', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '117', '19', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '44', '20', NULL);

DROP TABLE IF EXISTS `fields`;
CREATE TABLE `fields` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(128) CHARACTER SET ascii NOT NULL,
  `name` varchar(250) CHARACTER SET ascii NOT NULL,
  `flags` int(11) NOT NULL DEFAULT '0',
  `label` varchar(250) NOT NULL DEFAULT '',
  `data` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `type` (`type`)
) ENGINE=MyISAM AUTO_INCREMENT=157 DEFAULT CHARSET=utf8mb4;

INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('1', 'FieldtypePageTitleLanguage', 'title', '13', 'Title', '{\"required\":1,\"textformatters\":[\"TextformatterEntities\"],\"size\":0,\"maxlength\":255,\"label1012\":\"Titel\",\"langBlankInherit\":0}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('2', 'FieldtypeModule', 'process', '25', 'Process', '{\"description\":\"The process that is executed on this page. Since this is mostly used by ProcessWire internally, it is recommended that you don\'t change the value of this unless adding your own pages in the admin.\",\"collapsed\":1,\"required\":1,\"moduleTypes\":[\"Process\"],\"permanent\":1}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('3', 'FieldtypePassword', 'pass', '24', 'Set Password', '{\"collapsed\":1,\"size\":50,\"maxlength\":128}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('5', 'FieldtypePage', 'permissions', '24', 'Permissions', '{\"derefAsPage\":0,\"parent_id\":31,\"labelFieldName\":\"title\",\"inputfield\":\"InputfieldCheckboxes\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('4', 'FieldtypePage', 'roles', '24', 'Roles', '{\"derefAsPage\":0,\"parent_id\":30,\"labelFieldName\":\"name\",\"inputfield\":\"InputfieldCheckboxes\",\"description\":\"User will inherit the permissions assigned to each role. You may assign multiple roles to a user. When accessing a page, the user will only inherit permissions from the roles that are also assigned to the page\'s template.\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('92', 'FieldtypeEmail', 'email', '9', 'E-Mail Address', '{\"size\":70,\"maxlength\":255}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('105', 'FieldtypeImage', 'image', '0', '', '{\"label1012\":\"Bild\",\"extensions\":\"gif jpg jpeg png\",\"maxFiles\":1,\"outputFormat\":0,\"defaultValuePage\":0,\"useTags\":0,\"inputfieldClass\":\"InputfieldImage\",\"descriptionRows\":1,\"gridMode\":\"grid\",\"focusMode\":\"on\",\"resizeServer\":0,\"clientQuality\":90,\"maxReject\":0,\"dimensionsByAspectRatio\":0,\"columnWidth\":75,\"fileSchema\":270}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('106', 'FieldtypeTextareaLanguage', 'textblock', '0', '', '{\"label1012\":\"Textblock\",\"inputfieldClass\":\"InputfieldCKEditor\",\"contentType\":1,\"langBlankInherit\":0,\"collapsed\":0,\"minlength\":0,\"maxlength\":0,\"showCount\":0,\"rows\":5,\"htmlOptions\":[2,4,8,16]}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('44', 'FieldtypeImage', 'images', '0', 'Images', '{\"extensions\":\"gif jpg jpeg png\",\"adminThumbs\":1,\"inputfieldClass\":\"InputfieldImage\",\"maxFiles\":0,\"descriptionRows\":1,\"fileSchema\":270,\"outputFormat\":1,\"defaultValuePage\":0,\"defaultGrid\":0,\"icon\":\"camera\",\"label1012\":\"Bilder\",\"textformatters\":[\"TextformatterEntities\"]}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('79', 'FieldtypeTextareaLanguage', 'summary', '1', 'Summary', '{\"textformatters\":[\"TextformatterEntities\"],\"inputfieldClass\":\"InputfieldTextarea\",\"collapsed\":2,\"rows\":3,\"contentType\":0,\"label1012\":\"Zusammenfassung\",\"langBlankInherit\":0}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('76', 'FieldtypeTextareaLanguage', 'body', '0', 'Body', '{\"inputfieldClass\":\"InputfieldCKEditor\",\"rows\":10,\"contentType\":1,\"toolbar\":\"Format, Bold, Italic, -, RemoveFormat\\nJustifyLeft, JustifyCenter, JustifyRight, JustifyBlock\\nNumberedList, BulletedList, -, Blockquote\\nPWLink, Unlink, Anchor\\nPWImage, Table, HorizontalRule, SpecialChar\\nPasteText, PasteFromWord\\nScayt, -, Sourcedialog\",\"inlineMode\":0,\"useACF\":0,\"usePurifier\":1,\"formatTags\":\"p;h1;h2;h3;h4;h5;h6;pre;address\",\"extraPlugins\":[\"pwimage\",\"pwlink\",\"sourcedialog\"],\"removePlugins\":\"image,magicline\",\"toggles\":[2,4,8],\"label1012\":\"Inhalt\",\"langBlankInherit\":0,\"minlength\":0,\"maxlength\":0,\"showCount\":0}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('78', 'FieldtypeTextLanguage', 'headline', '0', 'Headline', '{\"description\":\"Use this instead of the field above if more text is needed for the page than for the navigation. \",\"textformatters\":[\"TextformatterEntities\"],\"collapsed\":2,\"size\":0,\"maxlength\":1024,\"langBlankInherit\":1,\"label1012\":\"\\u00dcberschrift\",\"description1012\":\"Verwenden Sie diese statt dem obigen Feld, wenn mehr Text f\\u00fcr die Seite als f\\u00fcr die Navigation ben\\u00f6tigt wird.j\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('97', 'FieldtypeFile', 'language_files', '24', 'Core Translation Files', '{\"extensions\":\"json csv\",\"maxFiles\":0,\"inputfieldClass\":\"InputfieldFile\",\"unzip\":1,\"descriptionRows\":0,\"fileSchema\":14,\"outputFormat\":0,\"defaultValuePage\":0,\"clone_field\":1,\"description\":\"Use this for field for [language packs](http:\\/\\/modules.processwire.com\\/categories\\/language-pack\\/). To delete all files, double-click the trash can for any file, then save.\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('98', 'FieldtypePage', 'language', '24', 'Language', '{\"derefAsPage\":1,\"parent_id\":1009,\"labelFieldName\":\"title\",\"inputfield\":\"InputfieldRadios\",\"required\":1}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('100', 'FieldtypeFile', 'language_files_site', '24', 'Site Translation Files', '{\"description\":\"Use this for field for translations specific to your site (like files in \\/site\\/templates\\/ for example).\",\"extensions\":\"json csv\",\"maxFiles\":0,\"inputfieldClass\":\"InputfieldFile\",\"unzip\":1,\"descriptionRows\":0,\"fileSchema\":14}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('103', 'FieldtypeModule', 'admin_theme', '8', 'Admin Theme', '{\"moduleTypes\":[\"AdminTheme\"],\"labelField\":\"title\",\"inputfieldClass\":\"InputfieldRadios\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('104', 'FieldtypeRepeater', 'text_fields', '0', 'Textblocks', '{\"label1012\":\"Textbl\\u00f6cke\",\"template_id\":44,\"parent_id\":1028,\"repeaterFields\":[78,106,105],\"repeaterTitle\":\"#n: {headline}\",\"repeaterAddLabel1012\":\"Neuer Textblock\",\"repeaterCollapse\":1,\"repeaterLoading\":1,\"collapsed\":0,\"repeaterAddLabel\":\"New textblock\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('107', 'FieldtypeCheckbox', 'image_alignment', '0', '', '{\"label1012\":\"Bildausrichtung\",\"collapsed\":0,\"columnWidth\":25}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('108', 'FieldtypeRepeater', 'simple_textblocks', '0', 'Simple textblocks', '{\"label1012\":\"Einfache Textbl\\u00f6cke\",\"template_id\":45,\"parent_id\":1031,\"repeaterFields\":[78,106],\"repeaterTitle\":\"#n: {headline}\",\"repeaterCollapse\":1,\"repeaterLoading\":1,\"collapsed\":0}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('109', 'FieldtypeFieldsetOpen', 'fieldset_why', '0', 'Why block', '{\"label1012\":\"Warum Block\",\"closeFieldID\":110,\"collapsed\":1}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('110', 'FieldtypeFieldsetClose', 'fieldset_why_END', '0', 'Close an open fieldset', '{\"description\":\"This field was added automatically to accompany fieldset \'fieldset_why\'.  It should be placed in your template\\/fieldgroup wherever you want the fieldset to end.\",\"openFieldID\":109}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('111', 'FieldtypeRepeater', 'repeater_why', '0', 'Why blocks', '{\"label1012\":\"Warum Bl\\u00f6cke\",\"template_id\":46,\"parent_id\":1033,\"repeaterFields\":[78,106],\"repeaterCollapse\":1,\"repeaterLoading\":1,\"collapsed\":0}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('112', 'FieldtypeImage', 'image_why', '0', 'Why image', '{\"label1012\":\"Warum Bild\",\"extensions\":\"gif jpg jpeg png\",\"maxFiles\":1,\"outputFormat\":0,\"defaultValuePage\":0,\"useTags\":0,\"inputfieldClass\":\"InputfieldImage\",\"descriptionRows\":1,\"gridMode\":\"grid\",\"focusMode\":\"on\",\"resizeServer\":0,\"clientQuality\":90,\"maxReject\":0,\"dimensionsByAspectRatio\":0,\"fileSchema\":270}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('113', 'FieldtypeTextLanguage', 'subheadline', '0', 'Subheadline', '{\"label1012\":\"Zweite \\u00dcberschrift\",\"minlength\":0,\"maxlength\":2048,\"showCount\":0,\"size\":0}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('114', 'FieldtypeText', 'why_headline', '0', 'Why headline', '{\"label1012\":\"Warum \\u00dcberschrift\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('115', 'FieldtypeText', 'why_subheadline', '0', 'Why subheadline', '{\"label1012\":\"Warum zweite \\u00dcberschrift\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('116', 'FieldtypeFieldsetOpen', 'fieldset_teaser', '0', 'Teaser', '{\"label1012\":\"Teaser\",\"closeFieldID\":117,\"collapsed\":1}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('117', 'FieldtypeFieldsetClose', 'fieldset_teaser_END', '0', 'Close an open fieldset', '{\"description\":\"This field was added automatically to accompany fieldset \'fieldset_teaser\'.  It should be placed in your template\\/fieldgroup wherever you want the fieldset to end.\",\"openFieldID\":116}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('118', 'FieldtypeImage', 'teaer_image', '0', 'Teaserimage', '{\"label1012\":\"Teaserbild\",\"extensions\":\"gif jpg jpeg png\",\"maxFiles\":1,\"outputFormat\":0,\"defaultValuePage\":0,\"useTags\":0,\"inputfieldClass\":\"InputfieldImage\",\"descriptionRows\":1,\"gridMode\":\"grid\",\"focusMode\":\"on\",\"resizeServer\":0,\"clientQuality\":90,\"maxReject\":0,\"dimensionsByAspectRatio\":0,\"fileSchema\":270}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('119', 'FieldtypeTextareaLanguage', 'teaser_text', '0', 'Teasertext', '{\"label1012\":\"Teasertext\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('120', 'FieldtypeURL', 'teaser_link', '0', 'Teaserlink', '{\"label1012\":\"Teaserlink\",\"noRelative\":0,\"allowIDN\":0,\"allowQuotes\":0,\"addRoot\":0,\"collapsed\":0,\"minlength\":0,\"maxlength\":1024,\"showCount\":0,\"size\":0}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('121', 'FieldtypeTextLanguage', 'teaser_linktext', '0', 'Teaserlink label', '{\"label1012\":\"Teaserlink Label\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('122', 'FieldtypeTextLanguage', 'teaser_headline', '0', 'Teaserheadline', '{\"label1012\":\"Teaser\\u00fcberschrift\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('123', 'FieldtypeImage', 'coverimage', '0', 'Coverimage', '{\"label1012\":\"Coverbild\",\"extensions\":\"gif jpg jpeg png\",\"maxFiles\":1,\"outputFormat\":0,\"defaultValuePage\":0,\"useTags\":0,\"inputfieldClass\":\"InputfieldImage\",\"descriptionRows\":1,\"gridMode\":\"grid\",\"focusMode\":\"on\",\"resizeServer\":0,\"clientQuality\":90,\"maxReject\":0,\"dimensionsByAspectRatio\":0,\"fileSchema\":270}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('124', 'FieldtypeImage', 'board_images', '0', 'Board Images', '{\"label1012\":\"Bilder\",\"extensions\":\"gif jpg jpeg png\",\"maxFiles\":0,\"outputFormat\":0,\"defaultValuePage\":0,\"useTags\":0,\"inputfieldClass\":\"InputfieldCroppableImage3\",\"descriptionRows\":1,\"gridMode\":\"grid\",\"focusMode\":\"on\",\"resizeServer\":0,\"clientQuality\":90,\"maxReject\":0,\"dimensionsByAspectRatio\":0,\"fileSchema\":270}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('125', 'FieldtypeTextareaLanguage', 'board_features', '0', 'Features', '{\"label1012\":\"Features\",\"inputfieldClass\":\"InputfieldCKEditor\",\"contentType\":1,\"langBlankInherit\":0,\"collapsed\":0,\"minlength\":0,\"maxlength\":0,\"showCount\":0,\"rows\":5}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('128', 'FieldtypeTextarea', 'video_url', '0', 'Video (Youtube / Vimeo)', '{\"textformatters\":[\"TextformatterVideoEmbed\"],\"minlength\":0,\"maxlength\":2048,\"showCount\":0,\"size\":0,\"inputfieldClass\":\"InputfieldCKEditor\",\"contentType\":0,\"collapsed\":0,\"rows\":5}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('127', 'FieldtypeOptions', 'page_color', '0', 'Page Color', '{\"inputfieldClass\":\"InputfieldSelect\",\"collapsed\":0}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('129', 'FieldtypeTextarea', 'snipcart_cart_custom_fields', '0', 'Custom Cart Fields Setup', '{\"description\":\"You can add custom fields to the checkout process. Whenever you define custom cart fields, a new tab\\/step called `Order infos` will be inserted before the `Billing address` during the checkout process.\",\"notes\":\"For detailed infos about custom cart fields setup, please visit [Snipcart v2.0 Custom Fields](https:\\/\\/docs.snipcart.com\\/v2\\/configuration\\/custom-fields).\",\"icon\":\"code\",\"rows\":12,\"tags\":\"Snipcart\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('130', 'FieldtypeTextarea', 'snipcart_item_custom_fields', '0', 'Custom Product Fields Setup', '{\"description\":\"You can add custom fields to this product. Whenever you define custom fields, a new input element will be added to each of these products in cart.\",\"notes\":\"For detailed infos about custom fields setup, please visit [Snipcart v2.0 Custom Fields](https:\\/\\/docs.snipcart.com\\/v2\\/configuration\\/custom-fields).\",\"icon\":\"code\",\"collapsed\":1,\"rows\":12,\"tags\":\"Snipcart\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('131', 'FieldtypeText', 'snipcart_item_price_eur', '0', 'Product Price (Euro)', '{\"notes\":\"Decimal with a dot (.) as separator e.g. 19.99\",\"maxlength\":20,\"required\":true,\"pattern\":\"[-+]?[0-9]*[.]?[0-9]+\",\"tags\":\"Snipcart\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('132', 'FieldtypeText', 'snipcart_item_id', '0', 'SKU', '{\"notes\":\"Individual ID for your product e.g. 1377 or NIKE_PEG-SW-43\",\"maxlength\":100,\"required\":true,\"pattern\":\"^[\\\\w\\\\-_*+.,]+$\",\"tags\":\"Snipcart\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('133', 'FieldtypeTextarea', 'snipcart_item_description', '0', 'Product Description', '{\"description\":\"The product description that your customers will see on product pages in cart and during checkout.\",\"notes\":\"Provide a short description of your product without HTML tags.\",\"maxlength\":300,\"rows\":3,\"showCount\":1,\"stripTags\":1,\"textformatters\":[\"TextformatterEntities\"],\"tags\":\"Snipcart\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('134', 'FieldtypeImage', 'snipcart_item_image', '0', 'Product Image(s)', '{\"description\":\"The product image(s) your customers will see on product pages in cart and during checkout.\",\"notes\":\"The image on first position will be used as the Snipcart thumbnail image. Only this image will be used in cart and during checkout\",\"extensions\":\"gif jpg jpeg png\",\"tags\":\"Snipcart\",\"fileSchema\":270,\"maxFiles\":0,\"outputFormat\":0,\"defaultValuePage\":0,\"useTags\":0,\"inputfieldClass\":\"InputfieldImage\",\"collapsed\":0,\"columnWidth\":50,\"required\":1,\"descriptionRows\":1,\"gridMode\":\"grid\",\"focusMode\":\"on\",\"resizeServer\":0,\"clientQuality\":90,\"maxReject\":0,\"dimensionsByAspectRatio\":0}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('135', 'FieldtypePage', 'snipcart_item_categories', '0', 'Categories', '{\"description\":\"The categories for this product.\",\"inputfield\":\"InputfieldAsmSelect\",\"labelFieldName\":\"title\",\"usePageEdit\":1,\"addable\":1,\"derefAsPage\":0,\"tags\":\"Snipcart\",\"parent_id\":0,\"collapsed\":0,\"columnWidth\":50}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('136', 'FieldtypeInteger', 'snipcart_item_weight', '0', 'Product Weight', '{\"description\":\"Set the weight for this product.\",\"notes\":\"Uses grams as weight unit.\",\"min\":1,\"inputType\":\"number\",\"tags\":\"Snipcart\",\"zeroNotEmpty\":0,\"collapsed\":0,\"columnWidth\":25,\"size\":10}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('137', 'FieldtypeInteger', 'snipcart_item_width', '0', 'Product Width', '{\"description\":\"Set the width for this product.\",\"notes\":\"Uses centimeters as unit.\",\"min\":1,\"inputType\":\"number\",\"tags\":\"Snipcart\",\"zeroNotEmpty\":0,\"collapsed\":0,\"columnWidth\":25,\"size\":10}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('138', 'FieldtypeInteger', 'snipcart_item_length', '0', 'Product Length', '{\"description\":\"Set the length for this product.\",\"notes\":\"Uses centimeters as unit.\",\"min\":1,\"inputType\":\"number\",\"tags\":\"Snipcart\",\"zeroNotEmpty\":0,\"collapsed\":0,\"columnWidth\":25,\"size\":10}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('139', 'FieldtypeInteger', 'snipcart_item_height', '0', 'Product Height', '{\"description\":\"Set the height for this product.\",\"notes\":\"Uses centimeters as unit.\",\"min\":1,\"inputType\":\"number\",\"tags\":\"Snipcart\",\"zeroNotEmpty\":0,\"collapsed\":0,\"columnWidth\":25,\"size\":10}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('140', 'FieldtypeInteger', 'snipcart_item_quantity', '0', 'Default Quantity', '{\"description\":\"The default quantity for the product that will be added to cart.\",\"notes\":\"Integer number (min value = 1).\",\"defaultValue\":1,\"min\":1,\"inputType\":\"number\",\"tags\":\"Snipcart\",\"zeroNotEmpty\":0,\"collapsed\":0,\"columnWidth\":25,\"size\":10}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('141', 'FieldtypeInteger', 'snipcart_item_max_quantity', '0', 'Maximum Quantity', '{\"description\":\"Set the maximum allowed quantity for this product.\",\"notes\":\"Leave empty for no limit.\",\"min\":1,\"inputType\":\"number\",\"tags\":\"Snipcart\",\"zeroNotEmpty\":0,\"collapsed\":0,\"columnWidth\":25,\"size\":10}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('142', 'FieldtypeInteger', 'snipcart_item_min_quantity', '0', 'Minimum Quantity', '{\"description\":\"Set the minimum allowed quantity for this product.\",\"notes\":\"Leave empty for no limit.\",\"min\":1,\"inputType\":\"number\",\"tags\":\"Snipcart\",\"zeroNotEmpty\":0,\"collapsed\":0,\"columnWidth\":25,\"size\":10}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('143', 'FieldtypeInteger', 'snipcart_item_quantity_step', '0', 'Quantity Step', '{\"description\":\"The quantity of a product will increment by this value.\",\"notes\":\"Integer number (min value = 1).\",\"defaultValue\":1,\"min\":1,\"inputType\":\"number\",\"tags\":\"Snipcart\",\"zeroNotEmpty\":0,\"collapsed\":0,\"columnWidth\":25,\"size\":10}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('144', 'FieldtypeCheckbox', 'snipcart_item_stackable', '0', 'Stackable', '{\"label2\":\"Product is stackable\",\"description\":\"Uncheck, if this product should be added to cart in distinct items instead of increasing quantity.\",\"tags\":\"Snipcart\",\"collapsed\":0,\"columnWidth\":25}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('145', 'FieldtypeCheckbox', 'snipcart_item_taxable', '0', 'Taxable', '{\"label2\":\"Product is taxable\",\"description\":\"Uncheck, if this product should be excluded from taxes calculation.\",\"tags\":\"Snipcart\",\"collapsed\":0,\"columnWidth\":25}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('146', 'FieldtypeSnipWireTaxSelector', 'snipcart_item_taxes', '0', 'VAT', '{\"description\":\"Select the tax which should be applied.\",\"tags\":\"Snipcart\",\"taxesType\":1,\"inputfieldClass\":\"InputfieldSelect\",\"collapsed\":0,\"columnWidth\":25}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('147', 'FieldtypeOptions', 'snipcart_item_payment_interval', '0', 'Payment Interval', '{\"description\":\"Choose an interval for recurring payment.\",\"inputfield\":\"InputfieldSelect\",\"required\":true,\"tags\":\"Snipcart\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('148', 'FieldtypeInteger', 'snipcart_item_payment_interval_count', '0', 'Interval Count', '{\"description\":\"Changes the payment interval count.\",\"notes\":\"Integer number (min value = 1).\",\"defaultValue\":1,\"min\":1,\"inputType\":\"number\",\"required\":true,\"tags\":\"Snipcart\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('149', 'FieldtypeInteger', 'snipcart_item_payment_trial', '0', 'Trial Period', '{\"description\":\"Trial period for customers in days. Empty for no trial!\",\"notes\":\"Integer number (min value = 1).\",\"min\":1,\"inputType\":\"number\",\"tags\":\"Snipcart\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('150', 'FieldtypeCheckbox', 'snipcart_item_recurring_shipping', '0', 'Recurring Shipping', '{\"label2\":\"Charge shipping only on initial order\",\"description\":\"Uncheck to add shipping costs to every upcoming recurring payment\",\"tags\":\"Snipcart\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('151', 'FieldtypeCheckbox', 'snipcart_item_shippable', '0', 'Shippable', '{\"label2\":\"Product is shippable\",\"description\":\"Uncheck, if this product should be flagged as not shippable.\",\"tags\":\"Snipcart\",\"collapsed\":0,\"columnWidth\":25}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('152', 'FieldtypeRepeater', 'prod_variant_item', '0', 'Variant item', '{\"template_id\":52,\"parent_id\":1085,\"repeaterFields\":[1,131],\"repeaterCollapse\":0,\"repeaterLoading\":1,\"collapsed\":0}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('153', 'FieldtypeRepeater', 'prod_variants', '0', 'Variants', '{\"template_id\":53,\"parent_id\":1086,\"repeaterFields\":[1,152],\"repeaterCollapse\":0,\"repeaterLoading\":1,\"collapsed\":0}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('154', 'FieldtypeImage', 'shop_coverimage', '0', 'Shop Image for tile', '{\"fileSchema\":270,\"extensions\":\"gif jpg jpeg png\",\"maxFiles\":1,\"outputFormat\":0,\"defaultValuePage\":0,\"useTags\":0,\"inputfieldClass\":\"InputfieldImage\",\"descriptionRows\":1,\"gridMode\":\"grid\",\"focusMode\":\"on\",\"resizeServer\":0,\"clientQuality\":90,\"maxReject\":0,\"dimensionsByAspectRatio\":0}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('155', 'FieldtypeTextLanguage', 'board_style', '0', 'Board Styles (Freeride / Freestyle)', '{\"langBlankInherit\":0,\"collapsed\":0,\"minlength\":0,\"maxlength\":2048,\"showCount\":0,\"size\":0}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('156', 'FieldtypeTextareaLanguage', 'body_2', '0', 'Body right side', '{\"inputfieldClass\":\"InputfieldCKEditor\",\"contentType\":1,\"langBlankInherit\":0,\"collapsed\":0,\"minlength\":0,\"maxlength\":0,\"showCount\":0,\"rows\":5,\"htmlOptions\":[2,4,8,16],\"toolbar\":\"Format, Styles, -, Bold, Italic, -, RemoveFormat\\nNumberedList, BulletedList, -, Blockquote\\nPWLink, Unlink, Anchor\\nPWImage, Table, HorizontalRule, SpecialChar\\nPasteText, PasteFromWord\\nScayt, -, Sourcedialog\",\"inlineMode\":0,\"useACF\":1,\"usePurifier\":1,\"formatTags\":\"p;h1;h2;h3;h4;h5;h6;pre;address\",\"extraPlugins\":[\"pwimage\",\"pwlink\",\"sourcedialog\"],\"removePlugins\":\"image,magicline\"}');

DROP TABLE IF EXISTS `fieldtype_options`;
CREATE TABLE `fieldtype_options` (
  `fields_id` int(10) unsigned NOT NULL,
  `option_id` int(10) unsigned NOT NULL,
  `title` text,
  `value` varchar(230) DEFAULT NULL,
  `sort` int(10) unsigned NOT NULL,
  `title1012` text,
  `value1012` varchar(230) DEFAULT NULL,
  `title1013` text,
  `value1013` varchar(230) DEFAULT NULL,
  PRIMARY KEY (`fields_id`,`option_id`),
  UNIQUE KEY `title` (`title`(230),`fields_id`),
  UNIQUE KEY `title1012` (`title1012`(230),`fields_id`),
  UNIQUE KEY `title1013` (`title1013`(230),`fields_id`),
  KEY `value` (`value`,`fields_id`),
  KEY `sort` (`sort`,`fields_id`),
  KEY `value1012` (`value1012`,`fields_id`),
  KEY `value1013` (`value1013`,`fields_id`),
  FULLTEXT KEY `title_value` (`title`,`value`),
  FULLTEXT KEY `title1012_value1012` (`title1012`,`value1012`),
  FULLTEXT KEY `title1013_value1013` (`title1013`,`value1013`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

INSERT INTO `fieldtype_options` (`fields_id`, `option_id`, `title`, `value`, `sort`, `title1012`, `value1012`, `title1013`, `value1013`) VALUES('127', '2', 'light', '', '1', NULL, NULL, NULL, NULL);
INSERT INTO `fieldtype_options` (`fields_id`, `option_id`, `title`, `value`, `sort`, `title1012`, `value1012`, `title1013`, `value1013`) VALUES('127', '1', 'dark', '', '0', NULL, NULL, NULL, NULL);
INSERT INTO `fieldtype_options` (`fields_id`, `option_id`, `title`, `value`, `sort`, `title1012`, `value1012`, `title1013`, `value1013`) VALUES('147', '1', 'Day', 'Day', '0', NULL, NULL, NULL, NULL);
INSERT INTO `fieldtype_options` (`fields_id`, `option_id`, `title`, `value`, `sort`, `title1012`, `value1012`, `title1013`, `value1013`) VALUES('147', '2', 'Week', 'Week', '1', NULL, NULL, NULL, NULL);
INSERT INTO `fieldtype_options` (`fields_id`, `option_id`, `title`, `value`, `sort`, `title1012`, `value1012`, `title1013`, `value1013`) VALUES('147', '3', 'Month', 'Month', '2', NULL, NULL, NULL, NULL);
INSERT INTO `fieldtype_options` (`fields_id`, `option_id`, `title`, `value`, `sort`, `title1012`, `value1012`, `title1013`, `value1013`) VALUES('147', '4', 'Year', 'Year', '3', NULL, NULL, NULL, NULL);

DROP TABLE IF EXISTS `modules`;
CREATE TABLE `modules` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `class` varchar(128) CHARACTER SET ascii NOT NULL,
  `flags` int(11) NOT NULL DEFAULT '0',
  `data` text NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `class` (`class`)
) ENGINE=MyISAM AUTO_INCREMENT=188 DEFAULT CHARSET=utf8mb4;

INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('1', 'FieldtypeTextarea', '0', '', '2020-02-05 08:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('2', 'FieldtypeNumber', '0', '', '2020-02-05 08:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('3', 'FieldtypeText', '1', '', '2020-02-05 08:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('4', 'FieldtypePage', '3', '', '2020-02-05 08:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('30', 'InputfieldForm', '0', '', '2020-02-05 08:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('6', 'FieldtypeFile', '1', '', '2020-02-05 08:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('7', 'ProcessPageEdit', '1', '', '2020-02-05 08:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('10', 'ProcessLogin', '0', '', '2020-02-05 08:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('12', 'ProcessPageList', '0', '{\"pageLabelField\":\"title\",\"paginationLimit\":25,\"limit\":50}', '2020-02-05 08:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('121', 'ProcessPageEditLink', '1', '', '2020-02-05 08:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('14', 'ProcessPageSort', '0', '', '2020-02-05 08:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('15', 'InputfieldPageListSelect', '0', '', '2020-02-05 08:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('117', 'JqueryUI', '1', '', '2020-02-05 08:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('17', 'ProcessPageAdd', '0', '', '2020-02-05 08:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('125', 'SessionLoginThrottle', '11', '', '2020-02-05 08:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('122', 'InputfieldPassword', '0', '', '2020-02-05 08:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('25', 'InputfieldAsmSelect', '0', '', '2020-02-05 08:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('116', 'JqueryCore', '1', '', '2020-02-05 08:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('27', 'FieldtypeModule', '1', '', '2020-02-05 08:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('28', 'FieldtypeDatetime', '0', '', '2020-02-05 08:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('29', 'FieldtypeEmail', '1', '', '2020-02-05 08:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('108', 'InputfieldURL', '0', '', '2020-02-05 08:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('32', 'InputfieldSubmit', '0', '', '2020-02-05 08:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('33', 'InputfieldWrapper', '0', '', '2020-02-05 08:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('34', 'InputfieldText', '0', '', '2020-02-05 08:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('35', 'InputfieldTextarea', '0', '', '2020-02-05 08:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('36', 'InputfieldSelect', '0', '', '2020-02-05 08:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('37', 'InputfieldCheckbox', '0', '', '2020-02-05 08:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('38', 'InputfieldCheckboxes', '0', '', '2020-02-05 08:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('39', 'InputfieldRadios', '0', '', '2020-02-05 08:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('40', 'InputfieldHidden', '0', '', '2020-02-05 08:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('41', 'InputfieldName', '0', '', '2020-02-05 08:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('43', 'InputfieldSelectMultiple', '0', '', '2020-02-05 08:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('45', 'JqueryWireTabs', '0', '', '2020-02-05 08:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('46', 'ProcessPage', '0', '', '2020-02-05 08:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('47', 'ProcessTemplate', '0', '', '2020-02-05 08:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('48', 'ProcessField', '32', '', '2020-02-05 08:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('50', 'ProcessModule', '0', '', '2020-02-05 08:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('114', 'PagePermissions', '3', '', '2020-02-05 08:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('97', 'FieldtypeCheckbox', '1', '', '2020-02-05 08:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('115', 'PageRender', '3', '{\"clearCache\":1}', '2020-02-05 08:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('55', 'InputfieldFile', '0', '', '2020-02-05 08:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('56', 'InputfieldImage', '0', '', '2020-02-05 08:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('57', 'FieldtypeImage', '1', '', '2020-02-05 08:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('60', 'InputfieldPage', '0', '{\"inputfieldClasses\":[\"InputfieldSelect\",\"InputfieldSelectMultiple\",\"InputfieldCheckboxes\",\"InputfieldRadios\",\"InputfieldAsmSelect\",\"InputfieldPageListSelect\",\"InputfieldPageListSelectMultiple\",\"InputfieldPageAutocomplete\"]}', '2020-02-05 08:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('61', 'TextformatterEntities', '0', '', '2020-02-05 08:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('66', 'ProcessUser', '0', '{\"showFields\":[\"name\",\"email\",\"roles\"]}', '2020-02-05 08:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('67', 'MarkupAdminDataTable', '0', '', '2020-02-05 08:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('68', 'ProcessRole', '0', '{\"showFields\":[\"name\"]}', '2020-02-05 08:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('76', 'ProcessList', '0', '', '2020-02-05 08:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('78', 'InputfieldFieldset', '0', '', '2020-02-05 08:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('79', 'InputfieldMarkup', '0', '', '2020-02-05 08:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('80', 'InputfieldEmail', '0', '', '2020-02-05 08:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('89', 'FieldtypeFloat', '1', '', '2020-02-05 08:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('83', 'ProcessPageView', '0', '', '2020-02-05 08:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('84', 'FieldtypeInteger', '0', '', '2020-02-05 08:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('85', 'InputfieldInteger', '0', '', '2020-02-05 08:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('86', 'InputfieldPageName', '0', '', '2020-02-05 08:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('87', 'ProcessHome', '0', '', '2020-02-05 08:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('90', 'InputfieldFloat', '0', '', '2020-02-05 08:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('94', 'InputfieldDatetime', '0', '', '2020-02-05 08:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('98', 'MarkupPagerNav', '0', '', '2020-02-05 08:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('129', 'ProcessPageEditImageSelect', '1', '', '2020-02-05 08:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('103', 'JqueryTableSorter', '1', '', '2020-02-05 08:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('104', 'ProcessPageSearch', '1', '{\"searchFields\":\"title\",\"displayField\":\"title path\"}', '2020-02-05 08:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('105', 'FieldtypeFieldsetOpen', '1', '', '2020-02-05 08:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('106', 'FieldtypeFieldsetClose', '1', '', '2020-02-05 08:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('107', 'FieldtypeFieldsetTabOpen', '1', '', '2020-02-05 08:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('109', 'ProcessPageTrash', '1', '', '2020-02-05 08:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('111', 'FieldtypePageTitle', '1', '', '2020-02-05 08:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('112', 'InputfieldPageTitle', '0', '', '2020-02-05 08:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('113', 'MarkupPageArray', '3', '', '2020-02-05 08:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('131', 'InputfieldButton', '0', '', '2020-02-05 08:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('133', 'FieldtypePassword', '1', '', '2020-02-05 08:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('134', 'ProcessPageType', '33', '{\"showFields\":[]}', '2020-02-05 08:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('135', 'FieldtypeURL', '1', '', '2020-02-05 08:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('136', 'ProcessPermission', '1', '{\"showFields\":[\"name\",\"title\"]}', '2020-02-05 08:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('137', 'InputfieldPageListSelectMultiple', '0', '', '2020-02-05 08:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('138', 'ProcessProfile', '1', '{\"profileFields\":[\"pass\",\"email\",\"language\",\"admin_theme\"]}', '2020-02-05 08:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('139', 'SystemUpdater', '1', '{\"systemVersion\":17,\"coreVersion\":\"3.0.153\"}', '2020-02-05 08:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('148', 'AdminThemeDefault', '10', '{\"colors\":\"classic\"}', '2020-02-05 08:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('149', 'InputfieldSelector', '42', '', '2020-02-05 08:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('150', 'ProcessPageLister', '32', '', '2020-02-05 08:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('151', 'JqueryMagnific', '1', '', '2020-02-05 08:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('152', 'PagePathHistory', '3', '', '2020-02-05 08:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('155', 'InputfieldCKEditor', '0', '', '2020-02-05 08:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('156', 'MarkupHTMLPurifier', '0', '', '2020-02-05 08:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('158', 'LanguageSupport', '35', '{\"languagesPageID\":1009,\"defaultLanguagePageID\":1010,\"otherLanguagePageIDs\":[1012],\"languageTranslatorPageID\":1011}', '2020-02-05 08:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('159', 'ProcessLanguage', '1', '', '2020-02-05 08:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('160', 'ProcessLanguageTranslator', '1', '', '2020-02-05 08:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('161', 'LanguageSupportFields', '3', '', '2020-02-05 08:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('162', 'FieldtypeTextLanguage', '1', '', '2020-02-05 08:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('163', 'FieldtypePageTitleLanguage', '1', '', '2020-02-05 08:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('164', 'FieldtypeTextareaLanguage', '1', '', '2020-02-05 08:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('165', 'LanguageSupportPageNames', '3', '{\"moduleVersion\":10,\"pageNumUrlPrefix1010\":\"page\",\"useHomeSegment\":0}', '2020-02-05 08:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('166', 'LanguageTabs', '11', '', '2020-02-05 08:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('168', 'ProcessRecentPages', '1', '', '2020-02-05 08:51:29');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('169', 'AdminThemeUikit', '10', '', '2020-02-05 08:51:30');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('170', 'ProcessLogger', '1', '', '2020-02-05 08:51:35');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('171', 'InputfieldIcon', '0', '', '2020-02-05 08:51:35');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('172', 'FieldtypeRepeater', '35', '{\"repeatersRootPageID\":1027,\"uninstall\":\"\",\"submit_save_module\":\"Submit\"}', '2020-04-20 06:38:23');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('173', 'InputfieldRepeater', '0', '', '2020-04-20 06:38:23');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('174', 'ProcessDatabaseBackups', '1', '', '2020-04-20 09:47:22');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('175', 'CroppableImage3', '0', '', '2020-04-22 12:50:40');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('176', 'FieldtypeCroppableImage3', '1', '', '2020-04-22 12:50:40');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('177', 'InputfieldCroppableImage3', '0', '', '2020-04-22 12:50:40');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('178', 'ProcessCroppableImage3', '1', '', '2020-04-22 12:50:40');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('179', 'DiagnoseImagehandling', '1', '', '2020-04-22 12:53:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('180', 'ProcessDiagnostics', '1', '', '2020-04-22 12:53:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('181', 'FieldtypeOptions', '1', '', '2020-04-22 17:45:41');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('182', 'TextformatterVideoEmbed', '1', '{\"maxWidth\":640,\"maxHeight\":480,\"responsive\":1,\"clearCache\":\"\",\"uninstall\":\"\",\"submit_save_module\":\"Submit\"}', '2020-04-23 12:28:03');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('183', 'SnipWire', '3', '{\"snipcart_environment\":\"0\",\"api_key\":\"YOUR_LIVE_API_KEY\",\"api_key_secret\":\"YOUR_LIVE_API_KEY_SECRET\",\"api_key_test\":\"MmQ0YzA2NDEtNzg3NS00OWFlLWE1YWUtMzI4YTg1ODQxOTNlNjM3MjMzNTYzMDMyOTI5Mjkz\",\"api_key_secret_test\":\"ST_NDJjYzU4ZTktMDk1Yy00NTIzLThiZGYtNTU1NTM0ODY0MzQ3NjM3MjMzNTYzOTExMTExNjU1\",\"credit_cards\":[\"visa\",\"mastercard\",\"maestro\"],\"currencies\":[\"eur\"],\"show_cart_automatically\":1,\"shipping_same_as_billing\":1,\"show_continue_shopping\":1,\"split_firstname_and_lastname\":1,\"cart_custom_fields_enabled\":1,\"snipcart_debug\":1,\"taxes_provider\":\"integrated\",\"taxes_included\":1,\"taxes\":\"[{\\\"name\\\":\\\"20% VAT\\\",\\\"numberForInvoice\\\":\\\"\\\",\\\"rate\\\":\\\"0.20\\\",\\\"appliesOnShipping\\\":[]},{\\\"name\\\":\\\"10% VAT\\\",\\\"numberForInvoice\\\":\\\"\\\",\\\"rate\\\":\\\"0.10\\\",\\\"appliesOnShipping\\\":[]},{\\\"name\\\":\\\"20% VAT\\\",\\\"numberForInvoice\\\":\\\"\\\",\\\"rate\\\":\\\"0.20\\\",\\\"appliesOnShipping\\\":[\\\"1\\\"]}]\",\"shipping_taxes_type\":\"3\",\"include_snipcart_css\":1,\"snipcart_css_path\":\"https:\\/\\/cdn.snipcart.com\\/themes\\/2.0\\/base\\/snipcart.min.css\",\"snipcart_css_integrity\":\"\",\"snipcart_js_path\":\"https:\\/\\/cdn.snipcart.com\\/scripts\\/2.0\\/snipcart.js\",\"snipcart_js_integrity\":\"\",\"include_jquery\":1,\"jquery_js_path\":\"https:\\/\\/code.jquery.com\\/jquery-3.3.1.min.js\",\"jquery_js_integrity\":\"sha256-FgpCb\\/KJQlLNfOu91ta32o\\/NMZxltwRo8QtmkMRdAu8=\",\"excluded_templates\":[],\"cart_image_width\":65,\"cart_image_height\":65,\"cart_image_quality\":70,\"cart_image_hidpi\":1,\"cart_image_hidpiQuality\":50,\"cart_image_cropping\":1,\"webhooks_endpoint\":\"\\/webhooks\\/snipcart\",\"product_templates\":[\"snipcart-product\",\"board-page\"],\"data_item_name_field\":\"title\",\"data_item_categories_field\":\"\",\"currency_param\":\"currency\",\"single_page_shop\":\"\",\"single_page_shop_page\":1,\"snipwire_debug\":\"\",\"uninstall\":\"\",\"submit_save_module\":\"Submit\",\"product_package\":true}', '2020-04-24 22:03:58');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('184', 'ProcessSnipWire', '1', '', '2020-04-24 22:03:58');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('185', 'MarkupSnipWire', '3', '', '2020-04-24 22:03:58');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('186', 'FieldtypeSnipWireTaxSelector', '1', '', '2020-04-24 22:03:58');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('187', 'InputfieldPageAutocomplete', '0', '', '2020-06-06 16:01:07');

DROP TABLE IF EXISTS `page_path_history`;
CREATE TABLE `page_path_history` (
  `path` varchar(250) NOT NULL,
  `pages_id` int(10) unsigned NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `language_id` int(10) unsigned DEFAULT '0',
  PRIMARY KEY (`path`),
  KEY `pages_id` (`pages_id`),
  KEY `created` (`created`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

INSERT INTO `page_path_history` (`path`, `pages_id`, `created`, `language_id`) VALUES('/de/uber/unterseite-seite-beispiel-1', '1002', '2020-04-15 09:32:43', '1012');
INSERT INTO `page_path_history` (`path`, `pages_id`, `created`, `language_id`) VALUES('/fi/tietoja/alasivu-sivu-esimerkki-1', '1002', '2020-04-15 09:32:43', '1013');
INSERT INTO `page_path_history` (`path`, `pages_id`, `created`, `language_id`) VALUES('/en/about/child-page-example-1', '1002', '2020-04-15 09:32:43', '0');
INSERT INTO `page_path_history` (`path`, `pages_id`, `created`, `language_id`) VALUES('/de/uber/unterseite-beispiel-2', '1004', '2020-04-15 09:33:21', '1012');
INSERT INTO `page_path_history` (`path`, `pages_id`, `created`, `language_id`) VALUES('/fi/tietoja/alasivu-esimerkki-2', '1004', '2020-04-15 09:33:21', '1013');
INSERT INTO `page_path_history` (`path`, `pages_id`, `created`, `language_id`) VALUES('/en/about/child-page-example-2', '1004', '2020-04-15 09:33:21', '0');
INSERT INTO `page_path_history` (`path`, `pages_id`, `created`, `language_id`) VALUES('/de/shop/1050.1020.0_boards', '1050', '2020-04-24 22:47:50', '1012');
INSERT INTO `page_path_history` (`path`, `pages_id`, `created`, `language_id`) VALUES('/fi/shop/1050.1020.0_boards', '1050', '2020-04-24 22:47:50', '1013');
INSERT INTO `page_path_history` (`path`, `pages_id`, `created`, `language_id`) VALUES('/de/1021.1.2_boards', '1021', '2020-04-25 00:02:32', '1012');
INSERT INTO `page_path_history` (`path`, `pages_id`, `created`, `language_id`) VALUES('/fi/1021.1.2_boards', '1021', '2020-04-25 00:02:32', '1013');
INSERT INTO `page_path_history` (`path`, `pages_id`, `created`, `language_id`) VALUES('/shop', '1020', '2020-04-25 00:02:52', '0');

DROP TABLE IF EXISTS `pages`;
CREATE TABLE `pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) unsigned NOT NULL DEFAULT '0',
  `templates_id` int(11) unsigned NOT NULL DEFAULT '0',
  `name` varchar(128) CHARACTER SET ascii NOT NULL,
  `status` int(10) unsigned NOT NULL DEFAULT '1',
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_users_id` int(10) unsigned NOT NULL DEFAULT '2',
  `created` timestamp NOT NULL DEFAULT '2015-12-18 05:09:00',
  `created_users_id` int(10) unsigned NOT NULL DEFAULT '2',
  `published` datetime DEFAULT NULL,
  `sort` int(11) NOT NULL DEFAULT '0',
  `name1012` varchar(128) CHARACTER SET ascii DEFAULT NULL,
  `status1012` int(10) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_parent_id` (`name`,`parent_id`),
  UNIQUE KEY `name1012_parent_id` (`name1012`,`parent_id`),
  KEY `parent_id` (`parent_id`),
  KEY `templates_id` (`templates_id`),
  KEY `modified` (`modified`),
  KEY `created` (`created`),
  KEY `status` (`status`),
  KEY `published` (`published`)
) ENGINE=MyISAM AUTO_INCREMENT=1122 DEFAULT CHARSET=utf8mb4;

INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1', '0', '1', 'en', '9', '2020-05-27 06:52:21', '41', '2020-02-05 08:51:13', '2', '2020-02-05 09:51:13', '0', 'de', '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('2', '1', '2', 'admin', '1035', '2020-02-05 08:51:30', '40', '2020-02-05 08:51:13', '2', '2020-02-05 09:51:13', '9', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('3', '2', '2', 'page', '21', '2020-02-05 08:51:13', '41', '2020-02-05 08:51:13', '2', '2020-02-05 09:51:13', '0', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('6', '3', '2', 'add', '21', '2020-02-05 08:51:39', '40', '2020-02-05 08:51:13', '2', '2020-02-05 09:51:13', '1', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('7', '1', '2', 'trash', '1039', '2020-02-05 08:51:13', '41', '2020-02-05 08:51:13', '2', '2020-02-05 09:51:13', '10', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('8', '3', '2', 'list', '21', '2020-02-05 08:51:41', '41', '2020-02-05 08:51:13', '2', '2020-02-05 09:51:13', '0', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('9', '3', '2', 'sort', '1047', '2020-02-05 08:51:13', '41', '2020-02-05 08:51:13', '2', '2020-02-05 09:51:13', '3', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('10', '3', '2', 'edit', '1045', '2020-02-05 08:51:40', '41', '2020-02-05 08:51:13', '2', '2020-02-05 09:51:13', '4', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('11', '22', '2', 'template', '21', '2020-02-05 08:51:13', '41', '2020-02-05 08:51:13', '2', '2020-02-05 09:51:13', '0', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('16', '22', '2', 'field', '21', '2020-02-05 08:51:13', '41', '2020-02-05 08:51:13', '2', '2020-02-05 09:51:13', '2', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('21', '2', '2', 'module', '21', '2020-02-05 08:51:13', '41', '2020-02-05 08:51:13', '2', '2020-02-05 09:51:13', '2', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('22', '2', '2', 'setup', '21', '2020-02-05 08:51:13', '41', '2020-02-05 08:51:13', '2', '2020-02-05 09:51:13', '1', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('23', '2', '2', 'login', '1035', '2020-02-05 08:51:13', '41', '2020-02-05 08:51:13', '2', '2020-02-05 09:51:13', '4', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('27', '1', '29', 'http404', '1035', '2020-02-05 08:51:13', '41', '2020-02-05 08:51:13', '3', '2020-02-05 09:51:13', '8', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('28', '2', '2', 'access', '13', '2020-02-05 08:51:13', '41', '2020-02-05 08:51:13', '2', '2020-02-05 09:51:13', '3', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('29', '28', '2', 'users', '29', '2020-02-05 08:51:13', '41', '2020-02-05 08:51:13', '2', '2020-02-05 09:51:13', '0', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('30', '28', '2', 'roles', '29', '2020-02-05 08:51:13', '41', '2020-02-05 08:51:13', '2', '2020-02-05 09:51:13', '1', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('31', '28', '2', 'permissions', '29', '2020-02-05 08:51:13', '41', '2020-02-05 08:51:13', '2', '2020-02-05 09:51:13', '2', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('32', '31', '5', 'page-edit', '25', '2020-02-05 08:51:13', '41', '2020-02-05 08:51:13', '2', '2020-02-05 09:51:13', '2', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('34', '31', '5', 'page-delete', '25', '2020-02-05 08:51:13', '41', '2020-02-05 08:51:13', '2', '2020-02-05 09:51:13', '3', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('35', '31', '5', 'page-move', '25', '2020-02-05 08:51:13', '41', '2020-02-05 08:51:13', '2', '2020-02-05 09:51:13', '4', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('36', '31', '5', 'page-view', '25', '2020-02-05 08:51:13', '41', '2020-02-05 08:51:13', '2', '2020-02-05 09:51:13', '0', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('37', '30', '4', 'guest', '25', '2020-02-05 08:51:13', '41', '2020-02-05 08:51:13', '2', '2020-02-05 09:51:13', '0', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('38', '30', '4', 'superuser', '25', '2020-02-05 08:51:13', '41', '2020-02-05 08:51:13', '2', '2020-02-05 09:51:13', '1', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('41', '29', '3', 'admin', '1', '2020-02-05 08:51:30', '40', '2020-02-05 08:51:13', '2', '2020-02-05 09:51:13', '0', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('40', '29', '3', 'guest', '25', '2020-02-05 08:51:13', '41', '2020-02-05 08:51:13', '2', '2020-02-05 09:51:13', '1', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('50', '31', '5', 'page-sort', '25', '2020-02-05 08:51:13', '41', '2020-02-05 08:51:13', '41', '2020-02-05 09:51:13', '5', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('51', '31', '5', 'page-template', '25', '2020-02-05 08:51:13', '41', '2020-02-05 08:51:13', '41', '2020-02-05 09:51:13', '6', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('52', '31', '5', 'user-admin', '25', '2020-02-05 08:51:13', '41', '2020-02-05 08:51:13', '41', '2020-02-05 09:51:13', '10', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('53', '31', '5', 'profile-edit', '1', '2020-02-05 08:51:13', '41', '2020-02-05 08:51:13', '41', '2020-02-05 09:51:13', '13', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('54', '31', '5', 'page-lock', '1', '2020-02-05 08:51:13', '41', '2020-02-05 08:51:13', '41', '2020-02-05 09:51:13', '8', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('300', '3', '2', 'search', '1045', '2020-02-05 08:51:13', '41', '2020-02-05 08:51:13', '2', '2020-02-05 09:51:13', '6', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('301', '3', '2', 'trash', '1047', '2020-02-05 08:51:13', '41', '2020-02-05 08:51:13', '2', '2020-02-05 09:51:13', '6', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('302', '3', '2', 'link', '1041', '2020-02-05 08:51:13', '41', '2020-02-05 08:51:13', '2', '2020-02-05 09:51:13', '7', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('303', '3', '2', 'image', '1041', '2020-02-05 08:51:13', '41', '2020-02-05 08:51:13', '2', '2020-02-05 09:51:13', '8', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('304', '2', '2', 'profile', '1025', '2020-02-05 08:51:13', '41', '2020-02-05 08:51:13', '41', '2020-02-05 09:51:13', '5', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1000', '1', '26', 'search', '1025', '2020-04-15 09:34:23', '41', '2020-02-05 08:51:13', '2', '2020-02-05 09:51:13', '7', 'suche', '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1001', '1', '29', 'about', '1', '2020-04-22 18:16:58', '41', '2020-02-05 08:51:13', '2', '2020-02-05 09:51:13', '3', 'uber', '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1002', '1001', '29', 'manufactur', '1', '2020-04-15 09:32:43', '41', '2020-02-05 08:51:13', '2', '2020-02-05 09:51:13', '0', 'manufaktur', '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1024', '1001', '29', 'team', '1', '2020-06-06 16:25:41', '41', '2020-04-15 09:33:35', '41', '2020-04-15 11:33:37', '2', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1004', '1001', '29', 'history', '1', '2020-04-15 09:33:21', '41', '2020-02-05 08:51:13', '2', '2020-02-05 09:51:13', '1', 'geschichte', '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1005', '1', '34', 'site-map', '1025', '2020-04-15 09:34:27', '41', '2020-02-05 08:51:13', '2', '2020-02-05 09:51:13', '6', 'sitemap', '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1006', '31', '5', 'page-lister', '1', '2020-02-05 08:51:13', '40', '2020-02-05 08:51:13', '40', '2020-02-05 09:51:13', '9', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1007', '3', '2', 'lister', '1', '2020-02-05 08:51:13', '40', '2020-02-05 08:51:13', '40', '2020-02-05 09:51:13', '9', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1009', '22', '2', 'languages', '16', '2020-02-05 08:51:13', '41', '2020-02-05 08:51:13', '41', '2020-02-05 09:51:13', '2', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1010', '1009', '43', 'default', '16', '2020-06-06 16:28:18', '41', '2020-02-05 08:51:13', '41', '2020-02-05 09:51:13', '0', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1011', '22', '2', 'language-translator', '1040', '2020-02-05 08:51:13', '41', '2020-02-05 08:51:13', '41', '2020-02-05 09:51:13', '3', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1012', '1009', '43', 'de', '1', '2020-06-06 16:28:01', '41', '2020-02-05 08:51:13', '41', '2020-02-05 09:51:13', '1', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1015', '3', '2', 'recent-pages', '1', '2020-02-05 08:51:29', '40', '2020-02-05 08:51:29', '40', '2020-02-05 09:51:29', '10', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1016', '31', '5', 'page-edit-recent', '1', '2020-02-05 08:51:29', '40', '2020-02-05 08:51:29', '40', '2020-02-05 09:51:29', '10', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1017', '22', '2', 'logs', '1', '2020-02-05 08:51:35', '40', '2020-02-05 08:51:35', '40', '2020-02-05 09:51:35', '4', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1018', '31', '5', 'logs-view', '1', '2020-02-05 08:51:35', '40', '2020-02-05 08:51:35', '40', '2020-02-05 09:51:35', '11', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1019', '31', '5', 'logs-edit', '1', '2020-02-05 08:51:35', '40', '2020-02-05 08:51:35', '40', '2020-02-05 09:51:35', '12', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1020', '1', '48', 'boards', '1', '2020-04-25 00:02:52', '41', '2020-04-15 09:31:08', '41', '2020-04-25 00:47:59', '0', 'boards', '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1021', '7', '48', '1021.1.2_boards', '8193', '2020-04-25 00:02:32', '41', '2020-04-15 09:31:23', '41', '2020-04-15 11:31:43', '2', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1022', '1021', '48', 'twin-tips', '8193', '2020-04-22 18:05:54', '41', '2020-04-15 09:31:36', '41', '2020-04-15 11:31:46', '1', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1023', '1021', '48', 'skimboards', '8193', '2020-04-22 12:23:53', '41', '2020-04-15 09:31:57', '41', '2020-04-15 11:31:59', '2', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1025', '1', '29', 'blog', '2049', '2020-04-20 09:00:38', '41', '2020-04-15 09:33:54', '41', '2020-04-15 11:33:57', '4', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1026', '1', '54', 'contact', '1', '2020-06-06 16:05:18', '41', '2020-04-15 09:34:51', '41', '2020-04-15 11:34:53', '5', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1027', '2', '2', 'repeaters', '1036', '2020-04-20 06:38:23', '41', '2020-04-20 06:38:23', '41', '2020-04-20 08:38:23', '6', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1028', '1027', '2', 'for-field-104', '17', '2020-04-20 06:47:53', '41', '2020-04-20 06:47:53', '41', '2020-04-20 08:47:53', '0', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1029', '1028', '2', 'for-page-1', '17', '2020-04-20 07:24:05', '41', '2020-04-20 07:24:05', '41', '2020-04-20 09:24:05', '0', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1030', '1029', '44', '1587365554-6473-1', '3073', '2020-04-20 07:53:33', '41', '2020-04-20 07:52:34', '41', NULL, '0', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1031', '1027', '2', 'for-field-108', '17', '2020-04-20 07:54:54', '41', '2020-04-20 07:54:54', '41', '2020-04-20 09:54:54', '1', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1032', '1031', '2', 'for-page-1', '17', '2020-04-20 07:55:51', '41', '2020-04-20 07:55:51', '41', '2020-04-20 09:55:51', '0', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1033', '1027', '2', 'for-field-111', '17', '2020-04-20 07:57:54', '41', '2020-04-20 07:57:54', '41', '2020-04-20 09:57:54', '2', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1034', '1033', '2', 'for-page-1', '17', '2020-04-20 08:07:14', '41', '2020-04-20 08:07:14', '41', '2020-04-20 10:07:14', '0', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1035', '1032', '45', '1587366473-433-1', '1', '2020-05-26 14:42:58', '41', '2020-04-20 08:07:53', '41', '2020-04-20 10:08:31', '0', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1036', '1032', '45', '1587366487-1834-1', '1', '2020-05-26 14:42:58', '41', '2020-04-20 08:08:07', '41', '2020-04-20 10:08:31', '1', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1037', '1032', '45', '1587366498-3997-1', '1', '2020-05-26 14:42:58', '41', '2020-04-20 08:08:18', '41', '2020-04-20 10:08:31', '2', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1038', '1032', '45', '1587366504-9452-1', '1', '2020-05-26 14:42:58', '41', '2020-04-20 08:08:24', '41', '2020-04-20 10:08:31', '3', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1043', '1028', '2', 'for-page-1025', '17', '2020-04-20 09:00:38', '41', '2020-04-20 09:00:38', '41', '2020-04-20 11:00:38', '1', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1039', '1034', '46', '1587366539-3996-1', '1', '2020-04-20 08:10:14', '41', '2020-04-20 08:08:59', '41', '2020-04-20 10:09:30', '0', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1040', '1034', '46', '1587366554-4292-1', '1', '2020-04-20 08:10:14', '41', '2020-04-20 08:09:14', '41', '2020-04-20 10:09:30', '1', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1041', '1034', '46', '1587366559-4722-1', '1', '2020-04-20 08:10:14', '41', '2020-04-20 08:09:19', '41', '2020-04-20 10:09:30', '2', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1042', '1034', '46', '1587366564-8884-1', '1', '2020-04-20 08:10:14', '41', '2020-04-20 08:09:24', '41', '2020-04-20 10:09:30', '3', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1044', '1028', '2', 'for-page-1001', '17', '2020-04-20 09:00:49', '41', '2020-04-20 09:00:49', '41', '2020-04-20 11:00:49', '2', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1045', '1044', '44', '1587369833-5524-1', '1', '2020-04-20 09:17:51', '41', '2020-04-20 09:03:53', '41', '2020-04-20 11:04:24', '0', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1048', '22', '2', 'db-backups', '1', '2020-04-20 09:47:22', '41', '2020-04-20 09:47:22', '41', '2020-04-20 11:47:22', '5', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1046', '1044', '44', '1587370386-2849-1', '1', '2020-04-20 09:19:24', '41', '2020-04-20 09:13:06', '41', '2020-04-20 11:13:30', '1', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1047', '1044', '44', '1587370388-9256-1', '3073', '2020-04-20 09:13:08', '41', '2020-04-20 09:13:08', '41', NULL, '2', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1049', '31', '5', 'db-backup', '1', '2020-04-20 09:47:22', '41', '2020-04-20 09:47:22', '41', '2020-04-20 11:47:22', '13', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1050', '7', '29', '1050.1020.0_boards', '8193', '2020-04-24 22:47:50', '41', '2020-04-20 10:04:25', '41', '2020-04-20 12:04:37', '0', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1051', '1028', '2', 'for-page-1050', '17', '2020-04-20 10:04:25', '41', '2020-04-20 10:04:25', '41', '2020-04-20 12:04:25', '3', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1052', '1022', '47', 'freestyler', '8193', '2020-04-23 12:59:40', '41', '2020-04-20 10:47:39', '41', '2020-04-22 13:58:53', '0', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1053', '1028', '2', 'for-page-1052', '17', '2020-04-20 10:47:39', '41', '2020-04-20 10:47:39', '41', '2020-04-20 12:47:39', '4', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1054', '1021', '48', 'waveboards', '8193', '2020-04-22 12:24:01', '41', '2020-04-22 12:22:55', '41', '2020-04-22 14:24:01', '3', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1055', '1028', '2', 'for-page-1054', '17', '2020-04-22 12:22:55', '41', '2020-04-22 12:22:55', '41', '2020-04-22 14:22:55', '5', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1056', '1028', '2', 'for-page-1022', '17', '2020-04-22 12:23:38', '41', '2020-04-22 12:23:38', '41', '2020-04-22 14:23:38', '6', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1057', '1028', '2', 'for-page-1023', '17', '2020-04-22 12:23:49', '41', '2020-04-22 12:23:49', '41', '2020-04-22 14:23:49', '7', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1058', '1023', '47', 'testboard', '8193', '2020-04-22 12:24:25', '41', '2020-04-22 12:24:16', '41', '2020-04-22 14:24:25', '0', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1059', '1028', '2', 'for-page-1058', '17', '2020-04-22 12:24:16', '41', '2020-04-22 12:24:16', '41', '2020-04-22 14:24:16', '8', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1060', '1054', '47', 'beaver', '8193', '2020-04-22 12:24:40', '41', '2020-04-22 12:24:37', '41', '2020-04-22 14:24:40', '0', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1061', '1028', '2', 'for-page-1060', '17', '2020-04-22 12:24:37', '41', '2020-04-22 12:24:37', '41', '2020-04-22 14:24:37', '9', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1062', '1028', '2', 'for-page-1021', '17', '2020-04-22 12:25:42', '41', '2020-04-22 12:25:42', '41', '2020-04-22 14:25:42', '10', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1063', '1022', '47', 'rote-rakete', '8193', '2020-04-22 18:24:09', '41', '2020-04-22 12:36:59', '41', '2020-04-22 14:37:04', '1', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1064', '1028', '2', 'for-page-1063', '17', '2020-04-22 12:36:59', '41', '2020-04-22 12:36:59', '41', '2020-04-22 14:36:59', '11', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1065', '1022', '47', 'flitzer', '8193', '2020-04-22 18:24:56', '41', '2020-04-22 12:37:17', '41', '2020-04-22 14:37:19', '2', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1066', '1028', '2', 'for-page-1065', '17', '2020-04-22 12:37:17', '41', '2020-04-22 12:37:17', '41', '2020-04-22 14:37:17', '12', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1067', '3', '2', 'croppable-image-3', '1025', '2020-04-22 12:50:40', '41', '2020-04-22 12:50:40', '41', '2020-04-22 14:50:40', '10', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1068', '31', '5', 'croppable-image-3', '1', '2020-04-22 12:50:40', '41', '2020-04-22 12:50:40', '41', '2020-04-22 14:50:40', '14', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1069', '22', '2', 'processdiagnostics', '1', '2020-04-22 12:53:13', '41', '2020-04-22 12:53:13', '41', '2020-04-22 14:53:13', '6', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1070', '1062', '44', '1587576401-1901-1', '1', '2020-04-22 18:28:21', '41', '2020-04-22 18:26:41', '41', '2020-04-22 20:27:25', '0', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1071', '1062', '44', '1587576517-5102-1', '1', '2020-04-22 18:29:26', '41', '2020-04-22 18:28:37', '41', '2020-04-22 20:29:02', '1', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1072', '1028', '2', 'for-page-1020', '17', '2020-04-22 19:02:34', '41', '2020-04-22 19:02:34', '41', '2020-04-22 21:02:34', '13', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1073', '22', '2', 'snipwire', '1', '2020-04-24 22:03:58', '41', '2020-04-24 22:03:58', '41', '2020-04-25 00:03:58', '7', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1074', '1073', '49', 'custom-cart-fields', '1040', '2020-04-24 22:03:58', '41', '2020-04-24 22:03:58', '41', '2020-04-25 00:03:58', '0', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1075', '31', '5', 'snipwire-dashboard', '1', '2020-04-24 22:03:58', '41', '2020-04-24 22:03:58', '41', '2020-04-25 00:03:58', '15', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1076', '1020', '50', 'freestyler', '1', '2020-05-27 07:01:39', '41', '2020-04-24 22:04:40', '41', '2020-04-25 00:04:40', '0', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1077', '1076', '51', 'freestyler-135', '1', '2020-05-27 09:01:41', '41', '2020-04-24 22:04:40', '41', '2020-04-25 00:04:40', '0', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1078', '7', '51', '1078.1076.1_festish-wet-warmer', '8193', '2020-04-24 22:50:07', '41', '2020-04-24 22:04:40', '41', '2020-04-25 00:04:40', '1', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1079', '7', '51', '1079.1076.2_axolotl-juicer', '8193', '2020-04-24 22:50:11', '41', '2020-04-24 22:04:40', '41', '2020-04-25 00:04:40', '2', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1080', '7', '51', '1080.1076.3_freestyler', '10241', '2020-04-24 22:50:15', '41', '2020-04-24 22:42:27', '41', NULL, '3', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1081', '7', '29', '1081.1.11_shoping', '10241', '2020-04-24 22:47:43', '41', '2020-04-24 22:47:39', '41', NULL, '11', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1082', '1028', '2', 'for-page-1081', '17', '2020-04-24 22:47:39', '41', '2020-04-24 22:47:39', '41', '2020-04-25 00:47:39', '14', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1083', '1076', '51', 'freestyler-140', '1', '2020-05-27 08:14:50', '41', '2020-04-24 22:53:07', '41', '2020-04-25 00:53:12', '1', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1085', '1027', '2', 'for-field-152', '17', '2020-04-28 06:19:31', '41', '2020-04-28 06:19:31', '41', '2020-04-28 08:19:31', '3', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1086', '1027', '2', 'for-field-153', '17', '2020-04-28 06:21:54', '41', '2020-04-28 06:21:54', '41', '2020-04-28 08:21:54', '4', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1087', '1086', '2', 'for-page-1077', '17', '2020-04-28 06:22:45', '41', '2020-04-28 06:22:45', '41', '2020-04-28 08:22:45', '0', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1088', '1087', '53', '1588051372-1268-1', '1', '2020-04-28 06:35:09', '41', '2020-04-28 06:22:52', '41', '2020-04-28 08:25:05', '0', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1089', '1085', '2', 'for-page-1088', '17', '2020-04-28 06:22:52', '41', '2020-04-28 06:22:52', '41', '2020-04-28 08:22:52', '0', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1090', '1089', '52', '1588051375-5055-1', '1', '2020-04-28 06:25:05', '41', '2020-04-28 06:22:55', '41', '2020-04-28 08:25:05', '2', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1091', '1089', '52', '1588051493-3907-1', '1', '2020-04-28 06:25:05', '41', '2020-04-28 06:24:53', '41', '2020-04-28 08:25:05', '3', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1092', '1089', '52', '1588051498-7915-1', '1', '2020-04-28 06:25:05', '41', '2020-04-28 06:24:58', '41', '2020-04-28 08:25:05', '4', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1093', '1087', '53', '1588052086-4136-1', '1', '2020-04-28 06:37:41', '41', '2020-04-28 06:34:46', '41', '2020-04-28 08:35:10', '1', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1094', '1085', '2', 'for-page-1093', '17', '2020-04-28 06:34:46', '41', '2020-04-28 06:34:46', '41', '2020-04-28 08:34:46', '1', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1095', '1094', '52', '1588052090-4925-1', '1', '2020-04-28 06:35:10', '41', '2020-04-28 06:34:50', '41', '2020-04-28 08:35:10', '2', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1096', '1094', '52', '1588052098-5988-1', '1', '2020-04-28 06:35:10', '41', '2020-04-28 06:34:58', '41', '2020-04-28 08:35:10', '3', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1097', '1087', '53', '1588052239-1408-1', '1', '2020-05-27 08:14:26', '41', '2020-04-28 06:37:19', '41', '2020-04-28 08:37:41', '2', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1098', '1085', '2', 'for-page-1097', '17', '2020-04-28 06:37:19', '41', '2020-04-28 06:37:19', '41', '2020-04-28 08:37:19', '2', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1099', '1098', '52', '1588052243-1188-1', '1', '2020-04-28 06:37:41', '41', '2020-04-28 06:37:23', '41', '2020-04-28 08:37:41', '2', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1100', '1098', '52', '1588052249-6365-1', '1', '2020-04-28 06:37:41', '41', '2020-04-28 06:37:29', '41', '2020-04-28 08:37:41', '3', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1101', '1086', '2', 'for-page-1076', '17', '2020-05-07 05:49:50', '41', '2020-05-07 05:49:50', '41', '2020-05-07 07:49:50', '1', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1102', '1101', '53', '1588826993-8588-1', '1', '2020-05-07 05:51:03', '41', '2020-05-07 05:49:53', '41', '2020-05-07 07:50:35', '0', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1103', '1085', '2', 'for-page-1102', '17', '2020-05-07 05:49:53', '41', '2020-05-07 05:49:53', '41', '2020-05-07 07:49:53', '3', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1104', '1103', '52', '1588827003-58-1', '1', '2020-05-07 05:50:35', '41', '2020-05-07 05:50:03', '41', '2020-05-07 07:50:35', '2', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1105', '1103', '52', '1588827028-1931-1', '1', '2020-05-07 05:50:35', '41', '2020-05-07 05:50:28', '41', '2020-05-07 07:50:35', '3', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1106', '1101', '53', '1588827044-6278-1', '1', '2020-05-26 13:08:59', '41', '2020-05-07 05:50:44', '41', '2020-05-07 07:51:03', '1', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1107', '1085', '2', 'for-page-1106', '17', '2020-05-07 05:50:44', '41', '2020-05-07 05:50:44', '41', '2020-05-07 07:50:44', '4', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1108', '1107', '52', '1588827049-9665-1', '1', '2020-05-07 05:51:03', '41', '2020-05-07 05:50:49', '41', '2020-05-07 07:51:03', '2', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1109', '1107', '52', '1588827055-4947-1', '1', '2020-05-07 05:51:03', '41', '2020-05-07 05:50:55', '41', '2020-05-07 07:51:03', '3', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1110', '1020', '50', 'skimboards', '1', '2020-05-27 07:01:46', '41', '2020-05-26 13:22:12', '41', '2020-05-26 13:22:36', '1', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1111', '1086', '2', 'for-page-1110', '17', '2020-05-26 13:22:12', '41', '2020-05-26 13:22:12', '41', '2020-05-26 13:22:12', '2', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1112', '1020', '50', 'waveboards', '1', '2020-05-27 07:01:48', '41', '2020-05-26 13:35:57', '41', '2020-05-26 13:37:01', '2', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1113', '1086', '2', 'for-page-1112', '17', '2020-05-26 13:35:57', '41', '2020-05-26 13:35:57', '41', '2020-05-26 13:35:57', '3', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1114', '1032', '45', '1590504161-5765-1', '1', '2020-05-27 06:52:21', '41', '2020-05-26 14:42:41', '41', '2020-05-26 14:42:58', '4', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1115', '1086', '2', 'for-page-1083', '17', '2020-05-27 08:14:41', '41', '2020-05-27 08:14:41', '41', '2020-05-27 08:14:41', '4', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1116', '1', '29', 'impressum', '1', '2020-06-06 15:46:32', '41', '2020-06-06 15:46:23', '41', '2020-06-06 15:46:32', '9', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1117', '1028', '2', 'for-page-1116', '17', '2020-06-06 15:46:23', '41', '2020-06-06 15:46:23', '41', '2020-06-06 15:46:23', '15', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1118', '1028', '2', 'for-page-1026', '17', '2020-06-06 15:56:17', '41', '2020-06-06 15:56:17', '41', '2020-06-06 15:56:17', '16', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1119', '1028', '2', 'for-page-1024', '17', '2020-06-06 16:17:46', '41', '2020-06-06 16:17:46', '41', '2020-06-06 16:17:46', '17', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1120', '1119', '44', '1591460326-9931-1', '1', '2020-06-06 16:25:26', '41', '2020-06-06 16:18:47', '41', '2020-06-06 16:19:23', '0', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1121', '1119', '44', '1591460597-8222-1', '1', '2020-06-06 16:25:41', '41', '2020-06-06 16:23:17', '41', '2020-06-06 16:23:43', '1', NULL, '1');

DROP TABLE IF EXISTS `pages_access`;
CREATE TABLE `pages_access` (
  `pages_id` int(11) NOT NULL,
  `templates_id` int(11) NOT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`pages_id`),
  KEY `templates_id` (`templates_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('37', '2', '2020-02-05 08:51:13');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('38', '2', '2020-02-05 08:51:13');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('32', '2', '2020-02-05 08:51:13');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('34', '2', '2020-02-05 08:51:13');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('35', '2', '2020-02-05 08:51:13');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('36', '2', '2020-02-05 08:51:13');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('50', '2', '2020-02-05 08:51:13');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('51', '2', '2020-02-05 08:51:13');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('52', '2', '2020-02-05 08:51:13');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('53', '2', '2020-02-05 08:51:13');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('54', '2', '2020-02-05 08:51:13');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1006', '2', '2020-02-05 08:51:13');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1010', '2', '2020-02-05 08:51:13');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1012', '2', '2020-02-05 08:51:13');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1016', '2', '2020-02-05 08:51:29');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1018', '2', '2020-02-05 08:51:35');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1019', '2', '2020-02-05 08:51:35');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1020', '1', '2020-04-15 09:31:08');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1021', '2', '2020-04-25 00:02:32');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1068', '2', '2020-04-22 12:50:40');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1065', '2', '2020-04-25 00:02:32');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1024', '1', '2020-04-15 09:33:35');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1025', '1', '2020-04-15 09:33:54');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1026', '1', '2020-04-15 09:34:51');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1030', '2', '2020-04-20 07:52:34');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1035', '2', '2020-04-20 08:07:53');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1036', '2', '2020-04-20 08:08:07');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1037', '2', '2020-04-20 08:08:18');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1038', '2', '2020-04-20 08:08:24');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1039', '2', '2020-04-20 08:08:59');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1040', '2', '2020-04-20 08:09:14');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1041', '2', '2020-04-20 08:09:19');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1042', '2', '2020-04-20 08:09:24');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1045', '2', '2020-04-20 09:03:53');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1046', '2', '2020-04-20 09:13:06');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1047', '2', '2020-04-20 09:13:08');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1049', '2', '2020-04-20 09:47:22');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1050', '2', '2020-04-24 22:47:50');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1071', '2', '2020-04-22 18:28:37');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1063', '2', '2020-04-25 00:02:32');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1070', '2', '2020-04-22 18:26:41');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1074', '2', '2020-04-24 22:03:58');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1075', '2', '2020-04-24 22:03:58');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1078', '2', '2020-04-24 22:50:07');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1083', '1', '2020-04-24 22:53:07');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1080', '2', '2020-04-24 22:50:15');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1079', '2', '2020-04-24 22:50:11');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1081', '2', '2020-04-24 22:47:43');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1052', '2', '2020-04-25 00:02:32');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1058', '2', '2020-04-25 00:02:32');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1060', '2', '2020-04-25 00:02:32');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1022', '2', '2020-04-25 00:02:32');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1023', '2', '2020-04-25 00:02:32');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1054', '2', '2020-04-25 00:02:32');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1088', '2', '2020-04-28 06:22:52');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1090', '2', '2020-04-28 06:22:55');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1091', '2', '2020-04-28 06:24:53');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1092', '2', '2020-04-28 06:24:58');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1093', '2', '2020-04-28 06:34:46');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1095', '2', '2020-04-28 06:34:50');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1096', '2', '2020-04-28 06:34:58');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1097', '2', '2020-04-28 06:37:19');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1099', '2', '2020-04-28 06:37:23');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1100', '2', '2020-04-28 06:37:29');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1102', '2', '2020-05-07 05:49:53');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1104', '2', '2020-05-07 05:50:03');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1105', '2', '2020-05-07 05:50:28');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1106', '2', '2020-05-07 05:50:44');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1108', '2', '2020-05-07 05:50:49');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1109', '2', '2020-05-07 05:50:55');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1110', '1', '2020-05-26 13:22:13');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1112', '1', '2020-05-26 13:35:57');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1114', '2', '2020-05-26 14:42:41');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1116', '1', '2020-06-06 15:46:23');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1120', '2', '2020-06-06 16:18:47');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1121', '2', '2020-06-06 16:23:17');

DROP TABLE IF EXISTS `pages_parents`;
CREATE TABLE `pages_parents` (
  `pages_id` int(10) unsigned NOT NULL,
  `parents_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`pages_id`,`parents_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('2', '1');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('3', '1');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('3', '2');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('22', '1');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('22', '2');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('28', '1');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('28', '2');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('29', '1');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('29', '2');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('29', '28');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('30', '1');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('30', '2');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('30', '28');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('31', '1');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('31', '2');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('31', '28');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1001', '1');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1002', '1');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1002', '1001');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1004', '1');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1004', '1001');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1005', '1');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1009', '1');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1009', '2');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1009', '22');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1021', '7');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1022', '7');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1022', '1021');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1023', '7');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1023', '1021');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1027', '2');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1028', '2');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1028', '1027');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1029', '2');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1029', '1027');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1029', '1028');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1031', '2');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1031', '1027');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1032', '2');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1032', '1027');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1032', '1031');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1033', '2');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1033', '1027');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1034', '2');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1034', '1027');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1034', '1033');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1044', '2');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1044', '1027');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1044', '1028');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1054', '7');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1054', '1021');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1062', '2');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1062', '1027');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1062', '1028');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1073', '2');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1073', '22');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1076', '1020');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1085', '2');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1085', '1027');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1086', '2');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1086', '1027');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1087', '2');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1087', '1027');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1087', '1086');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1089', '2');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1089', '1027');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1089', '1085');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1094', '2');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1094', '1027');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1094', '1085');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1098', '2');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1098', '1027');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1098', '1085');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1101', '2');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1101', '1027');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1101', '1086');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1103', '2');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1103', '1027');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1103', '1085');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1107', '2');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1107', '1027');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1107', '1085');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1119', '2');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1119', '1027');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1119', '1028');

DROP TABLE IF EXISTS `pages_sortfields`;
CREATE TABLE `pages_sortfields` (
  `pages_id` int(10) unsigned NOT NULL DEFAULT '0',
  `sortfield` varchar(20) NOT NULL DEFAULT '',
  PRIMARY KEY (`pages_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `session_login_throttle`;
CREATE TABLE `session_login_throttle` (
  `name` varchar(128) NOT NULL,
  `attempts` int(10) unsigned NOT NULL DEFAULT '0',
  `last_attempt` int(10) unsigned NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

INSERT INTO `session_login_throttle` (`name`, `attempts`, `last_attempt`) VALUES('admin', '1', '1591458368');

DROP TABLE IF EXISTS `templates`;
CREATE TABLE `templates` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(250) CHARACTER SET ascii NOT NULL,
  `fieldgroups_id` int(10) unsigned NOT NULL DEFAULT '0',
  `flags` int(11) NOT NULL DEFAULT '0',
  `cache_time` mediumint(9) NOT NULL DEFAULT '0',
  `data` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `fieldgroups_id` (`fieldgroups_id`)
) ENGINE=MyISAM AUTO_INCREMENT=55 DEFAULT CHARSET=utf8mb4;

INSERT INTO `templates` (`id`, `name`, `fieldgroups_id`, `flags`, `cache_time`, `data`) VALUES('2', 'admin', '2', '8', '0', '{\"useRoles\":1,\"parentTemplates\":[2],\"allowPageNum\":1,\"redirectLogin\":23,\"slashUrls\":1,\"noGlobal\":1,\"compile\":3,\"modified\":1590404553,\"ns\":\"ProcessWire\"}');
INSERT INTO `templates` (`id`, `name`, `fieldgroups_id`, `flags`, `cache_time`, `data`) VALUES('3', 'user', '3', '8', '0', '{\"useRoles\":1,\"noChildren\":1,\"parentTemplates\":[2],\"slashUrls\":1,\"pageClass\":\"User\",\"noGlobal\":1,\"noMove\":1,\"noTrash\":1,\"noSettings\":1,\"noChangeTemplate\":1,\"nameContentTab\":1}');
INSERT INTO `templates` (`id`, `name`, `fieldgroups_id`, `flags`, `cache_time`, `data`) VALUES('4', 'role', '4', '8', '0', '{\"noChildren\":1,\"parentTemplates\":[2],\"slashUrls\":1,\"pageClass\":\"Role\",\"noGlobal\":1,\"noMove\":1,\"noTrash\":1,\"noSettings\":1,\"noChangeTemplate\":1,\"nameContentTab\":1}');
INSERT INTO `templates` (`id`, `name`, `fieldgroups_id`, `flags`, `cache_time`, `data`) VALUES('5', 'permission', '5', '8', '0', '{\"noChildren\":1,\"parentTemplates\":[2],\"slashUrls\":1,\"guestSearchable\":1,\"pageClass\":\"Permission\",\"noGlobal\":1,\"noMove\":1,\"noTrash\":1,\"noSettings\":1,\"noChangeTemplate\":1,\"nameContentTab\":1}');
INSERT INTO `templates` (`id`, `name`, `fieldgroups_id`, `flags`, `cache_time`, `data`) VALUES('1', 'home', '1', '0', '0', '{\"useRoles\":1,\"noParents\":1,\"slashUrls\":1,\"compile\":3,\"label\":\"Home\",\"modified\":1591460917,\"ns\":\"ProcessWire\",\"label1012\":\"Zuhause\",\"roles\":[37]}');
INSERT INTO `templates` (`id`, `name`, `fieldgroups_id`, `flags`, `cache_time`, `data`) VALUES('29', 'basic-page', '83', '0', '0', '{\"slashUrls\":1,\"compile\":3,\"label\":\"Basic Page\",\"modified\":1591460917,\"ns\":\"ProcessWire\",\"label1012\":\"Grund Seite\"}');
INSERT INTO `templates` (`id`, `name`, `fieldgroups_id`, `flags`, `cache_time`, `data`) VALUES('26', 'search', '80', '0', '0', '{\"noChildren\":1,\"noParents\":1,\"allowPageNum\":1,\"slashUrls\":1,\"compile\":3,\"label\":\"Search\",\"modified\":1591460917,\"ns\":\"ProcessWire\",\"label1012\":\"Suche\"}');
INSERT INTO `templates` (`id`, `name`, `fieldgroups_id`, `flags`, `cache_time`, `data`) VALUES('34', 'sitemap', '88', '0', '0', '{\"noChildren\":1,\"noParents\":1,\"redirectLogin\":23,\"slashUrls\":1,\"compile\":3,\"label\":\"Site Map\",\"modified\":1591460917,\"ns\":\"ProcessWire\",\"label1012\":\"Sitemap\"}');
INSERT INTO `templates` (`id`, `name`, `fieldgroups_id`, `flags`, `cache_time`, `data`) VALUES('43', 'language', '97', '8', '0', '{\"parentTemplates\":[2],\"slashUrls\":1,\"pageClass\":\"Language\",\"pageLabelField\":\"name\",\"noGlobal\":1,\"noMove\":1,\"noTrash\":1,\"noChangeTemplate\":1,\"noUnpublish\":1,\"nameContentTab\":1,\"modified\":1409651146}');
INSERT INTO `templates` (`id`, `name`, `fieldgroups_id`, `flags`, `cache_time`, `data`) VALUES('44', 'repeater_text_fields', '98', '8', '0', '{\"noChildren\":1,\"noParents\":1,\"slashUrls\":1,\"pageClass\":\"RepeaterPage\",\"noGlobal\":1,\"compile\":3,\"modified\":1587361673}');
INSERT INTO `templates` (`id`, `name`, `fieldgroups_id`, `flags`, `cache_time`, `data`) VALUES('45', 'repeater_simple_textblocks', '99', '8', '0', '{\"noChildren\":1,\"noParents\":1,\"slashUrls\":1,\"pageClass\":\"RepeaterPage\",\"noGlobal\":1,\"compile\":3,\"modified\":1587365694}');
INSERT INTO `templates` (`id`, `name`, `fieldgroups_id`, `flags`, `cache_time`, `data`) VALUES('46', 'repeater_repeater_why', '100', '8', '0', '{\"noChildren\":1,\"noParents\":1,\"slashUrls\":1,\"pageClass\":\"RepeaterPage\",\"noGlobal\":1,\"compile\":3,\"modified\":1587365874}');
INSERT INTO `templates` (`id`, `name`, `fieldgroups_id`, `flags`, `cache_time`, `data`) VALUES('47', 'board-page', '101', '0', '0', '{\"slashUrls\":1,\"compile\":3,\"modified\":1590404553,\"ns\":\"ProcessWire\"}');
INSERT INTO `templates` (`id`, `name`, `fieldgroups_id`, `flags`, `cache_time`, `data`) VALUES('48', 'board-overview-page', '102', '0', '0', '{\"slashUrls\":1,\"compile\":3,\"modified\":1590565816,\"ns\":\"ProcessWire\"}');
INSERT INTO `templates` (`id`, `name`, `fieldgroups_id`, `flags`, `cache_time`, `data`) VALUES('49', 'snipcart-cart', '103', '0', '0', '{\"noChildren\":1,\"noParents\":1,\"slashUrls\":1,\"pageLabelField\":\"fa-cog title\",\"compile\":3,\"label\":\"Snipcart Cart (System)\",\"tags\":\"Snipcart\",\"modified\":1587762238}');
INSERT INTO `templates` (`id`, `name`, `fieldgroups_id`, `flags`, `cache_time`, `data`) VALUES('50', 'snipcart-shop', '104', '0', '0', '{\"childTemplates\":[51,47],\"slashUrls\":1,\"pageLabelField\":\"fa-tags title\",\"compile\":3,\"label\":\"Snipcart Shop\",\"tags\":\"Snipcart\",\"modified\":1590566146,\"ns\":\"ProcessWire\"}');
INSERT INTO `templates` (`id`, `name`, `fieldgroups_id`, `flags`, `cache_time`, `data`) VALUES('51', 'snipcart-product', '105', '0', '0', '{\"noChildren\":1,\"parentTemplates\":[50],\"slashUrls\":1,\"pageLabelField\":\"fa-tag title\",\"compile\":3,\"label\":\"Snipcart Product\",\"tags\":\"Snipcart\",\"modified\":1591464236,\"ns\":\"ProcessWire\"}');
INSERT INTO `templates` (`id`, `name`, `fieldgroups_id`, `flags`, `cache_time`, `data`) VALUES('52', 'repeater_prod_variant_item', '106', '8', '0', '{\"noChildren\":1,\"noParents\":1,\"slashUrls\":1,\"pageClass\":\"RepeaterPage\",\"noGlobal\":1,\"compile\":3,\"modified\":1588051251}');
INSERT INTO `templates` (`id`, `name`, `fieldgroups_id`, `flags`, `cache_time`, `data`) VALUES('53', 'repeater_prod_variants', '107', '8', '0', '{\"noChildren\":1,\"noParents\":1,\"slashUrls\":1,\"pageClass\":\"RepeaterPage\",\"noGlobal\":1,\"compile\":3,\"modified\":1588051314}');
INSERT INTO `templates` (`id`, `name`, `fieldgroups_id`, `flags`, `cache_time`, `data`) VALUES('54', 'basic-page-twoes', '108', '0', '0', '{\"slashUrls\":1,\"compile\":3,\"modified\":1591459406,\"ns\":\"ProcessWire\"}');

DROP TABLE IF EXISTS `textformatter_video_embed`;
CREATE TABLE `textformatter_video_embed` (
  `video_id` varchar(128) NOT NULL,
  `embed_code` varchar(1024) NOT NULL DEFAULT '',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`video_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `textformatter_video_embed` (`video_id`, `embed_code`, `created`) VALUES('16evq_vySAc', '<iframe width=\"640\" height=\"360\" src=\"https://www.youtube.com/embed/16evq_vySAc?feature=oembed\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>', '2020-04-23 12:36:12');

# --- /WireDatabaseBackup {"numTables":80,"numCreateTables":80,"numInserts":1259,"numSeconds":1}