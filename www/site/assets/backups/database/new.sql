# --- WireDatabaseBackup {"time":"2021-03-07 17:28:47","user":"admin","dbName":"myDb","description":"","tables":[],"excludeTables":[],"excludeCreateTables":[],"excludeExportTables":[]}

DROP TABLE IF EXISTS `caches`;
CREATE TABLE `caches` (
  `name` varchar(250) NOT NULL,
  `data` mediumtext NOT NULL,
  `expires` datetime NOT NULL,
  PRIMARY KEY (`name`),
  KEY `expires` (`expires`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('Modules.wire/modules/', 'Image/ImageSizerEngineAnimatedGif/ImageSizerEngineAnimatedGif.module\nImage/ImageSizerEngineIMagick/ImageSizerEngineIMagick.module\nLanguageSupport/FieldtypeTextareaLanguage.module\nLanguageSupport/LanguageTabs.module\nLanguageSupport/LanguageSupport.module\nLanguageSupport/ProcessLanguage.module\nLanguageSupport/LanguageSupportPageNames.module\nLanguageSupport/FieldtypePageTitleLanguage.module\nLanguageSupport/ProcessLanguageTranslator.module\nLanguageSupport/FieldtypeTextLanguage.module\nLanguageSupport/LanguageSupportFields.module\nSession/SessionLoginThrottle/SessionLoginThrottle.module\nSession/SessionHandlerDB/ProcessSessionDB.module\nSession/SessionHandlerDB/SessionHandlerDB.module\nPageRender.module\nLazyCron.module\nTextformatter/TextformatterNewlineBR.module\nTextformatter/TextformatterEntities.module\nTextformatter/TextformatterMarkdownExtra/TextformatterMarkdownExtra.module\nTextformatter/TextformatterNewlineUL.module\nTextformatter/TextformatterSmartypants/TextformatterSmartypants.module\nTextformatter/TextformatterStripTags.module\nTextformatter/TextformatterPstripper.module\nPagePaths.module\nMarkup/MarkupHTMLPurifier/MarkupHTMLPurifier.module\nMarkup/MarkupAdminDataTable/MarkupAdminDataTable.module\nMarkup/MarkupCache.module\nMarkup/MarkupPageFields.module\nMarkup/MarkupPagerNav/MarkupPagerNav.module\nMarkup/MarkupPageArray.module\nMarkup/MarkupRSS.module\nInputfield/InputfieldPage/InputfieldPage.module\nInputfield/InputfieldSelect.module\nInputfield/InputfieldPageName/InputfieldPageName.module\nInputfield/InputfieldPageAutocomplete/InputfieldPageAutocomplete.module\nInputfield/InputfieldPageListSelect/InputfieldPageListSelect.module\nInputfield/InputfieldPageListSelect/InputfieldPageListSelectMultiple.module\nInputfield/InputfieldAsmSelect/InputfieldAsmSelect.module\nInputfield/InputfieldButton.module\nInputfield/InputfieldFieldset.module\nInputfield/InputfieldEmail.module\nInputfield/InputfieldFloat.module\nInputfield/InputfieldCKEditor/InputfieldCKEditor.module\nInputfield/InputfieldText.module\nInputfield/InputfieldRadios/InputfieldRadios.module\nInputfield/InputfieldSelector/InputfieldSelector.module\nInputfield/InputfieldCheckbox.module\nInputfield/InputfieldCheckboxes/InputfieldCheckboxes.module\nInputfield/InputfieldPageTitle/InputfieldPageTitle.module\nInputfield/InputfieldPageTable/InputfieldPageTable.module\nInputfield/InputfieldToggle/InputfieldToggle.module\nInputfield/InputfieldForm.module\nInputfield/InputfieldURL.module\nInputfield/InputfieldName.module\nInputfield/InputfieldSelectMultiple.module\nInputfield/InputfieldMarkup.module\nInputfield/InputfieldFile/InputfieldFile.module\nInputfield/InputfieldPassword/InputfieldPassword.module\nInputfield/InputfieldSubmit/InputfieldSubmit.module\nInputfield/InputfieldIcon/InputfieldIcon.module\nInputfield/InputfieldImage/InputfieldImage.module\nInputfield/InputfieldInteger.module\nInputfield/InputfieldTextarea.module\nInputfield/InputfieldDatetime/InputfieldDatetime.module\nInputfield/InputfieldHidden.module\nJquery/JqueryMagnific/JqueryMagnific.module\nJquery/JqueryTableSorter/JqueryTableSorter.module\nJquery/JqueryCore/JqueryCore.module\nJquery/JqueryWireTabs/JqueryWireTabs.module\nJquery/JqueryUI/JqueryUI.module\nPagePermissions.module\nPagePathHistory.module\nProcess/ProcessPageTrash.module\nProcess/ProcessPageAdd/ProcessPageAdd.module\nProcess/ProcessRecentPages/ProcessRecentPages.module\nProcess/ProcessPageList/ProcessPageList.module\nProcess/ProcessPageSearch/ProcessPageSearch.module\nProcess/ProcessPageEdit/ProcessPageEdit.module\nProcess/ProcessPagesExportImport/ProcessPagesExportImport.module\nProcess/ProcessPageClone.module\nProcess/ProcessCommentsManager/ProcessCommentsManager.module\nProcess/ProcessForgotPassword.module\nProcess/ProcessTemplate/ProcessTemplate.module\nProcess/ProcessUser/ProcessUser.module\nProcess/ProcessHome.module\nProcess/ProcessPageView.module\nProcess/ProcessLogger/ProcessLogger.module\nProcess/ProcessField/ProcessField.module\nProcess/ProcessModule/ProcessModule.module\nProcess/ProcessProfile/ProcessProfile.module\nProcess/ProcessPageEditLink/ProcessPageEditLink.module\nProcess/ProcessPermission/ProcessPermission.module\nProcess/ProcessPageEditImageSelect/ProcessPageEditImageSelect.module\nProcess/ProcessList.module\nProcess/ProcessPageType/ProcessPageType.module\nProcess/ProcessRole/ProcessRole.module\nProcess/ProcessPageLister/ProcessPageLister.module\nProcess/ProcessPageSort.module\nProcess/ProcessLogin/ProcessLogin.module\nPage/PageFrontEdit/PageFrontEdit.module\nFieldtype/FieldtypeSelector.module\nFieldtype/FieldtypeImage.module\nFieldtype/FieldtypeModule.module\nFieldtype/FieldtypeTextarea.module\nFieldtype/FieldtypeOptions/FieldtypeOptions.module\nFieldtype/FieldtypeFloat.module\nFieldtype/FieldtypeText.module\nFieldtype/FieldtypePage.module\nFieldtype/FieldtypeFile.module\nFieldtype/FieldtypeInteger.module\nFieldtype/FieldtypeToggle.module\nFieldtype/FieldtypeDatetime.module\nFieldtype/FieldtypeCache.module\nFieldtype/FieldtypePageTitle.module\nFieldtype/FieldtypeFieldsetTabOpen.module\nFieldtype/FieldtypeComments/FieldtypeComments.module\nFieldtype/FieldtypeComments/CommentFilterAkismet.module\nFieldtype/FieldtypeComments/InputfieldCommentsAdmin.module\nFieldtype/FieldtypePassword.module\nFieldtype/FieldtypeCheckbox.module\nFieldtype/FieldtypeFieldsetClose.module\nFieldtype/FieldtypeURL.module\nFieldtype/FieldtypeEmail.module\nFieldtype/FieldtypePageTable.module\nFieldtype/FieldtypeFieldsetOpen.module\nFieldtype/FieldtypeRepeater/FieldtypeRepeater.module\nFieldtype/FieldtypeRepeater/FieldtypeFieldsetPage.module\nFieldtype/FieldtypeRepeater/InputfieldRepeater.module\nSystem/SystemNotifications/SystemNotifications.module\nSystem/SystemNotifications/FieldtypeNotifications.module\nSystem/SystemUpdater/SystemUpdater.module\nAdminTheme/AdminThemeDefault/AdminThemeDefault.module\nAdminTheme/AdminThemeUikit/AdminThemeUikit.module\nAdminTheme/AdminThemeReno/AdminThemeReno.module\nFileCompilerTags.module', '2010-04-08 03:10:01');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__96914bd8798f3c5fb506774354a9f5b6', '{\"source\":{\"file\":\"\\/var\\/www\\/html\\/site\\/modules\\/FieldtypeColorPicker\\/FieldtypeColorPicker.module\",\"hash\":\"a5662dad93c52208b2bb67ee927704a9\",\"size\":4310,\"time\":1615122570,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/var\\/www\\/html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/FieldtypeColorPicker\\/FieldtypeColorPicker.module\",\"hash\":\"075fa213e401de3a823507bd005ec87f\",\"size\":4479,\"time\":1615122570}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__421e3b7f4b74778748c0c30d586811e0', '{\"source\":{\"file\":\"D:\\/workspace\\/boardschmiede\\/site\\/modules\\/DiagnosePhp\\/DiagnoseWebserver.module\",\"hash\":\"dff2512a4382810bd7c6ce2123472fb1\",\"size\":4413,\"time\":1587556382,\"ns\":\"\\\\\"},\"target\":{\"file\":\"D:\\/workspace\\/boardschmiede\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/DiagnosePhp\\/DiagnoseWebserver.module\",\"hash\":\"eb203ddbb51e22987a643e21b5eb5482\",\"size\":4605,\"time\":1587556382}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__ae14da2f1639603ae9d2703c82d4e5fb', '{\"source\":{\"file\":\"D:\\/workspace\\/boardschmiede\\/site\\/modules\\/DiagnosePhp\\/ProcessDiagnostics.module\",\"hash\":\"4560b510543cc143dfd2d7d481cd7deb\",\"size\":10814,\"time\":1587556382,\"ns\":\"\\\\\"},\"target\":{\"file\":\"D:\\/workspace\\/boardschmiede\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/DiagnosePhp\\/ProcessDiagnostics.module\",\"hash\":\"e1612e7268b43349dcebe13459faf147\",\"size\":11021,\"time\":1587556382}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('Modules.site/modules/', 'DiagnosePhp/DiagnoseModules.module\nDiagnosePhp/DiagnoseExample.module\nDiagnosePhp/ProcessDiagnostics.module\nDiagnosePhp/DiagnoseWebserver.module\nDiagnosePhp/DiagnoseImagehandling.module\nDiagnosePhp/DiagnoseDatabase.module\nDiagnosePhp/DiagnoseFiles.module\nDiagnosePhp/DiagnosePhp.module\nTextformatterVideoEmbed/TextformatterVideoEmbed.module\nFieldtypeColorPicker/InputfieldColorPicker.module\nFieldtypeColorPicker/FieldtypeColorPicker.module\nHelloworld/Helloworld.module\nCroppableImage3/InputfieldCroppableImage3/InputfieldCroppableImage3.module\nCroppableImage3/CroppableImage3.module\nCroppableImage3/ProcessCroppableImage3/ProcessCroppableImage3.module\nCroppableImage3/FieldtypeCroppableImage3/FieldtypeCroppableImage3.module\nProcessDatabaseBackups/ProcessDatabaseBackups.module\nSnipWire/ProcessSnipWire/ProcessSnipWire.module.php\nSnipWire/SnipWire.module.php\nSnipWire/MarkupSnipWire/MarkupSnipWire.module.php\nSnipWire/FieldtypeSnipWireTaxSelector/FieldtypeSnipWireTaxSelector.module.php', '2010-04-08 03:10:01');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__5ae34ac82eb8933d4f53f0884eb122a1', '{\"source\":{\"file\":\"D:\\/workspace\\/boardschmiede\\/site\\/templates\\/admin.php\",\"hash\":\"9636f854995462a4cb877cb1204bc2fe\",\"size\":467,\"time\":1577984776,\"ns\":\"ProcessWire\"},\"target\":{\"file\":\"D:\\/workspace\\/boardschmiede\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/admin.php\",\"hash\":\"9636f854995462a4cb877cb1204bc2fe\",\"size\":467,\"time\":1577984776}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__df6816735208d9d77e71d4a6321bb79b', '{\"source\":{\"file\":\"D:\\/workspace\\/boardschmiede\\/site\\/modules\\/DiagnosePhp\\/DiagnoseDatabase.module\",\"hash\":\"c896ad8a79fd6d265db22845c46189ea\",\"size\":9660,\"time\":1587556382,\"ns\":\"\\\\\"},\"target\":{\"file\":\"D:\\/workspace\\/boardschmiede\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/DiagnosePhp\\/DiagnoseDatabase.module\",\"hash\":\"b3d567b03954855a292489ff87499f72\",\"size\":9850,\"time\":1587556382}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__7dcd25b51b01ac74a219fc8ef52a56db', '{\"source\":{\"file\":\"D:\\/workspace\\/boardschmiede\\/site\\/templates\\/_init.php\",\"hash\":\"03d948499ec1c07d62a674daefd3ad67\",\"size\":1035,\"time\":1587762335,\"ns\":\"ProcessWire\"},\"target\":{\"file\":\"D:\\/workspace\\/boardschmiede\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/_init.php\",\"hash\":\"03d948499ec1c07d62a674daefd3ad67\",\"size\":1035,\"time\":1587762335}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__5959a832c0f8b39b1aa4d23d82ee12ec', '{\"source\":{\"file\":\"D:\\/workspace\\/boardschmiede\\/site\\/templates\\/_main.php\",\"hash\":\"d858f9dace62bdbfab0579f1a2b8edb5\",\"size\":2908,\"time\":1589622620,\"ns\":\"ProcessWire\"},\"target\":{\"file\":\"D:\\/workspace\\/boardschmiede\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/_main.php\",\"hash\":\"d858f9dace62bdbfab0579f1a2b8edb5\",\"size\":2908,\"time\":1589622620}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__e1018a841de9d71b642846de25c5ee36', '{\"source\":{\"file\":\"D:\\/workspace\\/boardschmiede\\/site\\/templates\\/home.php\",\"hash\":\"d907e145d2d589a8810e0c02334528b0\",\"size\":4516,\"time\":1587755630,\"ns\":\"ProcessWire\"},\"target\":{\"file\":\"D:\\/workspace\\/boardschmiede\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/home.php\",\"hash\":\"d907e145d2d589a8810e0c02334528b0\",\"size\":4516,\"time\":1587755630}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__90649f37a650d3a10bdc0274a1195ec5', '{\"source\":{\"file\":\"D:\\/workspace\\/boardschmiede\\/site\\/templates\\/basic-page.php\",\"hash\":\"343a5c586f1e72cafba78739f0acd525\",\"size\":969,\"time\":1587754342,\"ns\":\"ProcessWire\"},\"target\":{\"file\":\"D:\\/workspace\\/boardschmiede\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/basic-page.php\",\"hash\":\"343a5c586f1e72cafba78739f0acd525\",\"size\":969,\"time\":1587754342}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__b794072abe17c7ede759dbfec87990f3', '{\"source\":{\"file\":\"D:\\/workspace\\/boardschmiede\\/site\\/modules\\/DiagnosePhp\\/DiagnoseModules.module\",\"hash\":\"9541494d63373f3c5df290126ddf4359\",\"size\":6597,\"time\":1587556382,\"ns\":\"\\\\\"},\"target\":{\"file\":\"D:\\/workspace\\/boardschmiede\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/DiagnosePhp\\/DiagnoseModules.module\",\"hash\":\"385ac5bb67dfd52de1ef8e49971f170e\",\"size\":6889,\"time\":1587556382}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__e1107e52278cf85427af9a0d145c7a53', '{\"source\":{\"file\":\"D:\\/workspace\\/boardschmiede\\/site\\/modules\\/DiagnosePhp\\/DiagnosePhp.module\",\"hash\":\"b36f04d3ae3e0a63753a40f40d68035d\",\"size\":8319,\"time\":1587556382,\"ns\":\"\\\\\"},\"target\":{\"file\":\"D:\\/workspace\\/boardschmiede\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/DiagnosePhp\\/DiagnosePhp.module\",\"hash\":\"4b32a4ab728343fb7e71e26cd2240b78\",\"size\":8499,\"time\":1587556382}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__20977776d24cd3dea39e9ba8ce3d0246', '{\"source\":{\"file\":\"D:\\/workspace\\/boardschmiede\\/site\\/templates\\/board-overview-page.php\",\"hash\":\"dc09884f99c596788ae7ccb7c69db1cc\",\"size\":2002,\"time\":1587764973,\"ns\":\"ProcessWire\"},\"target\":{\"file\":\"D:\\/workspace\\/boardschmiede\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/board-overview-page.php\",\"hash\":\"dc09884f99c596788ae7ccb7c69db1cc\",\"size\":2002,\"time\":1587764973}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__c5c7ce541a2ab620a161df216a462e7b', '{\"source\":{\"file\":\"D:\\/workspace\\/boardschmiede\\/site\\/modules\\/DiagnosePhp\\/DiagnoseExample.module\",\"hash\":\"cdaa285c8c566fe75265d4b47d337ffc\",\"size\":1476,\"time\":1587556382,\"ns\":\"\\\\\"},\"target\":{\"file\":\"D:\\/workspace\\/boardschmiede\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/DiagnosePhp\\/DiagnoseExample.module\",\"hash\":\"4096eb00a3c2570f06fb3239363809f4\",\"size\":1664,\"time\":1587556382}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__f3cca45a1191a9540ac8152dd3f20051', '{\"source\":{\"file\":\"D:\\/workspace\\/boardschmiede\\/site\\/modules\\/DiagnosePhp\\/DiagnoseFiles.module\",\"hash\":\"c0bbb0d9ddb27f412461f26cd1a1ef11\",\"size\":6110,\"time\":1587556382,\"ns\":\"\\\\\"},\"target\":{\"file\":\"D:\\/workspace\\/boardschmiede\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/DiagnosePhp\\/DiagnoseFiles.module\",\"hash\":\"96a1b363de5e0cccb7ac0940f3faf344\",\"size\":6294,\"time\":1587556382}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__c82002e99d4ffa99e968da946c80c8b1', '{\"source\":{\"file\":\"D:\\/workspace\\/boardschmiede\\/site\\/modules\\/DiagnosePhp\\/DiagnoseImagehandling.module\",\"hash\":\"7dd8fa804558f80a390773c9ba82aa29\",\"size\":6577,\"time\":1587556382,\"ns\":\"\\\\\"},\"target\":{\"file\":\"D:\\/workspace\\/boardschmiede\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/DiagnosePhp\\/DiagnoseImagehandling.module\",\"hash\":\"9f23df0ea1a1b64624700726868fcca0\",\"size\":6777,\"time\":1587556382}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__baffbabf1acbc47fe13788735b158c1e', '{\"source\":{\"file\":\"D:\\/workspace\\/boardschmiede\\/site\\/templates\\/board-page.php\",\"hash\":\"62efedd3f9a105fd78f3ba98bf7bf6b9\",\"size\":2337,\"time\":1587763911,\"ns\":\"ProcessWire\"},\"target\":{\"file\":\"D:\\/workspace\\/boardschmiede\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/board-page.php\",\"hash\":\"62efedd3f9a105fd78f3ba98bf7bf6b9\",\"size\":2337,\"time\":1587763911}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__08d2d0b637039fcf971af74cf221aa14', '{\"source\":{\"file\":\"\\/var\\/www\\/html\\/site\\/modules\\/DiagnosePhp\\/DiagnoseImagehandling.module\",\"hash\":\"7dd8fa804558f80a390773c9ba82aa29\",\"size\":6577,\"time\":1611219107,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/var\\/www\\/html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/DiagnosePhp\\/DiagnoseImagehandling.module\",\"hash\":\"f68ae20a082c18ebc0062dac893912c3\",\"size\":6751,\"time\":1611219107}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__dc7cea36f468e9fca8e2f46ffcc15a77', '{\"source\":{\"file\":\"\\/var\\/www\\/html\\/site\\/modules\\/DiagnosePhp\\/DiagnoseDatabase.module\",\"hash\":\"c896ad8a79fd6d265db22845c46189ea\",\"size\":9660,\"time\":1611219107,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/var\\/www\\/html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/DiagnosePhp\\/DiagnoseDatabase.module\",\"hash\":\"b8997138e09eea4cd8fd97e0bb9cb7bd\",\"size\":9824,\"time\":1611219107}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__28e37a78fe9a06a874481ff7dbe21dae', '{\"source\":{\"file\":\"\\/var\\/www\\/html\\/site\\/modules\\/DiagnosePhp\\/DiagnoseExample.module\",\"hash\":\"cdaa285c8c566fe75265d4b47d337ffc\",\"size\":1476,\"time\":1611219107,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/var\\/www\\/html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/DiagnosePhp\\/DiagnoseExample.module\",\"hash\":\"ffe1a583f9658c72297f796883fced66\",\"size\":1638,\"time\":1611219107}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__beee426143b91013fd0aa871bfca1609', '{\"source\":{\"file\":\"\\/var\\/www\\/html\\/site\\/modules\\/DiagnosePhp\\/DiagnoseFiles.module\",\"hash\":\"c0bbb0d9ddb27f412461f26cd1a1ef11\",\"size\":6110,\"time\":1611219107,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/var\\/www\\/html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/DiagnosePhp\\/DiagnoseFiles.module\",\"hash\":\"73d2184b1128f17fddaeca02017f6cd5\",\"size\":6268,\"time\":1611219107}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__16122b191f15b11469d30519ad824976', '{\"source\":{\"file\":\"\\/var\\/www\\/html\\/site\\/modules\\/DiagnosePhp\\/DiagnoseModules.module\",\"hash\":\"9541494d63373f3c5df290126ddf4359\",\"size\":6597,\"time\":1611219107,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/var\\/www\\/html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/DiagnosePhp\\/DiagnoseModules.module\",\"hash\":\"b87343fbee5f9cce6ea2ce91f741e6ac\",\"size\":6863,\"time\":1611219107}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__598541cb13523f3b812773211c09272a', '{\"source\":{\"file\":\"\\/var\\/www\\/html\\/site\\/modules\\/DiagnosePhp\\/DiagnosePhp.module\",\"hash\":\"b36f04d3ae3e0a63753a40f40d68035d\",\"size\":8319,\"time\":1611219107,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/var\\/www\\/html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/DiagnosePhp\\/DiagnosePhp.module\",\"hash\":\"571729220ee48a626b5fa323d08e2694\",\"size\":8473,\"time\":1611219107}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__94db96b2d46b7521f4bbb88cdf429b1e', '{\"source\":{\"file\":\"\\/var\\/www\\/html\\/site\\/modules\\/DiagnosePhp\\/DiagnoseWebserver.module\",\"hash\":\"dff2512a4382810bd7c6ce2123472fb1\",\"size\":4413,\"time\":1611219107,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/var\\/www\\/html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/DiagnosePhp\\/DiagnoseWebserver.module\",\"hash\":\"9c0878817a4cbeb492ddfd29e923e628\",\"size\":4579,\"time\":1611219107}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('ModulesVerbose.info', '{\"164\":{\"summary\":\"Field that stores a multiple lines of text in multiple languages\",\"core\":true,\"versionStr\":\"1.0.0\"},\"158\":{\"summary\":\"ProcessWire multi-language support.\",\"author\":\"Ryan Cramer\",\"core\":true,\"versionStr\":\"1.0.3\"},\"166\":{\"summary\":\"Organizes multi-language fields into tabs for a cleaner easier to use interface.\",\"author\":\"adamspruijt, ryan\",\"core\":true,\"versionStr\":\"1.1.4\"},\"159\":{\"summary\":\"Manage system languages\",\"author\":\"Ryan Cramer\",\"core\":true,\"versionStr\":\"1.0.3\",\"permissions\":{\"lang-edit\":\"Administer languages and static translation files\"}},\"163\":{\"summary\":\"Field that stores a page title in multiple languages. Use this only if you want title inputs created for ALL languages on ALL pages. Otherwise create separate languaged-named title fields, i.e. title_fr, title_es, title_fi, etc. \",\"author\":\"Ryan Cramer\",\"core\":true,\"versionStr\":\"1.0.0\"},\"160\":{\"summary\":\"Provides language translation capabilities for ProcessWire core and modules.\",\"author\":\"Ryan Cramer\",\"core\":true,\"versionStr\":\"1.0.1\"},\"162\":{\"summary\":\"Field that stores a single line of text in multiple languages\",\"core\":true,\"versionStr\":\"1.0.0\"},\"161\":{\"summary\":\"Required to use multi-language fields.\",\"author\":\"Ryan Cramer\",\"core\":true,\"versionStr\":\"1.0.0\"},\"165\":{\"summary\":\"Required to use multi-language page names.\",\"author\":\"Ryan Cramer\",\"core\":true,\"versionStr\":\"0.1.0\"},\"125\":{\"summary\":\"Throttles login attempts to help prevent dictionary attacks.\",\"core\":true,\"versionStr\":\"1.0.3\"},\"115\":{\"summary\":\"Adds a render method to Page and caches page output.\",\"core\":true,\"versionStr\":\"1.0.5\"},\"61\":{\"summary\":\"Entity encode ampersands, quotes (single and double) and greater-than\\/less-than signs using htmlspecialchars(str, ENT_QUOTES). It is recommended that you use this on all text\\/textarea fields except those using a rich text editor or a markup language like Markdown.\",\"core\":true,\"versionStr\":\"1.0.0\"},\"156\":{\"summary\":\"Front-end to the HTML Purifier library.\",\"core\":true,\"versionStr\":\"4.9.5\"},\"67\":{\"summary\":\"Generates markup for data tables used by ProcessWire admin\",\"core\":true,\"versionStr\":\"1.0.7\"},\"98\":{\"summary\":\"Generates markup for pagination navigation\",\"core\":true,\"versionStr\":\"1.0.5\"},\"113\":{\"summary\":\"Adds renderPager() method to all PaginatedArray types, for easy pagination output. Plus a render() method to PageArray instances.\",\"core\":true,\"versionStr\":\"1.0.0\"},\"60\":{\"summary\":\"Select one or more pages\",\"core\":true,\"versionStr\":\"1.0.7\"},\"36\":{\"summary\":\"Selection of a single value from a select pulldown\",\"core\":true,\"versionStr\":\"1.0.2\"},\"86\":{\"summary\":\"Text input validated as a ProcessWire Page name field\",\"core\":true,\"versionStr\":\"1.0.6\"},\"187\":{\"summary\":\"Multiple Page selection using auto completion and sorting capability. Intended for use as an input field for Page reference fields.\",\"core\":true,\"versionStr\":\"1.1.2\"},\"15\":{\"summary\":\"Selection of a single page from a ProcessWire page tree list\",\"core\":true,\"versionStr\":\"1.0.1\"},\"137\":{\"summary\":\"Selection of multiple pages from a ProcessWire page tree list\",\"core\":true,\"versionStr\":\"1.0.2\"},\"25\":{\"summary\":\"Multiple selection, progressive enhancement to select multiple\",\"core\":true,\"versionStr\":\"2.0.2\"},\"131\":{\"summary\":\"Form button element that you can optionally pass an href attribute to.\",\"core\":true,\"versionStr\":\"1.0.0\"},\"78\":{\"summary\":\"Groups one or more fields together in a container\",\"core\":true,\"versionStr\":\"1.0.1\"},\"80\":{\"summary\":\"E-Mail address in valid format\",\"core\":true,\"versionStr\":\"1.0.1\"},\"90\":{\"summary\":\"Floating point number with precision\",\"core\":true,\"versionStr\":\"1.0.4\"},\"155\":{\"summary\":\"CKEditor textarea rich text editor.\",\"core\":true,\"versionStr\":\"1.6.3\"},\"34\":{\"summary\":\"Single line of text\",\"core\":true,\"versionStr\":\"1.0.6\"},\"39\":{\"summary\":\"Radio buttons for selection of a single item\",\"core\":true,\"versionStr\":\"1.0.5\"},\"149\":{\"summary\":\"Build a page finding selector visually.\",\"author\":\"Avoine + ProcessWire\",\"core\":true,\"versionStr\":\"0.2.8\"},\"37\":{\"summary\":\"Single checkbox toggle\",\"core\":true,\"versionStr\":\"1.0.6\"},\"38\":{\"summary\":\"Multiple checkbox toggles\",\"core\":true,\"versionStr\":\"1.0.7\"},\"112\":{\"summary\":\"Handles input of Page Title and auto-generation of Page Name (when name is blank)\",\"core\":true,\"versionStr\":\"1.0.2\"},\"30\":{\"summary\":\"Contains one or more fields in a form\",\"core\":true,\"versionStr\":\"1.0.7\"},\"108\":{\"summary\":\"URL in valid format\",\"core\":true,\"versionStr\":\"1.0.2\"},\"41\":{\"summary\":\"Text input validated as a ProcessWire name field\",\"core\":true,\"versionStr\":\"1.0.0\"},\"43\":{\"summary\":\"Select multiple items from a list\",\"core\":true,\"versionStr\":\"1.0.1\"},\"79\":{\"summary\":\"Contains any other markup and optionally child Inputfields\",\"core\":true,\"versionStr\":\"1.0.2\"},\"55\":{\"summary\":\"One or more file uploads (sortable)\",\"core\":true,\"versionStr\":\"1.2.6\"},\"122\":{\"summary\":\"Password input with confirmation field that doesn&#039;t ever echo the input back.\",\"core\":true,\"versionStr\":\"1.0.2\"},\"32\":{\"summary\":\"Form submit button\",\"core\":true,\"versionStr\":\"1.0.2\"},\"171\":{\"summary\":\"Select an icon\",\"core\":true,\"versionStr\":\"0.0.2\"},\"56\":{\"summary\":\"One or more image uploads (sortable)\",\"core\":true,\"versionStr\":\"1.2.3\"},\"85\":{\"summary\":\"Integer (positive or negative)\",\"core\":true,\"versionStr\":\"1.0.5\"},\"35\":{\"summary\":\"Multiple lines of text\",\"core\":true,\"versionStr\":\"1.0.3\"},\"94\":{\"summary\":\"Inputfield that accepts date and optionally time\",\"core\":true,\"versionStr\":\"1.0.7\"},\"40\":{\"summary\":\"Hidden value in a form\",\"core\":true,\"versionStr\":\"1.0.1\"},\"151\":{\"summary\":\"Provides lightbox capability for image galleries. Replacement for FancyBox. Uses Magnific Popup by @dimsemenov.\",\"href\":\"http:\\/\\/dimsemenov.com\\/plugins\\/magnific-popup\\/\",\"core\":true,\"versionStr\":\"0.0.1\"},\"103\":{\"summary\":\"Provides a jQuery plugin for sorting tables.\",\"href\":\"http:\\/\\/mottie.github.io\\/tablesorter\\/\",\"core\":true,\"versionStr\":\"2.2.1\"},\"116\":{\"summary\":\"jQuery Core as required by ProcessWire Admin and plugins\",\"href\":\"http:\\/\\/jquery.com\",\"core\":true,\"versionStr\":\"1.8.3\"},\"45\":{\"summary\":\"Provides a jQuery plugin for generating tabs in ProcessWire.\",\"core\":true,\"versionStr\":\"1.1.0\"},\"117\":{\"summary\":\"jQuery UI as required by ProcessWire and plugins\",\"href\":\"http:\\/\\/ui.jquery.com\",\"core\":true,\"versionStr\":\"1.9.6\"},\"114\":{\"summary\":\"Adds various permission methods to Page objects that are used by Process modules.\",\"core\":true,\"versionStr\":\"1.0.5\"},\"152\":{\"summary\":\"Keeps track of past URLs where pages have lived and automatically redirects (301 permament) to the new location whenever the past URL is accessed.\",\"core\":true,\"versionStr\":\"0.0.5\"},\"109\":{\"summary\":\"Handles emptying of Page trash\",\"core\":true,\"versionStr\":\"1.0.3\"},\"17\":{\"summary\":\"Add a new page\",\"core\":true,\"versionStr\":\"1.0.8\"},\"168\":{\"summary\":\"Shows a list of recently edited pages in your admin.\",\"author\":\"Ryan Cramer\",\"href\":\"http:\\/\\/modules.processwire.com\\/\",\"core\":true,\"versionStr\":\"0.0.2\",\"permissions\":{\"page-edit-recent\":\"Can see recently edited pages\"},\"page\":{\"name\":\"recent-pages\",\"parent\":\"page\",\"title\":\"Recent\"}},\"12\":{\"summary\":\"List pages in a hierarchical tree structure\",\"core\":true,\"versionStr\":\"1.2.2\"},\"104\":{\"summary\":\"Provides a page search engine for admin use.\",\"core\":true,\"versionStr\":\"1.0.6\"},\"7\":{\"summary\":\"Edit a Page\",\"core\":true,\"versionStr\":\"1.0.9\"},\"47\":{\"summary\":\"List and edit the templates that control page output\",\"core\":true,\"versionStr\":\"1.1.4\",\"searchable\":\"templates\"},\"66\":{\"summary\":\"Manage system users\",\"core\":true,\"versionStr\":\"1.0.7\",\"searchable\":\"users\"},\"87\":{\"summary\":\"Acts as a placeholder Process for the admin root. Ensures proper flow control after login.\",\"core\":true,\"versionStr\":\"1.0.1\"},\"83\":{\"summary\":\"All page views are routed through this Process\",\"core\":true,\"versionStr\":\"1.0.4\"},\"170\":{\"summary\":\"View and manage system logs.\",\"author\":\"Ryan Cramer\",\"core\":true,\"versionStr\":\"0.0.2\",\"permissions\":{\"logs-view\":\"Can view system logs\",\"logs-edit\":\"Can manage system logs\"},\"page\":{\"name\":\"logs\",\"parent\":\"setup\",\"title\":\"Logs\"}},\"48\":{\"summary\":\"Edit individual fields that hold page data\",\"core\":true,\"versionStr\":\"1.1.3\",\"searchable\":\"fields\"},\"50\":{\"summary\":\"List, edit or install\\/uninstall modules\",\"core\":true,\"versionStr\":\"1.1.9\"},\"138\":{\"summary\":\"Enables user to change their password, email address and other settings that you define.\",\"core\":true,\"versionStr\":\"1.0.4\"},\"121\":{\"summary\":\"Provides a link capability as used by some Fieldtype modules (like rich text editors).\",\"core\":true,\"versionStr\":\"1.0.8\"},\"136\":{\"summary\":\"Manage system permissions\",\"core\":true,\"versionStr\":\"1.0.1\"},\"129\":{\"summary\":\"Provides image manipulation functions for image fields and rich text editors.\",\"core\":true,\"versionStr\":\"1.2.0\"},\"76\":{\"summary\":\"Lists the Process assigned to each child page of the current\",\"core\":true,\"versionStr\":\"1.0.1\"},\"134\":{\"summary\":\"List, Edit and Add pages of a specific type\",\"core\":true,\"versionStr\":\"1.0.1\"},\"68\":{\"summary\":\"Manage user roles and what permissions are attached\",\"core\":true,\"versionStr\":\"1.0.4\"},\"150\":{\"summary\":\"Admin tool for finding and listing pages by any property.\",\"author\":\"Ryan Cramer\",\"core\":true,\"versionStr\":\"0.2.6\",\"permissions\":{\"page-lister\":\"Use Page Lister\"}},\"14\":{\"summary\":\"Handles page sorting and moving for PageList\",\"core\":true,\"versionStr\":\"1.0.0\"},\"10\":{\"summary\":\"Login to ProcessWire\",\"core\":true,\"versionStr\":\"1.0.8\"},\"57\":{\"summary\":\"Field that stores one or more GIF, JPG, or PNG images\",\"core\":true,\"versionStr\":\"1.0.2\"},\"27\":{\"summary\":\"Field that stores a reference to another module\",\"core\":true,\"versionStr\":\"1.0.1\"},\"1\":{\"summary\":\"Field that stores multiple lines of text\",\"core\":true,\"versionStr\":\"1.0.7\"},\"181\":{\"summary\":\"Field that stores single and multi select options.\",\"core\":true,\"versionStr\":\"0.0.1\"},\"89\":{\"summary\":\"Field that stores a floating point (decimal) number\",\"core\":true,\"versionStr\":\"1.0.6\"},\"3\":{\"summary\":\"Field that stores a single line of text\",\"core\":true,\"versionStr\":\"1.0.1\"},\"4\":{\"summary\":\"Field that stores one or more references to ProcessWire pages\",\"core\":true,\"versionStr\":\"1.0.5\"},\"6\":{\"summary\":\"Field that stores one or more files\",\"core\":true,\"versionStr\":\"1.0.6\"},\"84\":{\"summary\":\"Field that stores an integer\",\"core\":true,\"versionStr\":\"1.0.2\"},\"28\":{\"summary\":\"Field that stores a date and optionally time\",\"core\":true,\"versionStr\":\"1.0.5\"},\"111\":{\"summary\":\"Field that stores a page title\",\"core\":true,\"versionStr\":\"1.0.0\"},\"107\":{\"summary\":\"Open a fieldset to group fields. Same as Fieldset (Open) except that it displays in a tab instead.\",\"core\":true,\"versionStr\":\"1.0.0\"},\"133\":{\"summary\":\"Field that stores a hashed and salted password\",\"core\":true,\"versionStr\":\"1.0.1\"},\"97\":{\"summary\":\"This Fieldtype stores an ON\\/OFF toggle via a single checkbox. The ON value is 1 and OFF value is 0.\",\"core\":true,\"versionStr\":\"1.0.1\"},\"106\":{\"summary\":\"Close a fieldset opened by FieldsetOpen. \",\"core\":true,\"versionStr\":\"1.0.0\"},\"135\":{\"summary\":\"Field that stores a URL\",\"core\":true,\"versionStr\":\"1.0.1\"},\"29\":{\"summary\":\"Field that stores an e-mail address\",\"core\":true,\"versionStr\":\"1.0.1\"},\"105\":{\"summary\":\"Open a fieldset to group fields. Should be followed by a Fieldset (Close) after one or more fields.\",\"core\":true,\"versionStr\":\"1.0.1\"},\"172\":{\"summary\":\"Maintains a collection of fields that are repeated for any number of times.\",\"core\":true,\"versionStr\":\"1.0.6\"},\"173\":{\"summary\":\"Repeats fields from another template. Provides the input for FieldtypeRepeater.\",\"core\":true,\"versionStr\":\"1.0.6\"},\"139\":{\"summary\":\"Manages system versions and upgrades.\",\"core\":true,\"versionStr\":\"0.1.8\"},\"148\":{\"summary\":\"Minimal admin theme that supports all ProcessWire features.\",\"core\":true,\"versionStr\":\"0.1.4\"},\"169\":{\"summary\":\"Uikit v3 admin theme\",\"core\":true,\"versionStr\":\"0.3.1\"},\"180\":{\"summary\":\"Allows evaluation of run-time environment and detection of changes.\",\"author\":\"Stephen Dickinson, QBox\",\"versionStr\":\"0.2.3\"},\"179\":{\"summary\":\"Allows collection of image handling diagnostics\",\"author\":\"Horst\",\"versionStr\":\"0.0.1\"},\"182\":{\"summary\":\"Enter a full YouTube or Vimeo URL by itself in any paragraph (example: http:\\/\\/www.youtube.com\\/watch?v=Wl4XiYadV_k) and this will automatically convert it to an embedded video. This formatter is intended to be run on trusted input. Recommended for use with TinyMCE textarea fields.\",\"author\":\"Ryan Cramer\",\"href\":\"https:\\/\\/modules.processwire.com\\/modules\\/textformatter-video-embed\\/\",\"versionStr\":\"1.1.1\"},\"177\":{\"summary\":\"One or more image uploads (optional predefined crops)\",\"author\":\"Horst Nogajski\",\"versionStr\":\"1.2.0\"},\"175\":{\"summary\":\"One or more image uploads (optional predefined crops)\",\"author\":\"Horst Nogajski\",\"versionStr\":\"1.2.0\"},\"178\":{\"summary\":\"One or more image uploads (optional predefined crops)\",\"author\":\"Horst Nogajski\",\"versionStr\":\"1.2.0\",\"permissions\":{\"croppable-image-3\":\"Crop images with CroppableImage3\"}},\"176\":{\"summary\":\"One or more image uploads (optional predefined crops)\",\"author\":\"Horst Nogajski\",\"versionStr\":\"1.2.0\"},\"174\":{\"summary\":\"Create and\\/or restore database backups from ProcessWire admin.\",\"author\":\"Ryan Cramer\",\"versionStr\":\"0.0.5\",\"permissions\":{\"db-backup\":\"Manage database backups (recommended for superuser only)\"},\"page\":{\"name\":\"db-backups\",\"parent\":\"setup\",\"title\":\"DB Backups\"}},\"184\":{\"summary\":\"Snipcart dashboard integration for ProcessWire.\",\"author\":\"Martin Gartner\",\"versionStr\":\"0.8.6\",\"permissions\":{\"snipwire-dashboard\":\"Use the SnipWire Dashboard sections\"},\"page\":{\"name\":\"snipwire\",\"title\":\"SnipWire\",\"parent\":\"setup\"}},\"183\":{\"summary\":\"Full Snipcart shopping cart integration for ProcessWire.\",\"author\":\"Martin Gartner\",\"href\":\"https:\\/\\/processwire.com\\/talk\\/topic\\/21554-snipwire-snipcart-integration-for-processwire\\/\",\"versionStr\":\"0.8.6\"},\"185\":{\"summary\":\"Snipcart markup output for SnipWire.\",\"author\":\"Martin Gartner\",\"versionStr\":\"0.8.6\"},\"186\":{\"summary\":\"Fieldtype which fetches taxes setting from SnipWire module config and builds a dropdown list.\",\"author\":\"Martin Gartner\",\"versionStr\":\"0.8.6\"},\"188\":{\"summary\":\"Fieldtype that stores a HEX color or the value transp. Color can be picked using a jQuery ColorPicker Plugin by http:\\/\\/www.eyecon.ro\\/colorpicker\\/ or from a configurable color swatch.\",\"href\":\"http:\\/\\/processwire.com\\/talk\\/topic\\/865-module-colorpicker\\/page__gopid__7340#entry7340\",\"versionStr\":\"2.0.3\"},\"189\":{\"summary\":\"Choose your colors the easy way.\",\"href\":\"http:\\/\\/processwire.com\\/talk\\/topic\\/865-module-colorpicker\\/page__gopid__7340#entry7340\",\"versionStr\":\"2.0.3\"}}', '2010-04-08 03:10:01');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('Permissions.names', '{\"croppable-image-3\":1068,\"db-backup\":1049,\"logs-edit\":1019,\"logs-view\":1018,\"page-delete\":34,\"page-edit\":32,\"page-edit-recent\":1016,\"page-lister\":1006,\"page-lock\":54,\"page-move\":35,\"page-sort\":50,\"page-template\":51,\"page-view\":36,\"profile-edit\":53,\"snipwire-dashboard\":1075,\"user-admin\":52}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('ModulesUninstalled.info', '{\"ImageSizerEngineAnimatedGif\":{\"name\":\"ImageSizerEngineAnimatedGif\",\"title\":\"Animated GIF Image Sizer\",\"version\":1,\"versionStr\":\"0.0.1\",\"author\":\"Horst Nogajski\",\"summary\":\"Upgrades image manipulations for animated GIFs.\",\"created\":1611221487,\"installed\":false,\"configurable\":4,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"ImageSizerEngineIMagick\":{\"name\":\"ImageSizerEngineIMagick\",\"title\":\"IMagick Image Sizer\",\"version\":3,\"versionStr\":\"0.0.3\",\"author\":\"Horst Nogajski\",\"summary\":\"Upgrades image manipulations to use PHP\'s ImageMagick library when possible.\",\"created\":1611221487,\"installed\":false,\"configurable\":4,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"ProcessSessionDB\":{\"name\":\"ProcessSessionDB\",\"title\":\"Sessions\",\"version\":4,\"versionStr\":\"0.0.4\",\"summary\":\"Enables you to browse active database sessions.\",\"icon\":\"dashboard\",\"requiresVersions\":{\"SessionHandlerDB\":[\">=\",0]},\"created\":1611221487,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true,\"page\":{\"name\":\"sessions-db\",\"parent\":\"access\",\"title\":\"Sessions\"}},\"SessionHandlerDB\":{\"name\":\"SessionHandlerDB\",\"title\":\"Session Handler Database\",\"version\":5,\"versionStr\":\"0.0.5\",\"summary\":\"Installing this module makes ProcessWire store sessions in the database rather than the file system. Note that this module will log you out after install or uninstall.\",\"installs\":[\"ProcessSessionDB\"],\"created\":1611221487,\"installed\":false,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"LazyCron\":{\"name\":\"LazyCron\",\"title\":\"Lazy Cron\",\"version\":102,\"versionStr\":\"1.0.2\",\"summary\":\"Provides hooks that are automatically executed at various intervals. It is called \'lazy\' because it\'s triggered by a pageview, so the interval is guaranteed to be at least the time requested, rather than exactly the time requested. This is fine for most cases, but you can make it not lazy by connecting this to a real CRON job. See the module file for details. \",\"href\":\"https:\\/\\/processwire.com\\/api\\/modules\\/lazy-cron\\/\",\"autoload\":true,\"singular\":true,\"created\":1611221487,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"TextformatterNewlineBR\":{\"name\":\"TextformatterNewlineBR\",\"title\":\"Newlines to XHTML Line Breaks\",\"version\":100,\"versionStr\":\"1.0.0\",\"summary\":\"Converts newlines to XHTML line break <br \\/> tags. \",\"created\":1611221487,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"TextformatterMarkdownExtra\":{\"name\":\"TextformatterMarkdownExtra\",\"title\":\"Markdown\\/Parsedown Extra\",\"version\":130,\"versionStr\":\"1.3.0\",\"summary\":\"Markdown\\/Parsedown extra lightweight markup language by Emanuil Rusev. Based on Markdown by John Gruber.\",\"created\":1611221487,\"installed\":false,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"TextformatterNewlineUL\":{\"name\":\"TextformatterNewlineUL\",\"title\":\"Newlines to Unordered List\",\"version\":100,\"versionStr\":\"1.0.0\",\"summary\":\"Converts newlines to <li> list items and surrounds in an <ul> unordered list. \",\"created\":1611221487,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"TextformatterSmartypants\":{\"name\":\"TextformatterSmartypants\",\"title\":\"SmartyPants Typographer\",\"version\":171,\"versionStr\":\"1.7.1\",\"summary\":\"Smart typography for web sites, by Michel Fortin based on SmartyPants by John Gruber. If combined with Markdown, it should be applied AFTER Markdown.\",\"created\":1611221487,\"installed\":false,\"configurable\":4,\"namespace\":\"ProcessWire\\\\\",\"core\":true,\"url\":\"https:\\/\\/github.com\\/michelf\\/php-smartypants\"},\"TextformatterStripTags\":{\"name\":\"TextformatterStripTags\",\"title\":\"Strip Markup Tags\",\"version\":100,\"versionStr\":\"1.0.0\",\"summary\":\"Strips HTML\\/XHTML Markup Tags\",\"created\":1611221487,\"installed\":false,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"TextformatterPstripper\":{\"name\":\"TextformatterPstripper\",\"title\":\"Paragraph Stripper\",\"version\":100,\"versionStr\":\"1.0.0\",\"summary\":\"Strips paragraph <p> tags that may have been applied by other text formatters before it. \",\"created\":1611221487,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"PagePaths\":{\"name\":\"PagePaths\",\"title\":\"Page Paths\",\"version\":1,\"versionStr\":\"0.0.1\",\"summary\":\"Enables page paths\\/urls to be queryable by selectors. Also offers potential for improved load performance. Builds an index at install (may take time on a large site). Currently supports only single languages sites.\",\"autoload\":true,\"singular\":true,\"created\":1611221487,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"MarkupCache\":{\"name\":\"MarkupCache\",\"title\":\"Markup Cache\",\"version\":101,\"versionStr\":\"1.0.1\",\"summary\":\"A simple way to cache segments of markup in your templates. \",\"href\":\"https:\\/\\/processwire.com\\/api\\/modules\\/markupcache\\/\",\"autoload\":true,\"singular\":true,\"created\":1611221487,\"installed\":false,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"MarkupPageFields\":{\"name\":\"MarkupPageFields\",\"title\":\"Markup Page Fields\",\"version\":100,\"versionStr\":\"1.0.0\",\"summary\":\"Adds $page->renderFields() and $page->images->render() methods that return basic markup for output during development and debugging.\",\"autoload\":true,\"singular\":true,\"created\":1611221487,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true,\"permanent\":true},\"MarkupRSS\":{\"name\":\"MarkupRSS\",\"title\":\"Markup RSS Feed\",\"version\":103,\"versionStr\":\"1.0.3\",\"summary\":\"Renders an RSS feed. Given a PageArray, renders an RSS feed of them.\",\"created\":1611221487,\"installed\":false,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"InputfieldPageTable\":{\"name\":\"InputfieldPageTable\",\"title\":\"ProFields: Page Table\",\"version\":13,\"versionStr\":\"0.1.3\",\"summary\":\"Inputfield to accompany FieldtypePageTable\",\"requiresVersions\":{\"FieldtypePageTable\":[\">=\",0]},\"created\":1611221487,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"InputfieldToggle\":{\"name\":\"InputfieldToggle\",\"title\":\"Toggle\",\"version\":1,\"versionStr\":\"0.0.1\",\"summary\":\"A toggle providing similar input capability to a checkbox but much more configurable.\",\"created\":1611221487,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"ProcessPagesExportImport\":{\"name\":\"ProcessPagesExportImport\",\"title\":\"Pages Export\\/Import\",\"version\":1,\"versionStr\":\"0.0.1\",\"author\":\"Ryan Cramer\",\"summary\":\"Enables exporting and importing of pages. Development version, not yet recommended for production use.\",\"icon\":\"paper-plane-o\",\"permission\":\"page-edit-export\",\"created\":1611221487,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true,\"page\":{\"name\":\"export-import\",\"parent\":\"page\",\"title\":\"Export\\/Import\"}},\"ProcessPageClone\":{\"name\":\"ProcessPageClone\",\"title\":\"Page Clone\",\"version\":104,\"versionStr\":\"1.0.4\",\"summary\":\"Provides ability to clone\\/copy\\/duplicate pages in the admin. Adds a &quot;copy&quot; option to all applicable pages in the PageList.\",\"permission\":\"page-clone\",\"permissions\":{\"page-clone\":\"Clone a page\",\"page-clone-tree\":\"Clone a tree of pages\"},\"autoload\":\"template=admin\",\"created\":1611221487,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true,\"page\":{\"name\":\"clone\",\"title\":\"Clone\",\"parent\":\"page\",\"status\":1024}},\"ProcessCommentsManager\":{\"name\":\"ProcessCommentsManager\",\"title\":\"Comments\",\"version\":10,\"versionStr\":\"0.1.0\",\"author\":\"Ryan Cramer\",\"summary\":\"Manage comments in your site outside of the page editor.\",\"icon\":\"comments\",\"requiresVersions\":{\"FieldtypeComments\":[\">=\",0]},\"permission\":\"comments-manager\",\"permissions\":{\"comments-manager\":\"Use the comments manager\"},\"created\":1611221487,\"installed\":false,\"searchable\":\"comments\",\"namespace\":\"ProcessWire\\\\\",\"core\":true,\"page\":{\"name\":\"comments\",\"parent\":\"setup\",\"title\":\"Comments\"},\"nav\":[{\"url\":\"?go=approved\",\"label\":\"Approved\"},{\"url\":\"?go=pending\",\"label\":\"Pending\"},{\"url\":\"?go=spam\",\"label\":\"Spam\"},{\"url\":\"?go=all\",\"label\":\"All\"}]},\"ProcessForgotPassword\":{\"name\":\"ProcessForgotPassword\",\"title\":\"Forgot Password\",\"version\":103,\"versionStr\":\"1.0.3\",\"summary\":\"Provides password reset\\/email capability for the Login process.\",\"permission\":\"page-view\",\"created\":1611221487,\"installed\":false,\"configurable\":4,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"PageFrontEdit\":{\"name\":\"PageFrontEdit\",\"title\":\"Front-End Page Editor\",\"version\":3,\"versionStr\":\"0.0.3\",\"author\":\"Ryan Cramer\",\"summary\":\"Enables front-end editing of page fields.\",\"icon\":\"cube\",\"permissions\":{\"page-edit-front\":\"Use the front-end page editor\"},\"autoload\":true,\"created\":1611221487,\"installed\":false,\"configurable\":\"PageFrontEditConfig.php\",\"namespace\":\"ProcessWire\\\\\",\"core\":true,\"license\":\"MPL 2.0\"},\"FieldtypeSelector\":{\"name\":\"FieldtypeSelector\",\"title\":\"Selector\",\"version\":13,\"versionStr\":\"0.1.3\",\"author\":\"Avoine + ProcessWire\",\"summary\":\"Build a page finding selector visually.\",\"created\":1611221487,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"FieldtypeToggle\":{\"name\":\"FieldtypeToggle\",\"title\":\"Toggle (Yes\\/No)\",\"version\":1,\"versionStr\":\"0.0.1\",\"summary\":\"Configurable yes\\/no, on\\/off toggle alternative to a checkbox, plus optional \\u201cother\\u201d option.\",\"requiresVersions\":{\"InputfieldToggle\":[\">=\",0]},\"created\":1611221487,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"FieldtypeCache\":{\"name\":\"FieldtypeCache\",\"title\":\"Cache\",\"version\":102,\"versionStr\":\"1.0.2\",\"summary\":\"Caches the values of other fields for fewer runtime queries. Can also be used to combine multiple text fields and have them all be searchable under the cached field name.\",\"created\":1611221487,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"FieldtypeComments\":{\"name\":\"FieldtypeComments\",\"title\":\"Comments\",\"version\":108,\"versionStr\":\"1.0.8\",\"summary\":\"Field that stores user posted comments for a single Page\",\"installs\":[\"InputfieldCommentsAdmin\"],\"created\":1611221487,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"CommentFilterAkismet\":{\"name\":\"CommentFilterAkismet\",\"title\":\"Comment Filter: Akismet\",\"version\":200,\"versionStr\":\"2.0.0\",\"summary\":\"Uses the Akismet service to identify comment spam. Module plugin for the Comments Fieldtype.\",\"requiresVersions\":{\"FieldtypeComments\":[\">=\",0]},\"created\":1611221487,\"installed\":false,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"InputfieldCommentsAdmin\":{\"name\":\"InputfieldCommentsAdmin\",\"title\":\"Comments Admin\",\"version\":104,\"versionStr\":\"1.0.4\",\"summary\":\"Provides an administrative interface for working with comments\",\"requiresVersions\":{\"FieldtypeComments\":[\">=\",0]},\"created\":1611221487,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"FieldtypePageTable\":{\"name\":\"FieldtypePageTable\",\"title\":\"ProFields: Page Table\",\"version\":8,\"versionStr\":\"0.0.8\",\"summary\":\"A fieldtype containing a group of editable pages.\",\"installs\":[\"InputfieldPageTable\"],\"autoload\":true,\"created\":1611221487,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"FieldtypeFieldsetPage\":{\"name\":\"FieldtypeFieldsetPage\",\"title\":\"Fieldset (Page)\",\"version\":1,\"versionStr\":\"0.0.1\",\"summary\":\"Fieldset with fields isolated to separate namespace (page), enabling re-use of fields.\",\"requiresVersions\":{\"FieldtypeRepeater\":[\">=\",0]},\"autoload\":true,\"created\":1611221487,\"installed\":false,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"SystemNotifications\":{\"name\":\"SystemNotifications\",\"title\":\"System Notifications\",\"version\":12,\"versionStr\":\"0.1.2\",\"summary\":\"Adds support for notifications in ProcessWire (currently in development)\",\"icon\":\"bell\",\"installs\":[\"FieldtypeNotifications\"],\"autoload\":true,\"created\":1611221487,\"installed\":false,\"configurable\":\"SystemNotificationsConfig.php\",\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"FieldtypeNotifications\":{\"name\":\"FieldtypeNotifications\",\"title\":\"Notifications\",\"version\":4,\"versionStr\":\"0.0.4\",\"summary\":\"Field that stores user notifications.\",\"requiresVersions\":{\"SystemNotifications\":[\">=\",0]},\"created\":1611221487,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"AdminThemeReno\":{\"name\":\"AdminThemeReno\",\"title\":\"Reno\",\"version\":17,\"versionStr\":\"0.1.7\",\"author\":\"Tom Reno (Renobird)\",\"summary\":\"Admin theme for ProcessWire 2.5+ by Tom Reno (Renobird)\",\"requiresVersions\":{\"AdminThemeDefault\":[\">=\",0]},\"autoload\":\"template=admin\",\"created\":1611221487,\"installed\":false,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"FileCompilerTags\":{\"name\":\"FileCompilerTags\",\"title\":\"Tags File Compiler\",\"version\":1,\"versionStr\":\"0.0.1\",\"summary\":\"Enables {var} or {var.property} variables in markup sections of a file. Can be used with any API variable.\",\"created\":1611221487,\"installed\":false,\"configurable\":4,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"DiagnoseModules\":{\"name\":\"DiagnoseModules\",\"title\":\"Module version checking\",\"version\":101,\"versionStr\":\"1.0.1\",\"author\":\"Nico Knoll\",\"summary\":\"Allows collection of module version diagnostics\",\"requiresVersions\":{\"ProcessDiagnostics\":[\">=\",0]},\"installs\":[\"ProcessDiagnostics\"],\"singular\":true,\"created\":1611219107,\"installed\":false,\"configurable\":true,\"namespace\":\"\\\\\"},\"DiagnoseExample\":{\"name\":\"DiagnoseExample\",\"title\":\"Example Diagnostics Provider\",\"version\":1,\"versionStr\":\"0.0.1\",\"author\":\"Stephen Dickinson, QBox\",\"summary\":\"Demonstrates how to write a simple diagnostics provider module\",\"requiresVersions\":{\"ProcessDiagnostics\":[\">=\",0]},\"installs\":[\"ProcessDiagnostics\"],\"singular\":true,\"created\":1611219107,\"installed\":false,\"namespace\":\"\\\\\"},\"DiagnoseWebserver\":{\"name\":\"DiagnoseWebserver\",\"title\":\"Webserver Diagnostics\",\"version\":2,\"versionStr\":\"0.0.2\",\"author\":\"Netcarver\",\"summary\":\"Allows collection of webserver diagnostics\",\"requiresVersions\":{\"ProcessDiagnostics\":[\">=\",0]},\"installs\":[\"ProcessDiagnostics\"],\"singular\":true,\"created\":1611219107,\"installed\":false,\"namespace\":\"\\\\\"},\"DiagnoseDatabase\":{\"name\":\"DiagnoseDatabase\",\"title\":\"Database Diagnostics\",\"version\":3,\"versionStr\":\"0.0.3\",\"author\":\"Stephen Dickinson, QBox\",\"summary\":\"Allows collection of database diagnostics\",\"requiresVersions\":{\"ProcessDiagnostics\":[\">=\",0]},\"installs\":[\"ProcessDiagnostics\"],\"singular\":true,\"created\":1611219107,\"installed\":false,\"namespace\":\"\\\\\"},\"DiagnoseFiles\":{\"name\":\"DiagnoseFiles\",\"title\":\"Filesystem Diagnostics\",\"version\":1,\"versionStr\":\"0.0.1\",\"author\":\"Stephen Dickinson, QBox.co\",\"summary\":\"Allows collection of file and directory diagnostics\",\"requiresVersions\":{\"ProcessDiagnostics\":[\">=\",0]},\"installs\":[\"ProcessDiagnostics\"],\"singular\":true,\"created\":1611219107,\"installed\":false,\"namespace\":\"\\\\\"},\"DiagnosePhp\":{\"name\":\"DiagnosePhp\",\"title\":\"PHP Diagnostics\",\"version\":7,\"versionStr\":\"0.0.7\",\"author\":\"Stephen Dickinson, QBox\",\"summary\":\"Allows collection of PHP diagnostics\",\"requiresVersions\":{\"ProcessDiagnostics\":[\">=\",0]},\"installs\":[\"ProcessDiagnostics\"],\"singular\":true,\"created\":1611219107,\"installed\":false,\"namespace\":\"\\\\\"},\"Helloworld\":{\"name\":\"Helloworld\",\"title\":\"Hello World\",\"version\":3,\"versionStr\":\"0.0.3\",\"summary\":\"An example module used for demonstration purposes.\",\"href\":\"https:\\/\\/processwire.com\",\"icon\":\"smile-o\",\"autoload\":true,\"singular\":true,\"created\":1611219107,\"installed\":false}}', '2010-04-08 03:10:01');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('SnipWire__Settings', '{\"currencies\":[{\"currency\":\"eur\",\"precision\":2,\"decimalSeparator\":\".\",\"thousandSeparator\":\".\",\"negativeNumberFormat\":\"- %s%v\",\"numberFormat\":\"%s%v\",\"currencySymbol\":\"\\u20ac\",\"id\":\"a355c52e-eb1c-45ab-b136-b43978dddd1a\"}],\"features\":{\"inventoryManagement\":false},\"onlyAllowGuests\":true,\"allowExportAsPdf\":false,\"allowShareByEmail\":false,\"useLegacyForCsvExport\":false,\"emailSignature\":\"\",\"customerName\":\"\",\"websiteUrl\":\"\",\"emailFrom\":\"\",\"businessAddressLine1\":\"\",\"businessAddressLine2\":\"\",\"businessCity\":\"\",\"businessCountryCode\":\"\",\"businessStateCode\":\"\",\"businessVatNumber\":\"\",\"businessPostalCode\":\"\",\"domain\":null,\"timeZone\":\"(UTC+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna\",\"enabledCountries\":[\"AX\",\"AL\",\"AD\",\"AT\",\"BY\",\"BE\",\"BA\",\"BG\",\"HR\",\"CY\",\"CZ\",\"DK\",\"EE\",\"FO\",\"FI\",\"FR\",\"DE\",\"GI\",\"GR\",\"GG\",\"HU\",\"IS\",\"IE\",\"IM\",\"IT\",\"JE\",\"XK\",\"LV\",\"LI\",\"LT\",\"LU\",\"MK\",\"MT\",\"MD\",\"MC\",\"ME\",\"NL\",\"NO\",\"PL\",\"PT\",\"RO\",\"RU\",\"SM\",\"RS\",\"SK\",\"SI\",\"ES\",\"SJ\",\"SE\",\"CH\",\"TR\",\"UA\",\"GB\",\"VA\"],\"notifyWhenNewOrderOccurs\":false,\"currency\":\"eur\",\"currencyChargedWith\":\"USD\",\"allowDeferredPayment\":false,\"webhooksUrls\":[],\"invoicePrefix\":\"\",\"invoiceStartNumber\":null,\"includeProductImagesInInvoice\":false,\"currencySymbol\":\"\\u20ac\",\"numberFormat\":\"%s%v\",\"negativeNumberFormat\":\"- %s%v\",\"thousandSeparator\":\".\",\"decimalSeparator\":\".\",\"precision\":2,\"onlyAllowDeferredPayment\":false,\"protocol\":\"\",\"logoUrl\":\"\",\"eCommerceAnalyticsEnabled\":false,\"expressCheckoutEnabled\":false,\"customGatewayEnabled\":false,\"sendInvoiceCopyInBcc\":false,\"notificationEmails\":\"\",\"paymentConfiguration\":{\"useAuthorizeAndCapture\":false},\"ordersHistoryUrl\":\"\",\"allowedDomains\":[],\"automaticallySendInvoices\":true,\"clientVersion\":\"2.1\",\"backendVersion\":\"1.0\",\"addressSpecified\":false,\"emailsToNotify\":[],\"businessAddressCompleted\":false,\"businessAddress\":{\"fullName\":null,\"firstName\":null,\"name\":null,\"company\":null,\"address1\":\"\",\"address2\":\"\",\"fullAddress\":\"\",\"city\":\"\",\"country\":\"\",\"postalCode\":\"\",\"province\":\"\",\"phone\":null,\"vatNumber\":\"\"},\"notificationPreferences\":{\"subscriptionCancelation\":true}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__10d1c7948d07eb592745401399e256b5', '{\"source\":{\"file\":\"D:\\/workspace\\/boardschmiede\\/site\\/templates\\/snipcart-shop.php\",\"hash\":\"acbe7e49e5692d7e62b7bcce191aec1c\",\"size\":4804,\"time\":1587764648,\"ns\":\"ProcessWire\"},\"target\":{\"file\":\"D:\\/workspace\\/boardschmiede\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/snipcart-shop.php\",\"hash\":\"acbe7e49e5692d7e62b7bcce191aec1c\",\"size\":4804,\"time\":1587764648}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__d08a355181dd83464397bec6cad4f0d9', '{\"source\":{\"file\":\"D:\\/workspace\\/boardschmiede\\/site\\/templates\\/snipcart-product.php\",\"hash\":\"960e57fb2e0621050bd5012f17b2c759\",\"size\":3608,\"time\":1589314380,\"ns\":\"ProcessWire\"},\"target\":{\"file\":\"D:\\/workspace\\/boardschmiede\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/snipcart-product.php\",\"hash\":\"960e57fb2e0621050bd5012f17b2c759\",\"size\":3608,\"time\":1589314380}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__9c2671026bf8475e299ca16b66fff0b0', '{\"source\":{\"file\":\"\\/var\\/www\\/vhosts\\/hosting105330.a2fa5.netcup.net\\/boardschmiede.startuppages.de\\/site\\/templates\\/admin.php\",\"hash\":\"9636f854995462a4cb877cb1204bc2fe\",\"size\":467,\"time\":1581147920,\"ns\":\"ProcessWire\"},\"target\":{\"file\":\"\\/var\\/www\\/vhosts\\/hosting105330.a2fa5.netcup.net\\/boardschmiede.startuppages.de\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/admin.php\",\"hash\":\"9636f854995462a4cb877cb1204bc2fe\",\"size\":467,\"time\":1581147920}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__719b1c92c0d71287cf49f0cb9ce72722', '{\"source\":{\"file\":\"\\/var\\/www\\/vhosts\\/hosting105330.a2fa5.netcup.net\\/boardschmiede.startuppages.de\\/site\\/templates\\/_init.php\",\"hash\":\"03d948499ec1c07d62a674daefd3ad67\",\"size\":1035,\"time\":1588086579,\"ns\":\"ProcessWire\"},\"target\":{\"file\":\"\\/var\\/www\\/vhosts\\/hosting105330.a2fa5.netcup.net\\/boardschmiede.startuppages.de\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/_init.php\",\"hash\":\"03d948499ec1c07d62a674daefd3ad67\",\"size\":1035,\"time\":1588086579}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__71e1d45129dd660f2bbe9d08e08eaabe', '{\"source\":{\"file\":\"\\/var\\/www\\/vhosts\\/hosting105330.a2fa5.netcup.net\\/boardschmiede.startuppages.de\\/site\\/templates\\/_main.php\",\"hash\":\"63696b89db0432571dfee20d3d71259c\",\"size\":4730,\"time\":1591464338,\"ns\":\"ProcessWire\"},\"target\":{\"file\":\"\\/var\\/www\\/vhosts\\/hosting105330.a2fa5.netcup.net\\/boardschmiede.startuppages.de\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/_main.php\",\"hash\":\"63696b89db0432571dfee20d3d71259c\",\"size\":4730,\"time\":1591464338}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__0637f534f5e599f4fa52bf1b2ce45e14', '{\"source\":{\"file\":\"\\/var\\/www\\/vhosts\\/hosting105330.a2fa5.netcup.net\\/boardschmiede.startuppages.de\\/site\\/templates\\/home.php\",\"hash\":\"ffe39a23d734e9c83747987ac2f14266\",\"size\":4183,\"time\":1590572396,\"ns\":\"ProcessWire\"},\"target\":{\"file\":\"\\/var\\/www\\/vhosts\\/hosting105330.a2fa5.netcup.net\\/boardschmiede.startuppages.de\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/home.php\",\"hash\":\"ffe39a23d734e9c83747987ac2f14266\",\"size\":4183,\"time\":1590572396}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__6934b55f6941570c3c11ba3bb30d0eb5', '{\"source\":{\"file\":\"\\/var\\/www\\/vhosts\\/hosting105330.a2fa5.netcup.net\\/boardschmiede.startuppages.de\\/site\\/templates\\/basic-page.php\",\"hash\":\"343a5c586f1e72cafba78739f0acd525\",\"size\":969,\"time\":1587755264,\"ns\":\"ProcessWire\"},\"target\":{\"file\":\"\\/var\\/www\\/vhosts\\/hosting105330.a2fa5.netcup.net\\/boardschmiede.startuppages.de\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/basic-page.php\",\"hash\":\"343a5c586f1e72cafba78739f0acd525\",\"size\":969,\"time\":1587755264}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__77e612c9318513cbfd0f58e50db92fe2', '{\"source\":{\"file\":\"\\/var\\/www\\/vhosts\\/hosting105330.a2fa5.netcup.net\\/boardschmiede.startuppages.de\\/site\\/templates\\/board-overview-page.php\",\"hash\":\"228da90ebb07876259c207b8acde13f2\",\"size\":969,\"time\":1590572396,\"ns\":\"ProcessWire\"},\"target\":{\"file\":\"\\/var\\/www\\/vhosts\\/hosting105330.a2fa5.netcup.net\\/boardschmiede.startuppages.de\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/board-overview-page.php\",\"hash\":\"228da90ebb07876259c207b8acde13f2\",\"size\":969,\"time\":1590572396}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__ed274b250801bc6c0a06ead473f2aa92', '{\"source\":{\"file\":\"\\/var\\/www\\/vhosts\\/hosting105330.a2fa5.netcup.net\\/boardschmiede.startuppages.de\\/site\\/templates\\/snipcart-product.php\",\"hash\":\"0869ba09c3ecec5488ac110b9ae1ef80\",\"size\":3542,\"time\":1591464338,\"ns\":\"ProcessWire\"},\"target\":{\"file\":\"\\/var\\/www\\/vhosts\\/hosting105330.a2fa5.netcup.net\\/boardschmiede.startuppages.de\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/snipcart-product.php\",\"hash\":\"0869ba09c3ecec5488ac110b9ae1ef80\",\"size\":3542,\"time\":1591464338}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__bf144429e626acd79085252c7d0628ad', '{\"source\":{\"file\":\"\\/var\\/www\\/html\\/site\\/templates\\/_init.php\",\"hash\":\"03d948499ec1c07d62a674daefd3ad67\",\"size\":1035,\"time\":1590404553,\"ns\":\"ProcessWire\"},\"target\":{\"file\":\"\\/var\\/www\\/html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/_init.php\",\"hash\":\"03d948499ec1c07d62a674daefd3ad67\",\"size\":1035,\"time\":1590404553}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__c9c7e241b39e7b8c09a07cb000306242', '{\"source\":{\"file\":\"\\/var\\/www\\/html\\/site\\/templates\\/_main.php\",\"hash\":\"8ba12b18339a15d291892d0fdeeebcb2\",\"size\":4950,\"time\":1615124631,\"ns\":\"ProcessWire\"},\"target\":{\"file\":\"\\/var\\/www\\/html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/_main.php\",\"hash\":\"8ba12b18339a15d291892d0fdeeebcb2\",\"size\":4950,\"time\":1615124631}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__874910242a2375010bfe6ba44dda8f7d', '{\"source\":{\"file\":\"\\/var\\/www\\/html\\/site\\/templates\\/home.php\",\"hash\":\"3a5d0901c2ced0a88bea1feaf5b84686\",\"size\":4604,\"time\":1615121588,\"ns\":\"ProcessWire\"},\"target\":{\"file\":\"\\/var\\/www\\/html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/home.php\",\"hash\":\"3a5d0901c2ced0a88bea1feaf5b84686\",\"size\":4604,\"time\":1615121588}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__edf85e782ce9741d852681e74e29205e', '{\"source\":{\"file\":\"\\/var\\/www\\/html\\/site\\/templates\\/admin.php\",\"hash\":\"9636f854995462a4cb877cb1204bc2fe\",\"size\":467,\"time\":1590404553,\"ns\":\"ProcessWire\"},\"target\":{\"file\":\"\\/var\\/www\\/html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/admin.php\",\"hash\":\"9636f854995462a4cb877cb1204bc2fe\",\"size\":467,\"time\":1590404553}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('ModulesVersions.info', '{\"90\":103}', '2010-04-08 03:10:01');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__be292a7a1566a2c9a5f504b4d767d32b', '{\"source\":{\"file\":\"\\/var\\/www\\/html\\/site\\/templates\\/snipcart-shop.php\",\"hash\":\"a96f4f0e12f0b5aa5bc19dfb154897f6\",\"size\":3853,\"time\":1615114693,\"ns\":\"ProcessWire\"},\"target\":{\"file\":\"\\/var\\/www\\/html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/snipcart-shop.php\",\"hash\":\"a96f4f0e12f0b5aa5bc19dfb154897f6\",\"size\":3853,\"time\":1615114693}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__dbb91cc358d7cba73b127b80e3224c38', '{\"source\":{\"file\":\"\\/var\\/www\\/html\\/site\\/templates\\/board-overview-page.php\",\"hash\":\"228da90ebb07876259c207b8acde13f2\",\"size\":969,\"time\":1611219107,\"ns\":\"ProcessWire\"},\"target\":{\"file\":\"\\/var\\/www\\/html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/board-overview-page.php\",\"hash\":\"228da90ebb07876259c207b8acde13f2\",\"size\":969,\"time\":1611219107}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__e18d6d706f7cc3fd46f16657b1515d05', '{\"source\":{\"file\":\"\\/var\\/www\\/html\\/site\\/templates\\/snipcart-product.php\",\"hash\":\"9cf3e76a7938d7a225bf4ad295806c37\",\"size\":5064,\"time\":1615128330,\"ns\":\"ProcessWire\"},\"target\":{\"file\":\"\\/var\\/www\\/html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/snipcart-product.php\",\"hash\":\"9cf3e76a7938d7a225bf4ad295806c37\",\"size\":5064,\"time\":1615128330}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__fb488b2a5cdaed9adaa824e46d8928bd', '{\"source\":{\"file\":\"\\/var\\/www\\/html\\/site\\/modules\\/DiagnosePhp\\/ProcessDiagnostics.module\",\"hash\":\"4560b510543cc143dfd2d7d481cd7deb\",\"size\":10814,\"time\":1611219107,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/var\\/www\\/html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/DiagnosePhp\\/ProcessDiagnostics.module\",\"hash\":\"f8d637f56c389feadc1d06516d54ba97\",\"size\":10995,\"time\":1611219107}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__78d9f32bd441b87dc4cee5fcb33d9e14', '{\"source\":{\"file\":\"\\/var\\/www\\/html\\/site\\/templates\\/basic-page-twoes.php\",\"hash\":\"95d5126652f84841675220e35d2465e7\",\"size\":1482,\"time\":1611219107,\"ns\":\"ProcessWire\"},\"target\":{\"file\":\"\\/var\\/www\\/html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/basic-page-twoes.php\",\"hash\":\"95d5126652f84841675220e35d2465e7\",\"size\":1482,\"time\":1611219107}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__83ca1f68dea864aaeecc719bed1f7e58', '{\"source\":{\"file\":\"\\/var\\/www\\/vhosts\\/hosting105330.a2fa5.netcup.net\\/boardschmiede.startuppages.de\\/site\\/modules\\/DiagnosePhp\\/DiagnoseImagehandling.module\",\"hash\":\"7dd8fa804558f80a390773c9ba82aa29\",\"size\":6577,\"time\":1587643984,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/var\\/www\\/vhosts\\/hosting105330.a2fa5.netcup.net\\/boardschmiede.startuppages.de\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/DiagnosePhp\\/DiagnoseImagehandling.module\",\"hash\":\"b7da8796303c74668c0d657ed4c84239\",\"size\":6877,\"time\":1587643984}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__5207bcfc6e29ffc4a068d4726ce9743e', '{\"source\":{\"file\":\"\\/var\\/www\\/vhosts\\/hosting105330.a2fa5.netcup.net\\/boardschmiede.startuppages.de\\/site\\/modules\\/DiagnosePhp\\/ProcessDiagnostics.module\",\"hash\":\"4560b510543cc143dfd2d7d481cd7deb\",\"size\":10814,\"time\":1587643984,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/var\\/www\\/vhosts\\/hosting105330.a2fa5.netcup.net\\/boardschmiede.startuppages.de\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/DiagnosePhp\\/ProcessDiagnostics.module\",\"hash\":\"bdc1f290ff05e17d276920adb4668a32\",\"size\":11121,\"time\":1587643984}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__c2d19fcfe61841581a46d928931b06a0', '{\"source\":{\"file\":\"\\/var\\/www\\/vhosts\\/hosting105330.a2fa5.netcup.net\\/boardschmiede.startuppages.de\\/site\\/templates\\/snipcart-shop.php\",\"hash\":\"e0c46fd9c59c14bc031a2e87032c7629\",\"size\":3826,\"time\":1590572396,\"ns\":\"ProcessWire\"},\"target\":{\"file\":\"\\/var\\/www\\/vhosts\\/hosting105330.a2fa5.netcup.net\\/boardschmiede.startuppages.de\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/snipcart-shop.php\",\"hash\":\"e0c46fd9c59c14bc031a2e87032c7629\",\"size\":3826,\"time\":1590572396}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__6a93c26a6d4fecfd179f7b59e309d19b', '{\"source\":{\"file\":\"\\/var\\/www\\/vhosts\\/hosting105330.a2fa5.netcup.net\\/boardschmiede.startuppages.de\\/site\\/templates\\/basic-page-twoes.php\",\"hash\":\"95d5126652f84841675220e35d2465e7\",\"size\":1482,\"time\":1591464338,\"ns\":\"ProcessWire\"},\"target\":{\"file\":\"\\/var\\/www\\/vhosts\\/hosting105330.a2fa5.netcup.net\\/boardschmiede.startuppages.de\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/basic-page-twoes.php\",\"hash\":\"95d5126652f84841675220e35d2465e7\",\"size\":1482,\"time\":1591464338}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__6e3ef7a4bd4ea6cd643685ec40576860', '{\"source\":{\"file\":\"\\/var\\/www\\/html\\/site\\/templates\\/gallery.php\",\"hash\":\"6f605e9be345a729484b4ce9f3b7a515\",\"size\":255,\"time\":1615115223,\"ns\":\"ProcessWire\"},\"target\":{\"file\":\"\\/var\\/www\\/html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/gallery.php\",\"hash\":\"6f605e9be345a729484b4ce9f3b7a515\",\"size\":255,\"time\":1615115223}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__2df9314273afdeed8cb4f684610e2cd5', '{\"source\":{\"file\":\"\\/var\\/www\\/html\\/site\\/templates\\/basic-page.php\",\"hash\":\"0bec5b24da78988fda736adb9bd87024\",\"size\":1276,\"time\":1615117815,\"ns\":\"ProcessWire\"},\"target\":{\"file\":\"\\/var\\/www\\/html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/basic-page.php\",\"hash\":\"0bec5b24da78988fda736adb9bd87024\",\"size\":1276,\"time\":1615117815}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('FileCompiler__1c031aca598ba8b2e8b5310e061dc7a6', '{\"source\":{\"file\":\"\\/var\\/www\\/html\\/site\\/modules\\/FieldtypeColorPicker\\/InputfieldColorPicker.module\",\"hash\":\"61be546def986999e0fdff5833779d84\",\"size\":3009,\"time\":1615122570,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/var\\/www\\/html\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/FieldtypeColorPicker\\/InputfieldColorPicker.module\",\"hash\":\"e164f2881909deb7457cf4eef67a4701\",\"size\":3022,\"time\":1615122570}}', '2010-04-08 03:10:10');
INSERT INTO `caches` (`name`, `data`, `expires`) VALUES('Modules.info', '{\"164\":{\"name\":\"FieldtypeTextareaLanguage\",\"title\":\"Textarea (Multi-language)\",\"version\":100,\"requiresVersions\":{\"LanguageSupportFields\":[\">=\",0]},\"singular\":true,\"created\":1580881873,\"namespace\":\"ProcessWire\\\\\"},\"158\":{\"name\":\"LanguageSupport\",\"title\":\"Languages Support\",\"version\":103,\"installs\":[\"ProcessLanguage\",\"ProcessLanguageTranslator\"],\"autoload\":true,\"singular\":true,\"created\":1580881873,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"addFlag\":32},\"166\":{\"name\":\"LanguageTabs\",\"title\":\"Languages Support - Tabs\",\"version\":114,\"requiresVersions\":{\"LanguageSupport\":[\">=\",0]},\"autoload\":\"template=admin\",\"singular\":true,\"created\":1580881873,\"configurable\":4,\"namespace\":\"ProcessWire\\\\\"},\"159\":{\"name\":\"ProcessLanguage\",\"title\":\"Languages\",\"version\":103,\"icon\":\"language\",\"requiresVersions\":{\"LanguageSupport\":[\">=\",0]},\"permission\":\"lang-edit\",\"singular\":1,\"created\":1580881873,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"useNavJSON\":true},\"163\":{\"name\":\"FieldtypePageTitleLanguage\",\"title\":\"Page Title (Multi-Language)\",\"version\":100,\"requiresVersions\":{\"LanguageSupportFields\":[\">=\",0],\"FieldtypeTextLanguage\":[\">=\",0]},\"singular\":true,\"created\":1580881873,\"namespace\":\"ProcessWire\\\\\"},\"160\":{\"name\":\"ProcessLanguageTranslator\",\"title\":\"Language Translator\",\"version\":101,\"requiresVersions\":{\"LanguageSupport\":[\">=\",0]},\"permission\":\"lang-edit\",\"singular\":1,\"created\":1580881873,\"namespace\":\"ProcessWire\\\\\"},\"162\":{\"name\":\"FieldtypeTextLanguage\",\"title\":\"Text (Multi-language)\",\"version\":100,\"requiresVersions\":{\"LanguageSupportFields\":[\">=\",0]},\"singular\":true,\"created\":1580881873,\"namespace\":\"ProcessWire\\\\\"},\"161\":{\"name\":\"LanguageSupportFields\",\"title\":\"Languages Support - Fields\",\"version\":100,\"requiresVersions\":{\"LanguageSupport\":[\">=\",0]},\"installs\":[\"FieldtypePageTitleLanguage\",\"FieldtypeTextareaLanguage\",\"FieldtypeTextLanguage\"],\"autoload\":true,\"singular\":true,\"created\":1580881873,\"namespace\":\"ProcessWire\\\\\"},\"165\":{\"name\":\"LanguageSupportPageNames\",\"title\":\"Languages Support - Page Names\",\"version\":10,\"requiresVersions\":{\"LanguageSupport\":[\">=\",0],\"LanguageSupportFields\":[\">=\",0]},\"autoload\":true,\"singular\":true,\"created\":1580881873,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\"},\"125\":{\"name\":\"SessionLoginThrottle\",\"title\":\"Session Login Throttle\",\"version\":103,\"autoload\":\"function\",\"singular\":true,\"created\":1580881873,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\"},\"115\":{\"name\":\"PageRender\",\"title\":\"Page Render\",\"version\":105,\"autoload\":true,\"singular\":true,\"created\":1580881873,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"61\":{\"name\":\"TextformatterEntities\",\"title\":\"HTML Entity Encoder (htmlspecialchars)\",\"version\":100,\"created\":1580881873,\"namespace\":\"ProcessWire\\\\\"},\"156\":{\"name\":\"MarkupHTMLPurifier\",\"title\":\"HTML Purifier\",\"version\":495,\"created\":1580881873,\"namespace\":\"ProcessWire\\\\\"},\"67\":{\"name\":\"MarkupAdminDataTable\",\"title\":\"Admin Data Table\",\"version\":107,\"created\":1580881873,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"98\":{\"name\":\"MarkupPagerNav\",\"title\":\"Pager (Pagination) Navigation\",\"version\":105,\"created\":1580881873,\"namespace\":\"ProcessWire\\\\\"},\"113\":{\"name\":\"MarkupPageArray\",\"title\":\"PageArray Markup\",\"version\":100,\"autoload\":true,\"singular\":true,\"created\":1580881873,\"namespace\":\"ProcessWire\\\\\"},\"60\":{\"name\":\"InputfieldPage\",\"title\":\"Page\",\"version\":107,\"created\":1580881873,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"36\":{\"name\":\"InputfieldSelect\",\"title\":\"Select\",\"version\":102,\"created\":1580881873,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"86\":{\"name\":\"InputfieldPageName\",\"title\":\"Page Name\",\"version\":106,\"created\":1580881873,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"187\":{\"name\":\"InputfieldPageAutocomplete\",\"title\":\"Page Auto Complete\",\"version\":112,\"created\":1591441267,\"namespace\":\"ProcessWire\\\\\"},\"15\":{\"name\":\"InputfieldPageListSelect\",\"title\":\"Page List Select\",\"version\":101,\"created\":1580881873,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"137\":{\"name\":\"InputfieldPageListSelectMultiple\",\"title\":\"Page List Select Multiple\",\"version\":102,\"created\":1580881873,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"25\":{\"name\":\"InputfieldAsmSelect\",\"title\":\"asmSelect\",\"version\":202,\"created\":1580881873,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"131\":{\"name\":\"InputfieldButton\",\"title\":\"Button\",\"version\":100,\"created\":1580881873,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"78\":{\"name\":\"InputfieldFieldset\",\"title\":\"Fieldset\",\"version\":101,\"created\":1580881873,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"80\":{\"name\":\"InputfieldEmail\",\"title\":\"Email\",\"version\":101,\"created\":1580881873,\"namespace\":\"ProcessWire\\\\\"},\"90\":{\"name\":\"InputfieldFloat\",\"title\":\"Float\",\"version\":104,\"created\":1580881873,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"155\":{\"name\":\"InputfieldCKEditor\",\"title\":\"CKEditor\",\"version\":163,\"installs\":[\"MarkupHTMLPurifier\"],\"created\":1580881873,\"namespace\":\"ProcessWire\\\\\"},\"34\":{\"name\":\"InputfieldText\",\"title\":\"Text\",\"version\":106,\"created\":1580881873,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"39\":{\"name\":\"InputfieldRadios\",\"title\":\"Radio Buttons\",\"version\":105,\"created\":1580881873,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"149\":{\"name\":\"InputfieldSelector\",\"title\":\"Selector\",\"version\":28,\"autoload\":\"template=admin\",\"created\":1580881873,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"addFlag\":32},\"37\":{\"name\":\"InputfieldCheckbox\",\"title\":\"Checkbox\",\"version\":106,\"created\":1580881873,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"38\":{\"name\":\"InputfieldCheckboxes\",\"title\":\"Checkboxes\",\"version\":107,\"created\":1580881873,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"112\":{\"name\":\"InputfieldPageTitle\",\"title\":\"Page Title\",\"version\":102,\"created\":1580881873,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"30\":{\"name\":\"InputfieldForm\",\"title\":\"Form\",\"version\":107,\"created\":1580881873,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"108\":{\"name\":\"InputfieldURL\",\"title\":\"URL\",\"version\":102,\"created\":1580881873,\"namespace\":\"ProcessWire\\\\\"},\"41\":{\"name\":\"InputfieldName\",\"title\":\"Name\",\"version\":100,\"created\":1580881873,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"43\":{\"name\":\"InputfieldSelectMultiple\",\"title\":\"Select Multiple\",\"version\":101,\"created\":1580881873,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"79\":{\"name\":\"InputfieldMarkup\",\"title\":\"Markup\",\"version\":102,\"created\":1580881873,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"55\":{\"name\":\"InputfieldFile\",\"title\":\"Files\",\"version\":126,\"created\":1580881873,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"122\":{\"name\":\"InputfieldPassword\",\"title\":\"Password\",\"version\":102,\"created\":1580881873,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"32\":{\"name\":\"InputfieldSubmit\",\"title\":\"Submit\",\"version\":102,\"created\":1580881873,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"171\":{\"name\":\"InputfieldIcon\",\"title\":\"Icon\",\"version\":2,\"created\":1580881895,\"namespace\":\"ProcessWire\\\\\"},\"56\":{\"name\":\"InputfieldImage\",\"title\":\"Images\",\"version\":123,\"created\":1580881873,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"85\":{\"name\":\"InputfieldInteger\",\"title\":\"Integer\",\"version\":105,\"created\":1580881873,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"35\":{\"name\":\"InputfieldTextarea\",\"title\":\"Textarea\",\"version\":103,\"created\":1580881873,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"94\":{\"name\":\"InputfieldDatetime\",\"title\":\"Datetime\",\"version\":107,\"created\":1580881873,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"40\":{\"name\":\"InputfieldHidden\",\"title\":\"Hidden\",\"version\":101,\"created\":1580881873,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"151\":{\"name\":\"JqueryMagnific\",\"title\":\"jQuery Magnific Popup\",\"version\":1,\"singular\":1,\"created\":1580881873,\"namespace\":\"ProcessWire\\\\\"},\"103\":{\"name\":\"JqueryTableSorter\",\"title\":\"jQuery Table Sorter Plugin\",\"version\":221,\"singular\":1,\"created\":1580881873,\"namespace\":\"ProcessWire\\\\\"},\"116\":{\"name\":\"JqueryCore\",\"title\":\"jQuery Core\",\"version\":183,\"singular\":true,\"created\":1580881873,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"45\":{\"name\":\"JqueryWireTabs\",\"title\":\"jQuery Wire Tabs Plugin\",\"version\":110,\"created\":1580881873,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"117\":{\"name\":\"JqueryUI\",\"title\":\"jQuery UI\",\"version\":196,\"singular\":true,\"created\":1580881873,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"114\":{\"name\":\"PagePermissions\",\"title\":\"Page Permissions\",\"version\":105,\"autoload\":true,\"singular\":true,\"created\":1580881873,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"152\":{\"name\":\"PagePathHistory\",\"title\":\"Page Path History\",\"version\":5,\"autoload\":true,\"singular\":true,\"created\":1580881873,\"configurable\":4,\"namespace\":\"ProcessWire\\\\\"},\"109\":{\"name\":\"ProcessPageTrash\",\"title\":\"Page Trash\",\"version\":103,\"singular\":1,\"created\":1580881873,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"17\":{\"name\":\"ProcessPageAdd\",\"title\":\"Page Add\",\"version\":108,\"icon\":\"plus-circle\",\"permission\":\"page-edit\",\"created\":1580881873,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true,\"useNavJSON\":true},\"168\":{\"name\":\"ProcessRecentPages\",\"title\":\"Recent Pages\",\"version\":2,\"icon\":\"clock-o\",\"permission\":\"page-edit-recent\",\"singular\":1,\"created\":1580881889,\"namespace\":\"ProcessWire\\\\\",\"useNavJSON\":true,\"nav\":[{\"url\":\"?edited=1\",\"label\":\"Edited\",\"icon\":\"users\",\"navJSON\":\"navJSON\\/?edited=1\"},{\"url\":\"?added=1\",\"label\":\"Created\",\"icon\":\"users\",\"navJSON\":\"navJSON\\/?added=1\"},{\"url\":\"?edited=1&me=1\",\"label\":\"Edited by me\",\"icon\":\"user\",\"navJSON\":\"navJSON\\/?edited=1&me=1\"},{\"url\":\"?added=1&me=1\",\"label\":\"Created by me\",\"icon\":\"user\",\"navJSON\":\"navJSON\\/?added=1&me=1\"},{\"url\":\"another\\/\",\"label\":\"Add another\",\"icon\":\"plus-circle\",\"navJSON\":\"anotherNavJSON\\/\"}]},\"12\":{\"name\":\"ProcessPageList\",\"title\":\"Page List\",\"version\":122,\"icon\":\"sitemap\",\"permission\":\"page-edit\",\"created\":1580881873,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true,\"useNavJSON\":true},\"104\":{\"name\":\"ProcessPageSearch\",\"title\":\"Page Search\",\"version\":106,\"permission\":\"page-edit\",\"singular\":1,\"created\":1580881873,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"7\":{\"name\":\"ProcessPageEdit\",\"title\":\"Page Edit\",\"version\":109,\"icon\":\"edit\",\"permission\":\"page-edit\",\"singular\":1,\"created\":1580881873,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true,\"useNavJSON\":true},\"47\":{\"name\":\"ProcessTemplate\",\"title\":\"Templates\",\"version\":114,\"icon\":\"cubes\",\"permission\":\"template-admin\",\"created\":1580881873,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true,\"useNavJSON\":true},\"66\":{\"name\":\"ProcessUser\",\"title\":\"Users\",\"version\":107,\"icon\":\"group\",\"permission\":\"user-admin\",\"created\":1580881873,\"configurable\":\"ProcessUserConfig.php\",\"namespace\":\"ProcessWire\\\\\",\"permanent\":true,\"useNavJSON\":true},\"87\":{\"name\":\"ProcessHome\",\"title\":\"Admin Home\",\"version\":101,\"permission\":\"page-view\",\"created\":1580881873,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"83\":{\"name\":\"ProcessPageView\",\"title\":\"Page View\",\"version\":104,\"permission\":\"page-view\",\"created\":1580881873,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"170\":{\"name\":\"ProcessLogger\",\"title\":\"Logs\",\"version\":2,\"icon\":\"tree\",\"permission\":\"logs-view\",\"singular\":1,\"created\":1580881895,\"namespace\":\"ProcessWire\\\\\",\"useNavJSON\":true},\"48\":{\"name\":\"ProcessField\",\"title\":\"Fields\",\"version\":113,\"icon\":\"cube\",\"permission\":\"field-admin\",\"created\":1580881873,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true,\"useNavJSON\":true,\"addFlag\":32},\"50\":{\"name\":\"ProcessModule\",\"title\":\"Modules\",\"version\":119,\"permission\":\"module-admin\",\"created\":1580881873,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true,\"useNavJSON\":true,\"nav\":[{\"url\":\"?site#tab_site_modules\",\"label\":\"Site\",\"icon\":\"plug\",\"navJSON\":\"navJSON\\/?site=1\"},{\"url\":\"?core#tab_core_modules\",\"label\":\"Core\",\"icon\":\"plug\",\"navJSON\":\"navJSON\\/?core=1\"},{\"url\":\"?configurable#tab_configurable_modules\",\"label\":\"Configure\",\"icon\":\"gear\",\"navJSON\":\"navJSON\\/?configurable=1\"},{\"url\":\"?install#tab_install_modules\",\"label\":\"Install\",\"icon\":\"sign-in\",\"navJSON\":\"navJSON\\/?install=1\"},{\"url\":\"?new#tab_new_modules\",\"label\":\"New\",\"icon\":\"plus\"},{\"url\":\"?reset=1\",\"label\":\"Refresh\",\"icon\":\"refresh\"}]},\"138\":{\"name\":\"ProcessProfile\",\"title\":\"User Profile\",\"version\":104,\"permission\":\"profile-edit\",\"singular\":1,\"created\":1580881873,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"121\":{\"name\":\"ProcessPageEditLink\",\"title\":\"Page Edit Link\",\"version\":108,\"icon\":\"link\",\"permission\":\"page-edit\",\"singular\":1,\"created\":1580881873,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"136\":{\"name\":\"ProcessPermission\",\"title\":\"Permissions\",\"version\":101,\"icon\":\"gear\",\"permission\":\"permission-admin\",\"singular\":1,\"created\":1580881873,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true,\"useNavJSON\":true},\"129\":{\"name\":\"ProcessPageEditImageSelect\",\"title\":\"Page Edit Image\",\"version\":120,\"permission\":\"page-edit\",\"singular\":1,\"created\":1580881873,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"76\":{\"name\":\"ProcessList\",\"title\":\"List\",\"version\":101,\"permission\":\"page-view\",\"created\":1580881873,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"134\":{\"name\":\"ProcessPageType\",\"title\":\"Page Type\",\"version\":101,\"singular\":1,\"created\":1580881873,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true,\"useNavJSON\":true,\"addFlag\":32},\"68\":{\"name\":\"ProcessRole\",\"title\":\"Roles\",\"version\":104,\"icon\":\"gears\",\"permission\":\"role-admin\",\"created\":1580881873,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true,\"useNavJSON\":true},\"150\":{\"name\":\"ProcessPageLister\",\"title\":\"Lister\",\"version\":26,\"icon\":\"search\",\"permission\":\"page-lister\",\"created\":1580881873,\"configurable\":true,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true,\"useNavJSON\":true,\"addFlag\":32},\"14\":{\"name\":\"ProcessPageSort\",\"title\":\"Page Sort and Move\",\"version\":100,\"permission\":\"page-edit\",\"created\":1580881873,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"10\":{\"name\":\"ProcessLogin\",\"title\":\"Login\",\"version\":108,\"permission\":\"page-view\",\"created\":1580881873,\"configurable\":4,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"57\":{\"name\":\"FieldtypeImage\",\"title\":\"Images\",\"version\":102,\"singular\":true,\"created\":1580881873,\"configurable\":4,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"27\":{\"name\":\"FieldtypeModule\",\"title\":\"Module Reference\",\"version\":101,\"singular\":true,\"created\":1580881873,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"1\":{\"name\":\"FieldtypeTextarea\",\"title\":\"Textarea\",\"version\":107,\"created\":1580881873,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"181\":{\"name\":\"FieldtypeOptions\",\"title\":\"Select Options\",\"version\":1,\"singular\":true,\"created\":1587559541,\"namespace\":\"ProcessWire\\\\\"},\"89\":{\"name\":\"FieldtypeFloat\",\"title\":\"Float\",\"version\":106,\"singular\":1,\"created\":1580881873,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"3\":{\"name\":\"FieldtypeText\",\"title\":\"Text\",\"version\":101,\"singular\":true,\"created\":1580881873,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"4\":{\"name\":\"FieldtypePage\",\"title\":\"Page Reference\",\"version\":105,\"autoload\":true,\"singular\":true,\"created\":1580881873,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"6\":{\"name\":\"FieldtypeFile\",\"title\":\"Files\",\"version\":106,\"singular\":true,\"created\":1580881873,\"configurable\":4,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"84\":{\"name\":\"FieldtypeInteger\",\"title\":\"Integer\",\"version\":102,\"created\":1580881873,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"28\":{\"name\":\"FieldtypeDatetime\",\"title\":\"Datetime\",\"version\":105,\"created\":1580881873,\"namespace\":\"ProcessWire\\\\\"},\"111\":{\"name\":\"FieldtypePageTitle\",\"title\":\"Page Title\",\"version\":100,\"singular\":1,\"created\":1580881873,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"107\":{\"name\":\"FieldtypeFieldsetTabOpen\",\"title\":\"Fieldset in Tab (Open)\",\"version\":100,\"singular\":1,\"created\":1580881873,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"133\":{\"name\":\"FieldtypePassword\",\"title\":\"Password\",\"version\":101,\"singular\":true,\"created\":1580881873,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"97\":{\"name\":\"FieldtypeCheckbox\",\"title\":\"Checkbox\",\"version\":101,\"singular\":true,\"created\":1580881873,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"106\":{\"name\":\"FieldtypeFieldsetClose\",\"title\":\"Fieldset (Close)\",\"version\":100,\"singular\":true,\"created\":1580881873,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"135\":{\"name\":\"FieldtypeURL\",\"title\":\"URL\",\"version\":101,\"singular\":true,\"created\":1580881873,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"29\":{\"name\":\"FieldtypeEmail\",\"title\":\"E-Mail\",\"version\":101,\"singular\":true,\"created\":1580881873,\"namespace\":\"ProcessWire\\\\\"},\"105\":{\"name\":\"FieldtypeFieldsetOpen\",\"title\":\"Fieldset (Open)\",\"version\":101,\"singular\":true,\"created\":1580881873,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"172\":{\"name\":\"FieldtypeRepeater\",\"title\":\"Repeater\",\"version\":106,\"installs\":[\"InputfieldRepeater\"],\"autoload\":true,\"singular\":true,\"created\":1587346703,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\"},\"173\":{\"name\":\"InputfieldRepeater\",\"title\":\"Repeater\",\"version\":106,\"requiresVersions\":{\"FieldtypeRepeater\":[\">=\",0]},\"created\":1587346703,\"namespace\":\"ProcessWire\\\\\"},\"139\":{\"name\":\"SystemUpdater\",\"title\":\"System Updater\",\"version\":18,\"singular\":true,\"created\":1580881873,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"148\":{\"name\":\"AdminThemeDefault\",\"title\":\"Default\",\"version\":14,\"autoload\":\"template=admin\",\"created\":1580881873,\"configurable\":19,\"namespace\":\"ProcessWire\\\\\"},\"169\":{\"name\":\"AdminThemeUikit\",\"title\":\"Uikit\",\"version\":31,\"requiresVersions\":{\"ProcessWire\":[\">=\",\"3.0.100\"]},\"autoload\":\"template=admin\",\"created\":1580881890,\"configurable\":4,\"namespace\":\"ProcessWire\\\\\"},\"180\":{\"name\":\"ProcessDiagnostics\",\"title\":\"Diagnostics Page\",\"version\":23,\"singular\":true,\"created\":1587541993,\"namespace\":\"\\\\\"},\"179\":{\"name\":\"DiagnoseImagehandling\",\"title\":\"Image Handling\",\"version\":1,\"requiresVersions\":{\"ProcessDiagnostics\":[\">=\",0]},\"installs\":[\"ProcessDiagnostics\"],\"singular\":true,\"created\":1587541993,\"namespace\":\"\\\\\"},\"182\":{\"name\":\"TextformatterVideoEmbed\",\"title\":\"Video embed for YouTube\\/Vimeo\",\"version\":111,\"singular\":1,\"created\":1587626883,\"configurable\":3},\"177\":{\"name\":\"InputfieldCroppableImage3\",\"title\":\"Croppable Image 3 (Inputfield)\",\"version\":\"1.2.0\",\"icon\":\"crop\",\"requiresVersions\":{\"ProcessWire\":[\">=\",\"3.0.20\"],\"PHP\":[\">=\",\"5.3.8\"],\"FieldtypeCroppableImage3\":[\">=\",\"1.2.0\"]},\"created\":1587541840},\"175\":{\"name\":\"CroppableImage3\",\"title\":\"Croppable Image 3 (Wrapper-Module)\",\"version\":\"1.2.0\",\"icon\":\"crop\",\"requiresVersions\":{\"ProcessWire\":[\">=\",\"3.0.20\"],\"PHP\":[\">=\",\"5.3.8\"]},\"installs\":[\"FieldtypeCroppableImage3\"],\"created\":1587541840},\"178\":{\"name\":\"ProcessCroppableImage3\",\"title\":\"Croppable Images 3 (Process)\",\"version\":\"1.2.0\",\"icon\":\"crop\",\"requiresVersions\":{\"ProcessWire\":[\">=\",\"3.0.20\"],\"PHP\":[\">=\",\"5.3.8\"],\"FieldtypeCroppableImage3\":[\">=\",\"1.2.0\"]},\"permission\":\"croppable-image-3\",\"singular\":1,\"created\":1587541840},\"176\":{\"name\":\"FieldtypeCroppableImage3\",\"title\":\"Croppable Image 3 (Fieldtype & Main-Module)\",\"version\":\"1.2.0\",\"icon\":\"crop\",\"requiresVersions\":{\"ProcessWire\":[\">=\",\"3.0.20\"],\"PHP\":[\">=\",\"5.3.8\"],\"CroppableImage3\":[\">=\",\"1.2.0\"]},\"installs\":[\"InputfieldCroppableImage3\",\"ProcessCroppableImage3\"],\"singular\":1,\"created\":1587541840,\"configurable\":true},\"174\":{\"name\":\"ProcessDatabaseBackups\",\"title\":\"Database Backups\",\"version\":5,\"icon\":\"database\",\"requiresVersions\":{\"ProcessWire\":[\">=\",\"3.0.62\"]},\"permission\":\"db-backup\",\"singular\":1,\"created\":1587358042,\"nav\":[{\"url\":\".\\/\",\"label\":\"View\",\"icon\":\"list\"},{\"url\":\"backup\\/\",\"label\":\"Backup\",\"icon\":\"plus-circle\"},{\"url\":\"upload\\/\",\"label\":\"Upload\",\"icon\":\"cloud-upload\"}]},\"184\":{\"name\":\"ProcessSnipWire\",\"title\":\"SnipWire Dashboard\",\"version\":\"0.8.6\",\"icon\":\"shopping-cart\",\"requiresVersions\":{\"ProcessWire\":[\">=\",\"3.0.148\"],\"SnipWire\":[\">=\",0],\"PHP\":[\">=\",\"7.0.0\"]},\"permission\":\"snipwire-dashboard\",\"singular\":1,\"created\":1587747838,\"nav\":[{\"url\":\"orders\\/\",\"label\":\"Orders\",\"icon\":\"file-text\"},{\"url\":\"subscriptions\\/\",\"label\":\"Subscriptions\",\"icon\":\"calendar\"},{\"url\":\"abandoned-carts\\/\",\"label\":\"Abandoned Carts\",\"icon\":\"shopping-cart\"},{\"url\":\"customers\\/\",\"label\":\"Customers\",\"icon\":\"user\"},{\"url\":\"products\\/\",\"label\":\"Products\",\"icon\":\"tag\"},{\"url\":\"discounts\\/\",\"label\":\"Discounts\",\"icon\":\"scissors\"},{\"url\":\"settings\\/\",\"label\":\"Settings\",\"icon\":\"cog\"}]},\"183\":{\"name\":\"SnipWire\",\"title\":\"SnipWire\",\"version\":\"0.8.6\",\"icon\":\"shopping-cart\",\"requiresVersions\":{\"ProcessWire\":[\">=\",\"3.0.148\"],\"PHP\":[\">=\",\"7.0.0\"]},\"installs\":[\"ProcessSnipWire\",\"MarkupSnipWire\",\"FieldtypeSnipWireTaxSelector\",\"FieldtypeOptions\"],\"autoload\":true,\"singular\":true,\"created\":1587747838,\"configurable\":\"SnipWireConfig.php\"},\"185\":{\"name\":\"MarkupSnipWire\",\"title\":\"SnipWire Markup\",\"version\":\"0.8.6\",\"icon\":\"shopping-cart\",\"requiresVersions\":{\"ProcessWire\":[\">=\",\"3.0.148\"],\"SnipWire\":[\">=\",0],\"PHP\":[\">=\",\"7.0.0\"]},\"autoload\":true,\"singular\":true,\"created\":1587747838},\"186\":{\"name\":\"FieldtypeSnipWireTaxSelector\",\"title\":\"SnipWire TaxSelector\",\"version\":\"0.8.6\",\"icon\":\"shopping-cart\",\"requiresVersions\":{\"ProcessWire\":[\">=\",\"3.0.148\"],\"SnipWire\":[\">=\",0],\"InputfieldSelect\":[\">=\",0],\"PHP\":[\">=\",\"7.0.0\"]},\"singular\":true,\"created\":1587747838},\"188\":{\"name\":\"FieldtypeColorPicker\",\"title\":\"ColorPicker\",\"version\":203,\"installs\":[\"InputfieldColorPicker\"],\"singular\":true,\"namespace\":\"\\\\\"},\"189\":{\"name\":\"InputfieldColorPicker\",\"title\":\"ColorPicker\",\"version\":203,\"requiresVersions\":{\"FieldtypeColorPicker\":[\">=\",0]},\"namespace\":\"\\\\\"}}', '2010-04-08 03:10:01');

DROP TABLE IF EXISTS `field_admin_theme`;
CREATE TABLE `field_admin_theme` (
  `pages_id` int unsigned NOT NULL,
  `data` int NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


DROP TABLE IF EXISTS `field_biglinks`;
CREATE TABLE `field_biglinks` (
  `pages_id` int unsigned NOT NULL,
  `data` text NOT NULL,
  `count` int NOT NULL,
  `parent_id` int NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(1)),
  KEY `count` (`count`,`pages_id`),
  KEY `parent_id` (`parent_id`,`pages_id`),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_biglinks` (`pages_id`, `data`, `count`, `parent_id`) VALUES('1', '1229,1230,1231', '3', '1228');

DROP TABLE IF EXISTS `field_board_features`;
CREATE TABLE `field_board_features` (
  `pages_id` int unsigned NOT NULL,
  `data` mediumtext NOT NULL,
  `data1012` mediumtext,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`),
  FULLTEXT KEY `data1012` (`data1012`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `field_board_features` (`pages_id`, `data`, `data1012`) VALUES('1052', '<p><strong>Sizes:</strong></p>\n\n<p>136x42<br />\n140x50</p>', '');
INSERT INTO `field_board_features` (`pages_id`, `data`, `data1012`) VALUES('1058', '', '');
INSERT INTO `field_board_features` (`pages_id`, `data`, `data1012`) VALUES('1060', '', '');
INSERT INTO `field_board_features` (`pages_id`, `data`, `data1012`) VALUES('1063', '', '');
INSERT INTO `field_board_features` (`pages_id`, `data`, `data1012`) VALUES('1065', '', '');
INSERT INTO `field_board_features` (`pages_id`, `data`, `data1012`) VALUES('1077', '<p><strong>Sizes:</strong></p>\n\n<ul><li>130x38 </li>\n	<li>132x39 </li>\n	<li>135x40 </li>\n	<li>137x41 </li>\n	<li>139x42 </li>\n	<li>141x43 </li>\n	<li>custom Size Anfrage </li>\n</ul>', '');
INSERT INTO `field_board_features` (`pages_id`, `data`, `data1012`) VALUES('1083', '<p><strong>Sizes:</strong></p>\n\n<ul><li>130x38 </li>\n	<li>132x39 </li>\n	<li>135x40 </li>\n	<li>137x41 </li>\n	<li>139x42 </li>\n	<li>141x43 </li>\n	<li>custom Size Anfrage </li>\n</ul>', '');
INSERT INTO `field_board_features` (`pages_id`, `data`, `data1012`) VALUES('1142', '', '');
INSERT INTO `field_board_features` (`pages_id`, `data`, `data1012`) VALUES('1144', '', '');
INSERT INTO `field_board_features` (`pages_id`, `data`, `data1012`) VALUES('1146', '', '');
INSERT INTO `field_board_features` (`pages_id`, `data`, `data1012`) VALUES('1148', '', '');
INSERT INTO `field_board_features` (`pages_id`, `data`, `data1012`) VALUES('1150', '', '');
INSERT INTO `field_board_features` (`pages_id`, `data`, `data1012`) VALUES('1158', '<p><strong>Zielgruppe</strong><br />\nDieses Board eignet sich besonders für Hüpfer.</p>\n\n<p><strong>Flex</strong><br />\nFli fla flexi. Hier steht ein Text über den Flex. Der Text sollte schon ein paar Sätze beinhalten.</p>\n\n<p><strong>Größen</strong></p>\n\n<ul><li>130x38 </li>\n	<li>132x39 </li>\n	<li>135x40 </li>\n	<li>137x41 </li>\n	<li>139x42 </li>\n	<li>141x43 </li>\n	<li>custom Size Anfrage </li>\n</ul><p><strong>Rail &amp; Logo</strong><br />\nWähle Deine Lieblingsfarbe</p>', '');
INSERT INTO `field_board_features` (`pages_id`, `data`, `data1012`) VALUES('1160', '', '');
INSERT INTO `field_board_features` (`pages_id`, `data`, `data1012`) VALUES('1162', '', '');
INSERT INTO `field_board_features` (`pages_id`, `data`, `data1012`) VALUES('1164', '', '');
INSERT INTO `field_board_features` (`pages_id`, `data`, `data1012`) VALUES('1166', '', '');
INSERT INTO `field_board_features` (`pages_id`, `data`, `data1012`) VALUES('1168', '', '');
INSERT INTO `field_board_features` (`pages_id`, `data`, `data1012`) VALUES('1170', '', '');
INSERT INTO `field_board_features` (`pages_id`, `data`, `data1012`) VALUES('1172', '', '');
INSERT INTO `field_board_features` (`pages_id`, `data`, `data1012`) VALUES('1174', '', '');
INSERT INTO `field_board_features` (`pages_id`, `data`, `data1012`) VALUES('1182', '', '');
INSERT INTO `field_board_features` (`pages_id`, `data`, `data1012`) VALUES('1186', '', '');

DROP TABLE IF EXISTS `field_board_images`;
CREATE TABLE `field_board_images` (
  `pages_id` int unsigned NOT NULL,
  `data` varchar(250) NOT NULL,
  `sort` int unsigned NOT NULL,
  `description` text NOT NULL,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `filedata` mediumtext,
  `filesize` int DEFAULT NULL,
  `created_users_id` int unsigned NOT NULL DEFAULT '0',
  `modified_users_id` int unsigned NOT NULL DEFAULT '0',
  `width` int DEFAULT NULL,
  `height` int DEFAULT NULL,
  `ratio` decimal(4,2) DEFAULT NULL,
  PRIMARY KEY (`pages_id`,`sort`),
  KEY `data` (`data`),
  KEY `modified` (`modified`),
  KEY `created` (`created`),
  KEY `filesize` (`filesize`),
  KEY `width` (`width`),
  KEY `height` (`height`),
  KEY `ratio` (`ratio`),
  FULLTEXT KEY `description` (`description`),
  FULLTEXT KEY `filedata` (`filedata`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `field_board_images` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`, `filesize`, `created_users_id`, `modified_users_id`, `width`, `height`, `ratio`) VALUES('1052', 'twintip_front_cutout_edited-v1.png', '0', '[\"\"]', '2020-04-23 13:59:28', '2020-04-23 13:59:28', '', '3718553', '41', '41', '1160', '3968', '0.29');
INSERT INTO `field_board_images` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`, `filesize`, `created_users_id`, `modified_users_id`, `width`, `height`, `ratio`) VALUES('1065', 'lw_back.png', '0', '[\"\"]', '2020-04-22 19:24:55', '2020-04-22 19:24:55', '', '13654724', '41', '41', '3128', '5568', '0.56');
INSERT INTO `field_board_images` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`, `filesize`, `created_users_id`, `modified_users_id`, `width`, `height`, `ratio`) VALUES('1063', 'ladys_front.png', '0', '[\"\"]', '2020-04-22 19:24:08', '2020-04-22 19:24:08', '', '14450572', '41', '41', '3128', '5568', '0.56');

DROP TABLE IF EXISTS `field_board_style`;
CREATE TABLE `field_board_style` (
  `pages_id` int unsigned NOT NULL,
  `data` text NOT NULL,
  `data1012` text,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  KEY `data_exact1012` (`data1012`(250)),
  FULLTEXT KEY `data` (`data`),
  FULLTEXT KEY `data1012` (`data1012`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_board_style` (`pages_id`, `data`, `data1012`) VALUES('1077', 'Freeride', '');
INSERT INTO `field_board_style` (`pages_id`, `data`, `data1012`) VALUES('1083', 'Freeride', '');
INSERT INTO `field_board_style` (`pages_id`, `data`, `data1012`) VALUES('1142', 'Freestyle', '');
INSERT INTO `field_board_style` (`pages_id`, `data`, `data1012`) VALUES('1144', 'Freeride', '');
INSERT INTO `field_board_style` (`pages_id`, `data`, `data1012`) VALUES('1146', 'Freeride', '');
INSERT INTO `field_board_style` (`pages_id`, `data`, `data1012`) VALUES('1148', '', '');
INSERT INTO `field_board_style` (`pages_id`, `data`, `data1012`) VALUES('1150', '', '');
INSERT INTO `field_board_style` (`pages_id`, `data`, `data1012`) VALUES('1158', '', '');
INSERT INTO `field_board_style` (`pages_id`, `data`, `data1012`) VALUES('1160', '', '');
INSERT INTO `field_board_style` (`pages_id`, `data`, `data1012`) VALUES('1162', '', '');
INSERT INTO `field_board_style` (`pages_id`, `data`, `data1012`) VALUES('1164', '', '');
INSERT INTO `field_board_style` (`pages_id`, `data`, `data1012`) VALUES('1166', '', '');
INSERT INTO `field_board_style` (`pages_id`, `data`, `data1012`) VALUES('1168', '', '');
INSERT INTO `field_board_style` (`pages_id`, `data`, `data1012`) VALUES('1170', '', '');
INSERT INTO `field_board_style` (`pages_id`, `data`, `data1012`) VALUES('1172', '', '');
INSERT INTO `field_board_style` (`pages_id`, `data`, `data1012`) VALUES('1174', '', '');
INSERT INTO `field_board_style` (`pages_id`, `data`, `data1012`) VALUES('1182', '', '');
INSERT INTO `field_board_style` (`pages_id`, `data`, `data1012`) VALUES('1186', '', '');

DROP TABLE IF EXISTS `field_body`;
CREATE TABLE `field_body` (
  `pages_id` int unsigned NOT NULL,
  `data` mediumtext NOT NULL,
  `data1012` mediumtext,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`),
  FULLTEXT KEY `data1012` (`data1012`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `field_body` (`pages_id`, `data`, `data1012`) VALUES('27', '<h3>The page you were looking for is not found.</h3>\n\n<p>Please use our search engine or navigation above to find the page.</p>', '<h3>Die Seite, die Sie suchen, wurde nicht gefunden. </h3>\n\n<p>Bitte verwenden Sie die Suchmaschine oder die Navigation oben, um die Seite zu finden.</p>');
INSERT INTO `field_body` (`pages_id`, `data`, `data1012`) VALUES('1', '<h1 style=\"text-align:center;\">THIS IS JUST THE BEGINNING OF YOUR JOURNEY</h1>\n\n<p style=\"text-align:center;\"><em>We are a small manufacturer located at the popular kite spot Workum in the Netherlands, who have set ourselves the goal to \"forge\" the ultimate board. Years of honing our craft, combined with a real passion for perfection, ensures every board is made to the highest possible standard.</em></p>', '<h2>Was ist ProcessWire?</h2>\n\n<p>ProcessWire gibt Ihnen volle Kontrolle über Ihre Felder, Vorlagen und Markup. Es bietet ein mächtiges Templating-System, das sich ganz nach Ihren richtet. Mit dem ProcessWire API bearbeiten Sie Inhalte spielend einfach und bequem. <a href=\"http://de.processwire.com\">Mehr erfahren</a></p>\n\n<h3>Über dieses Webseiten-Profil</h3>\n\n<p>Dieses Demo-Profil ist eine einfache Webseite, die Sie als Grundlage für die Entwicklung Ihrer eigenen Webseiten verwenden können oder um sich mit dem System vertraut zu machen. Die Seiten dienen lediglich als Beispiele und erheben nicht den Anspruch, alle ProcessWire Features demonstrieren zu wollen. Wenn Sie Ihre eigene Webseiten bauen, ist dieses Profil ein guter Ausgangspunkt. Sie können die vorhandenen Vorlagen und das Design verwenden wie sie sind, oder nach Belieben austauschen.</p>\n\n<h3>Diese Seite durchsuchen</h3>');
INSERT INTO `field_body` (`pages_id`, `data`, `data1012`) VALUES('1021', '<h2 style=\"text-align:center;\">THE NEW BOARDSCHMIEDE BOARDS</h2>\n\n<p style=\"text-align:center;\">odylorem ipsum. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.</p>', '');
INSERT INTO `field_body` (`pages_id`, `data`, `data1012`) VALUES('1022', '<p>Perfect boards for every rider</p>', '');
INSERT INTO `field_body` (`pages_id`, `data`, `data1012`) VALUES('1023', '', '');
INSERT INTO `field_body` (`pages_id`, `data`, `data1012`) VALUES('1024', '<h1 style=\"text-align:center;\">MEET THE TEAM OF BOARDSCHMIEDE</h1>', '');
INSERT INTO `field_body` (`pages_id`, `data`, `data1012`) VALUES('1025', '', '');
INSERT INTO `field_body` (`pages_id`, `data`, `data1012`) VALUES('1026', '<h3>ADDRESS</h3>\n\n<p>Suderseleane 7<br />\n8711GX Workum<br />\nThe Netherlands</p>\n\n<p>Phone: +49 151 510 10 236</p>\n\n<p>Email: <a href=\"mailto:info@boardschmiede.com\">info@boardschmiede.com</a></p>\n\n<h3>SOCIAL MEDIA</h3>\n\n<p><a href=\"https://www.facebook.com/customboards.de/\" target=\"_blank\" rel=\"noreferrer noopener\">Facebook</a></p>\n\n<p><a href=\"https://www.instagram.com/boardschmiede\" target=\"_blank\" rel=\"noreferrer noopener\">Instagram</a></p>', '');
INSERT INTO `field_body` (`pages_id`, `data`, `data1012`) VALUES('1050', '', '');
INSERT INTO `field_body` (`pages_id`, `data`, `data1012`) VALUES('1052', '<p>Bodylorem ipsum. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus.</p>', '');
INSERT INTO `field_body` (`pages_id`, `data`, `data1012`) VALUES('1002', '<h2>Ut capio feugiat saepius torqueo olim</h2>\n\n<h3>In utinam facilisi eum vicis feugait nimis</h3>\n\n<p>Iusto incassum appellatio cui macto genitus vel. Lobortis aliquam luctus, roto enim, imputo wisi tamen. Ratis odio, genitus acsi, neo illum consequat consectetuer ut.</p>\n\n<blockquote>\n<p>Wisi fere virtus cogo, ex ut vel nullus similis vel iusto. Tation incassum adsum in, quibus capto premo diam suscipere facilisi. Uxor laoreet mos capio premo feugait ille et. Pecus abigo immitto epulae duis vel. Neque causa, indoles verto, decet ingenium dignissim.</p>\n</blockquote>\n\n<p>Patria iriure vel vel autem proprius indoles ille sit. Tation blandit refoveo, accumsan ut ulciscor lucidus inhibeo capto aptent opes, foras.</p>\n\n<h3>Dolore ea valde refero feugait utinam luctus</h3>\n\n<p>Usitas, nostrud transverbero, in, amet, nostrud ad. Ex feugiat opto diam os aliquam regula lobortis dolore ut ut quadrum. Esse eu quis nunc jugis iriure volutpat wisi, fere blandit inhibeo melior, hendrerit, saluto velit. Eu bene ideo dignissim delenit accumsan nunc. Usitas ille autem camur consequat typicus feugait elit ex accumsan nutus accumsan nimis pagus, occuro. Immitto populus, qui feugiat opto pneum letalis paratus. Mara conventio torqueo nibh caecus abigo sit eum brevitas. Populus, duis ex quae exerci hendrerit, si antehabeo nobis, consequat ea praemitto zelus.</p>\n\n<p>Immitto os ratis euismod conventio erat jus caecus sudo. code test Appellatio consequat, et ibidem ludus nulla dolor augue abdo tego euismod plaga lenis. Sit at nimis venio venio tego os et pecus enim pneum magna nobis ad pneum. Saepius turpis probo refero molior nonummy aliquam neque appellatio jus luctus acsi. Ulciscor refero pagus imputo eu refoveo valetudo duis dolore usitas. Consequat suscipere quod torqueo ratis ullamcorper, dolore lenis, letalis quia quadrum plaga minim.</p>', '');
INSERT INTO `field_body` (`pages_id`, `data`, `data1012`) VALUES('1001', '<h2 style=\"text-align:center;\">ABOUT BOARDSCHMIEDE</h2>\n\n<p style=\"text-align:center;\">Dolore ad nunc, mos accumsan paratus duis suscipit luptatum facilisis macto uxor iaceo quadrum. Demoveo, appellatio elit neque ad commodo ea. Wisi, iaceo, tincidunt at commoveo rusticus et, ludus. Feugait at blandit bene blandit suscipere abdo duis ideo bis commoveo pagus ex, velit. Consequat commodo roto accumsan, duis transverbero.</p>', '<h2>Hinter den Wortbergen</h2>\n\n<p>Weit hinten, hinter den Wortbergen, fern der Länder Vokalien und Konsonantien leben die Blindtexte. Abgeschieden wohnen sie in Buchstabhausen an der Küste des Semantik, eines großen Sprachozeans. Ein kleines Bächlein namens Duden fließt durch ihren Ort und versorgt sie mit den nötigen Regelialien. Es ist ein paradiesmatisches Land, in dem einem gebratene Satzteile in den Mund fliegen. Nicht einmal von der allmächtigen Interpunktion werden die Blindtexte beherrscht – ein geradezu unorthographisches Leben.</p>');
INSERT INTO `field_body` (`pages_id`, `data`, `data1012`) VALUES('1004', '<h2>Pertineo vel dignissim, natu letalis fere odio</h2>\n\n<p>Magna in gemino, gilvus iusto capto jugis abdo mos aptent acsi qui. Utrum inhibeo humo humo duis quae. Lucidus paulatim facilisi scisco quibus hendrerit conventio adsum.</p>\n\n<h3>Si lobortis singularis genitus ibidem saluto</h3>\n\n<ul>\n	<li>Feugiat eligo foras ex elit sed indoles hos elit ex antehabeo defui et nostrud.</li>\n	<li>Letatio valetudo multo consequat inhibeo ille dignissim pagus et in quadrum eum eu.</li>\n	<li>Aliquam si consequat, ut nulla amet et turpis exerci, adsum luctus ne decet, delenit.</li>\n	<li>Commoveo nunc diam valetudo cui, aptent commoveo at obruo uxor nulla aliquip augue.</li>\n</ul>\n\n<p>Iriure, ex velit, praesent vulpes delenit capio vero gilvus inhibeo letatio aliquip metuo qui eros. Transverbero demoveo euismod letatio torqueo melior. Ut odio in suscipit paulatim amet huic letalis suscipere eros causa, letalis magna.</p>\n\n<ol>\n	<li>Feugiat eligo foras ex elit sed indoles hos elit ex antehabeo defui et nostrud.</li>\n	<li>Letatio valetudo multo consequat inhibeo ille dignissim pagus et in quadrum eum eu.</li>\n	<li>Aliquam si consequat, ut nulla amet et turpis exerci, adsum luctus ne decet, delenit.</li>\n	<li>Commoveo nunc diam valetudo cui, aptent commoveo at obruo uxor nulla aliquip augue.</li>\n</ol>', '');
INSERT INTO `field_body` (`pages_id`, `data`, `data1012`) VALUES('1020', '', '');
INSERT INTO `field_body` (`pages_id`, `data`, `data1012`) VALUES('1054', '', '');
INSERT INTO `field_body` (`pages_id`, `data`, `data1012`) VALUES('1058', '', '');
INSERT INTO `field_body` (`pages_id`, `data`, `data1012`) VALUES('1060', '', '');
INSERT INTO `field_body` (`pages_id`, `data`, `data1012`) VALUES('1063', '', '');
INSERT INTO `field_body` (`pages_id`, `data`, `data1012`) VALUES('1065', '', '');
INSERT INTO `field_body` (`pages_id`, `data`, `data1012`) VALUES('1081', '', '');
INSERT INTO `field_body` (`pages_id`, `data`, `data1012`) VALUES('1116', '<h2>Impressum</h2>\n\n<p>Angaben gemäß § 5 TMG</p>\n\n<p>Fa.Boardschmiede<br />\nSuderseleane 7<br />\n8711 GX Workum</p>\n\n<p><strong>Inhaber:</strong></p>\n\n<p>Thilo Schwemlein</p>\n\n<p><strong>Kontakt:</strong><br />\nTelefon: +49 / 15151010236<br />\nE-Mail: <a href=\"mailto:kontakt@boardschmiede.de\">kontakt@boardschmiede.de</a></p>\n\n<p>KVK 65509242</p>\n\n<p>BTW NL291671378B01</p>\n\n<p><strong>Haftungsausschluss:</strong></p>\n\n<p><strong>Haftung für Inhalte</strong></p>\n\n<p>Die Inhalte unserer Seiten wurden mit größter Sorgfalt erstellt. Für die Richtigkeit, Vollständigkeit und Aktualität der Inhalte können wir jedoch keine Gewähr übernehmen. Als Diensteanbieter sind wir gemäß § 7 Abs.1 TMG für eigene Inhalte auf diesen Seiten nach den allgemeinen Gesetzen verantwortlich. Nach §§ 8 bis 10 TMG sind wir als Diensteanbieter jedoch nicht verpflichtet, übermittelte oder gespeicherte fremde Informationen zu überwachen oder nach Umständen zu forschen, die auf eine rechtswidrige Tätigkeit hinweisen. Verpflichtungen zur Entfernung oder Sperrung der Nutzung von Informationen nach den allgemeinen Gesetzen bleiben hiervon unberührt. Eine diesbezügliche Haftung ist jedoch erst ab dem Zeitpunkt der Kenntnis einer konkreten Rechtsverletzung möglich. Bei Bekanntwerden von entsprechenden Rechtsverletzungen werden wir diese Inhalte umgehend entfernen.</p>\n\n<p><strong>Haftung für Links</strong></p>\n\n<p>Unser Angebot enthält Links zu externen Webseiten Dritter, auf deren Inhalte wir keinen Einfluss haben. Deshalb können wir für diese fremden Inhalte auch keine Gewähr übernehmen. Für die Inhalte der verlinkten Seiten ist stets der jeweilige Anbieter oder Betreiber der Seiten verantwortlich. Die verlinkten Seiten wurden zum Zeitpunkt der Verlinkung auf mögliche Rechtsverstöße überprüft. Rechtswidrige Inhalte waren zum Zeitpunkt der Verlinkung nicht erkennbar. Eine permanente inhaltliche Kontrolle der verlinkten Seiten ist jedoch ohne konkrete Anhaltspunkte einer Rechtsverletzung nicht zumutbar. Bei Bekanntwerden von Rechtsverletzungen werden wir derartige Links umgehend entfernen.</p>\n\n<p><strong>Urheberrecht</strong></p>\n\n<p>Die durch die Seitenbetreiber erstellten Inhalte und Werke auf diesen Seiten unterliegen dem deutschen Urheberrecht. Die Vervielfältigung, Bearbeitung, Verbreitung und jede Art der Verwertung außerhalb der Grenzen des Urheberrechtes bedürfen der schriftlichen Zustimmung des jeweiligen Autors bzw. Erstellers. Downloads und Kopien dieser Seite sind nur für den privaten, nicht kommerziellen Gebrauch gestattet. Soweit die Inhalte auf dieser Seite nicht vom Betreiber erstellt wurden, werden die Urheberrechte Dritter beachtet. Insbesondere werden Inhalte Dritter als solche gekennzeichnet. Sollten Sie trotzdem auf eine Urheberrechtsverletzung aufmerksam werden, bitten wir um einen entsprechenden Hinweis. Bei Bekanntwerden von Rechtsverletzungen werden wir derartige Inhalte umgehend entfernen.</p>\n\n<p><strong>Datenschutz</strong></p>\n\n<p>Die Nutzung unserer Webseite ist in der Regel ohne Angabe personenbezogener Daten möglich. Soweit auf unseren Seiten personenbezogene Daten (beispielsweise Name, Anschrift oder eMail-Adressen) erhoben werden, erfolgt dies, soweit möglich, stets auf freiwilliger Basis. Diese Daten werden ohne Ihre ausdrückliche Zustimmung nicht an Dritte weitergegeben.<br />\nWir weisen darauf hin, dass die Datenübertragung im Internet (z.B. bei der Kommunikation per E-Mail) Sicherheitslücken aufweisen kann. Ein lückenloser Schutz der Daten vor dem Zugriff durch Dritte ist nicht möglich.<br />\nDer Nutzung von im Rahmen der Impressumspflicht veröffentlichten Kontaktdaten durch Dritte zur Übersendung von nicht ausdrücklich angeforderter Werbung und Informationsmaterialien wird hiermit ausdrücklich widersprochen. Die Betreiber der Seiten behalten sich ausdrücklich rechtliche Schritte im Falle der unverlangten Zusendung von Werbeinformationen, etwa durch Spam-Mails, vor.</p>', '<h2>Impressum</h2>\n\n<p>Angaben gemäß § 5 TMG</p>\n\n<p>Fa.Boardschmiede<br />\nSuderseleane 7<br />\n8711 GX Workum</p>\n\n<p><strong>Inhaber:</strong></p>\n\n<p>Thilo Schwemlein</p>\n\n<p><strong>Kontakt:</strong><br />\nTelefon: +49 / 15151010236<br />\nE-Mail: <a href=\"mailto:kontakt@boardschmiede.de\">kontakt@boardschmiede.de</a></p>\n\n<p>KVK 65509242</p>\n\n<p>BTW NL291671378B01</p>\n\n<p><strong>Haftungsausschluss:</strong></p>\n\n<p><strong>Haftung für Inhalte</strong></p>\n\n<p>Die Inhalte unserer Seiten wurden mit größter Sorgfalt erstellt. Für die Richtigkeit, Vollständigkeit und Aktualität der Inhalte können wir jedoch keine Gewähr übernehmen. Als Diensteanbieter sind wir gemäß § 7 Abs.1 TMG für eigene Inhalte auf diesen Seiten nach den allgemeinen Gesetzen verantwortlich. Nach §§ 8 bis 10 TMG sind wir als Diensteanbieter jedoch nicht verpflichtet, übermittelte oder gespeicherte fremde Informationen zu überwachen oder nach Umständen zu forschen, die auf eine rechtswidrige Tätigkeit hinweisen. Verpflichtungen zur Entfernung oder Sperrung der Nutzung von Informationen nach den allgemeinen Gesetzen bleiben hiervon unberührt. Eine diesbezügliche Haftung ist jedoch erst ab dem Zeitpunkt der Kenntnis einer konkreten Rechtsverletzung möglich. Bei Bekanntwerden von entsprechenden Rechtsverletzungen werden wir diese Inhalte umgehend entfernen.</p>\n\n<p><strong>Haftung für Links</strong></p>\n\n<p>Unser Angebot enthält Links zu externen Webseiten Dritter, auf deren Inhalte wir keinen Einfluss haben. Deshalb können wir für diese fremden Inhalte auch keine Gewähr übernehmen. Für die Inhalte der verlinkten Seiten ist stets der jeweilige Anbieter oder Betreiber der Seiten verantwortlich. Die verlinkten Seiten wurden zum Zeitpunkt der Verlinkung auf mögliche Rechtsverstöße überprüft. Rechtswidrige Inhalte waren zum Zeitpunkt der Verlinkung nicht erkennbar. Eine permanente inhaltliche Kontrolle der verlinkten Seiten ist jedoch ohne konkrete Anhaltspunkte einer Rechtsverletzung nicht zumutbar. Bei Bekanntwerden von Rechtsverletzungen werden wir derartige Links umgehend entfernen.</p>\n\n<p><strong>Urheberrecht</strong></p>\n\n<p>Die durch die Seitenbetreiber erstellten Inhalte und Werke auf diesen Seiten unterliegen dem deutschen Urheberrecht. Die Vervielfältigung, Bearbeitung, Verbreitung und jede Art der Verwertung außerhalb der Grenzen des Urheberrechtes bedürfen der schriftlichen Zustimmung des jeweiligen Autors bzw. Erstellers. Downloads und Kopien dieser Seite sind nur für den privaten, nicht kommerziellen Gebrauch gestattet. Soweit die Inhalte auf dieser Seite nicht vom Betreiber erstellt wurden, werden die Urheberrechte Dritter beachtet. Insbesondere werden Inhalte Dritter als solche gekennzeichnet. Sollten Sie trotzdem auf eine Urheberrechtsverletzung aufmerksam werden, bitten wir um einen entsprechenden Hinweis. Bei Bekanntwerden von Rechtsverletzungen werden wir derartige Inhalte umgehend entfernen.</p>\n\n<p><strong>Datenschutz</strong></p>\n\n<p>Die Nutzung unserer Webseite ist in der Regel ohne Angabe personenbezogener Daten möglich. Soweit auf unseren Seiten personenbezogene Daten (beispielsweise Name, Anschrift oder eMail-Adressen) erhoben werden, erfolgt dies, soweit möglich, stets auf freiwilliger Basis. Diese Daten werden ohne Ihre ausdrückliche Zustimmung nicht an Dritte weitergegeben.<br />\nWir weisen darauf hin, dass die Datenübertragung im Internet (z.B. bei der Kommunikation per E-Mail) Sicherheitslücken aufweisen kann. Ein lückenloser Schutz der Daten vor dem Zugriff durch Dritte ist nicht möglich.<br />\nDer Nutzung von im Rahmen der Impressumspflicht veröffentlichten Kontaktdaten durch Dritte zur Übersendung von nicht ausdrücklich angeforderter Werbung und Informationsmaterialien wird hiermit ausdrücklich widersprochen. Die Betreiber der Seiten behalten sich ausdrücklich rechtliche Schritte im Falle der unverlangten Zusendung von Werbeinformationen, etwa durch Spam-Mails, vor.</p>');
INSERT INTO `field_body` (`pages_id`, `data`, `data1012`) VALUES('1190', '', '');
INSERT INTO `field_body` (`pages_id`, `data`, `data1012`) VALUES('1192', '', '');
INSERT INTO `field_body` (`pages_id`, `data`, `data1012`) VALUES('1154', '<h2>Ut capio feugiat saepius torqueo olim</h2>\n\n<h3>In utinam facilisi eum vicis feugait nimis</h3>\n\n<p>Iusto incassum appellatio cui macto genitus vel. Lobortis aliquam luctus, roto enim, imputo wisi tamen. Ratis odio, genitus acsi, neo illum consequat consectetuer ut.</p>\n\n<blockquote>\n<p>Wisi fere virtus cogo, ex ut vel nullus similis vel iusto. Tation incassum adsum in, quibus capto premo diam suscipere facilisi. Uxor laoreet mos capio premo feugait ille et. Pecus abigo immitto epulae duis vel. Neque causa, indoles verto, decet ingenium dignissim.</p>\n</blockquote>\n\n<p>Patria iriure vel vel autem proprius indoles ille sit. Tation blandit refoveo, accumsan ut ulciscor lucidus inhibeo capto aptent opes, foras.</p>', '');
INSERT INTO `field_body` (`pages_id`, `data`, `data1012`) VALUES('1229', '<p>The best of the world</p>', '');
INSERT INTO `field_body` (`pages_id`, `data`, `data1012`) VALUES('1230', '<p><strong>Custom colors, prints, logos and many more...</strong></p>', '');
INSERT INTO `field_body` (`pages_id`, `data`, `data1012`) VALUES('1231', '', '');

DROP TABLE IF EXISTS `field_body_2`;
CREATE TABLE `field_body_2` (
  `pages_id` int unsigned NOT NULL,
  `data` mediumtext NOT NULL,
  `data1012` mediumtext,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`),
  FULLTEXT KEY `data1012` (`data1012`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_color`;
CREATE TABLE `field_color` (
  `pages_id` int unsigned NOT NULL,
  `data` char(6) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_color` (`pages_id`, `data`) VALUES('1255', 'FFD54D');
INSERT INTO `field_color` (`pages_id`, `data`) VALUES('1254', '030501');
INSERT INTO `field_color` (`pages_id`, `data`) VALUES('1253', 'FFFFFF');
INSERT INTO `field_color` (`pages_id`, `data`) VALUES('1256', 'FFD54D');
INSERT INTO `field_color` (`pages_id`, `data`) VALUES('1257', 'F23D1C');
INSERT INTO `field_color` (`pages_id`, `data`) VALUES('1258', 'A31719');
INSERT INTO `field_color` (`pages_id`, `data`) VALUES('1259', 'BF1773');
INSERT INTO `field_color` (`pages_id`, `data`) VALUES('1260', '2973B8');
INSERT INTO `field_color` (`pages_id`, `data`) VALUES('1261', '001660');
INSERT INTO `field_color` (`pages_id`, `data`) VALUES('1262', '389482');
INSERT INTO `field_color` (`pages_id`, `data`) VALUES('1263', '4EA833');
INSERT INTO `field_color` (`pages_id`, `data`) VALUES('1264', '918F87');

DROP TABLE IF EXISTS `field_coverimage`;
CREATE TABLE `field_coverimage` (
  `pages_id` int unsigned NOT NULL,
  `data` varchar(250) NOT NULL,
  `sort` int unsigned NOT NULL,
  `description` text NOT NULL,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `filedata` mediumtext,
  `filesize` int DEFAULT NULL,
  `created_users_id` int unsigned NOT NULL DEFAULT '0',
  `modified_users_id` int unsigned NOT NULL DEFAULT '0',
  `width` int DEFAULT NULL,
  `height` int DEFAULT NULL,
  `ratio` decimal(4,2) DEFAULT NULL,
  PRIMARY KEY (`pages_id`,`sort`),
  KEY `data` (`data`),
  KEY `modified` (`modified`),
  KEY `created` (`created`),
  KEY `filesize` (`filesize`),
  KEY `width` (`width`),
  KEY `height` (`height`),
  KEY `ratio` (`ratio`),
  FULLTEXT KEY `description` (`description`),
  FULLTEXT KEY `filedata` (`filedata`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `field_coverimage` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`, `filesize`, `created_users_id`, `modified_users_id`, `width`, `height`, `ratio`) VALUES('1050', 'img_0278.jpeg', '0', '[\"\"]', '2020-04-20 11:04:37', '2020-04-20 11:04:37', '', NULL, '0', '0', NULL, NULL, NULL);
INSERT INTO `field_coverimage` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`, `filesize`, `created_users_id`, `modified_users_id`, `width`, `height`, `ratio`) VALUES('1021', 'p1340624.jpg', '0', '[\"\"]', '2020-04-22 19:08:57', '2020-04-22 19:08:57', '', '554082', '41', '41', '1500', '1000', '1.50');
INSERT INTO `field_coverimage` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`, `filesize`, `created_users_id`, `modified_users_id`, `width`, `height`, `ratio`) VALUES('1001', 'p1020733.jpg', '0', '[\"\"]', '0000-00-00 00:00:00', '2020-04-22 19:15:44', '', '440466', '41', '41', '1999', '622', '3.21');
INSERT INTO `field_coverimage` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`, `filesize`, `created_users_id`, `modified_users_id`, `width`, `height`, `ratio`) VALUES('1', 'skull_banner.jpg', '0', '[\"\"]', '2021-01-21 12:41:11', '2021-01-21 12:41:11', '{\"focus\":{\"top\":13.4,\"left\":52.5,\"zoom\":0}}', '1957673', '41', '41', '2880', '1620', '1.78');

DROP TABLE IF EXISTS `field_email`;
CREATE TABLE `field_email` (
  `pages_id` int unsigned NOT NULL,
  `data` varchar(250) NOT NULL DEFAULT '',
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `field_email` (`pages_id`, `data`) VALUES('41', 'barkow86@gmail.com');

DROP TABLE IF EXISTS `field_fieldset_teaser`;
CREATE TABLE `field_fieldset_teaser` (
  `pages_id` int unsigned NOT NULL,
  `data` int NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


DROP TABLE IF EXISTS `field_fieldset_teaser_end`;
CREATE TABLE `field_fieldset_teaser_end` (
  `pages_id` int unsigned NOT NULL,
  `data` int NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


DROP TABLE IF EXISTS `field_fieldset_why`;
CREATE TABLE `field_fieldset_why` (
  `pages_id` int unsigned NOT NULL,
  `data` int NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


DROP TABLE IF EXISTS `field_fieldset_why_end`;
CREATE TABLE `field_fieldset_why_end` (
  `pages_id` int unsigned NOT NULL,
  `data` int NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


DROP TABLE IF EXISTS `field_headline`;
CREATE TABLE `field_headline` (
  `pages_id` int unsigned NOT NULL,
  `data` text NOT NULL,
  `data1012` text,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  KEY `data_exact1012` (`data1012`(250)),
  FULLTEXT KEY `data` (`data`),
  FULLTEXT KEY `data1012` (`data1012`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `field_headline` (`pages_id`, `data`, `data1012`) VALUES('1', 'THIS IS JUST THE BEGINNING OF YOUR JOURNEY', 'Kleine Demo-Webseite');
INSERT INTO `field_headline` (`pages_id`, `data`, `data1012`) VALUES('1030', '', '');
INSERT INTO `field_headline` (`pages_id`, `data`, `data1012`) VALUES('1035', 'What we do', '');
INSERT INTO `field_headline` (`pages_id`, `data`, `data1012`) VALUES('1036', 'Who we are', '');
INSERT INTO `field_headline` (`pages_id`, `data`, `data1012`) VALUES('1037', 'Where we are', '');
INSERT INTO `field_headline` (`pages_id`, `data`, `data1012`) VALUES('1038', 'Service', '');
INSERT INTO `field_headline` (`pages_id`, `data`, `data1012`) VALUES('1039', 'Perfektion in Shape gebracht', 'Perfektion in Shape gebracht');
INSERT INTO `field_headline` (`pages_id`, `data`, `data1012`) VALUES('1040', 'Leidenschaft', 'Leidenschaft');
INSERT INTO `field_headline` (`pages_id`, `data`, `data1012`) VALUES('1041', 'Custom made', 'Custom made');
INSERT INTO `field_headline` (`pages_id`, `data`, `data1012`) VALUES('1042', 'Manufaktur alles aus einer Hand', 'Manufaktur alles aus einer Hand');
INSERT INTO `field_headline` (`pages_id`, `data`, `data1012`) VALUES('1045', 'Manufactur', '');
INSERT INTO `field_headline` (`pages_id`, `data`, `data1012`) VALUES('1046', 'Team', '');
INSERT INTO `field_headline` (`pages_id`, `data`, `data1012`) VALUES('1047', '', '');
INSERT INTO `field_headline` (`pages_id`, `data`, `data1012`) VALUES('1070', 'Lightweight', '');
INSERT INTO `field_headline` (`pages_id`, `data`, `data1012`) VALUES('1071', 'Bulletproof', '');
INSERT INTO `field_headline` (`pages_id`, `data`, `data1012`) VALUES('1114', 'Wha a Boardschmiede?', '');
INSERT INTO `field_headline` (`pages_id`, `data`, `data1012`) VALUES('1120', 'TOBIAS BURG', '');
INSERT INTO `field_headline` (`pages_id`, `data`, `data1012`) VALUES('1121', 'THILO SCHWEMLEIN', '');
INSERT INTO `field_headline` (`pages_id`, `data`, `data1012`) VALUES('1195', 'Made with love', '');
INSERT INTO `field_headline` (`pages_id`, `data`, `data1012`) VALUES('1196', 'Skimboards', '');

DROP TABLE IF EXISTS `field_hide_header`;
CREATE TABLE `field_hide_header` (
  `pages_id` int unsigned NOT NULL,
  `data` tinyint NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_image`;
CREATE TABLE `field_image` (
  `pages_id` int unsigned NOT NULL,
  `data` varchar(250) NOT NULL,
  `sort` int unsigned NOT NULL,
  `description` text NOT NULL,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `filedata` mediumtext,
  `filesize` int DEFAULT NULL,
  `created_users_id` int unsigned NOT NULL DEFAULT '0',
  `modified_users_id` int unsigned NOT NULL DEFAULT '0',
  `width` int DEFAULT NULL,
  `height` int DEFAULT NULL,
  `ratio` decimal(4,2) DEFAULT NULL,
  PRIMARY KEY (`pages_id`,`sort`),
  KEY `data` (`data`),
  KEY `modified` (`modified`),
  KEY `created` (`created`),
  KEY `filesize` (`filesize`),
  KEY `width` (`width`),
  KEY `height` (`height`),
  KEY `ratio` (`ratio`),
  FULLTEXT KEY `description` (`description`),
  FULLTEXT KEY `filedata` (`filedata`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `field_image` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`, `filesize`, `created_users_id`, `modified_users_id`, `width`, `height`, `ratio`) VALUES('1045', 'img_8948.jpg', '0', '[\"\"]', '2020-04-20 10:04:24', '2020-04-20 10:04:24', '', NULL, '0', '0', NULL, NULL, NULL);
INSERT INTO `field_image` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`, `filesize`, `created_users_id`, `modified_users_id`, `width`, `height`, `ratio`) VALUES('1046', 'img_8947_2.jpg', '0', '[\"\"]', '2020-04-20 10:13:30', '2020-04-20 10:13:30', '', NULL, '0', '0', NULL, NULL, NULL);
INSERT INTO `field_image` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`, `filesize`, `created_users_id`, `modified_users_id`, `width`, `height`, `ratio`) VALUES('1070', 'ladys_front.png', '0', '[\"\"]', '0000-00-00 00:00:00', '2020-04-22 19:27:21', '', '39912', '41', '41', '189', '336', '0.56');
INSERT INTO `field_image` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`, `filesize`, `created_users_id`, `modified_users_id`, `width`, `height`, `ratio`) VALUES('1071', 'beaver_2020_v2_front_1.png', '0', '[\"\"]', '0000-00-00 00:00:00', '2020-04-22 19:28:51', '', '35509', '41', '41', '189', '252', '0.75');
INSERT INTO `field_image` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`, `filesize`, `created_users_id`, `modified_users_id`, `width`, `height`, `ratio`) VALUES('1120', '8b9c9043-39fc-4866-9806-0c79fb4db863.jpg', '0', '[\"\"]', '2020-06-06 18:25:26', '2020-06-06 18:25:26', '', '259626', '41', '41', '1080', '1079', '1.00');
INSERT INTO `field_image` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`, `filesize`, `created_users_id`, `modified_users_id`, `width`, `height`, `ratio`) VALUES('1121', '4a1cee7b-a6d4-408c-9e8b-632c38ded7c7.jpg', '0', '[\"\"]', '2020-06-06 18:25:37', '2020-06-06 18:25:37', '', '61899', '41', '41', '1600', '1066', '1.50');
INSERT INTO `field_image` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`, `filesize`, `created_users_id`, `modified_users_id`, `width`, `height`, `ratio`) VALUES('1230', 'img_3436.jpg', '0', '[\"\"]', '2021-03-07 15:00:34', '2021-03-07 15:00:34', '', '3559371', '41', '41', '4032', '3024', '1.33');
INSERT INTO `field_image` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`, `filesize`, `created_users_id`, `modified_users_id`, `width`, `height`, `ratio`) VALUES('1229', 'kt_dakhla_-_121.jpg', '0', '[\"\"]', '2021-03-07 15:01:43', '2021-03-07 15:01:43', '', '15010860', '41', '41', '5551', '3701', '1.50');
INSERT INTO `field_image` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`, `filesize`, `created_users_id`, `modified_users_id`, `width`, `height`, `ratio`) VALUES('1231', 'img_8966.jpg', '0', '[\"\"]', '2021-03-07 14:57:18', '2021-03-07 14:57:18', '', '1117799', '41', '41', '3008', '2000', '1.50');

DROP TABLE IF EXISTS `field_image_alignment`;
CREATE TABLE `field_image_alignment` (
  `pages_id` int unsigned NOT NULL,
  `data` tinyint NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


DROP TABLE IF EXISTS `field_image_why`;
CREATE TABLE `field_image_why` (
  `pages_id` int unsigned NOT NULL,
  `data` varchar(250) NOT NULL,
  `sort` int unsigned NOT NULL,
  `description` text NOT NULL,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `filedata` mediumtext,
  `filesize` int DEFAULT NULL,
  `created_users_id` int unsigned NOT NULL DEFAULT '0',
  `modified_users_id` int unsigned NOT NULL DEFAULT '0',
  `width` int DEFAULT NULL,
  `height` int DEFAULT NULL,
  `ratio` decimal(4,2) DEFAULT NULL,
  PRIMARY KEY (`pages_id`,`sort`),
  KEY `data` (`data`),
  KEY `modified` (`modified`),
  KEY `created` (`created`),
  KEY `filesize` (`filesize`),
  KEY `width` (`width`),
  KEY `height` (`height`),
  KEY `ratio` (`ratio`),
  FULLTEXT KEY `description` (`description`),
  FULLTEXT KEY `filedata` (`filedata`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `field_image_why` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`, `filesize`, `created_users_id`, `modified_users_id`, `width`, `height`, `ratio`) VALUES('1', 'shirt_black_print_backside_kopie.png', '0', '[\"\"]', '2021-03-05 16:07:52', '2021-03-05 16:07:52', '', '1981660', '41', '41', '3508', '2480', '1.41');

DROP TABLE IF EXISTS `field_images`;
CREATE TABLE `field_images` (
  `pages_id` int unsigned NOT NULL,
  `data` varchar(250) NOT NULL,
  `sort` int unsigned NOT NULL,
  `description` text NOT NULL,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `filedata` mediumtext,
  `filesize` int DEFAULT NULL,
  `created_users_id` int unsigned NOT NULL DEFAULT '0',
  `modified_users_id` int unsigned NOT NULL DEFAULT '0',
  `width` int DEFAULT NULL,
  `height` int DEFAULT NULL,
  `ratio` decimal(4,2) DEFAULT NULL,
  PRIMARY KEY (`pages_id`,`sort`),
  KEY `data` (`data`),
  KEY `modified` (`modified`),
  KEY `created` (`created`),
  KEY `filesize` (`filesize`),
  KEY `width` (`width`),
  KEY `height` (`height`),
  KEY `ratio` (`ratio`),
  FULLTEXT KEY `description` (`description`),
  FULLTEXT KEY `filedata` (`filedata`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `field_images` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`, `filesize`, `created_users_id`, `modified_users_id`, `width`, `height`, `ratio`) VALUES('1', 'p1020733.jpg', '0', '[\"\"]', '2020-04-24 20:56:42', '2020-04-24 20:56:42', '', '1036566', '41', '41', '2000', '1333', '1.50');
INSERT INTO `field_images` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`, `filesize`, `created_users_id`, `modified_users_id`, `width`, `height`, `ratio`) VALUES('1', 'maxresdefault_2.jpg', '1', '[\"\"]', '2020-04-24 20:56:43', '2020-04-24 20:56:43', '', '178143', '41', '41', '1280', '720', '1.78');
INSERT INTO `field_images` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`, `filesize`, `created_users_id`, `modified_users_id`, `width`, `height`, `ratio`) VALUES('1', 'p1340624-1.jpg', '2', '[\"\"]', '2020-04-24 20:56:44', '2020-04-24 20:56:44', '', '554082', '41', '41', '1500', '1000', '1.50');
INSERT INTO `field_images` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`, `filesize`, `created_users_id`, `modified_users_id`, `width`, `height`, `ratio`) VALUES('1077', '56da4514-ddab-4f1f-8252-f28a60302e2f.jpg', '2', '[\"\"]', '2020-05-27 10:29:01', '2020-05-27 10:29:01', '', '182508', '41', '41', '1198', '1600', '0.75');
INSERT INTO `field_images` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`, `filesize`, `created_users_id`, `modified_users_id`, `width`, `height`, `ratio`) VALUES('1077', '83408820-a384-4e56-8cbb-08ffd5e17893.jpg', '1', '[\"\"]', '2020-05-27 11:01:34', '2020-05-27 11:01:34', '', '109986', '41', '41', '750', '1334', '0.56');
INSERT INTO `field_images` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`, `filesize`, `created_users_id`, `modified_users_id`, `width`, `height`, `ratio`) VALUES('1077', 'twintip-front-standard-450x600-min_1.png', '0', '[\"\"]', '2020-05-27 10:29:03', '2020-05-27 10:29:03', '', '57363', '41', '41', '450', '600', '0.75');
INSERT INTO `field_images` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`, `filesize`, `created_users_id`, `modified_users_id`, `width`, `height`, `ratio`) VALUES('1192', '63df60fa-9346-4fee-9e03-65073dddfe08.jpg', '1', '[\"\"]', '2021-03-07 13:02:46', '2021-03-07 13:02:46', '', '189381', '41', '41', '1600', '900', '1.78');
INSERT INTO `field_images` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`, `filesize`, `created_users_id`, `modified_users_id`, `width`, `height`, `ratio`) VALUES('1192', '8501d808-4fe6-409a-835f-b8f838d1af06.jpg', '2', '[\"\"]', '2021-03-07 13:02:46', '2021-03-07 13:02:46', '', '428255', '41', '41', '1600', '900', '1.78');
INSERT INTO `field_images` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`, `filesize`, `created_users_id`, `modified_users_id`, `width`, `height`, `ratio`) VALUES('1192', '328555c1-39ef-498f-9886-97ba531f1c92.jpg', '3', '[\"\"]', '2021-03-07 13:02:46', '2021-03-07 13:02:46', '', '151386', '41', '41', '1600', '900', '1.78');
INSERT INTO `field_images` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`, `filesize`, `created_users_id`, `modified_users_id`, `width`, `height`, `ratio`) VALUES('1192', 'caace700-6707-4fa8-a4f8-3b534cb91dc9.jpg', '4', '[\"\"]', '2021-03-07 13:02:46', '2021-03-07 13:02:46', '', '128585', '41', '41', '768', '1024', '0.75');
INSERT INTO `field_images` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`, `filesize`, `created_users_id`, `modified_users_id`, `width`, `height`, `ratio`) VALUES('1192', 'gptempdownload_2.jpg', '5', '[\"\"]', '2021-03-07 13:02:46', '2021-03-07 13:02:46', '', '165412', '41', '41', '1920', '1080', '1.78');
INSERT INTO `field_images` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`, `filesize`, `created_users_id`, `modified_users_id`, `width`, `height`, `ratio`) VALUES('1192', 'gptempdownload_3.jpg', '6', '[\"\"]', '2021-03-07 13:02:46', '2021-03-07 13:02:46', '', '364683', '41', '41', '1920', '1080', '1.78');
INSERT INTO `field_images` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`, `filesize`, `created_users_id`, `modified_users_id`, `width`, `height`, `ratio`) VALUES('1192', '4a1cee7b-a6d4-408c-9e8b-632c38ded7c7_2.jpg', '0', '[\"\"]', '2021-03-07 13:02:46', '2021-03-07 13:02:46', '', '61899', '41', '41', '1600', '1066', '1.50');

DROP TABLE IF EXISTS `field_language`;
CREATE TABLE `field_language` (
  `pages_id` int unsigned NOT NULL,
  `data` int NOT NULL,
  `sort` int unsigned NOT NULL,
  PRIMARY KEY (`pages_id`,`sort`),
  KEY `data` (`data`,`pages_id`,`sort`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `field_language` (`pages_id`, `data`, `sort`) VALUES('40', '1010', '0');
INSERT INTO `field_language` (`pages_id`, `data`, `sort`) VALUES('41', '1010', '0');

DROP TABLE IF EXISTS `field_language_files`;
CREATE TABLE `field_language_files` (
  `pages_id` int unsigned NOT NULL,
  `data` varchar(250) NOT NULL,
  `sort` int unsigned NOT NULL,
  `description` text NOT NULL,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `filesize` int DEFAULT NULL,
  `created_users_id` int unsigned NOT NULL DEFAULT '0',
  `modified_users_id` int unsigned NOT NULL DEFAULT '0',
  `filedata` mediumtext,
  PRIMARY KEY (`pages_id`,`sort`),
  KEY `data` (`data`),
  KEY `modified` (`modified`),
  KEY `created` (`created`),
  KEY `filesize` (`filesize`),
  FULLTEXT KEY `description` (`description`),
  FULLTEXT KEY `filedata` (`filedata`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--templates-admin--debug-inc.json', '117', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--textformatter--textformatterentities-module.json', '116', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--system--systemupdater--systemupdater-module.json', '115', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--session--sessionloginthrottle--sessionloginthrottle-module.json', '114', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--session--sessionhandlerdb--sessionhandlerdb-module.json', '113', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--session--sessionhandlerdb--processsessiondb-module.json', '112', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--process--processuser--processuser-module.json', '111', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--process--processtemplate--processtemplateexportimport-php.json', '110', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--process--processtemplate--processtemplate-module.json', '109', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--process--processprofile--processprofile-module.json', '107', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--process--processrole--processrole-module.json', '108', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--process--processpermission--processpermission-module.json', '106', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--process--processpageview-module.json', '105', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--process--processpagetype--processpagetype-module.json', '104', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--process--processpagetrash-module.json', '103', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--process--processpagesort-module.json', '102', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--process--processpagesearch--processpagesearch-module.json', '101', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--process--processpagelister--processpagelister-module.json', '100', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--process--processpagelist--processpagelist-module.json', '99', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--process--processpageedit--processpageedit-module.json', '96', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--process--processpageeditlink--processpageeditlink-module.json', '98', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--process--processpageeditimageselect--processpageeditimageselect-module.json', '97', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--process--processpageclone-module.json', '95', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--process--processpageadd--processpageadd-module.json', '94', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--process--processmodule--processmoduleinstall-php.json', '93', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--process--processlogin--processlogin-module.json', '91', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--process--processmodule--processmodule-module.json', '92', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--process--processlist-module.json', '90', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--process--processhome-module.json', '89', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--process--processforgotpassword-module.json', '88', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--process--processfield--processfield-module.json', '86', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--process--processfield--processfieldexportimport-php.json', '87', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--pagerender-module.json', '85', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--pagepaths-module.json', '84', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--languagesupport--languagetabs-module.json', '80', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--languagesupport--processlanguage-module.json', '81', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--markup--markuppagefields-module.json', '82', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--markup--markuppagernav--markuppagernav-module.json', '83', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--languagesupport--languagesupportpagenames-module.json', '79', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--languagesupport--languageparser-php.json', '76', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--languagesupport--languagesupport-module.json', '77', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--languagesupport--languagesupportfields-module.json', '78', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--inputfield--inputfieldurl-module.json', '74', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--jquery--jquerywiretabs--jquerywiretabs-module.json', '75', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--inputfield--inputfieldtinymce--inputfieldtinymce-module.json', '73', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--inputfield--inputfieldtextarea-module.json', '72', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--inputfield--inputfieldselectmultiple-module.json', '68', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--inputfield--inputfieldselector--inputfieldselector-module.json', '69', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--inputfield--inputfieldtext-module.json', '71', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--inputfield--inputfieldsubmit--inputfieldsubmit-module.json', '70', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--inputfield--inputfieldselect-module.json', '67', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--inputfield--inputfieldradios--inputfieldradios-module.json', '66', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--inputfield--inputfieldpassword-module.json', '65', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--inputfield--inputfieldpagename--inputfieldpagename-module.json', '61', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--inputfield--inputfieldpagetable--inputfieldpagetable-module.json', '62', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--inputfield--inputfieldpagetable--inputfieldpagetableajax-php.json', '63', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--inputfield--inputfieldpagetitle--inputfieldpagetitle-module.json', '64', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--inputfield--inputfieldpagelistselect--inputfieldpagelistselectmultiple-module.json', '60', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--inputfield--inputfieldpagelistselect--inputfieldpagelistselect-module.json', '59', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--inputfield--inputfieldpageautocomplete--inputfieldpageautocomplete-module.json', '58', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--inputfield--inputfieldpage--inputfieldpage-module.json', '57', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--inputfield--inputfieldname-module.json', '56', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--inputfield--inputfieldmarkup-module.json', '55', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--inputfield--inputfieldinteger-module.json', '54', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--inputfield--inputfieldfile--inputfieldfile-module.json', '49', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--inputfield--inputfieldfloat-module.json', '50', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--inputfield--inputfieldform-module.json', '51', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--inputfield--inputfieldhidden-module.json', '52', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--inputfield--inputfieldimage--inputfieldimage-module.json', '53', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--inputfield--inputfieldfieldset-module.json', '48', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--inputfield--inputfieldemail-module.json', '47', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--inputfield--inputfielddatetime--inputfielddatetime-module.json', '46', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--inputfield--inputfieldckeditor--inputfieldckeditor-module.json', '45', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--inputfield--inputfieldcheckboxes--inputfieldcheckboxes-module.json', '44', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--inputfield--inputfieldbutton-module.json', '42', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--inputfield--inputfieldcheckbox-module.json', '43', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--fieldtype--fieldtypetextarea-module.json', '39', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--fieldtype--fieldtypeurl-module.json', '40', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--inputfield--inputfieldasmselect--inputfieldasmselect-module.json', '41', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--fieldtype--fieldtypetext-module.json', '38', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--fieldtype--fieldtypeselector-module.json', '37', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--fieldtype--fieldtyperepeater--inputfieldrepeater-module.json', '36', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--fieldtype--fieldtyperepeater--fieldtyperepeater-module.json', '35', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--fieldtype--fieldtypepagetable-module.json', '34', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--fieldtype--fieldtypepage-module.json', '33', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--fieldtype--fieldtypemodule-module.json', '32', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--fieldtype--fieldtypefloat-module.json', '31', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--fieldtype--fieldtypefile-module.json', '30', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--fieldtype--fieldtypedatetime-module.json', '29', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--fieldtype--fieldtypecomments--inputfieldcommentsadmin-module.json', '28', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--fieldtype--fieldtypecomments--fieldtypecomments-module.json', '27', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--fieldtype--fieldtypecomments--commentfilterakismet-module.json', '24', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--fieldtype--fieldtypecomments--commentform-php.json', '25', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--fieldtype--fieldtypecomments--commentlist-php.json', '26', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--modules--admintheme--adminthemedefault--adminthemedefault-module.json', '23', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--core--wireupload-php.json', '22', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--core--wiretempdir-php.json', '21', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--core--sessioncsrf-php.json', '18', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--core--wirecache-php.json', '19', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--core--wirehttp-php.json', '20', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--core--session-php.json', '17', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--core--pages-php.json', '13', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--core--password-php.json', '14', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--core--process-php.json', '15', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--core--sanitizer-php.json', '16', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--core--pagefile-php.json', '11', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--core--pageimage-php.json', '12', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--core--modules-php.json', '10', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--core--inputfieldwrapper-php.json', '9', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--core--inputfield-php.json', '8', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--core--functions-php.json', '7', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--core--fieldtypemulti-php.json', '6', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--core--fieldtype-php.json', '5', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--core--fieldselectorinfo-php.json', '4', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--core--fields-php.json', '2', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--core--admintheme-php.json', '3', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--core--fieldgroups-php.json', '1', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--core--field-php.json', '0', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'wire--templates-admin--default-php.json', '118', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);

DROP TABLE IF EXISTS `field_language_files_site`;
CREATE TABLE `field_language_files_site` (
  `pages_id` int unsigned NOT NULL,
  `data` varchar(250) NOT NULL,
  `sort` int unsigned NOT NULL,
  `description` text NOT NULL,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `filesize` int DEFAULT NULL,
  `created_users_id` int unsigned NOT NULL DEFAULT '0',
  `modified_users_id` int unsigned NOT NULL DEFAULT '0',
  `filedata` mediumtext,
  PRIMARY KEY (`pages_id`,`sort`),
  KEY `data` (`data`),
  KEY `modified` (`modified`),
  KEY `created` (`created`),
  KEY `filesize` (`filesize`),
  FULLTEXT KEY `description` (`description`),
  FULLTEXT KEY `filedata` (`filedata`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `field_language_files_site` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'site--templates--_main-php.json', '0', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);
INSERT INTO `field_language_files_site` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filesize`, `created_users_id`, `modified_users_id`, `filedata`) VALUES('1012', 'site--templates--search-php.json', '1', '[\"\"]', '2020-02-05 09:51:13', '2020-02-05 09:51:13', NULL, '0', '0', NULL);

DROP TABLE IF EXISTS `field_page_color`;
CREATE TABLE `field_page_color` (
  `pages_id` int unsigned NOT NULL,
  `data` int unsigned NOT NULL,
  `sort` int unsigned NOT NULL,
  PRIMARY KEY (`pages_id`,`sort`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `field_page_color` (`pages_id`, `data`, `sort`) VALUES('1021', '2', '0');
INSERT INTO `field_page_color` (`pages_id`, `data`, `sort`) VALUES('1052', '2', '0');
INSERT INTO `field_page_color` (`pages_id`, `data`, `sort`) VALUES('1020', '2', '0');
INSERT INTO `field_page_color` (`pages_id`, `data`, `sort`) VALUES('1077', '2', '0');
INSERT INTO `field_page_color` (`pages_id`, `data`, `sort`) VALUES('1026', '2', '0');
INSERT INTO `field_page_color` (`pages_id`, `data`, `sort`) VALUES('1156', '2', '0');
INSERT INTO `field_page_color` (`pages_id`, `data`, `sort`) VALUES('1192', '2', '0');
INSERT INTO `field_page_color` (`pages_id`, `data`, `sort`) VALUES('1154', '2', '0');

DROP TABLE IF EXISTS `field_pass`;
CREATE TABLE `field_pass` (
  `pages_id` int unsigned NOT NULL,
  `data` char(40) NOT NULL,
  `salt` char(32) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=ascii;

INSERT INTO `field_pass` (`pages_id`, `data`, `salt`) VALUES('41', 'rRuy2J/8XkgpCb6KUJa81k43L99bvpC', '$2y$11$vx7h330W.hszUCwNxn8lo.');
INSERT INTO `field_pass` (`pages_id`, `data`, `salt`) VALUES('40', '', '');

DROP TABLE IF EXISTS `field_permissions`;
CREATE TABLE `field_permissions` (
  `pages_id` int unsigned NOT NULL,
  `data` int NOT NULL,
  `sort` int unsigned NOT NULL,
  PRIMARY KEY (`pages_id`,`sort`),
  KEY `data` (`data`,`pages_id`,`sort`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `field_permissions` (`pages_id`, `data`, `sort`) VALUES('38', '32', '1');
INSERT INTO `field_permissions` (`pages_id`, `data`, `sort`) VALUES('38', '34', '2');
INSERT INTO `field_permissions` (`pages_id`, `data`, `sort`) VALUES('38', '35', '3');
INSERT INTO `field_permissions` (`pages_id`, `data`, `sort`) VALUES('37', '36', '0');
INSERT INTO `field_permissions` (`pages_id`, `data`, `sort`) VALUES('38', '36', '0');
INSERT INTO `field_permissions` (`pages_id`, `data`, `sort`) VALUES('38', '50', '4');
INSERT INTO `field_permissions` (`pages_id`, `data`, `sort`) VALUES('38', '51', '5');
INSERT INTO `field_permissions` (`pages_id`, `data`, `sort`) VALUES('38', '52', '7');
INSERT INTO `field_permissions` (`pages_id`, `data`, `sort`) VALUES('38', '53', '8');
INSERT INTO `field_permissions` (`pages_id`, `data`, `sort`) VALUES('38', '54', '6');

DROP TABLE IF EXISTS `field_process`;
CREATE TABLE `field_process` (
  `pages_id` int NOT NULL DEFAULT '0',
  `data` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `field_process` (`pages_id`, `data`) VALUES('6', '17');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('3', '12');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('8', '12');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('9', '14');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('10', '7');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('11', '47');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('16', '48');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('300', '104');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('21', '50');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('29', '66');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('23', '10');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('304', '138');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('31', '136');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('22', '76');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('30', '68');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('303', '129');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('2', '87');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('302', '121');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('301', '109');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('28', '76');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('1007', '150');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('1009', '159');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('1011', '160');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('1015', '168');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('1017', '170');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('1048', '174');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('1067', '178');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('1069', '180');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('1073', '184');

DROP TABLE IF EXISTS `field_prod_custom_variant_item`;
CREATE TABLE `field_prod_custom_variant_item` (
  `pages_id` int unsigned NOT NULL,
  `data` text NOT NULL,
  `count` int NOT NULL,
  `parent_id` int NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(1)),
  KEY `count` (`count`,`pages_id`),
  KEY `parent_id` (`parent_id`,`pages_id`),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_prod_custom_variant_item` (`pages_id`, `data`, `count`, `parent_id`) VALUES('1251', '1253,1254,1255,1256,1257,1258,1259,1260,1261,1262,1263,1264', '12', '1252');

DROP TABLE IF EXISTS `field_prod_custom_variants`;
CREATE TABLE `field_prod_custom_variants` (
  `pages_id` int unsigned NOT NULL,
  `data` text NOT NULL,
  `count` int NOT NULL,
  `parent_id` int NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(1)),
  KEY `count` (`count`,`pages_id`),
  KEY `parent_id` (`parent_id`,`pages_id`),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_prod_custom_variants` (`pages_id`, `data`, `count`, `parent_id`) VALUES('1152', '1251', '1', '1244');

DROP TABLE IF EXISTS `field_prod_variant_item`;
CREATE TABLE `field_prod_variant_item` (
  `pages_id` int unsigned NOT NULL,
  `data` text NOT NULL,
  `count` int NOT NULL,
  `parent_id` int NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(1)),
  KEY `count` (`count`,`pages_id`),
  KEY `parent_id` (`parent_id`,`pages_id`),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `field_prod_variant_item` (`pages_id`, `data`, `count`, `parent_id`) VALUES('1088', '1090,1091,1092,1124,1125,1126', '6', '1089');
INSERT INTO `field_prod_variant_item` (`pages_id`, `data`, `count`, `parent_id`) VALUES('1133', '1135,1136,1137,1138,1139,1140,1141', '7', '1134');
INSERT INTO `field_prod_variant_item` (`pages_id`, `data`, `count`, `parent_id`) VALUES('1102', '1104,1105,1122,1123', '4', '1103');
INSERT INTO `field_prod_variant_item` (`pages_id`, `data`, `count`, `parent_id`) VALUES('1215', '1217,1218,1219', '3', '1216');
INSERT INTO `field_prod_variant_item` (`pages_id`, `data`, `count`, `parent_id`) VALUES('1200', '1202,1204,1205,1206,1207,1209', '6', '1201');
INSERT INTO `field_prod_variant_item` (`pages_id`, `data`, `count`, `parent_id`) VALUES('1210', '1212,1213,1214', '3', '1211');
INSERT INTO `field_prod_variant_item` (`pages_id`, `data`, `count`, `parent_id`) VALUES('1220', '1222', '1', '1221');
INSERT INTO `field_prod_variant_item` (`pages_id`, `data`, `count`, `parent_id`) VALUES('1223', '1225,1226', '2', '1224');

DROP TABLE IF EXISTS `field_prod_variants`;
CREATE TABLE `field_prod_variants` (
  `pages_id` int unsigned NOT NULL,
  `data` text NOT NULL,
  `count` int NOT NULL,
  `parent_id` int NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(1)),
  KEY `count` (`count`,`pages_id`),
  KEY `parent_id` (`parent_id`,`pages_id`),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `field_prod_variants` (`pages_id`, `data`, `count`, `parent_id`) VALUES('1077', '1088', '1', '1087');
INSERT INTO `field_prod_variants` (`pages_id`, `data`, `count`, `parent_id`) VALUES('1076', '1102', '1', '1101');
INSERT INTO `field_prod_variants` (`pages_id`, `data`, `count`, `parent_id`) VALUES('1110', '', '0', '1111');
INSERT INTO `field_prod_variants` (`pages_id`, `data`, `count`, `parent_id`) VALUES('1112', '', '0', '1113');
INSERT INTO `field_prod_variants` (`pages_id`, `data`, `count`, `parent_id`) VALUES('1083', '1133', '1', '1115');
INSERT INTO `field_prod_variants` (`pages_id`, `data`, `count`, `parent_id`) VALUES('1142', '', '0', '1143');
INSERT INTO `field_prod_variants` (`pages_id`, `data`, `count`, `parent_id`) VALUES('1144', '', '0', '1145');
INSERT INTO `field_prod_variants` (`pages_id`, `data`, `count`, `parent_id`) VALUES('1146', '', '0', '1147');
INSERT INTO `field_prod_variants` (`pages_id`, `data`, `count`, `parent_id`) VALUES('1148', '', '0', '1149');
INSERT INTO `field_prod_variants` (`pages_id`, `data`, `count`, `parent_id`) VALUES('1150', '', '0', '1151');
INSERT INTO `field_prod_variants` (`pages_id`, `data`, `count`, `parent_id`) VALUES('1152', '', '0', '1153');
INSERT INTO `field_prod_variants` (`pages_id`, `data`, `count`, `parent_id`) VALUES('1179', '', '0', '1180');
INSERT INTO `field_prod_variants` (`pages_id`, `data`, `count`, `parent_id`) VALUES('1158', '1200,1210', '2', '1159');
INSERT INTO `field_prod_variants` (`pages_id`, `data`, `count`, `parent_id`) VALUES('1160', '1215', '1', '1161');
INSERT INTO `field_prod_variants` (`pages_id`, `data`, `count`, `parent_id`) VALUES('1162', '1220,1223', '2', '1163');
INSERT INTO `field_prod_variants` (`pages_id`, `data`, `count`, `parent_id`) VALUES('1164', '', '0', '1165');
INSERT INTO `field_prod_variants` (`pages_id`, `data`, `count`, `parent_id`) VALUES('1166', '', '0', '1167');
INSERT INTO `field_prod_variants` (`pages_id`, `data`, `count`, `parent_id`) VALUES('1168', '', '0', '1169');
INSERT INTO `field_prod_variants` (`pages_id`, `data`, `count`, `parent_id`) VALUES('1170', '', '0', '1171');
INSERT INTO `field_prod_variants` (`pages_id`, `data`, `count`, `parent_id`) VALUES('1172', '', '0', '1173');
INSERT INTO `field_prod_variants` (`pages_id`, `data`, `count`, `parent_id`) VALUES('1174', '', '0', '1175');
INSERT INTO `field_prod_variants` (`pages_id`, `data`, `count`, `parent_id`) VALUES('1182', '', '0', '1183');
INSERT INTO `field_prod_variants` (`pages_id`, `data`, `count`, `parent_id`) VALUES('1184', '', '0', '1185');
INSERT INTO `field_prod_variants` (`pages_id`, `data`, `count`, `parent_id`) VALUES('1186', '', '0', '1187');
INSERT INTO `field_prod_variants` (`pages_id`, `data`, `count`, `parent_id`) VALUES('1188', '', '0', '1189');

DROP TABLE IF EXISTS `field_repeater_why`;
CREATE TABLE `field_repeater_why` (
  `pages_id` int unsigned NOT NULL,
  `data` text NOT NULL,
  `count` int NOT NULL,
  `parent_id` int NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(1)),
  KEY `count` (`count`,`pages_id`),
  KEY `parent_id` (`parent_id`,`pages_id`),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `field_repeater_why` (`pages_id`, `data`, `count`, `parent_id`) VALUES('1', '1039,1040,1041,1042', '4', '1034');

DROP TABLE IF EXISTS `field_roles`;
CREATE TABLE `field_roles` (
  `pages_id` int unsigned NOT NULL,
  `data` int NOT NULL,
  `sort` int unsigned NOT NULL,
  PRIMARY KEY (`pages_id`,`sort`),
  KEY `data` (`data`,`pages_id`,`sort`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `field_roles` (`pages_id`, `data`, `sort`) VALUES('40', '37', '0');
INSERT INTO `field_roles` (`pages_id`, `data`, `sort`) VALUES('41', '37', '0');
INSERT INTO `field_roles` (`pages_id`, `data`, `sort`) VALUES('41', '38', '2');

DROP TABLE IF EXISTS `field_shop_coverimage`;
CREATE TABLE `field_shop_coverimage` (
  `pages_id` int unsigned NOT NULL,
  `data` varchar(250) NOT NULL,
  `sort` int unsigned NOT NULL,
  `description` text NOT NULL,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `filedata` mediumtext,
  `filesize` int DEFAULT NULL,
  `created_users_id` int unsigned NOT NULL DEFAULT '0',
  `modified_users_id` int unsigned NOT NULL DEFAULT '0',
  `width` int DEFAULT NULL,
  `height` int DEFAULT NULL,
  `ratio` decimal(4,2) DEFAULT NULL,
  PRIMARY KEY (`pages_id`,`sort`),
  KEY `data` (`data`),
  KEY `modified` (`modified`),
  KEY `created` (`created`),
  KEY `filesize` (`filesize`),
  KEY `width` (`width`),
  KEY `height` (`height`),
  KEY `ratio` (`ratio`),
  FULLTEXT KEY `description` (`description`),
  FULLTEXT KEY `filedata` (`filedata`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_shop_coverimage` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`, `filesize`, `created_users_id`, `modified_users_id`, `width`, `height`, `ratio`) VALUES('1076', '83408820-a384-4e56-8cbb-08ffd5e17893.jpg', '0', '[\"\"]', '2020-05-27 09:01:18', '2020-05-27 09:01:18', '', '109986', '41', '41', '750', '1334', '0.56');
INSERT INTO `field_shop_coverimage` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`, `filesize`, `created_users_id`, `modified_users_id`, `width`, `height`, `ratio`) VALUES('1110', 'img_7463.jpeg', '0', '[\"\"]', '2020-05-27 09:01:30', '2020-05-27 09:01:30', '', '1866190', '41', '41', '4032', '3024', '1.33');
INSERT INTO `field_shop_coverimage` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`, `filesize`, `created_users_id`, `modified_users_id`, `width`, `height`, `ratio`) VALUES('1112', '56da4514-ddab-4f1f-8252-f28a60302e2f.jpg', '0', '[\"\"]', '2020-05-27 09:01:34', '2020-05-27 09:01:34', '', '182508', '41', '41', '1198', '1600', '0.75');

DROP TABLE IF EXISTS `field_simple_textblocks`;
CREATE TABLE `field_simple_textblocks` (
  `pages_id` int unsigned NOT NULL,
  `data` text NOT NULL,
  `count` int NOT NULL,
  `parent_id` int NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(1)),
  KEY `count` (`count`,`pages_id`),
  KEY `parent_id` (`parent_id`,`pages_id`),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `field_simple_textblocks` (`pages_id`, `data`, `count`, `parent_id`) VALUES('1', '1035,1036,1037,1038,1114', '5', '1032');

DROP TABLE IF EXISTS `field_snipcart_cart_custom_fields`;
CREATE TABLE `field_snipcart_cart_custom_fields` (
  `pages_id` int unsigned NOT NULL,
  `data` mediumtext NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `field_snipcart_cart_custom_fields` (`pages_id`, `data`) VALUES('1074', 'data-cart-custom1-name=\"By checking this box, I have read and agree to the <a href=\'https://www.domain.com/terms-and-conditions\' class=\'js-real-link\' target=\'_blank\'>Terms &amp; Conditions</a>\"\r\ndata-cart-custom1-options=\"true|false\"\r\ndata-cart-custom1-required=\"true\"');

DROP TABLE IF EXISTS `field_snipcart_item_categories`;
CREATE TABLE `field_snipcart_item_categories` (
  `pages_id` int unsigned NOT NULL,
  `data` int NOT NULL,
  `sort` int unsigned NOT NULL,
  PRIMARY KEY (`pages_id`,`sort`),
  KEY `data` (`data`,`pages_id`,`sort`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


DROP TABLE IF EXISTS `field_snipcart_item_custom_fields`;
CREATE TABLE `field_snipcart_item_custom_fields` (
  `pages_id` int unsigned NOT NULL,
  `data` mediumtext NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


DROP TABLE IF EXISTS `field_snipcart_item_description`;
CREATE TABLE `field_snipcart_item_description` (
  `pages_id` int unsigned NOT NULL,
  `data` mediumtext NOT NULL,
  `data1012` mediumtext,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`),
  FULLTEXT KEY `data1012` (`data1012`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_snipcart_item_description` (`pages_id`, `data`, `data1012`) VALUES('1077', '<p>One fits all.</p>\n\n<p>Wir bieten Dir hier ein Twintip das dem Fahrer nach den ersten Metern ein zufriedenes Lächeln ins Gesicht zaubert und sofort durch viel Kontrolle, Laufruhe und höchsten Komfort überzeugt.</p>\n\n<p><strong>Auf Anfrage bieten wir Dir verschiedenen Flex an:</strong><br />\nSoft- sehr leichte Fahrer bis 55 kg<br />\nMedium(Standart) 55 bis 85 kg<br />\nHart 85 bis Team Flex- betrifft Dich wenn Du gerne sehr sportlich im Big Air unterwegs bist und Kiteloops auf´s Wasser bringst. Und das unabhängig vom Fahrlevel. Garantiert!</p>', '');
INSERT INTO `field_snipcart_item_description` (`pages_id`, `data`, `data1012`) VALUES('1078', 'Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', NULL);
INSERT INTO `field_snipcart_item_description` (`pages_id`, `data`, `data1012`) VALUES('1079', 'Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.', NULL);
INSERT INTO `field_snipcart_item_description` (`pages_id`, `data`, `data1012`) VALUES('1083', '<p>Mit dem Freeride plus sprechen wir die Fahrer an die den extra Grip suchen und etwas Komfort gegen direkteres Feedback tauschen möchten .</p>\n\n<p>Das V Basis im Board auf dem all unsere Boards aufgebaut sind fällt hier etwas geringer aus, die Doppel Concave wurde erhöht und mit Channels ergänzt in Nose und Tail.</p>\n\n<p><strong>Auf Anfrage bieten wir Dir verschiedenen Flex an:</strong><br />\nSoft- sehr leichte Fahrer bis 55 kg<br />\nMedium(Standart) 55 bis 85 kg<br />\nHart 85 bis Team Flex- betrifft Dich wenn Du gerne sehr sportlich im Big Air unterwegs bist und Kiteloops auf´s Wasser bringst. Und das unabhängig vom Fahrlevel. Garantiert!</p>', '');
INSERT INTO `field_snipcart_item_description` (`pages_id`, `data`, `data1012`) VALUES('1142', '<p>Hier wird der unhooked Fahrer auf seine Kosten kommen.</p>\n\n<p>Dieses Board bietet Dir explosiven Pop und Grip satt. Auf spontanen Pop reagiert das Board mit einer Explosion, hier wirst Du Dich beim erstmal erschrecken und verlieben.</p>\n\n<p>Wir setzen beim Freestyler absichtlich auf Schlaufen und verzichten auf Boots um noch so viel wie möglich an Flex und Komfort im Board zurück zu lassen.<br />\nMehr Flex bedeutet mehr Laufruhe und mehr pop. Boots Variante nur auf Anfrage.</p>', '');
INSERT INTO `field_snipcart_item_description` (`pages_id`, `data`, `data1012`) VALUES('1144', '', '');
INSERT INTO `field_snipcart_item_description` (`pages_id`, `data`, `data1012`) VALUES('1146', '', '');
INSERT INTO `field_snipcart_item_description` (`pages_id`, `data`, `data1012`) VALUES('1148', '', '');
INSERT INTO `field_snipcart_item_description` (`pages_id`, `data`, `data1012`) VALUES('1150', '', '');
INSERT INTO `field_snipcart_item_description` (`pages_id`, `data`, `data1012`) VALUES('1158', '', '');
INSERT INTO `field_snipcart_item_description` (`pages_id`, `data`, `data1012`) VALUES('1160', '', '');
INSERT INTO `field_snipcart_item_description` (`pages_id`, `data`, `data1012`) VALUES('1162', '', '');
INSERT INTO `field_snipcart_item_description` (`pages_id`, `data`, `data1012`) VALUES('1164', '', '');
INSERT INTO `field_snipcart_item_description` (`pages_id`, `data`, `data1012`) VALUES('1166', '', '');
INSERT INTO `field_snipcart_item_description` (`pages_id`, `data`, `data1012`) VALUES('1168', '', '');
INSERT INTO `field_snipcart_item_description` (`pages_id`, `data`, `data1012`) VALUES('1170', '', '');
INSERT INTO `field_snipcart_item_description` (`pages_id`, `data`, `data1012`) VALUES('1172', '', '');
INSERT INTO `field_snipcart_item_description` (`pages_id`, `data`, `data1012`) VALUES('1174', '', '');
INSERT INTO `field_snipcart_item_description` (`pages_id`, `data`, `data1012`) VALUES('1182', '', '');
INSERT INTO `field_snipcart_item_description` (`pages_id`, `data`, `data1012`) VALUES('1186', '', '');

DROP TABLE IF EXISTS `field_snipcart_item_height`;
CREATE TABLE `field_snipcart_item_height` (
  `pages_id` int unsigned NOT NULL,
  `data` int NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


DROP TABLE IF EXISTS `field_snipcart_item_id`;
CREATE TABLE `field_snipcart_item_id` (
  `pages_id` int unsigned NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `field_snipcart_item_id` (`pages_id`, `data`) VALUES('1077', 'FREE-10001');
INSERT INTO `field_snipcart_item_id` (`pages_id`, `data`) VALUES('1078', 'BEER-10002');
INSERT INTO `field_snipcart_item_id` (`pages_id`, `data`) VALUES('1079', 'BEER-10003');
INSERT INTO `field_snipcart_item_id` (`pages_id`, `data`) VALUES('1080', '1080');
INSERT INTO `field_snipcart_item_id` (`pages_id`, `data`) VALUES('1083', 'lightwind-planb-2');
INSERT INTO `field_snipcart_item_id` (`pages_id`, `data`) VALUES('1142', 'free-3');
INSERT INTO `field_snipcart_item_id` (`pages_id`, `data`) VALUES('1144', '1144');
INSERT INTO `field_snipcart_item_id` (`pages_id`, `data`) VALUES('1146', '1146');
INSERT INTO `field_snipcart_item_id` (`pages_id`, `data`) VALUES('1148', '1148');
INSERT INTO `field_snipcart_item_id` (`pages_id`, `data`) VALUES('1150', '1150');
INSERT INTO `field_snipcart_item_id` (`pages_id`, `data`) VALUES('1158', '1158');
INSERT INTO `field_snipcart_item_id` (`pages_id`, `data`) VALUES('1160', '1160');
INSERT INTO `field_snipcart_item_id` (`pages_id`, `data`) VALUES('1162', '1162');
INSERT INTO `field_snipcart_item_id` (`pages_id`, `data`) VALUES('1164', '1164');
INSERT INTO `field_snipcart_item_id` (`pages_id`, `data`) VALUES('1166', '1166');
INSERT INTO `field_snipcart_item_id` (`pages_id`, `data`) VALUES('1168', '1168');
INSERT INTO `field_snipcart_item_id` (`pages_id`, `data`) VALUES('1170', '1170');
INSERT INTO `field_snipcart_item_id` (`pages_id`, `data`) VALUES('1172', '1172');
INSERT INTO `field_snipcart_item_id` (`pages_id`, `data`) VALUES('1174', '1174');
INSERT INTO `field_snipcart_item_id` (`pages_id`, `data`) VALUES('1182', '1182');
INSERT INTO `field_snipcart_item_id` (`pages_id`, `data`) VALUES('1186', '1186');

DROP TABLE IF EXISTS `field_snipcart_item_image`;
CREATE TABLE `field_snipcart_item_image` (
  `pages_id` int unsigned NOT NULL,
  `data` varchar(250) NOT NULL,
  `sort` int unsigned NOT NULL,
  `description` text NOT NULL,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `filedata` mediumtext,
  `filesize` int DEFAULT NULL,
  `created_users_id` int unsigned NOT NULL DEFAULT '0',
  `modified_users_id` int unsigned NOT NULL DEFAULT '0',
  `width` int DEFAULT NULL,
  `height` int DEFAULT NULL,
  `ratio` decimal(4,2) DEFAULT NULL,
  PRIMARY KEY (`pages_id`,`sort`),
  KEY `data` (`data`),
  KEY `modified` (`modified`),
  KEY `created` (`created`),
  KEY `filesize` (`filesize`),
  KEY `width` (`width`),
  KEY `height` (`height`),
  KEY `ratio` (`ratio`),
  FULLTEXT KEY `description` (`description`),
  FULLTEXT KEY `filedata` (`filedata`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `field_snipcart_item_image` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`, `filesize`, `created_users_id`, `modified_users_id`, `width`, `height`, `ratio`) VALUES('1083', 'beaver_2020_v2_front_1.png', '0', '[\"\"]', '2020-04-24 23:55:16', '2020-04-24 23:55:16', '', '4516205', '41', '41', '3024', '4032', '0.75');
INSERT INTO `field_snipcart_item_image` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`, `filesize`, `created_users_id`, `modified_users_id`, `width`, `height`, `ratio`) VALUES('1078', 'beer2.jpg', '0', '[\"\"]', '2020-04-24 23:04:40', '2020-04-24 23:04:40', '', '499786', '41', '41', '1600', '1200', '1.33');
INSERT INTO `field_snipcart_item_image` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`, `filesize`, `created_users_id`, `modified_users_id`, `width`, `height`, `ratio`) VALUES('1079', 'beer3.jpg', '0', '[\"\"]', '2020-04-24 23:04:40', '2020-04-24 23:04:40', '', '487606', '41', '41', '1600', '1200', '1.33');
INSERT INTO `field_snipcart_item_image` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`, `filesize`, `created_users_id`, `modified_users_id`, `width`, `height`, `ratio`) VALUES('1077', 'ladys_front.png', '0', '[\"\"]', '2020-04-24 23:34:49', '2020-04-24 23:34:49', '', '14450572', '41', '41', '3128', '5568', '0.56');
INSERT INTO `field_snipcart_item_image` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`, `filesize`, `created_users_id`, `modified_users_id`, `width`, `height`, `ratio`) VALUES('1142', '83408820-a384-4e56-8cbb-08ffd5e17893.jpg', '0', '[\"\"]', '2020-06-13 19:40:46', '2020-06-13 19:40:46', '', '109986', '41', '41', '750', '1334', '0.56');
INSERT INTO `field_snipcart_item_image` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`, `filesize`, `created_users_id`, `modified_users_id`, `width`, `height`, `ratio`) VALUES('1164', 'top_skully_red_3020.jpg', '1', '[\"\"]', '2021-03-07 13:44:09', '2021-03-07 13:44:09', '', '1270870', '41', '41', '2894', '9094', '0.32');
INSERT INTO `field_snipcart_item_image` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`, `filesize`, `created_users_id`, `modified_users_id`, `width`, `height`, `ratio`) VALUES('1160', 'lady_bot_01-pink_4010.jpg', '1', '[\"\"]', '2021-03-07 13:35:11', '2021-03-07 13:35:11', '', '1446432', '41', '41', '2835', '9331', '0.30');
INSERT INTO `field_snipcart_item_image` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`, `filesize`, `created_users_id`, `modified_users_id`, `width`, `height`, `ratio`) VALUES('1162', 'twintip-front-standard-450x600-min.png', '0', '[\"\"]', '2021-03-07 12:30:13', '2021-03-07 12:30:13', '', '57363', '41', '41', '450', '600', '0.75');
INSERT INTO `field_snipcart_item_image` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`, `filesize`, `created_users_id`, `modified_users_id`, `width`, `height`, `ratio`) VALUES('1166', 'explorer_bottom.jpg', '1', '[\"\"]', '2021-03-07 13:43:48', '2021-03-07 13:43:48', '', '67672', '41', '41', '313', '1024', '0.31');
INSERT INTO `field_snipcart_item_image` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`, `filesize`, `created_users_id`, `modified_users_id`, `width`, `height`, `ratio`) VALUES('1168', 'twintip-front-standard-450x600-min.png', '0', '[\"\"]', '2021-03-07 12:32:17', '2021-03-07 12:32:17', '', '57363', '41', '41', '450', '600', '0.75');
INSERT INTO `field_snipcart_item_image` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`, `filesize`, `created_users_id`, `modified_users_id`, `width`, `height`, `ratio`) VALUES('1170', 'twintip-front-standard-450x600-min.png', '0', '[\"\"]', '2021-03-07 12:32:49', '2021-03-07 12:32:49', '', '57363', '41', '41', '450', '600', '0.75');
INSERT INTO `field_snipcart_item_image` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`, `filesize`, `created_users_id`, `modified_users_id`, `width`, `height`, `ratio`) VALUES('1172', 'twintip-front-standard-450x600-min.png', '0', '[\"\"]', '2021-03-07 12:33:42', '2021-03-07 12:33:42', '', '57363', '41', '41', '450', '600', '0.75');
INSERT INTO `field_snipcart_item_image` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`, `filesize`, `created_users_id`, `modified_users_id`, `width`, `height`, `ratio`) VALUES('1174', 'twintip-front-standard-450x600-min.png', '0', '[\"\"]', '2021-03-07 12:34:02', '2021-03-07 12:34:02', '', '57363', '41', '41', '450', '600', '0.75');
INSERT INTO `field_snipcart_item_image` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`, `filesize`, `created_users_id`, `modified_users_id`, `width`, `height`, `ratio`) VALUES('1182', 'twintip-front-standard-450x600-min.png', '0', '[\"\"]', '2021-03-07 12:37:45', '2021-03-07 12:37:45', '', '57363', '41', '41', '450', '600', '0.75');
INSERT INTO `field_snipcart_item_image` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`, `filesize`, `created_users_id`, `modified_users_id`, `width`, `height`, `ratio`) VALUES('1186', 'twintip-front-standard-450x600-min.png', '0', '[\"\"]', '2021-03-07 12:39:03', '2021-03-07 12:39:03', '', '57363', '41', '41', '450', '600', '0.75');
INSERT INTO `field_snipcart_item_image` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`, `filesize`, `created_users_id`, `modified_users_id`, `width`, `height`, `ratio`) VALUES('1164', 'skully_white_orange.jpg', '0', '[\"\"]', '2021-03-07 13:44:09', '2021-03-07 13:44:09', '', '1296430', '41', '41', '2894', '9094', '0.32');
INSERT INTO `field_snipcart_item_image` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`, `filesize`, `created_users_id`, `modified_users_id`, `width`, `height`, `ratio`) VALUES('1158', 'standart_bot_01-white_9003.jpg', '1', '[\"\"]', '2021-03-07 13:22:20', '2021-03-07 13:22:20', '', '1481015', '41', '41', '2835', '9331', '0.30');
INSERT INTO `field_snipcart_item_image` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`, `filesize`, `created_users_id`, `modified_users_id`, `width`, `height`, `ratio`) VALUES('1158', 'standart_top_01-white_9003.jpg', '0', '[\"\"]', '2021-03-07 13:22:20', '2021-03-07 13:22:20', '', '1156518', '41', '41', '2835', '9331', '0.30');
INSERT INTO `field_snipcart_item_image` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`, `filesize`, `created_users_id`, `modified_users_id`, `width`, `height`, `ratio`) VALUES('1160', 'lady_top_01-pink_4010.jpg', '0', '[\"\"]', '2021-03-07 13:35:11', '2021-03-07 13:35:11', '', '1142278', '41', '41', '2835', '9331', '0.30');
INSERT INTO `field_snipcart_item_image` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`, `filesize`, `created_users_id`, `modified_users_id`, `width`, `height`, `ratio`) VALUES('1166', 'explorer_top.jpg', '0', '[\"\"]', '2021-03-07 13:43:48', '2021-03-07 13:43:48', '', '70805', '41', '41', '313', '1024', '0.31');

DROP TABLE IF EXISTS `field_snipcart_item_length`;
CREATE TABLE `field_snipcart_item_length` (
  `pages_id` int unsigned NOT NULL,
  `data` int NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


DROP TABLE IF EXISTS `field_snipcart_item_max_quantity`;
CREATE TABLE `field_snipcart_item_max_quantity` (
  `pages_id` int unsigned NOT NULL,
  `data` int NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


DROP TABLE IF EXISTS `field_snipcart_item_min_quantity`;
CREATE TABLE `field_snipcart_item_min_quantity` (
  `pages_id` int unsigned NOT NULL,
  `data` int NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


DROP TABLE IF EXISTS `field_snipcart_item_payment_interval`;
CREATE TABLE `field_snipcart_item_payment_interval` (
  `pages_id` int unsigned NOT NULL,
  `data` int unsigned NOT NULL,
  `sort` int unsigned NOT NULL,
  PRIMARY KEY (`pages_id`,`sort`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


DROP TABLE IF EXISTS `field_snipcart_item_payment_interval_count`;
CREATE TABLE `field_snipcart_item_payment_interval_count` (
  `pages_id` int unsigned NOT NULL,
  `data` int NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


DROP TABLE IF EXISTS `field_snipcart_item_payment_trial`;
CREATE TABLE `field_snipcart_item_payment_trial` (
  `pages_id` int unsigned NOT NULL,
  `data` int NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


DROP TABLE IF EXISTS `field_snipcart_item_price_eur`;
CREATE TABLE `field_snipcart_item_price_eur` (
  `pages_id` int unsigned NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `field_snipcart_item_price_eur` (`pages_id`, `data`) VALUES('1077', '1099');
INSERT INTO `field_snipcart_item_price_eur` (`pages_id`, `data`) VALUES('1078', '19.90');
INSERT INTO `field_snipcart_item_price_eur` (`pages_id`, `data`) VALUES('1079', '1199');
INSERT INTO `field_snipcart_item_price_eur` (`pages_id`, `data`) VALUES('1083', '1099');
INSERT INTO `field_snipcart_item_price_eur` (`pages_id`, `data`) VALUES('1090', '0');
INSERT INTO `field_snipcart_item_price_eur` (`pages_id`, `data`) VALUES('1091', '0');
INSERT INTO `field_snipcart_item_price_eur` (`pages_id`, `data`) VALUES('1092', '0');
INSERT INTO `field_snipcart_item_price_eur` (`pages_id`, `data`) VALUES('1142', '1099');
INSERT INTO `field_snipcart_item_price_eur` (`pages_id`, `data`) VALUES('1104', '0');
INSERT INTO `field_snipcart_item_price_eur` (`pages_id`, `data`) VALUES('1105', '0');
INSERT INTO `field_snipcart_item_price_eur` (`pages_id`, `data`) VALUES('1125', '0');
INSERT INTO `field_snipcart_item_price_eur` (`pages_id`, `data`) VALUES('1124', '0');
INSERT INTO `field_snipcart_item_price_eur` (`pages_id`, `data`) VALUES('1122', '0');
INSERT INTO `field_snipcart_item_price_eur` (`pages_id`, `data`) VALUES('1123', '0');
INSERT INTO `field_snipcart_item_price_eur` (`pages_id`, `data`) VALUES('1126', '0');
INSERT INTO `field_snipcart_item_price_eur` (`pages_id`, `data`) VALUES('1135', '0');
INSERT INTO `field_snipcart_item_price_eur` (`pages_id`, `data`) VALUES('1136', '0');
INSERT INTO `field_snipcart_item_price_eur` (`pages_id`, `data`) VALUES('1137', '0');
INSERT INTO `field_snipcart_item_price_eur` (`pages_id`, `data`) VALUES('1138', '0');
INSERT INTO `field_snipcart_item_price_eur` (`pages_id`, `data`) VALUES('1139', '0');
INSERT INTO `field_snipcart_item_price_eur` (`pages_id`, `data`) VALUES('1140', '0');
INSERT INTO `field_snipcart_item_price_eur` (`pages_id`, `data`) VALUES('1141', '0');
INSERT INTO `field_snipcart_item_price_eur` (`pages_id`, `data`) VALUES('1144', '1234');
INSERT INTO `field_snipcart_item_price_eur` (`pages_id`, `data`) VALUES('1158', '1099');
INSERT INTO `field_snipcart_item_price_eur` (`pages_id`, `data`) VALUES('1160', '1099');
INSERT INTO `field_snipcart_item_price_eur` (`pages_id`, `data`) VALUES('1162', '1099');
INSERT INTO `field_snipcart_item_price_eur` (`pages_id`, `data`) VALUES('1164', '1099');
INSERT INTO `field_snipcart_item_price_eur` (`pages_id`, `data`) VALUES('1166', '1099');
INSERT INTO `field_snipcart_item_price_eur` (`pages_id`, `data`) VALUES('1168', '1099');
INSERT INTO `field_snipcart_item_price_eur` (`pages_id`, `data`) VALUES('1170', '1099');
INSERT INTO `field_snipcart_item_price_eur` (`pages_id`, `data`) VALUES('1172', '1099');
INSERT INTO `field_snipcart_item_price_eur` (`pages_id`, `data`) VALUES('1174', '1099');
INSERT INTO `field_snipcart_item_price_eur` (`pages_id`, `data`) VALUES('1182', '1099');
INSERT INTO `field_snipcart_item_price_eur` (`pages_id`, `data`) VALUES('1186', '29');
INSERT INTO `field_snipcart_item_price_eur` (`pages_id`, `data`) VALUES('1217', '0');
INSERT INTO `field_snipcart_item_price_eur` (`pages_id`, `data`) VALUES('1202', '0');
INSERT INTO `field_snipcart_item_price_eur` (`pages_id`, `data`) VALUES('1204', '0');
INSERT INTO `field_snipcart_item_price_eur` (`pages_id`, `data`) VALUES('1205', '0');
INSERT INTO `field_snipcart_item_price_eur` (`pages_id`, `data`) VALUES('1206', '0');
INSERT INTO `field_snipcart_item_price_eur` (`pages_id`, `data`) VALUES('1207', '0');
INSERT INTO `field_snipcart_item_price_eur` (`pages_id`, `data`) VALUES('1209', '0');
INSERT INTO `field_snipcart_item_price_eur` (`pages_id`, `data`) VALUES('1212', '0');
INSERT INTO `field_snipcart_item_price_eur` (`pages_id`, `data`) VALUES('1213', '0');
INSERT INTO `field_snipcart_item_price_eur` (`pages_id`, `data`) VALUES('1214', '0');
INSERT INTO `field_snipcart_item_price_eur` (`pages_id`, `data`) VALUES('1218', '0');
INSERT INTO `field_snipcart_item_price_eur` (`pages_id`, `data`) VALUES('1219', '0');
INSERT INTO `field_snipcart_item_price_eur` (`pages_id`, `data`) VALUES('1222', '0');
INSERT INTO `field_snipcart_item_price_eur` (`pages_id`, `data`) VALUES('1225', '0');
INSERT INTO `field_snipcart_item_price_eur` (`pages_id`, `data`) VALUES('1226', '0');
INSERT INTO `field_snipcart_item_price_eur` (`pages_id`, `data`) VALUES('1255', '40');
INSERT INTO `field_snipcart_item_price_eur` (`pages_id`, `data`) VALUES('1254', '40');
INSERT INTO `field_snipcart_item_price_eur` (`pages_id`, `data`) VALUES('1253', '0');
INSERT INTO `field_snipcart_item_price_eur` (`pages_id`, `data`) VALUES('1256', '40');
INSERT INTO `field_snipcart_item_price_eur` (`pages_id`, `data`) VALUES('1257', '40');
INSERT INTO `field_snipcart_item_price_eur` (`pages_id`, `data`) VALUES('1258', '40');
INSERT INTO `field_snipcart_item_price_eur` (`pages_id`, `data`) VALUES('1259', '40');
INSERT INTO `field_snipcart_item_price_eur` (`pages_id`, `data`) VALUES('1260', '40');
INSERT INTO `field_snipcart_item_price_eur` (`pages_id`, `data`) VALUES('1261', '40');
INSERT INTO `field_snipcart_item_price_eur` (`pages_id`, `data`) VALUES('1262', '40');
INSERT INTO `field_snipcart_item_price_eur` (`pages_id`, `data`) VALUES('1263', '40');
INSERT INTO `field_snipcart_item_price_eur` (`pages_id`, `data`) VALUES('1264', '40');

DROP TABLE IF EXISTS `field_snipcart_item_quantity`;
CREATE TABLE `field_snipcart_item_quantity` (
  `pages_id` int unsigned NOT NULL,
  `data` int NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `field_snipcart_item_quantity` (`pages_id`, `data`) VALUES('1077', '1');
INSERT INTO `field_snipcart_item_quantity` (`pages_id`, `data`) VALUES('1083', '1');
INSERT INTO `field_snipcart_item_quantity` (`pages_id`, `data`) VALUES('1142', '1');
INSERT INTO `field_snipcart_item_quantity` (`pages_id`, `data`) VALUES('1144', '1');
INSERT INTO `field_snipcart_item_quantity` (`pages_id`, `data`) VALUES('1146', '1');
INSERT INTO `field_snipcart_item_quantity` (`pages_id`, `data`) VALUES('1148', '1');
INSERT INTO `field_snipcart_item_quantity` (`pages_id`, `data`) VALUES('1150', '1');
INSERT INTO `field_snipcart_item_quantity` (`pages_id`, `data`) VALUES('1158', '1');
INSERT INTO `field_snipcart_item_quantity` (`pages_id`, `data`) VALUES('1160', '1');
INSERT INTO `field_snipcart_item_quantity` (`pages_id`, `data`) VALUES('1162', '1');
INSERT INTO `field_snipcart_item_quantity` (`pages_id`, `data`) VALUES('1164', '1');
INSERT INTO `field_snipcart_item_quantity` (`pages_id`, `data`) VALUES('1166', '1');
INSERT INTO `field_snipcart_item_quantity` (`pages_id`, `data`) VALUES('1168', '1');
INSERT INTO `field_snipcart_item_quantity` (`pages_id`, `data`) VALUES('1170', '1');
INSERT INTO `field_snipcart_item_quantity` (`pages_id`, `data`) VALUES('1172', '1');
INSERT INTO `field_snipcart_item_quantity` (`pages_id`, `data`) VALUES('1174', '1');
INSERT INTO `field_snipcart_item_quantity` (`pages_id`, `data`) VALUES('1182', '1');
INSERT INTO `field_snipcart_item_quantity` (`pages_id`, `data`) VALUES('1186', '1');

DROP TABLE IF EXISTS `field_snipcart_item_quantity_step`;
CREATE TABLE `field_snipcart_item_quantity_step` (
  `pages_id` int unsigned NOT NULL,
  `data` int NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `field_snipcart_item_quantity_step` (`pages_id`, `data`) VALUES('1077', '1');
INSERT INTO `field_snipcart_item_quantity_step` (`pages_id`, `data`) VALUES('1083', '1');
INSERT INTO `field_snipcart_item_quantity_step` (`pages_id`, `data`) VALUES('1142', '1');
INSERT INTO `field_snipcart_item_quantity_step` (`pages_id`, `data`) VALUES('1144', '1');
INSERT INTO `field_snipcart_item_quantity_step` (`pages_id`, `data`) VALUES('1146', '1');
INSERT INTO `field_snipcart_item_quantity_step` (`pages_id`, `data`) VALUES('1148', '1');
INSERT INTO `field_snipcart_item_quantity_step` (`pages_id`, `data`) VALUES('1150', '1');
INSERT INTO `field_snipcart_item_quantity_step` (`pages_id`, `data`) VALUES('1158', '1');
INSERT INTO `field_snipcart_item_quantity_step` (`pages_id`, `data`) VALUES('1160', '1');
INSERT INTO `field_snipcart_item_quantity_step` (`pages_id`, `data`) VALUES('1162', '1');
INSERT INTO `field_snipcart_item_quantity_step` (`pages_id`, `data`) VALUES('1164', '1');
INSERT INTO `field_snipcart_item_quantity_step` (`pages_id`, `data`) VALUES('1166', '1');
INSERT INTO `field_snipcart_item_quantity_step` (`pages_id`, `data`) VALUES('1168', '1');
INSERT INTO `field_snipcart_item_quantity_step` (`pages_id`, `data`) VALUES('1170', '1');
INSERT INTO `field_snipcart_item_quantity_step` (`pages_id`, `data`) VALUES('1172', '1');
INSERT INTO `field_snipcart_item_quantity_step` (`pages_id`, `data`) VALUES('1174', '1');
INSERT INTO `field_snipcart_item_quantity_step` (`pages_id`, `data`) VALUES('1182', '1');
INSERT INTO `field_snipcart_item_quantity_step` (`pages_id`, `data`) VALUES('1186', '1');

DROP TABLE IF EXISTS `field_snipcart_item_recurring_shipping`;
CREATE TABLE `field_snipcart_item_recurring_shipping` (
  `pages_id` int unsigned NOT NULL,
  `data` tinyint NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


DROP TABLE IF EXISTS `field_snipcart_item_shippable`;
CREATE TABLE `field_snipcart_item_shippable` (
  `pages_id` int unsigned NOT NULL,
  `data` tinyint NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `field_snipcart_item_shippable` (`pages_id`, `data`) VALUES('1077', '1');
INSERT INTO `field_snipcart_item_shippable` (`pages_id`, `data`) VALUES('1078', '1');
INSERT INTO `field_snipcart_item_shippable` (`pages_id`, `data`) VALUES('1079', '1');
INSERT INTO `field_snipcart_item_shippable` (`pages_id`, `data`) VALUES('1080', '1');
INSERT INTO `field_snipcart_item_shippable` (`pages_id`, `data`) VALUES('1142', '1');
INSERT INTO `field_snipcart_item_shippable` (`pages_id`, `data`) VALUES('1144', '1');
INSERT INTO `field_snipcart_item_shippable` (`pages_id`, `data`) VALUES('1146', '1');
INSERT INTO `field_snipcart_item_shippable` (`pages_id`, `data`) VALUES('1148', '1');
INSERT INTO `field_snipcart_item_shippable` (`pages_id`, `data`) VALUES('1150', '1');
INSERT INTO `field_snipcart_item_shippable` (`pages_id`, `data`) VALUES('1158', '1');
INSERT INTO `field_snipcart_item_shippable` (`pages_id`, `data`) VALUES('1160', '1');
INSERT INTO `field_snipcart_item_shippable` (`pages_id`, `data`) VALUES('1162', '1');
INSERT INTO `field_snipcart_item_shippable` (`pages_id`, `data`) VALUES('1164', '1');
INSERT INTO `field_snipcart_item_shippable` (`pages_id`, `data`) VALUES('1166', '1');
INSERT INTO `field_snipcart_item_shippable` (`pages_id`, `data`) VALUES('1170', '1');
INSERT INTO `field_snipcart_item_shippable` (`pages_id`, `data`) VALUES('1172', '1');
INSERT INTO `field_snipcart_item_shippable` (`pages_id`, `data`) VALUES('1174', '1');
INSERT INTO `field_snipcart_item_shippable` (`pages_id`, `data`) VALUES('1182', '1');
INSERT INTO `field_snipcart_item_shippable` (`pages_id`, `data`) VALUES('1186', '1');

DROP TABLE IF EXISTS `field_snipcart_item_stackable`;
CREATE TABLE `field_snipcart_item_stackable` (
  `pages_id` int unsigned NOT NULL,
  `data` tinyint NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `field_snipcart_item_stackable` (`pages_id`, `data`) VALUES('1078', '1');
INSERT INTO `field_snipcart_item_stackable` (`pages_id`, `data`) VALUES('1079', '1');
INSERT INTO `field_snipcart_item_stackable` (`pages_id`, `data`) VALUES('1080', '1');
INSERT INTO `field_snipcart_item_stackable` (`pages_id`, `data`) VALUES('1142', '1');
INSERT INTO `field_snipcart_item_stackable` (`pages_id`, `data`) VALUES('1144', '1');
INSERT INTO `field_snipcart_item_stackable` (`pages_id`, `data`) VALUES('1146', '1');
INSERT INTO `field_snipcart_item_stackable` (`pages_id`, `data`) VALUES('1148', '1');
INSERT INTO `field_snipcart_item_stackable` (`pages_id`, `data`) VALUES('1150', '1');
INSERT INTO `field_snipcart_item_stackable` (`pages_id`, `data`) VALUES('1158', '1');
INSERT INTO `field_snipcart_item_stackable` (`pages_id`, `data`) VALUES('1160', '1');
INSERT INTO `field_snipcart_item_stackable` (`pages_id`, `data`) VALUES('1162', '1');
INSERT INTO `field_snipcart_item_stackable` (`pages_id`, `data`) VALUES('1164', '1');
INSERT INTO `field_snipcart_item_stackable` (`pages_id`, `data`) VALUES('1166', '1');
INSERT INTO `field_snipcart_item_stackable` (`pages_id`, `data`) VALUES('1168', '1');
INSERT INTO `field_snipcart_item_stackable` (`pages_id`, `data`) VALUES('1170', '1');
INSERT INTO `field_snipcart_item_stackable` (`pages_id`, `data`) VALUES('1172', '1');
INSERT INTO `field_snipcart_item_stackable` (`pages_id`, `data`) VALUES('1174', '1');
INSERT INTO `field_snipcart_item_stackable` (`pages_id`, `data`) VALUES('1182', '1');
INSERT INTO `field_snipcart_item_stackable` (`pages_id`, `data`) VALUES('1186', '1');

DROP TABLE IF EXISTS `field_snipcart_item_taxable`;
CREATE TABLE `field_snipcart_item_taxable` (
  `pages_id` int unsigned NOT NULL,
  `data` tinyint NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `field_snipcart_item_taxable` (`pages_id`, `data`) VALUES('1077', '1');
INSERT INTO `field_snipcart_item_taxable` (`pages_id`, `data`) VALUES('1078', '1');
INSERT INTO `field_snipcart_item_taxable` (`pages_id`, `data`) VALUES('1079', '1');
INSERT INTO `field_snipcart_item_taxable` (`pages_id`, `data`) VALUES('1080', '1');
INSERT INTO `field_snipcart_item_taxable` (`pages_id`, `data`) VALUES('1142', '1');
INSERT INTO `field_snipcart_item_taxable` (`pages_id`, `data`) VALUES('1144', '1');
INSERT INTO `field_snipcart_item_taxable` (`pages_id`, `data`) VALUES('1146', '1');
INSERT INTO `field_snipcart_item_taxable` (`pages_id`, `data`) VALUES('1148', '1');
INSERT INTO `field_snipcart_item_taxable` (`pages_id`, `data`) VALUES('1150', '1');
INSERT INTO `field_snipcart_item_taxable` (`pages_id`, `data`) VALUES('1158', '1');
INSERT INTO `field_snipcart_item_taxable` (`pages_id`, `data`) VALUES('1160', '1');
INSERT INTO `field_snipcart_item_taxable` (`pages_id`, `data`) VALUES('1162', '1');
INSERT INTO `field_snipcart_item_taxable` (`pages_id`, `data`) VALUES('1164', '1');
INSERT INTO `field_snipcart_item_taxable` (`pages_id`, `data`) VALUES('1166', '1');
INSERT INTO `field_snipcart_item_taxable` (`pages_id`, `data`) VALUES('1168', '1');
INSERT INTO `field_snipcart_item_taxable` (`pages_id`, `data`) VALUES('1170', '1');
INSERT INTO `field_snipcart_item_taxable` (`pages_id`, `data`) VALUES('1172', '1');
INSERT INTO `field_snipcart_item_taxable` (`pages_id`, `data`) VALUES('1174', '1');
INSERT INTO `field_snipcart_item_taxable` (`pages_id`, `data`) VALUES('1182', '1');
INSERT INTO `field_snipcart_item_taxable` (`pages_id`, `data`) VALUES('1186', '1');

DROP TABLE IF EXISTS `field_snipcart_item_taxes`;
CREATE TABLE `field_snipcart_item_taxes` (
  `pages_id` int unsigned NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `field_snipcart_item_taxes` (`pages_id`, `data`) VALUES('1080', '20% VAT');
INSERT INTO `field_snipcart_item_taxes` (`pages_id`, `data`) VALUES('1077', '20% VAT');
INSERT INTO `field_snipcart_item_taxes` (`pages_id`, `data`) VALUES('1142', '20% VAT');
INSERT INTO `field_snipcart_item_taxes` (`pages_id`, `data`) VALUES('1144', '20% VAT');
INSERT INTO `field_snipcart_item_taxes` (`pages_id`, `data`) VALUES('1146', '20% VAT');
INSERT INTO `field_snipcart_item_taxes` (`pages_id`, `data`) VALUES('1148', '20% VAT');
INSERT INTO `field_snipcart_item_taxes` (`pages_id`, `data`) VALUES('1150', '20% VAT');
INSERT INTO `field_snipcart_item_taxes` (`pages_id`, `data`) VALUES('1158', '20% VAT');
INSERT INTO `field_snipcart_item_taxes` (`pages_id`, `data`) VALUES('1160', '20% VAT');
INSERT INTO `field_snipcart_item_taxes` (`pages_id`, `data`) VALUES('1162', '20% VAT');
INSERT INTO `field_snipcart_item_taxes` (`pages_id`, `data`) VALUES('1164', '20% VAT');
INSERT INTO `field_snipcart_item_taxes` (`pages_id`, `data`) VALUES('1166', '20% VAT');
INSERT INTO `field_snipcart_item_taxes` (`pages_id`, `data`) VALUES('1168', '20% VAT');
INSERT INTO `field_snipcart_item_taxes` (`pages_id`, `data`) VALUES('1170', '20% VAT');
INSERT INTO `field_snipcart_item_taxes` (`pages_id`, `data`) VALUES('1172', '20% VAT');
INSERT INTO `field_snipcart_item_taxes` (`pages_id`, `data`) VALUES('1174', '20% VAT');
INSERT INTO `field_snipcart_item_taxes` (`pages_id`, `data`) VALUES('1182', '20% VAT');
INSERT INTO `field_snipcart_item_taxes` (`pages_id`, `data`) VALUES('1186', '20% VAT');

DROP TABLE IF EXISTS `field_snipcart_item_weight`;
CREATE TABLE `field_snipcart_item_weight` (
  `pages_id` int unsigned NOT NULL,
  `data` int NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


DROP TABLE IF EXISTS `field_snipcart_item_width`;
CREATE TABLE `field_snipcart_item_width` (
  `pages_id` int unsigned NOT NULL,
  `data` int NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


DROP TABLE IF EXISTS `field_subheadline`;
CREATE TABLE `field_subheadline` (
  `pages_id` int unsigned NOT NULL,
  `data` text NOT NULL,
  `data1012` text,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  KEY `data_exact1012` (`data1012`(250)),
  FULLTEXT KEY `data` (`data`),
  FULLTEXT KEY `data1012` (`data1012`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


DROP TABLE IF EXISTS `field_summary`;
CREATE TABLE `field_summary` (
  `pages_id` int unsigned NOT NULL,
  `data` mediumtext NOT NULL,
  `data1012` mediumtext,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`),
  FULLTEXT KEY `data1012` (`data1012`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `field_summary` (`pages_id`, `data`, `data1012`) VALUES('1002', 'Dolore ea valde refero feugait utinam luctus. Probo velit commoveo et, delenit praesent, suscipit zelus, hendrerit zelus illum facilisi, regula. ', '');
INSERT INTO `field_summary` (`pages_id`, `data`, `data1012`) VALUES('1001', 'This is a placeholder page with two child pages to serve as an example. ', 'Dies ist eine Beispiel-Seite mit zwei Unterseiten.');
INSERT INTO `field_summary` (`pages_id`, `data`, `data1012`) VALUES('1005', 'View this template\'s source for a demonstration of how to create a basic site map. ', 'Schauen Sie sich den Quell-Code dieser Musterseite an, um zu sehen, wie man einfache Sitemaps erstellt.');
INSERT INTO `field_summary` (`pages_id`, `data`, `data1012`) VALUES('1004', 'Mos erat reprobo in praesent, mara premo, obruo iustum pecus velit lobortis te sagaciter populus.', '');
INSERT INTO `field_summary` (`pages_id`, `data`, `data1012`) VALUES('1', 'We are a small manufacturer located at the popular kite spot Workum in the Netherlands, who have set ourselves the goal to \"forge\" the ultimate board. Years of honing our craft, combined with a real passion for perfection, ensures every board is made to the highest possible standard.', 'ProcessWire ist ein Open-Source-CMS und Web-Applikations-Framework, das sich ganz den Anforderungen von Designern, Entwicklern und deren Kunden anpaßt.');
INSERT INTO `field_summary` (`pages_id`, `data`, `data1012`) VALUES('1020', '', '');
INSERT INTO `field_summary` (`pages_id`, `data`, `data1012`) VALUES('1021', '', '');
INSERT INTO `field_summary` (`pages_id`, `data`, `data1012`) VALUES('1022', '', '');
INSERT INTO `field_summary` (`pages_id`, `data`, `data1012`) VALUES('1023', '', '');
INSERT INTO `field_summary` (`pages_id`, `data`, `data1012`) VALUES('1024', '', '');
INSERT INTO `field_summary` (`pages_id`, `data`, `data1012`) VALUES('1025', '', '');
INSERT INTO `field_summary` (`pages_id`, `data`, `data1012`) VALUES('1026', '', '');
INSERT INTO `field_summary` (`pages_id`, `data`, `data1012`) VALUES('1050', '', '');
INSERT INTO `field_summary` (`pages_id`, `data`, `data1012`) VALUES('1052', 'Perfect Freestyle Board for riders up to 65kg', '');
INSERT INTO `field_summary` (`pages_id`, `data`, `data1012`) VALUES('1054', '', '');
INSERT INTO `field_summary` (`pages_id`, `data`, `data1012`) VALUES('1058', 'Iam stylish', '');
INSERT INTO `field_summary` (`pages_id`, `data`, `data1012`) VALUES('1060', '', '');
INSERT INTO `field_summary` (`pages_id`, `data`, `data1012`) VALUES('1063', '', '');
INSERT INTO `field_summary` (`pages_id`, `data`, `data1012`) VALUES('1065', '', '');
INSERT INTO `field_summary` (`pages_id`, `data`, `data1012`) VALUES('1081', '', '');
INSERT INTO `field_summary` (`pages_id`, `data`, `data1012`) VALUES('1116', '', '');
INSERT INTO `field_summary` (`pages_id`, `data`, `data1012`) VALUES('1190', '', '');

DROP TABLE IF EXISTS `field_teaer_image`;
CREATE TABLE `field_teaer_image` (
  `pages_id` int unsigned NOT NULL,
  `data` varchar(250) NOT NULL,
  `sort` int unsigned NOT NULL,
  `description` text NOT NULL,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `filedata` mediumtext,
  `filesize` int DEFAULT NULL,
  `created_users_id` int unsigned NOT NULL DEFAULT '0',
  `modified_users_id` int unsigned NOT NULL DEFAULT '0',
  `width` int DEFAULT NULL,
  `height` int DEFAULT NULL,
  `ratio` decimal(4,2) DEFAULT NULL,
  PRIMARY KEY (`pages_id`,`sort`),
  KEY `data` (`data`),
  KEY `modified` (`modified`),
  KEY `created` (`created`),
  KEY `filesize` (`filesize`),
  KEY `width` (`width`),
  KEY `height` (`height`),
  KEY `ratio` (`ratio`),
  FULLTEXT KEY `description` (`description`),
  FULLTEXT KEY `filedata` (`filedata`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `field_teaer_image` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`, `filesize`, `created_users_id`, `modified_users_id`, `width`, `height`, `ratio`) VALUES('1001', 'morning-1935072_1920.jpg', '0', '[\"\"]', '2020-04-20 10:19:24', '2020-04-20 10:19:24', '', NULL, '0', '0', NULL, NULL, NULL);
INSERT INTO `field_teaer_image` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`, `filesize`, `created_users_id`, `modified_users_id`, `width`, `height`, `ratio`) VALUES('1', '328555c1-39ef-498f-9886-97ba531f1c92.jpg', '0', '[\"\"]', '2021-03-05 15:40:15', '2021-03-05 15:40:15', '', '151386', '41', '41', '1600', '900', '1.78');
INSERT INTO `field_teaer_image` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`, `filesize`, `created_users_id`, `modified_users_id`, `width`, `height`, `ratio`) VALUES('1154', '328555c1-39ef-498f-9886-97ba531f1c92.jpg', '0', '[\"\"]', '2021-03-07 13:49:22', '2021-03-07 13:49:22', '', '151386', '41', '41', '1600', '900', '1.78');

DROP TABLE IF EXISTS `field_teaser_headline`;
CREATE TABLE `field_teaser_headline` (
  `pages_id` int unsigned NOT NULL,
  `data` text NOT NULL,
  `data1012` text,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  KEY `data_exact1012` (`data1012`(250)),
  FULLTEXT KEY `data` (`data`),
  FULLTEXT KEY `data1012` (`data1012`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `field_teaser_headline` (`pages_id`, `data`, `data1012`) VALUES('1', 'UNLEASH YOUR POTENTIAL', '');
INSERT INTO `field_teaser_headline` (`pages_id`, `data`, `data1012`) VALUES('1001', 'TEaser?', '');
INSERT INTO `field_teaser_headline` (`pages_id`, `data`, `data1012`) VALUES('1050', '', '');
INSERT INTO `field_teaser_headline` (`pages_id`, `data`, `data1012`) VALUES('1052', '', '');
INSERT INTO `field_teaser_headline` (`pages_id`, `data`, `data1012`) VALUES('1054', '', '');
INSERT INTO `field_teaser_headline` (`pages_id`, `data`, `data1012`) VALUES('1058', '', '');
INSERT INTO `field_teaser_headline` (`pages_id`, `data`, `data1012`) VALUES('1060', '', '');
INSERT INTO `field_teaser_headline` (`pages_id`, `data`, `data1012`) VALUES('1063', '', '');
INSERT INTO `field_teaser_headline` (`pages_id`, `data`, `data1012`) VALUES('1065', '', '');
INSERT INTO `field_teaser_headline` (`pages_id`, `data`, `data1012`) VALUES('1081', '', '');
INSERT INTO `field_teaser_headline` (`pages_id`, `data`, `data1012`) VALUES('1116', '', '');
INSERT INTO `field_teaser_headline` (`pages_id`, `data`, `data1012`) VALUES('1190', '', '');
INSERT INTO `field_teaser_headline` (`pages_id`, `data`, `data1012`) VALUES('1192', '', '');
INSERT INTO `field_teaser_headline` (`pages_id`, `data`, `data1012`) VALUES('1154', 'Get your custom board now!', '');

DROP TABLE IF EXISTS `field_teaser_link`;
CREATE TABLE `field_teaser_link` (
  `pages_id` int unsigned NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `field_teaser_link` (`pages_id`, `data`) VALUES('1', '/boards');
INSERT INTO `field_teaser_link` (`pages_id`, `data`) VALUES('1154', '/custom');
INSERT INTO `field_teaser_link` (`pages_id`, `data`) VALUES('1229', '/twintips');
INSERT INTO `field_teaser_link` (`pages_id`, `data`) VALUES('1231', '/about/manufactory/');

DROP TABLE IF EXISTS `field_teaser_linktext`;
CREATE TABLE `field_teaser_linktext` (
  `pages_id` int unsigned NOT NULL,
  `data` text NOT NULL,
  `data1012` text,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  KEY `data_exact1012` (`data1012`(250)),
  FULLTEXT KEY `data` (`data`),
  FULLTEXT KEY `data1012` (`data1012`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `field_teaser_linktext` (`pages_id`, `data`, `data1012`) VALUES('1', 'Shop now!', '');
INSERT INTO `field_teaser_linktext` (`pages_id`, `data`, `data1012`) VALUES('1050', '', '');
INSERT INTO `field_teaser_linktext` (`pages_id`, `data`, `data1012`) VALUES('1052', '', '');
INSERT INTO `field_teaser_linktext` (`pages_id`, `data`, `data1012`) VALUES('1054', '', '');
INSERT INTO `field_teaser_linktext` (`pages_id`, `data`, `data1012`) VALUES('1058', '', '');
INSERT INTO `field_teaser_linktext` (`pages_id`, `data`, `data1012`) VALUES('1060', '', '');
INSERT INTO `field_teaser_linktext` (`pages_id`, `data`, `data1012`) VALUES('1063', '', '');
INSERT INTO `field_teaser_linktext` (`pages_id`, `data`, `data1012`) VALUES('1065', '', '');
INSERT INTO `field_teaser_linktext` (`pages_id`, `data`, `data1012`) VALUES('1081', '', '');
INSERT INTO `field_teaser_linktext` (`pages_id`, `data`, `data1012`) VALUES('1116', '', '');
INSERT INTO `field_teaser_linktext` (`pages_id`, `data`, `data1012`) VALUES('1190', '', '');
INSERT INTO `field_teaser_linktext` (`pages_id`, `data`, `data1012`) VALUES('1192', '', '');
INSERT INTO `field_teaser_linktext` (`pages_id`, `data`, `data1012`) VALUES('1154', 'GET IT NOW!', '');
INSERT INTO `field_teaser_linktext` (`pages_id`, `data`, `data1012`) VALUES('1229', 'GET A TWINTIP...', '');
INSERT INTO `field_teaser_linktext` (`pages_id`, `data`, `data1012`) VALUES('1230', '', '');
INSERT INTO `field_teaser_linktext` (`pages_id`, `data`, `data1012`) VALUES('1231', 'Read more...', '');

DROP TABLE IF EXISTS `field_teaser_text`;
CREATE TABLE `field_teaser_text` (
  `pages_id` int unsigned NOT NULL,
  `data` mediumtext NOT NULL,
  `data1012` mediumtext,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`),
  FULLTEXT KEY `data1012` (`data1012`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `field_teaser_text` (`pages_id`, `data`, `data1012`) VALUES('1', 'and get a board that boosts you up to your limits! Boardschmiede Boards are made in heaven and you\'ll never give it back once you tried it out.', '');
INSERT INTO `field_teaser_text` (`pages_id`, `data`, `data1012`) VALUES('1190', '', '');
INSERT INTO `field_teaser_text` (`pages_id`, `data`, `data1012`) VALUES('1192', '', '');
INSERT INTO `field_teaser_text` (`pages_id`, `data`, `data1012`) VALUES('1001', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem', '');
INSERT INTO `field_teaser_text` (`pages_id`, `data`, `data1012`) VALUES('1050', '', '');
INSERT INTO `field_teaser_text` (`pages_id`, `data`, `data1012`) VALUES('1052', '', '');
INSERT INTO `field_teaser_text` (`pages_id`, `data`, `data1012`) VALUES('1054', '', '');
INSERT INTO `field_teaser_text` (`pages_id`, `data`, `data1012`) VALUES('1058', '', '');
INSERT INTO `field_teaser_text` (`pages_id`, `data`, `data1012`) VALUES('1060', '', '');
INSERT INTO `field_teaser_text` (`pages_id`, `data`, `data1012`) VALUES('1063', '', '');
INSERT INTO `field_teaser_text` (`pages_id`, `data`, `data1012`) VALUES('1065', '', '');
INSERT INTO `field_teaser_text` (`pages_id`, `data`, `data1012`) VALUES('1081', '', '');
INSERT INTO `field_teaser_text` (`pages_id`, `data`, `data1012`) VALUES('1116', '', '');

DROP TABLE IF EXISTS `field_text_fields`;
CREATE TABLE `field_text_fields` (
  `pages_id` int unsigned NOT NULL,
  `data` text NOT NULL,
  `count` int NOT NULL,
  `parent_id` int NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(1)),
  KEY `count` (`count`,`pages_id`),
  KEY `parent_id` (`parent_id`,`pages_id`),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `field_text_fields` (`pages_id`, `data`, `count`, `parent_id`) VALUES('1001', '1045,1046', '2', '1044');
INSERT INTO `field_text_fields` (`pages_id`, `data`, `count`, `parent_id`) VALUES('1050', '', '0', '1051');
INSERT INTO `field_text_fields` (`pages_id`, `data`, `count`, `parent_id`) VALUES('1052', '', '0', '1053');
INSERT INTO `field_text_fields` (`pages_id`, `data`, `count`, `parent_id`) VALUES('1054', '', '0', '1055');
INSERT INTO `field_text_fields` (`pages_id`, `data`, `count`, `parent_id`) VALUES('1058', '', '0', '1059');
INSERT INTO `field_text_fields` (`pages_id`, `data`, `count`, `parent_id`) VALUES('1060', '', '0', '1061');
INSERT INTO `field_text_fields` (`pages_id`, `data`, `count`, `parent_id`) VALUES('1063', '', '0', '1064');
INSERT INTO `field_text_fields` (`pages_id`, `data`, `count`, `parent_id`) VALUES('1065', '', '0', '1066');
INSERT INTO `field_text_fields` (`pages_id`, `data`, `count`, `parent_id`) VALUES('1021', '1070,1071', '2', '1062');
INSERT INTO `field_text_fields` (`pages_id`, `data`, `count`, `parent_id`) VALUES('1081', '', '0', '1082');
INSERT INTO `field_text_fields` (`pages_id`, `data`, `count`, `parent_id`) VALUES('1116', '', '0', '1117');
INSERT INTO `field_text_fields` (`pages_id`, `data`, `count`, `parent_id`) VALUES('1024', '1120,1121', '2', '1119');
INSERT INTO `field_text_fields` (`pages_id`, `data`, `count`, `parent_id`) VALUES('1190', '', '0', '1191');
INSERT INTO `field_text_fields` (`pages_id`, `data`, `count`, `parent_id`) VALUES('1154', '1195,1196', '2', '1194');

DROP TABLE IF EXISTS `field_textblock`;
CREATE TABLE `field_textblock` (
  `pages_id` int unsigned NOT NULL,
  `data` mediumtext NOT NULL,
  `data1012` mediumtext,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`),
  FULLTEXT KEY `data1012` (`data1012`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `field_textblock` (`pages_id`, `data`, `data1012`) VALUES('1030', '', '');
INSERT INTO `field_textblock` (`pages_id`, `data`, `data1012`) VALUES('1035', 'From small series to custom made, we will be happy to shape the kitesurf board of your choice that fits your needs.', '');
INSERT INTO `field_textblock` (`pages_id`, `data`, `data1012`) VALUES('1036', 'We are a small team of craftsmen, with a lot of love for the sport and passion for our work.', '');
INSERT INTO `field_textblock` (`pages_id`, `data`, `data1012`) VALUES('1037', 'We are located together with surfshop Core ‘n More, close to the beautiful kite spot Workum in the Netherlands.', '');
INSERT INTO `field_textblock` (`pages_id`, `data`, `data1012`) VALUES('1038', 'Looking for a custom-made board that fits your needs, than come for expertise and personal advice to our shop, located at surfshop “Core ‘n More” in Workum, the Netherlands.', '');
INSERT INTO `field_textblock` (`pages_id`, `data`, `data1012`) VALUES('1039', '<p>Nicht weniger als ein perfekt funktionierendes Board ist unser stetiges streben, glücklich, zufriedene und überzeugte Kunden unser Antrieb.</p>', '<p>Nicht weniger als ein perfekt funktionierendes Board ist unser stetiges streben, glücklich, zufriedene und überzeugte Kunden unser Antrieb.</p>');
INSERT INTO `field_textblock` (`pages_id`, `data`, `data1012`) VALUES('1040', '<p>In all unseren Boards steckt die Leidenschaft für unseren Sport was unsere Kunden zu schätzen wissen und spüren nach den ersten Metern</p>', '<p>In all unseren Boards steckt die Leidenschaft für unseren Sport was unsere Kunden zu schätzen wissen und spüren nach den ersten Metern</p>');
INSERT INTO `field_textblock` (`pages_id`, `data`, `data1012`) VALUES('1041', '<p>Du hast eine Idee, oder Änderungswünsche wir setzen sie Dir diese gerne in die Tat um, sprich und einfach darauf an.</p>', '<p>Du hast eine Idee, oder Änderungswünsche wir setzen sie Dir diese gerne in die Tat um, sprich und einfach darauf an.</p>');
INSERT INTO `field_textblock` (`pages_id`, `data`, `data1012`) VALUES('1042', '<p>Von der Idee über den Entwurf, zum Prototypenbau, Werkzeuge, und custom sowie Serienboards das alles gescheit in unserer Werksatt in Workum am Ijsselmeer 400 Meter vom Spot entfernt wo Du jederzeit einen Blick hinter die Kulissen werfen darfst wie Dein Board.</p>', '<p>Von der Idee über den Entwurf, zum Prototypenbau, Werkzeuge, und custom sowie Serienboards das alles gescheit in unserer Werksatt in Workum am Ijsselmeer 400 Meter vom Spot entfernt wo Du jederzeit einen Blick hinter die Kulissen werfen darfst wie Dein Board.</p>');
INSERT INTO `field_textblock` (`pages_id`, `data`, `data1012`) VALUES('1045', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem', '');
INSERT INTO `field_textblock` (`pages_id`, `data`, `data1012`) VALUES('1046', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem \n\nLorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem', '');
INSERT INTO `field_textblock` (`pages_id`, `data`, `data1012`) VALUES('1047', '', '');
INSERT INTO `field_textblock` (`pages_id`, `data`, `data1012`) VALUES('1070', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem.', '');
INSERT INTO `field_textblock` (`pages_id`, `data`, `data1012`) VALUES('1071', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem.', '');
INSERT INTO `field_textblock` (`pages_id`, `data`, `data1012`) VALUES('1114', 'Don’t ask, try!\nYou’re welcome to come visit us and see for yourself...\nBecause in the end, we will let our boards do the talking!', '');
INSERT INTO `field_textblock` (`pages_id`, `data`, `data1012`) VALUES('1120', '<p><em>Shaper Boardschmiede, Teamrider</em></p>\n\n<p>Nationalitity: German, living in the Netherlands for the last 6 years.<br />\nBorn: 1990<br />\nLanguages: German, Dutch, English<br />\nSports: Kitesurfing, SUP, Mountainbiking, Crossfit<br />\nFavorite Boards: 135x40 Freeride Twintip, Wave-Skim, Surfboard Beaver<br />\nFavorite Kites: Duotone Rebel 13, Duotone Neo 7m for wave<br />\nKiting Style: I love to switch between Oldschool/Airstyle and skim<br />\nFavorite Trick: Board offs<br />\nShaping Kiteboards since: 2017<br />\nMain Responsibilities: Manufactur Twintip Serie, Dutch customer service</p>', '');
INSERT INTO `field_textblock` (`pages_id`, `data`, `data1012`) VALUES('1121', '<p><em>Owner Boardschmiede, head shaper</em></p>\n\n<p>Nationality: German<br />\nBorn: 1977<br />\nLanguages: German, English and very little Dutch<br />\nSports: Kitesurfing, windsurfing, fitness<br />\nFavorite Boards: I love all our boards<br />\nFavorite Kites: Duotone Rebel,Duotone Neo<br />\nKiting Style: Oldsschool/airstyle, strapless, wave<br />\nFavorite Trick: Rodeo<br />\nShaping since: 2012<br />\nMainjob before: Since 1999 composite specialist</p>', '');
INSERT INTO `field_textblock` (`pages_id`, `data`, `data1012`) VALUES('1195', '<h2>Waveboards</h2>\n\n<h3>In utinam facilisi eum vicis feugait nimis</h3>\n\n<p>Iusto incassum appellatio cui macto genitus vel. Lobortis aliquam luctus, roto enim, imputo wisi tamen. Ratis odio, genitus acsi, neo illum consequat consectetuer ut</p>', '');
INSERT INTO `field_textblock` (`pages_id`, `data`, `data1012`) VALUES('1196', '<p>Iusto incassum appellatio cui macto genitus vel. Lobortis aliquam luctus, roto enim, imputo wisi tamen. Ratis odio, genitus acsi, neo illum consequat consectetuer ut</p>', '');

DROP TABLE IF EXISTS `field_title`;
CREATE TABLE `field_title` (
  `pages_id` int unsigned NOT NULL,
  `data` text NOT NULL,
  `data1012` text,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  KEY `data_exact1012` (`data1012`(250)),
  FULLTEXT KEY `data` (`data`),
  FULLTEXT KEY `data1012` (`data1012`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('11', 'Templates', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('16', 'Fields', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('22', 'Setup', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('3', 'Pages', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('6', 'Add Page', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('8', 'Tree', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('9', 'Save Sort', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('10', 'Edit', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('21', 'Modules', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('29', 'Users', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('30', 'Roles', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('2', 'Admin', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('7', 'Trash', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('27', '404 Page', '404 Seite');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('302', 'Insert Link', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('23', 'Login', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('304', 'Profile', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('301', 'Empty Trash', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('300', 'Search', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('303', 'Insert Image', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('28', 'Access', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('31', 'Permissions', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('32', 'Edit pages', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('34', 'Delete pages', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('35', 'Move pages (change parent)', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('36', 'View pages', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('50', 'Sort child pages', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('51', 'Change templates on pages', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('52', 'Administer users', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('53', 'User can update profile/password', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('54', 'Lock or unlock a page', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1', 'Home', 'Startseite');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1001', 'About', 'Über');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1002', 'Manufactory', 'Manufaktur');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1000', 'Search', 'Suche');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1004', 'History', 'Geschichte');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1005', 'Site Map', 'Sitemap');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1006', 'Use Page Lister', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1007', 'Find', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1009', 'Languages', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1010', 'EN', 'EN');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1011', 'Language Translator', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1012', 'GER', 'DE');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1122', 'Hard', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1015', 'Recent', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1016', 'Can see recently edited pages', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1017', 'Logs', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1018', 'Can view system logs', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1019', 'Can manage system logs', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1020', 'Boards', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1021', 'Boards', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1022', 'Twin-Tips', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1023', 'Skimboards', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1024', 'Team', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1025', 'Blog', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1026', 'Contact', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1027', 'Repeaters', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1028', 'text_fields', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1029', 'en', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1031', 'simple_textblocks', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1032', 'en', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1033', 'repeater_why', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1034', 'en', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1043', 'blog', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1044', 'about', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1048', 'DB Backups', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1049', 'Manage database backups (recommended for superuser only)', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1050', 'Boards', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1051', 'boards', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1052', 'Freestyler 135', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1053', 'freestyler', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1054', 'Waveboards', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1055', 'waveboards', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1056', 'twin-tips', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1057', 'skimboards', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1058', 'Testboard', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1059', 'testboard', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1060', 'Beaver', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1061', 'beaver', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1062', 'boards', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1063', 'Rote Rakete', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1064', 'rote-rakete', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1065', 'Flitzer', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1066', 'flitzer', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1067', 'Croppable Images 3', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1068', 'Crop images with CroppableImage3', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1069', 'Diagnostics', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1072', 'shop', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1073', 'SnipWire', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1074', 'Custom Cart Fields', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1075', 'Use the SnipWire Dashboard sections', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1076', 'Freestyler', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1077', 'Freerider Lady', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1078', 'Festish Wet Warmer', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1079', 'Axolotl Juicer', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1080', 'Freestyler', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1081', 'Shoping', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1082', 'shoping', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1083', 'Lightwind Plan-B', 'Leichtwind Plan-B');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1085', 'prod_variants', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1086', 'prod_variants', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1087', 'freestyler-135', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1089', '1588051372-1268-1', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1090', '130x38', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1091', '132x39', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1092', '135x40', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1088', 'Size', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1142', 'Special Edition', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1143', 'freestyle', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1101', 'freestyler', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1102', 'Flex', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1103', '1588826993-8588-1', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1104', 'Soft', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1105', 'Medium', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1124', '137x41', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1125', '139x42', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1126', '141x43', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1110', 'Skimboards', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1111', 'skimboards', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1112', 'Waveboards', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1113', 'waveboards', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1115', 'freestyler-140', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1116', 'Impressum', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1117', 'impressum', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1118', 'contact', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1119', 'team', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1123', 'Team Flex', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1133', 'Size', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1134', '1592069847-7351-1', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1135', '130x38', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1136', '132x39', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1137', '135x40', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1138', '137x41', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1139', '139x42', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1140', '141x43', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1141', 'custom Size Anfrage', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1144', 'Freerider Explorer', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1145', 'freerider-explorer', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1146', 'Chrome Edition', 'Chrom Edition');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1147', 'chrome-edition', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1148', 'Big Air Challenger', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1149', 'big-air-challenger', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1150', 'Freestyler', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1151', 'freestyler', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1152', 'Twintips', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1153', 'twintips', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1154', 'Customs', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1216', '1615119021-8129-1', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1156', 'Shop', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1177', 'Stock Boards', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1158', 'Freerider', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1159', 'freerider-explorer', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1160', 'Freerider Lady', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1161', 'freerider-lady', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1162', 'Lightwind Plan-B', 'Leichtwind Plan-B');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1163', 'lightwind-plan-b', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1164', 'Special Edition', 'Spezial Edition');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1165', 'special-edition', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1166', 'Chrome Edition', 'Chrom Edition');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1167', 'chrome-edition', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1168', 'Big Air Challenger', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1169', 'big-air-challenger', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1170', 'Freestyler', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1171', 'freestyler', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1172', 'Waveboard', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1173', 'waveboard', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1174', 'Skimboard', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1175', 'skimboards', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1176', 'shop', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1182', 'Freestyler', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1179', 'Muster stock Board', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1180', 'muster-stock-board', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1181', 'stock-boards', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1183', 'freestyler', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1184', 'Equipment', 'Zubehör');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1185', 'equipment', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1186', 'Straps', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1187', 'straps', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1188', 'Promotion', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1189', 'promotion', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1190', 'Testpoint', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1191', 'testpoint', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1192', 'Gallery', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1194', 'customs', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1215', 'Size', 'Größe');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1200', 'Size', 'Größe');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1201', '1615118283-1115-1', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1202', '130x38cm', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1203', '', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1204', '132x39cm', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1205', '135x40cm', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1206', '137x41cm', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1207', '139x42cm', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1209', '142x43cm', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1210', 'Flex', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1211', '1615118649-951-1', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1212', 'Soft < 55kg', 'Weich < 55kg');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1213', 'Medium 55-85kg', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1214', 'Hard / Team > 85kg', 'Hart / Team > 85kg');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1217', '128x38cm', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1218', '130x38.5cm', '130x38;5cm');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1219', '132x39cm', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1220', 'Size', 'Größe');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1221', '1615119135-792-1', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1222', '152x45cm', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1223', 'Flex', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1224', '1615119154-0203-1', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1225', 'Standard 65-85kg', 'Standart');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1226', 'Hard > 85kg', 'Hart > 85kg');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1227', 'bigLinks', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1228', 'en', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1229', 'TWINTIPS', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1230', 'Customs', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1231', 'Manufactory', 'Manufaktur');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1234', 'manufactur', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1251', 'Rail color', 'Rail-Farbe');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1252', '1615123757-6252-1', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1253', 'White', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1254', 'Black', 'Schwarz');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1242', 'prod_custom_variants', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1243', 'prod_custom_variant_item', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1244', 'twintips', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1255', 'Yellow', 'Gelb');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1256', 'Yellow neon', 'Neongelb');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1257', 'Orange neon', 'Neonorange');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1258', 'Red', 'Rot');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1259', 'Pink', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1260', 'Blue light', 'Hellblau');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1261', 'Blue dark', 'Dunkelblau');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1262', 'Turquoise', 'Türkis');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1263', 'Green', 'Grün');
INSERT INTO `field_title` (`pages_id`, `data`, `data1012`) VALUES('1264', 'Grey', 'Grau');

DROP TABLE IF EXISTS `field_video_url`;
CREATE TABLE `field_video_url` (
  `pages_id` int unsigned NOT NULL,
  `data` mediumtext NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `field_video_url` (`pages_id`, `data`) VALUES('1052', '<p>http://www.youtube.com/watch?v=16evq_vySAc</p>');
INSERT INTO `field_video_url` (`pages_id`, `data`) VALUES('1077', '<p>http://www.youtube.com/watch?v=16evq_vySAc</p>');

DROP TABLE IF EXISTS `field_why_headline`;
CREATE TABLE `field_why_headline` (
  `pages_id` int unsigned NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `field_why_headline` (`pages_id`, `data`) VALUES('1', 'WHY YOU SHOULD CHOOSE BOARDSCHMIEDE');

DROP TABLE IF EXISTS `field_why_subheadline`;
CREATE TABLE `field_why_subheadline` (
  `pages_id` int unsigned NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


DROP TABLE IF EXISTS `fieldgroups`;
CREATE TABLE `fieldgroups` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(250) CHARACTER SET ascii COLLATE ascii_general_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=113 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `fieldgroups` (`id`, `name`) VALUES('2', 'admin');
INSERT INTO `fieldgroups` (`id`, `name`) VALUES('3', 'user');
INSERT INTO `fieldgroups` (`id`, `name`) VALUES('4', 'role');
INSERT INTO `fieldgroups` (`id`, `name`) VALUES('5', 'permission');
INSERT INTO `fieldgroups` (`id`, `name`) VALUES('1', 'home');
INSERT INTO `fieldgroups` (`id`, `name`) VALUES('88', 'sitemap');
INSERT INTO `fieldgroups` (`id`, `name`) VALUES('83', 'basic-page');
INSERT INTO `fieldgroups` (`id`, `name`) VALUES('80', 'search');
INSERT INTO `fieldgroups` (`id`, `name`) VALUES('97', 'language');
INSERT INTO `fieldgroups` (`id`, `name`) VALUES('98', 'repeater_text_fields');
INSERT INTO `fieldgroups` (`id`, `name`) VALUES('99', 'repeater_simple_textblocks');
INSERT INTO `fieldgroups` (`id`, `name`) VALUES('100', 'repeater_repeater_why');
INSERT INTO `fieldgroups` (`id`, `name`) VALUES('101', 'board-page');
INSERT INTO `fieldgroups` (`id`, `name`) VALUES('102', 'board-overview-page');
INSERT INTO `fieldgroups` (`id`, `name`) VALUES('103', 'snipcart-cart');
INSERT INTO `fieldgroups` (`id`, `name`) VALUES('104', 'snipcart-shop');
INSERT INTO `fieldgroups` (`id`, `name`) VALUES('105', 'snipcart-product');
INSERT INTO `fieldgroups` (`id`, `name`) VALUES('106', 'repeater_prod_variant_item');
INSERT INTO `fieldgroups` (`id`, `name`) VALUES('107', 'repeater_prod_variants');
INSERT INTO `fieldgroups` (`id`, `name`) VALUES('108', 'basic-page-twoes');
INSERT INTO `fieldgroups` (`id`, `name`) VALUES('109', 'gallery');
INSERT INTO `fieldgroups` (`id`, `name`) VALUES('110', 'repeater_bigLinks');
INSERT INTO `fieldgroups` (`id`, `name`) VALUES('111', 'repeater_prod_custom_variants');
INSERT INTO `fieldgroups` (`id`, `name`) VALUES('112', 'repeater_prod_custom_variant_item');

DROP TABLE IF EXISTS `fieldgroups_fields`;
CREATE TABLE `fieldgroups_fields` (
  `fieldgroups_id` int unsigned NOT NULL DEFAULT '0',
  `fields_id` int unsigned NOT NULL DEFAULT '0',
  `sort` int unsigned NOT NULL DEFAULT '0',
  `data` text,
  PRIMARY KEY (`fieldgroups_id`,`fields_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('2', '2', '1', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('3', '4', '2', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('4', '5', '0', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('5', '1', '0', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('3', '98', '3', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '116', '13', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('80', '1', '0', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('98', '105', '2', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '111', '11', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('88', '1', '0', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('99', '106', '1', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('88', '79', '1', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('83', '76', '5', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('97', '97', '2', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('3', '92', '1', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('83', '44', '14', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('97', '1', '0', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '108', '6', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('3', '103', '4', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('2', '1', '0', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('83', '1', '2', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('97', '100', '1', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('3', '3', '0', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('98', '78', '0', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('83', '123', '3', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '109', '7', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('83', '116', '7', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '123', '2', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('83', '79', '4', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('99', '78', '0', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '120', '17', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('100', '78', '0', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '110', '12', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '112', '10', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '115', '9', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '114', '8', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('83', '157', '1', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '1', '1', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('83', '127', '0', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('102', '122', '8', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('101', '120', '13', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('101', '155', '2', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('101', '79', '3', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('101', '76', '4', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('101', '127', '0', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('102', '127', '0', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('102', '1', '1', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('83', '121', '12', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('102', '79', '3', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('101', '125', '6', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '127', '0', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('100', '106', '1', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('83', '120', '11', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('102', '119', '9', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('101', '119', '12', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('105', '146', '22', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('104', '153', '1', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('105', '137', '13', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('105', '138', '14', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('105', '139', '15', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('105', '140', '16', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('105', '141', '17', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('105', '142', '18', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('105', '143', '19', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('105', '144', '20', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('105', '155', '2', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('105', '132', '3', '{\"columnWidth\":30}');
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('105', '131', '4', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('105', '133', '5', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('105', '127', '0', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('105', '1', '1', '{\"columnWidth\":70,\"label\":\"Product Name (Title)\",\"notes\":\"Name of the product to be used in catalogue and cart.\"}');
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('106', '131', '1', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('107', '1', '0', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('104', '1', '0', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('83', '117', '13', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('102', '123', '2', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('101', '122', '11', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('101', '1', '1', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('98', '106', '1', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('103', '1', '0', '{\"collapsed\":4}');
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('83', '104', '6', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('102', '118', '7', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('101', '124', '5', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('107', '152', '1', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('112', '1', '0', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('105', '145', '21', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('105', '134', '10', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('105', '135', '11', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('105', '136', '12', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('108', '76', '4', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('108', '156', '5', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('108', '123', '2', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('108', '79', '3', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('83', '119', '10', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('108', '119', '10', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('108', '127', '0', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('108', '1', '1', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('102', '120', '10', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('101', '128', '7', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '119', '16', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '118', '14', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '122', '15', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('109', '117', '10', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('109', '127', '0', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('109', '157', '1', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('109', '1', '2', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('109', '76', '3', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('109', '116', '4', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('109', '118', '5', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('109', '122', '6', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('109', '119', '7', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('109', '120', '8', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('109', '121', '9', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('83', '118', '8', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('83', '122', '9', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('108', '122', '9', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('108', '104', '6', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('108', '118', '8', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('108', '116', '7', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('102', '116', '6', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('102', '104', '5', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('102', '76', '4', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('101', '118', '10', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('101', '116', '9', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('101', '104', '8', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('109', '44', '11', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '76', '5', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '79', '4', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '78', '3', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('103', '129', '1', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('105', '125', '6', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('105', '153', '7', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('105', '44', '8', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('105', '128', '9', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('110', '120', '3', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('110', '105', '1', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('110', '1', '0', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('108', '120', '11', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('108', '121', '12', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('108', '117', '13', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('108', '44', '14', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('102', '121', '11', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('102', '117', '12', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('102', '44', '13', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('101', '121', '14', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('101', '117', '15', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('101', '44', '16', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '121', '18', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '117', '19', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '44', '20', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '158', '21', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('110', '76', '2', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('105', '151', '23', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('104', '160', '2', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('110', '121', '4', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('106', '1', '0', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('111', '1', '0', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('112', '159', '1', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('112', '131', '2', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('104', '154', '3', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('111', '161', '1', NULL);

DROP TABLE IF EXISTS `fields`;
CREATE TABLE `fields` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(128) CHARACTER SET ascii COLLATE ascii_general_ci NOT NULL,
  `name` varchar(250) CHARACTER SET ascii COLLATE ascii_general_ci NOT NULL,
  `flags` int NOT NULL DEFAULT '0',
  `label` varchar(250) NOT NULL DEFAULT '',
  `data` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `type` (`type`)
) ENGINE=MyISAM AUTO_INCREMENT=162 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('1', 'FieldtypePageTitleLanguage', 'title', '13', 'Title', '{\"required\":1,\"textformatters\":[\"TextformatterEntities\"],\"size\":0,\"maxlength\":255,\"label1012\":\"Titel\",\"langBlankInherit\":0}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('2', 'FieldtypeModule', 'process', '25', 'Process', '{\"description\":\"The process that is executed on this page. Since this is mostly used by ProcessWire internally, it is recommended that you don\'t change the value of this unless adding your own pages in the admin.\",\"collapsed\":1,\"required\":1,\"moduleTypes\":[\"Process\"],\"permanent\":1}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('3', 'FieldtypePassword', 'pass', '24', 'Set Password', '{\"collapsed\":1,\"size\":50,\"maxlength\":128}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('5', 'FieldtypePage', 'permissions', '24', 'Permissions', '{\"derefAsPage\":0,\"parent_id\":31,\"labelFieldName\":\"title\",\"inputfield\":\"InputfieldCheckboxes\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('4', 'FieldtypePage', 'roles', '24', 'Roles', '{\"derefAsPage\":0,\"parent_id\":30,\"labelFieldName\":\"name\",\"inputfield\":\"InputfieldCheckboxes\",\"description\":\"User will inherit the permissions assigned to each role. You may assign multiple roles to a user. When accessing a page, the user will only inherit permissions from the roles that are also assigned to the page\'s template.\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('92', 'FieldtypeEmail', 'email', '9', 'E-Mail Address', '{\"size\":70,\"maxlength\":255}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('105', 'FieldtypeImage', 'image', '0', '', '{\"label1012\":\"Bild\",\"extensions\":\"gif jpg jpeg png\",\"maxFiles\":1,\"outputFormat\":0,\"defaultValuePage\":0,\"useTags\":0,\"inputfieldClass\":\"InputfieldImage\",\"descriptionRows\":1,\"gridMode\":\"grid\",\"focusMode\":\"on\",\"resizeServer\":0,\"clientQuality\":90,\"maxReject\":0,\"dimensionsByAspectRatio\":0,\"columnWidth\":75,\"fileSchema\":270}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('106', 'FieldtypeTextareaLanguage', 'textblock', '0', '', '{\"label1012\":\"Textblock\",\"inputfieldClass\":\"InputfieldCKEditor\",\"contentType\":1,\"langBlankInherit\":0,\"collapsed\":0,\"minlength\":0,\"maxlength\":0,\"showCount\":0,\"rows\":5,\"htmlOptions\":[2,4,8,16]}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('44', 'FieldtypeImage', 'images', '0', 'Images', '{\"extensions\":\"gif jpg jpeg png\",\"adminThumbs\":1,\"inputfieldClass\":\"InputfieldImage\",\"maxFiles\":0,\"descriptionRows\":1,\"fileSchema\":270,\"outputFormat\":1,\"defaultValuePage\":0,\"defaultGrid\":0,\"icon\":\"camera\",\"label1012\":\"Bilder\",\"textformatters\":[\"TextformatterEntities\"]}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('79', 'FieldtypeTextareaLanguage', 'summary', '1', 'Summary', '{\"textformatters\":[\"TextformatterEntities\"],\"inputfieldClass\":\"InputfieldTextarea\",\"collapsed\":2,\"rows\":3,\"contentType\":0,\"label1012\":\"Zusammenfassung\",\"langBlankInherit\":0}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('76', 'FieldtypeTextareaLanguage', 'body', '0', 'Body', '{\"inputfieldClass\":\"InputfieldCKEditor\",\"rows\":10,\"contentType\":1,\"toolbar\":\"Format, Bold, Italic, -, RemoveFormat\\nJustifyLeft, JustifyCenter, JustifyRight, JustifyBlock\\nNumberedList, BulletedList, -, Blockquote\\nPWLink, Unlink, Anchor\\nPWImage, Table, HorizontalRule, SpecialChar\\nPasteText, PasteFromWord\\nScayt, -, Sourcedialog\",\"inlineMode\":0,\"useACF\":0,\"usePurifier\":1,\"formatTags\":\"p;h1;h2;h3;h4;h5;h6;pre;address\",\"extraPlugins\":[\"pwimage\",\"pwlink\",\"sourcedialog\"],\"removePlugins\":\"image,magicline\",\"toggles\":[2,4,8],\"label1012\":\"Inhalt\",\"langBlankInherit\":0,\"minlength\":0,\"maxlength\":0,\"showCount\":0}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('78', 'FieldtypeTextLanguage', 'headline', '0', 'Headline', '{\"description\":\"Use this instead of the field above if more text is needed for the page than for the navigation. \",\"textformatters\":[\"TextformatterEntities\"],\"collapsed\":2,\"size\":0,\"maxlength\":1024,\"langBlankInherit\":1,\"label1012\":\"\\u00dcberschrift\",\"description1012\":\"Verwenden Sie diese statt dem obigen Feld, wenn mehr Text f\\u00fcr die Seite als f\\u00fcr die Navigation ben\\u00f6tigt wird.j\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('97', 'FieldtypeFile', 'language_files', '24', 'Core Translation Files', '{\"extensions\":\"json csv\",\"maxFiles\":0,\"inputfieldClass\":\"InputfieldFile\",\"unzip\":1,\"descriptionRows\":0,\"fileSchema\":14,\"outputFormat\":0,\"defaultValuePage\":0,\"clone_field\":1,\"description\":\"Use this for field for [language packs](http:\\/\\/modules.processwire.com\\/categories\\/language-pack\\/). To delete all files, double-click the trash can for any file, then save.\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('98', 'FieldtypePage', 'language', '24', 'Language', '{\"derefAsPage\":1,\"parent_id\":1009,\"labelFieldName\":\"title\",\"inputfield\":\"InputfieldRadios\",\"required\":1}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('100', 'FieldtypeFile', 'language_files_site', '24', 'Site Translation Files', '{\"description\":\"Use this for field for translations specific to your site (like files in \\/site\\/templates\\/ for example).\",\"extensions\":\"json csv\",\"maxFiles\":0,\"inputfieldClass\":\"InputfieldFile\",\"unzip\":1,\"descriptionRows\":0,\"fileSchema\":14}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('103', 'FieldtypeModule', 'admin_theme', '8', 'Admin Theme', '{\"moduleTypes\":[\"AdminTheme\"],\"labelField\":\"title\",\"inputfieldClass\":\"InputfieldRadios\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('104', 'FieldtypeRepeater', 'text_fields', '0', 'Textblocks', '{\"label1012\":\"Textbl\\u00f6cke\",\"template_id\":44,\"parent_id\":1028,\"repeaterFields\":[78,106,105],\"repeaterTitle\":\"#n: {headline}\",\"repeaterAddLabel1012\":\"Neuer Textblock\",\"repeaterCollapse\":1,\"repeaterLoading\":1,\"collapsed\":0,\"repeaterAddLabel\":\"New textblock\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('107', 'FieldtypeCheckbox', 'image_alignment', '0', '', '{\"label1012\":\"Bildausrichtung\",\"collapsed\":0,\"columnWidth\":25}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('108', 'FieldtypeRepeater', 'simple_textblocks', '0', 'Simple textblocks', '{\"label1012\":\"Einfache Textbl\\u00f6cke\",\"template_id\":45,\"parent_id\":1031,\"repeaterFields\":[78,106],\"repeaterTitle\":\"#n: {headline}\",\"repeaterCollapse\":1,\"repeaterLoading\":1,\"collapsed\":0}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('109', 'FieldtypeFieldsetOpen', 'fieldset_why', '0', 'Why block', '{\"label1012\":\"Warum Block\",\"closeFieldID\":110,\"collapsed\":1}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('110', 'FieldtypeFieldsetClose', 'fieldset_why_END', '0', 'Close an open fieldset', '{\"description\":\"This field was added automatically to accompany fieldset \'fieldset_why\'.  It should be placed in your template\\/fieldgroup wherever you want the fieldset to end.\",\"openFieldID\":109}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('111', 'FieldtypeRepeater', 'repeater_why', '0', 'Why blocks', '{\"label1012\":\"Warum Bl\\u00f6cke\",\"template_id\":46,\"parent_id\":1033,\"repeaterFields\":[78,106],\"repeaterCollapse\":1,\"repeaterLoading\":1,\"collapsed\":0}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('112', 'FieldtypeImage', 'image_why', '0', 'Why image', '{\"label1012\":\"Warum Bild\",\"extensions\":\"gif jpg jpeg png\",\"maxFiles\":1,\"outputFormat\":0,\"defaultValuePage\":0,\"useTags\":0,\"inputfieldClass\":\"InputfieldImage\",\"descriptionRows\":1,\"gridMode\":\"grid\",\"focusMode\":\"on\",\"resizeServer\":0,\"clientQuality\":90,\"maxReject\":0,\"dimensionsByAspectRatio\":0,\"fileSchema\":270}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('113', 'FieldtypeTextLanguage', 'subheadline', '0', 'Subheadline', '{\"label1012\":\"Zweite \\u00dcberschrift\",\"minlength\":0,\"maxlength\":2048,\"showCount\":0,\"size\":0}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('114', 'FieldtypeText', 'why_headline', '0', 'Why headline', '{\"label1012\":\"Warum \\u00dcberschrift\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('115', 'FieldtypeText', 'why_subheadline', '0', 'Why subheadline', '{\"label1012\":\"Warum zweite \\u00dcberschrift\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('116', 'FieldtypeFieldsetOpen', 'fieldset_teaser', '0', 'Teaser', '{\"label1012\":\"Teaser\",\"closeFieldID\":117,\"collapsed\":1}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('117', 'FieldtypeFieldsetClose', 'fieldset_teaser_END', '0', 'Close an open fieldset', '{\"description\":\"This field was added automatically to accompany fieldset \'fieldset_teaser\'.  It should be placed in your template\\/fieldgroup wherever you want the fieldset to end.\",\"openFieldID\":116}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('118', 'FieldtypeImage', 'teaer_image', '0', 'Teaserimage', '{\"label1012\":\"Teaserbild\",\"extensions\":\"gif jpg jpeg png\",\"maxFiles\":1,\"outputFormat\":0,\"defaultValuePage\":0,\"useTags\":0,\"inputfieldClass\":\"InputfieldImage\",\"descriptionRows\":1,\"gridMode\":\"grid\",\"focusMode\":\"on\",\"resizeServer\":0,\"clientQuality\":90,\"maxReject\":0,\"dimensionsByAspectRatio\":0,\"fileSchema\":270}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('119', 'FieldtypeTextareaLanguage', 'teaser_text', '0', 'Teasertext', '{\"label1012\":\"Teasertext\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('120', 'FieldtypeURL', 'teaser_link', '0', 'Link', '{\"label1012\":\"Teaserlink\",\"noRelative\":0,\"allowIDN\":0,\"allowQuotes\":0,\"addRoot\":0,\"collapsed\":0,\"minlength\":0,\"maxlength\":1024,\"showCount\":0,\"size\":0}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('121', 'FieldtypeTextLanguage', 'teaser_linktext', '0', 'Link Label', '{\"label1012\":\"Teaserlink Label\",\"langBlankInherit\":0,\"collapsed\":0,\"minlength\":0,\"maxlength\":2048,\"showCount\":0,\"size\":0}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('122', 'FieldtypeTextLanguage', 'teaser_headline', '0', 'Teaserheadline', '{\"label1012\":\"Teaser\\u00fcberschrift\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('123', 'FieldtypeImage', 'coverimage', '0', 'Coverimage', '{\"label1012\":\"Coverbild\",\"extensions\":\"gif jpg jpeg png\",\"maxFiles\":1,\"outputFormat\":0,\"defaultValuePage\":0,\"useTags\":0,\"inputfieldClass\":\"InputfieldImage\",\"descriptionRows\":1,\"gridMode\":\"grid\",\"focusMode\":\"on\",\"resizeServer\":0,\"clientQuality\":90,\"maxReject\":0,\"dimensionsByAspectRatio\":0,\"fileSchema\":270}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('124', 'FieldtypeImage', 'board_images', '0', 'Board Images', '{\"label1012\":\"Bilder\",\"extensions\":\"gif jpg jpeg png\",\"maxFiles\":0,\"outputFormat\":0,\"defaultValuePage\":0,\"useTags\":0,\"inputfieldClass\":\"InputfieldCroppableImage3\",\"descriptionRows\":1,\"gridMode\":\"grid\",\"focusMode\":\"on\",\"resizeServer\":0,\"clientQuality\":90,\"maxReject\":0,\"dimensionsByAspectRatio\":0,\"fileSchema\":270}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('125', 'FieldtypeTextareaLanguage', 'board_features', '0', 'Features', '{\"label1012\":\"Features\",\"inputfieldClass\":\"InputfieldCKEditor\",\"contentType\":1,\"langBlankInherit\":0,\"collapsed\":0,\"minlength\":0,\"maxlength\":0,\"showCount\":0,\"rows\":5}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('128', 'FieldtypeTextarea', 'video_url', '0', 'Video (Youtube / Vimeo)', '{\"textformatters\":[\"TextformatterVideoEmbed\"],\"minlength\":0,\"maxlength\":2048,\"showCount\":0,\"size\":0,\"inputfieldClass\":\"InputfieldCKEditor\",\"contentType\":0,\"collapsed\":0,\"rows\":5}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('127', 'FieldtypeOptions', 'page_color', '0', 'Page Color', '{\"inputfieldClass\":\"InputfieldSelect\",\"collapsed\":0,\"columnWidth\":50}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('129', 'FieldtypeTextarea', 'snipcart_cart_custom_fields', '0', 'Custom Cart Fields Setup', '{\"description\":\"You can add custom fields to the checkout process. Whenever you define custom cart fields, a new tab\\/step called `Order infos` will be inserted before the `Billing address` during the checkout process.\",\"notes\":\"For detailed infos about custom cart fields setup, please visit [Snipcart v2.0 Custom Fields](https:\\/\\/docs.snipcart.com\\/v2\\/configuration\\/custom-fields).\",\"icon\":\"code\",\"rows\":12,\"tags\":\"Snipcart\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('130', 'FieldtypeTextarea', 'snipcart_item_custom_fields', '0', 'Custom Product Fields Setup', '{\"description\":\"You can add custom fields to this product. Whenever you define custom fields, a new input element will be added to each of these products in cart.\",\"notes\":\"For detailed infos about custom fields setup, please visit [Snipcart v2.0 Custom Fields](https:\\/\\/docs.snipcart.com\\/v2\\/configuration\\/custom-fields).\",\"icon\":\"code\",\"collapsed\":1,\"rows\":12,\"tags\":\"Snipcart\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('131', 'FieldtypeText', 'snipcart_item_price_eur', '0', 'Product Price (Euro)', '{\"notes\":\"Decimal with a dot (.) as separator e.g. 19.99\",\"maxlength\":20,\"required\":true,\"pattern\":\"[-+]?[0-9]*[.]?[0-9]+\",\"tags\":\"Snipcart\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('132', 'FieldtypeText', 'snipcart_item_id', '0', 'SKU', '{\"notes\":\"Individual ID for your product e.g. 1377 or NIKE_PEG-SW-43\",\"maxlength\":100,\"required\":true,\"pattern\":\"^[\\\\w\\\\-_*+.,]+$\",\"tags\":\"Snipcart\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('133', 'FieldtypeTextareaLanguage', 'snipcart_item_description', '0', 'Product Description', '{\"description\":\"The product description that your customers will see on product pages in cart and during checkout.\",\"notes\":\"Provide a short description of your product without HTML tags.\",\"maxlength\":0,\"rows\":12,\"showCount\":0,\"tags\":\"Snipcart\",\"inputfieldClass\":\"InputfieldCKEditor\",\"contentType\":1,\"minlength\":0,\"htmlOptions\":[2,4,8,16],\"langBlankInherit\":0,\"collapsed\":0,\"toolbar\":\"Format, Styles, -, Bold, Italic, -, RemoveFormat\\nNumberedList, BulletedList, -, Blockquote\\nPWLink, Unlink, Anchor\\nPWImage, Table, HorizontalRule, SpecialChar\\nPasteText, PasteFromWord\\nScayt, -, Sourcedialog\",\"inlineMode\":0,\"useACF\":1,\"usePurifier\":1,\"formatTags\":\"p;h1;h2;h3;h4;h5;h6;pre;address\",\"extraPlugins\":[\"pwimage\",\"pwlink\",\"sourcedialog\"],\"removePlugins\":\"image,magicline\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('134', 'FieldtypeImage', 'snipcart_item_image', '0', 'Product Image(s)', '{\"description\":\"The product image(s) your customers will see on product pages in cart and during checkout.\",\"notes\":\"The image on first position will be used as the Snipcart thumbnail image. Only this image will be used in cart and during checkout\",\"extensions\":\"gif jpg jpeg png\",\"tags\":\"Snipcart\",\"fileSchema\":270,\"maxFiles\":0,\"outputFormat\":0,\"defaultValuePage\":0,\"useTags\":0,\"inputfieldClass\":\"InputfieldImage\",\"collapsed\":0,\"columnWidth\":50,\"required\":1,\"descriptionRows\":1,\"gridMode\":\"grid\",\"focusMode\":\"on\",\"resizeServer\":0,\"clientQuality\":90,\"maxReject\":0,\"dimensionsByAspectRatio\":0}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('135', 'FieldtypePage', 'snipcart_item_categories', '0', 'Categories', '{\"description\":\"The categories for this product.\",\"inputfield\":\"InputfieldAsmSelect\",\"labelFieldName\":\"title\",\"usePageEdit\":1,\"addable\":1,\"derefAsPage\":0,\"tags\":\"Snipcart\",\"parent_id\":0,\"collapsed\":0,\"columnWidth\":50}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('136', 'FieldtypeInteger', 'snipcart_item_weight', '0', 'Product Weight', '{\"description\":\"Set the weight for this product.\",\"notes\":\"Uses grams as weight unit.\",\"min\":1,\"inputType\":\"number\",\"tags\":\"Snipcart\",\"zeroNotEmpty\":0,\"collapsed\":0,\"columnWidth\":25,\"size\":10}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('137', 'FieldtypeInteger', 'snipcart_item_width', '0', 'Product Width', '{\"description\":\"Set the width for this product.\",\"notes\":\"Uses centimeters as unit.\",\"min\":1,\"inputType\":\"number\",\"tags\":\"Snipcart\",\"zeroNotEmpty\":0,\"collapsed\":0,\"columnWidth\":25,\"size\":10}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('138', 'FieldtypeInteger', 'snipcart_item_length', '0', 'Product Length', '{\"description\":\"Set the length for this product.\",\"notes\":\"Uses centimeters as unit.\",\"min\":1,\"inputType\":\"number\",\"tags\":\"Snipcart\",\"zeroNotEmpty\":0,\"collapsed\":0,\"columnWidth\":25,\"size\":10}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('139', 'FieldtypeInteger', 'snipcart_item_height', '0', 'Product Height', '{\"description\":\"Set the height for this product.\",\"notes\":\"Uses centimeters as unit.\",\"min\":1,\"inputType\":\"number\",\"tags\":\"Snipcart\",\"zeroNotEmpty\":0,\"collapsed\":0,\"columnWidth\":25,\"size\":10}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('140', 'FieldtypeInteger', 'snipcart_item_quantity', '0', 'Default Quantity', '{\"description\":\"The default quantity for the product that will be added to cart.\",\"notes\":\"Integer number (min value = 1).\",\"defaultValue\":1,\"min\":1,\"inputType\":\"number\",\"tags\":\"Snipcart\",\"zeroNotEmpty\":0,\"collapsed\":0,\"columnWidth\":25,\"size\":10}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('141', 'FieldtypeInteger', 'snipcart_item_max_quantity', '0', 'Maximum Quantity', '{\"description\":\"Set the maximum allowed quantity for this product.\",\"notes\":\"Leave empty for no limit.\",\"min\":1,\"inputType\":\"number\",\"tags\":\"Snipcart\",\"zeroNotEmpty\":0,\"collapsed\":0,\"columnWidth\":25,\"size\":10}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('142', 'FieldtypeInteger', 'snipcart_item_min_quantity', '0', 'Minimum Quantity', '{\"description\":\"Set the minimum allowed quantity for this product.\",\"notes\":\"Leave empty for no limit.\",\"min\":1,\"inputType\":\"number\",\"tags\":\"Snipcart\",\"zeroNotEmpty\":0,\"collapsed\":0,\"columnWidth\":25,\"size\":10}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('143', 'FieldtypeInteger', 'snipcart_item_quantity_step', '0', 'Quantity Step', '{\"description\":\"The quantity of a product will increment by this value.\",\"notes\":\"Integer number (min value = 1).\",\"defaultValue\":1,\"min\":1,\"inputType\":\"number\",\"tags\":\"Snipcart\",\"zeroNotEmpty\":0,\"collapsed\":0,\"columnWidth\":25,\"size\":10}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('144', 'FieldtypeCheckbox', 'snipcart_item_stackable', '0', 'Stackable', '{\"label2\":\"Product is stackable\",\"description\":\"Uncheck, if this product should be added to cart in distinct items instead of increasing quantity.\",\"tags\":\"Snipcart\",\"collapsed\":0,\"columnWidth\":25}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('145', 'FieldtypeCheckbox', 'snipcart_item_taxable', '0', 'Taxable', '{\"label2\":\"Product is taxable\",\"description\":\"Uncheck, if this product should be excluded from taxes calculation.\",\"tags\":\"Snipcart\",\"collapsed\":0,\"columnWidth\":25}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('146', 'FieldtypeSnipWireTaxSelector', 'snipcart_item_taxes', '0', 'VAT', '{\"description\":\"Select the tax which should be applied.\",\"tags\":\"Snipcart\",\"taxesType\":1,\"inputfieldClass\":\"InputfieldSelect\",\"collapsed\":0,\"columnWidth\":25}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('147', 'FieldtypeOptions', 'snipcart_item_payment_interval', '0', 'Payment Interval', '{\"description\":\"Choose an interval for recurring payment.\",\"inputfield\":\"InputfieldSelect\",\"required\":true,\"tags\":\"Snipcart\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('148', 'FieldtypeInteger', 'snipcart_item_payment_interval_count', '0', 'Interval Count', '{\"description\":\"Changes the payment interval count.\",\"notes\":\"Integer number (min value = 1).\",\"defaultValue\":1,\"min\":1,\"inputType\":\"number\",\"required\":true,\"tags\":\"Snipcart\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('149', 'FieldtypeInteger', 'snipcart_item_payment_trial', '0', 'Trial Period', '{\"description\":\"Trial period for customers in days. Empty for no trial!\",\"notes\":\"Integer number (min value = 1).\",\"min\":1,\"inputType\":\"number\",\"tags\":\"Snipcart\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('150', 'FieldtypeCheckbox', 'snipcart_item_recurring_shipping', '0', 'Recurring Shipping', '{\"label2\":\"Charge shipping only on initial order\",\"description\":\"Uncheck to add shipping costs to every upcoming recurring payment\",\"tags\":\"Snipcart\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('151', 'FieldtypeCheckbox', 'snipcart_item_shippable', '0', 'Shippable', '{\"label2\":\"Product is shippable\",\"description\":\"Uncheck, if this product should be flagged as not shippable.\",\"tags\":\"Snipcart\",\"collapsed\":0,\"columnWidth\":25}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('152', 'FieldtypeRepeater', 'prod_variant_item', '0', 'Variant item', '{\"template_id\":52,\"parent_id\":1085,\"repeaterFields\":[1,131],\"repeaterCollapse\":0,\"repeaterLoading\":1,\"collapsed\":0,\"repeaterTitle\":\"#n: {title} - +{snipcart_item_price_eur}\\u20ac\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('153', 'FieldtypeRepeater', 'prod_variants', '0', 'Variants', '{\"template_id\":53,\"parent_id\":1086,\"repeaterFields\":[1,152],\"repeaterCollapse\":0,\"repeaterLoading\":1,\"collapsed\":0,\"repeaterTitle\":\"#n: {title}\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('154', 'FieldtypeImage', 'shop_coverimage', '0', 'Shop Image for tile', '{\"fileSchema\":270,\"extensions\":\"gif jpg jpeg png\",\"maxFiles\":1,\"outputFormat\":0,\"defaultValuePage\":0,\"useTags\":0,\"inputfieldClass\":\"InputfieldImage\",\"descriptionRows\":1,\"gridMode\":\"grid\",\"focusMode\":\"on\",\"resizeServer\":0,\"clientQuality\":90,\"maxReject\":0,\"dimensionsByAspectRatio\":0}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('155', 'FieldtypeTextLanguage', 'board_style', '0', 'Board Styles (Freeride / Freestyle)', '{\"langBlankInherit\":0,\"collapsed\":0,\"minlength\":0,\"maxlength\":2048,\"showCount\":0,\"size\":0}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('156', 'FieldtypeTextareaLanguage', 'body_2', '0', 'Body right side', '{\"inputfieldClass\":\"InputfieldCKEditor\",\"contentType\":1,\"langBlankInherit\":0,\"collapsed\":0,\"minlength\":0,\"maxlength\":0,\"showCount\":0,\"rows\":5,\"htmlOptions\":[2,4,8,16],\"toolbar\":\"Format, Styles, -, Bold, Italic, -, RemoveFormat\\nNumberedList, BulletedList, -, Blockquote\\nPWLink, Unlink, Anchor\\nPWImage, Table, HorizontalRule, SpecialChar\\nPasteText, PasteFromWord\\nScayt, -, Sourcedialog\",\"inlineMode\":0,\"useACF\":1,\"usePurifier\":1,\"formatTags\":\"p;h1;h2;h3;h4;h5;h6;pre;address\",\"extraPlugins\":[\"pwimage\",\"pwlink\",\"sourcedialog\"],\"removePlugins\":\"image,magicline\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('157', 'FieldtypeCheckbox', 'hide_header', '0', 'Hide header', '{\"collapsed\":0,\"columnWidth\":50}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('158', 'FieldtypeRepeater', 'bigLinks', '0', 'Tile links', '{\"template_id\":56,\"parent_id\":1227,\"repeaterFields\":[1,105,76,120,121],\"repeaterCollapse\":0,\"repeaterLoading\":1,\"collapsed\":0,\"repeaterTitle\":\"#n: {title}\",\"repeaterMaxItems\":4}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('159', 'FieldtypeColorPicker', 'color', '0', 'Color', '{\"formatting\":1,\"collapsed\":0}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('161', 'FieldtypeRepeater', 'prod_custom_variant_item', '0', 'Custom Variant Item', '{\"template_id\":58,\"parent_id\":1243,\"repeaterFields\":[1,159,131],\"repeaterTitle\":\"#n: {title} - +{snipcart_item_price_eur}\\u20ac\",\"repeaterCollapse\":0,\"repeaterLoading\":1,\"collapsed\":0}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('160', 'FieldtypeRepeater', 'prod_custom_variants', '0', 'Custom Variants', '{\"template_id\":57,\"parent_id\":1242,\"repeaterCollapse\":0,\"repeaterLoading\":1,\"collapsed\":0,\"repeaterFields\":[1,161]}');

DROP TABLE IF EXISTS `fieldtype_options`;
CREATE TABLE `fieldtype_options` (
  `fields_id` int unsigned NOT NULL,
  `option_id` int unsigned NOT NULL,
  `title` text,
  `value` varchar(230) DEFAULT NULL,
  `sort` int unsigned NOT NULL,
  `title1012` text,
  `value1012` varchar(230) DEFAULT NULL,
  `title1013` text,
  `value1013` varchar(230) DEFAULT NULL,
  PRIMARY KEY (`fields_id`,`option_id`),
  UNIQUE KEY `title` (`title`(230),`fields_id`),
  UNIQUE KEY `title1012` (`title1012`(230),`fields_id`),
  UNIQUE KEY `title1013` (`title1013`(230),`fields_id`),
  KEY `value` (`value`,`fields_id`),
  KEY `sort` (`sort`,`fields_id`),
  KEY `value1012` (`value1012`,`fields_id`),
  KEY `value1013` (`value1013`,`fields_id`),
  FULLTEXT KEY `title_value` (`title`,`value`),
  FULLTEXT KEY `title1012_value1012` (`title1012`,`value1012`),
  FULLTEXT KEY `title1013_value1013` (`title1013`,`value1013`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `fieldtype_options` (`fields_id`, `option_id`, `title`, `value`, `sort`, `title1012`, `value1012`, `title1013`, `value1013`) VALUES('127', '2', 'light', '', '1', NULL, NULL, NULL, NULL);
INSERT INTO `fieldtype_options` (`fields_id`, `option_id`, `title`, `value`, `sort`, `title1012`, `value1012`, `title1013`, `value1013`) VALUES('127', '1', 'dark', '', '0', NULL, NULL, NULL, NULL);
INSERT INTO `fieldtype_options` (`fields_id`, `option_id`, `title`, `value`, `sort`, `title1012`, `value1012`, `title1013`, `value1013`) VALUES('147', '1', 'Day', 'Day', '0', NULL, NULL, NULL, NULL);
INSERT INTO `fieldtype_options` (`fields_id`, `option_id`, `title`, `value`, `sort`, `title1012`, `value1012`, `title1013`, `value1013`) VALUES('147', '2', 'Week', 'Week', '1', NULL, NULL, NULL, NULL);
INSERT INTO `fieldtype_options` (`fields_id`, `option_id`, `title`, `value`, `sort`, `title1012`, `value1012`, `title1013`, `value1013`) VALUES('147', '3', 'Month', 'Month', '2', NULL, NULL, NULL, NULL);
INSERT INTO `fieldtype_options` (`fields_id`, `option_id`, `title`, `value`, `sort`, `title1012`, `value1012`, `title1013`, `value1013`) VALUES('147', '4', 'Year', 'Year', '3', NULL, NULL, NULL, NULL);

DROP TABLE IF EXISTS `modules`;
CREATE TABLE `modules` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `class` varchar(128) CHARACTER SET ascii COLLATE ascii_general_ci NOT NULL,
  `flags` int NOT NULL DEFAULT '0',
  `data` text NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `class` (`class`)
) ENGINE=MyISAM AUTO_INCREMENT=190 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('1', 'FieldtypeTextarea', '0', '', '2020-02-05 07:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('2', 'FieldtypeNumber', '0', '', '2020-02-05 07:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('3', 'FieldtypeText', '1', '', '2020-02-05 07:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('4', 'FieldtypePage', '3', '', '2020-02-05 07:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('30', 'InputfieldForm', '0', '', '2020-02-05 07:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('6', 'FieldtypeFile', '1', '', '2020-02-05 07:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('7', 'ProcessPageEdit', '1', '', '2020-02-05 07:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('10', 'ProcessLogin', '0', '', '2020-02-05 07:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('12', 'ProcessPageList', '0', '{\"pageLabelField\":\"title\",\"paginationLimit\":25,\"limit\":50}', '2020-02-05 07:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('121', 'ProcessPageEditLink', '1', '', '2020-02-05 07:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('14', 'ProcessPageSort', '0', '', '2020-02-05 07:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('15', 'InputfieldPageListSelect', '0', '', '2020-02-05 07:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('117', 'JqueryUI', '1', '', '2020-02-05 07:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('17', 'ProcessPageAdd', '0', '', '2020-02-05 07:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('125', 'SessionLoginThrottle', '11', '', '2020-02-05 07:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('122', 'InputfieldPassword', '0', '', '2020-02-05 07:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('25', 'InputfieldAsmSelect', '0', '', '2020-02-05 07:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('116', 'JqueryCore', '1', '', '2020-02-05 07:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('27', 'FieldtypeModule', '1', '', '2020-02-05 07:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('28', 'FieldtypeDatetime', '0', '', '2020-02-05 07:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('29', 'FieldtypeEmail', '1', '', '2020-02-05 07:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('108', 'InputfieldURL', '0', '', '2020-02-05 07:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('32', 'InputfieldSubmit', '0', '', '2020-02-05 07:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('33', 'InputfieldWrapper', '0', '', '2020-02-05 07:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('34', 'InputfieldText', '0', '', '2020-02-05 07:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('35', 'InputfieldTextarea', '0', '', '2020-02-05 07:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('36', 'InputfieldSelect', '0', '', '2020-02-05 07:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('37', 'InputfieldCheckbox', '0', '', '2020-02-05 07:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('38', 'InputfieldCheckboxes', '0', '', '2020-02-05 07:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('39', 'InputfieldRadios', '0', '', '2020-02-05 07:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('40', 'InputfieldHidden', '0', '', '2020-02-05 07:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('41', 'InputfieldName', '0', '', '2020-02-05 07:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('43', 'InputfieldSelectMultiple', '0', '', '2020-02-05 07:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('45', 'JqueryWireTabs', '0', '', '2020-02-05 07:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('46', 'ProcessPage', '0', '', '2020-02-05 07:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('47', 'ProcessTemplate', '0', '', '2020-02-05 07:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('48', 'ProcessField', '32', '', '2020-02-05 07:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('50', 'ProcessModule', '0', '', '2020-02-05 07:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('114', 'PagePermissions', '3', '', '2020-02-05 07:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('97', 'FieldtypeCheckbox', '1', '', '2020-02-05 07:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('115', 'PageRender', '3', '{\"clearCache\":1}', '2020-02-05 07:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('55', 'InputfieldFile', '0', '', '2020-02-05 07:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('56', 'InputfieldImage', '0', '', '2020-02-05 07:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('57', 'FieldtypeImage', '1', '', '2020-02-05 07:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('60', 'InputfieldPage', '0', '{\"inputfieldClasses\":[\"InputfieldSelect\",\"InputfieldSelectMultiple\",\"InputfieldCheckboxes\",\"InputfieldRadios\",\"InputfieldAsmSelect\",\"InputfieldPageListSelect\",\"InputfieldPageListSelectMultiple\",\"InputfieldPageAutocomplete\"]}', '2020-02-05 07:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('61', 'TextformatterEntities', '0', '', '2020-02-05 07:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('66', 'ProcessUser', '0', '{\"showFields\":[\"name\",\"email\",\"roles\"]}', '2020-02-05 07:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('67', 'MarkupAdminDataTable', '0', '', '2020-02-05 07:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('68', 'ProcessRole', '0', '{\"showFields\":[\"name\"]}', '2020-02-05 07:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('76', 'ProcessList', '0', '', '2020-02-05 07:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('78', 'InputfieldFieldset', '0', '', '2020-02-05 07:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('79', 'InputfieldMarkup', '0', '', '2020-02-05 07:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('80', 'InputfieldEmail', '0', '', '2020-02-05 07:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('89', 'FieldtypeFloat', '1', '', '2020-02-05 07:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('83', 'ProcessPageView', '0', '', '2020-02-05 07:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('84', 'FieldtypeInteger', '0', '', '2020-02-05 07:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('85', 'InputfieldInteger', '0', '', '2020-02-05 07:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('86', 'InputfieldPageName', '0', '', '2020-02-05 07:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('87', 'ProcessHome', '0', '', '2020-02-05 07:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('90', 'InputfieldFloat', '0', '', '2020-02-05 07:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('94', 'InputfieldDatetime', '0', '', '2020-02-05 07:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('98', 'MarkupPagerNav', '0', '', '2020-02-05 07:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('129', 'ProcessPageEditImageSelect', '1', '', '2020-02-05 07:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('103', 'JqueryTableSorter', '1', '', '2020-02-05 07:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('104', 'ProcessPageSearch', '1', '{\"searchFields\":\"title\",\"displayField\":\"title path\"}', '2020-02-05 07:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('105', 'FieldtypeFieldsetOpen', '1', '', '2020-02-05 07:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('106', 'FieldtypeFieldsetClose', '1', '', '2020-02-05 07:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('107', 'FieldtypeFieldsetTabOpen', '1', '', '2020-02-05 07:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('109', 'ProcessPageTrash', '1', '', '2020-02-05 07:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('111', 'FieldtypePageTitle', '1', '', '2020-02-05 07:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('112', 'InputfieldPageTitle', '0', '', '2020-02-05 07:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('113', 'MarkupPageArray', '3', '', '2020-02-05 07:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('131', 'InputfieldButton', '0', '', '2020-02-05 07:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('133', 'FieldtypePassword', '1', '', '2020-02-05 07:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('134', 'ProcessPageType', '33', '{\"showFields\":[]}', '2020-02-05 07:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('135', 'FieldtypeURL', '1', '', '2020-02-05 07:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('136', 'ProcessPermission', '1', '{\"showFields\":[\"name\",\"title\"]}', '2020-02-05 07:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('137', 'InputfieldPageListSelectMultiple', '0', '', '2020-02-05 07:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('138', 'ProcessProfile', '1', '{\"profileFields\":[\"pass\",\"email\",\"language\",\"admin_theme\"]}', '2020-02-05 07:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('139', 'SystemUpdater', '1', '{\"systemVersion\":18,\"coreVersion\":\"3.0.165\"}', '2020-02-05 07:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('148', 'AdminThemeDefault', '10', '{\"colors\":\"classic\"}', '2020-02-05 07:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('149', 'InputfieldSelector', '42', '', '2020-02-05 07:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('150', 'ProcessPageLister', '32', '', '2020-02-05 07:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('151', 'JqueryMagnific', '1', '', '2020-02-05 07:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('152', 'PagePathHistory', '3', '', '2020-02-05 07:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('155', 'InputfieldCKEditor', '0', '', '2020-02-05 07:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('156', 'MarkupHTMLPurifier', '0', '', '2020-02-05 07:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('158', 'LanguageSupport', '35', '{\"languagesPageID\":1009,\"defaultLanguagePageID\":1010,\"otherLanguagePageIDs\":[1012],\"languageTranslatorPageID\":1011}', '2020-02-05 07:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('159', 'ProcessLanguage', '1', '', '2020-02-05 07:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('160', 'ProcessLanguageTranslator', '1', '', '2020-02-05 07:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('161', 'LanguageSupportFields', '3', '', '2020-02-05 07:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('162', 'FieldtypeTextLanguage', '1', '', '2020-02-05 07:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('163', 'FieldtypePageTitleLanguage', '1', '', '2020-02-05 07:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('164', 'FieldtypeTextareaLanguage', '1', '', '2020-02-05 07:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('165', 'LanguageSupportPageNames', '3', '{\"moduleVersion\":10,\"pageNumUrlPrefix1010\":\"page\",\"useHomeSegment\":0}', '2020-02-05 07:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('166', 'LanguageTabs', '11', '', '2020-02-05 07:51:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('168', 'ProcessRecentPages', '1', '', '2020-02-05 07:51:29');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('169', 'AdminThemeUikit', '10', '', '2020-02-05 07:51:30');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('170', 'ProcessLogger', '1', '', '2020-02-05 07:51:35');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('171', 'InputfieldIcon', '0', '', '2020-02-05 07:51:35');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('172', 'FieldtypeRepeater', '35', '{\"repeatersRootPageID\":1027,\"uninstall\":\"\",\"submit_save_module\":\"Submit\"}', '2020-04-20 04:38:23');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('173', 'InputfieldRepeater', '0', '', '2020-04-20 04:38:23');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('174', 'ProcessDatabaseBackups', '1', '', '2020-04-20 07:47:22');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('175', 'CroppableImage3', '0', '', '2020-04-22 10:50:40');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('176', 'FieldtypeCroppableImage3', '1', '', '2020-04-22 10:50:40');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('177', 'InputfieldCroppableImage3', '0', '', '2020-04-22 10:50:40');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('178', 'ProcessCroppableImage3', '1', '', '2020-04-22 10:50:40');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('179', 'DiagnoseImagehandling', '1', '', '2020-04-22 10:53:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('180', 'ProcessDiagnostics', '1', '', '2020-04-22 10:53:13');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('181', 'FieldtypeOptions', '1', '', '2020-04-22 15:45:41');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('182', 'TextformatterVideoEmbed', '1', '{\"maxWidth\":640,\"maxHeight\":480,\"responsive\":1,\"clearCache\":\"\",\"uninstall\":\"\",\"submit_save_module\":\"Submit\"}', '2020-04-23 10:28:03');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('183', 'SnipWire', '3', '{\"snipcart_environment\":\"0\",\"api_key\":\"YOUR_LIVE_API_KEY\",\"api_key_secret\":\"YOUR_LIVE_API_KEY_SECRET\",\"api_key_test\":\"MmQ0YzA2NDEtNzg3NS00OWFlLWE1YWUtMzI4YTg1ODQxOTNlNjM3MjMzNTYzMDMyOTI5Mjkz\",\"api_key_secret_test\":\"ST_NDJjYzU4ZTktMDk1Yy00NTIzLThiZGYtNTU1NTM0ODY0MzQ3NjM3MjMzNTYzOTExMTExNjU1\",\"credit_cards\":[\"visa\",\"mastercard\",\"maestro\"],\"currencies\":[\"eur\"],\"show_cart_automatically\":1,\"shipping_same_as_billing\":1,\"show_continue_shopping\":1,\"split_firstname_and_lastname\":1,\"cart_custom_fields_enabled\":1,\"snipcart_debug\":1,\"taxes_provider\":\"integrated\",\"taxes_included\":1,\"taxes\":\"[{\\\"name\\\":\\\"20% VAT\\\",\\\"numberForInvoice\\\":\\\"\\\",\\\"rate\\\":\\\"0.20\\\",\\\"appliesOnShipping\\\":[]},{\\\"name\\\":\\\"10% VAT\\\",\\\"numberForInvoice\\\":\\\"\\\",\\\"rate\\\":\\\"0.10\\\",\\\"appliesOnShipping\\\":[]},{\\\"name\\\":\\\"20% VAT\\\",\\\"numberForInvoice\\\":\\\"\\\",\\\"rate\\\":\\\"0.20\\\",\\\"appliesOnShipping\\\":[\\\"1\\\"]}]\",\"shipping_taxes_type\":\"3\",\"include_snipcart_css\":1,\"snipcart_css_path\":\"https:\\/\\/cdn.snipcart.com\\/themes\\/2.0\\/base\\/snipcart.min.css\",\"snipcart_css_integrity\":\"\",\"snipcart_js_path\":\"https:\\/\\/cdn.snipcart.com\\/scripts\\/2.0\\/snipcart.js\",\"snipcart_js_integrity\":\"\",\"include_jquery\":1,\"jquery_js_path\":\"https:\\/\\/code.jquery.com\\/jquery-3.3.1.min.js\",\"jquery_js_integrity\":\"sha256-FgpCb\\/KJQlLNfOu91ta32o\\/NMZxltwRo8QtmkMRdAu8=\",\"excluded_templates\":[],\"cart_image_width\":65,\"cart_image_height\":65,\"cart_image_quality\":70,\"cart_image_hidpi\":1,\"cart_image_hidpiQuality\":50,\"cart_image_cropping\":1,\"webhooks_endpoint\":\"\\/webhooks\\/snipcart\",\"product_templates\":[\"snipcart-product\",\"board-page\"],\"data_item_name_field\":\"title\",\"data_item_categories_field\":\"\",\"currency_param\":\"currency\",\"single_page_shop\":\"\",\"single_page_shop_page\":1,\"snipwire_debug\":\"\",\"uninstall\":\"\",\"submit_save_module\":\"Submit\",\"product_package\":true}', '2020-04-24 20:03:58');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('184', 'ProcessSnipWire', '1', '', '2020-04-24 20:03:58');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('185', 'MarkupSnipWire', '3', '', '2020-04-24 20:03:58');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('186', 'FieldtypeSnipWireTaxSelector', '1', '', '2020-04-24 20:03:58');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('187', 'InputfieldPageAutocomplete', '0', '', '2020-06-06 14:01:07');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('188', 'FieldtypeColorPicker', '1', '', '2021-03-07 13:09:38');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('189', 'InputfieldColorPicker', '0', '', '2021-03-07 13:09:38');

DROP TABLE IF EXISTS `page_path_history`;
CREATE TABLE `page_path_history` (
  `path` varchar(250) NOT NULL,
  `pages_id` int unsigned NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `language_id` int unsigned DEFAULT '0',
  PRIMARY KEY (`path`),
  KEY `pages_id` (`pages_id`),
  KEY `created` (`created`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `page_path_history` (`path`, `pages_id`, `created`, `language_id`) VALUES('/de/uber/unterseite-seite-beispiel-1', '1002', '2020-04-15 07:32:43', '1012');
INSERT INTO `page_path_history` (`path`, `pages_id`, `created`, `language_id`) VALUES('/fi/tietoja/alasivu-sivu-esimerkki-1', '1002', '2020-04-15 07:32:43', '1013');
INSERT INTO `page_path_history` (`path`, `pages_id`, `created`, `language_id`) VALUES('/en/about/child-page-example-1', '1002', '2020-04-15 07:32:43', '0');
INSERT INTO `page_path_history` (`path`, `pages_id`, `created`, `language_id`) VALUES('/de/uber/unterseite-beispiel-2', '1004', '2020-04-15 07:33:21', '1012');
INSERT INTO `page_path_history` (`path`, `pages_id`, `created`, `language_id`) VALUES('/fi/tietoja/alasivu-esimerkki-2', '1004', '2020-04-15 07:33:21', '1013');
INSERT INTO `page_path_history` (`path`, `pages_id`, `created`, `language_id`) VALUES('/en/about/child-page-example-2', '1004', '2020-04-15 07:33:21', '0');
INSERT INTO `page_path_history` (`path`, `pages_id`, `created`, `language_id`) VALUES('/de/shop/1050.1020.0_boards', '1050', '2020-04-24 20:47:50', '1012');
INSERT INTO `page_path_history` (`path`, `pages_id`, `created`, `language_id`) VALUES('/fi/shop/1050.1020.0_boards', '1050', '2020-04-24 20:47:50', '1013');
INSERT INTO `page_path_history` (`path`, `pages_id`, `created`, `language_id`) VALUES('/de/1021.1.2_boards', '1021', '2020-04-24 22:02:32', '1012');
INSERT INTO `page_path_history` (`path`, `pages_id`, `created`, `language_id`) VALUES('/fi/1021.1.2_boards', '1021', '2020-04-24 22:02:32', '1013');
INSERT INTO `page_path_history` (`path`, `pages_id`, `created`, `language_id`) VALUES('/shop', '1020', '2020-04-24 22:02:52', '0');
INSERT INTO `page_path_history` (`path`, `pages_id`, `created`, `language_id`) VALUES('/en/boards/freestyler/freestyler-135', '1077', '2020-06-13 15:18:44', '0');
INSERT INTO `page_path_history` (`path`, `pages_id`, `created`, `language_id`) VALUES('/en/boards/freestyler/freeride', '1077', '2021-02-01 07:49:44', '0');
INSERT INTO `page_path_history` (`path`, `pages_id`, `created`, `language_id`) VALUES('/en/boards/freestyler/freestyler-140', '1083', '2021-02-01 07:51:12', '0');
INSERT INTO `page_path_history` (`path`, `pages_id`, `created`, `language_id`) VALUES('/en/boards/freestyler/freestyle', '1142', '2021-02-01 07:51:49', '0');
INSERT INTO `page_path_history` (`path`, `pages_id`, `created`, `language_id`) VALUES('/en/customs/skimboards', '1174', '2021-03-07 10:34:18', '0');
INSERT INTO `page_path_history` (`path`, `pages_id`, `created`, `language_id`) VALUES('/de/shop/stock-boards/1179.1177.0_muster-stock-board', '1179', '2021-03-07 10:37:14', '1012');
INSERT INTO `page_path_history` (`path`, `pages_id`, `created`, `language_id`) VALUES('/en/twintips/freerider-explorer', '1158', '2021-03-07 11:13:48', '0');
INSERT INTO `page_path_history` (`path`, `pages_id`, `created`, `language_id`) VALUES('/de/customs/1172.1154.0_waveboard', '1172', '2021-03-07 11:45:11', '1012');
INSERT INTO `page_path_history` (`path`, `pages_id`, `created`, `language_id`) VALUES('/de/customs/skimboard', '1174', '2021-03-07 11:45:12', '1012');
INSERT INTO `page_path_history` (`path`, `pages_id`, `created`, `language_id`) VALUES('/en/about/manufactur', '1002', '2021-03-07 13:02:23', '0');

DROP TABLE IF EXISTS `pages`;
CREATE TABLE `pages` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int unsigned NOT NULL DEFAULT '0',
  `templates_id` int unsigned NOT NULL DEFAULT '0',
  `name` varchar(128) CHARACTER SET ascii COLLATE ascii_general_ci NOT NULL,
  `status` int unsigned NOT NULL DEFAULT '1',
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_users_id` int unsigned NOT NULL DEFAULT '2',
  `created` timestamp NOT NULL DEFAULT '2015-12-18 04:09:00',
  `created_users_id` int unsigned NOT NULL DEFAULT '2',
  `published` datetime DEFAULT NULL,
  `sort` int NOT NULL DEFAULT '0',
  `name1012` varchar(128) CHARACTER SET ascii COLLATE ascii_general_ci DEFAULT NULL,
  `status1012` int unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_parent_id` (`name`,`parent_id`),
  UNIQUE KEY `name1012_parent_id` (`name1012`,`parent_id`),
  KEY `parent_id` (`parent_id`),
  KEY `templates_id` (`templates_id`),
  KEY `modified` (`modified`),
  KEY `created` (`created`),
  KEY `status` (`status`),
  KEY `published` (`published`)
) ENGINE=MyISAM AUTO_INCREMENT=1265 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1', '0', '1', 'en', '9', '2021-03-07 13:06:27', '41', '2020-02-05 07:51:13', '2', '2020-02-05 09:51:13', '0', 'de', '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('2', '1', '2', 'admin', '1035', '2020-02-05 07:51:30', '40', '2020-02-05 07:51:13', '2', '2020-02-05 09:51:13', '12', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('3', '2', '2', 'page', '21', '2020-02-05 07:51:13', '41', '2020-02-05 07:51:13', '2', '2020-02-05 09:51:13', '0', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('6', '3', '2', 'add', '21', '2020-02-05 07:51:39', '40', '2020-02-05 07:51:13', '2', '2020-02-05 09:51:13', '1', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('7', '1', '2', 'trash', '1039', '2020-02-05 07:51:13', '41', '2020-02-05 07:51:13', '2', '2020-02-05 09:51:13', '13', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('8', '3', '2', 'list', '21', '2020-02-05 07:51:41', '41', '2020-02-05 07:51:13', '2', '2020-02-05 09:51:13', '0', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('9', '3', '2', 'sort', '1047', '2020-02-05 07:51:13', '41', '2020-02-05 07:51:13', '2', '2020-02-05 09:51:13', '3', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('10', '3', '2', 'edit', '1045', '2020-02-05 07:51:40', '41', '2020-02-05 07:51:13', '2', '2020-02-05 09:51:13', '4', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('11', '22', '2', 'template', '21', '2020-02-05 07:51:13', '41', '2020-02-05 07:51:13', '2', '2020-02-05 09:51:13', '0', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('16', '22', '2', 'field', '21', '2020-02-05 07:51:13', '41', '2020-02-05 07:51:13', '2', '2020-02-05 09:51:13', '2', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('21', '2', '2', 'module', '21', '2020-02-05 07:51:13', '41', '2020-02-05 07:51:13', '2', '2020-02-05 09:51:13', '2', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('22', '2', '2', 'setup', '21', '2020-02-05 07:51:13', '41', '2020-02-05 07:51:13', '2', '2020-02-05 09:51:13', '1', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('23', '2', '2', 'login', '1035', '2020-02-05 07:51:13', '41', '2020-02-05 07:51:13', '2', '2020-02-05 09:51:13', '4', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('27', '1', '29', 'http404', '1035', '2020-02-05 07:51:13', '41', '2020-02-05 07:51:13', '3', '2020-02-05 09:51:13', '11', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('28', '2', '2', 'access', '13', '2020-02-05 07:51:13', '41', '2020-02-05 07:51:13', '2', '2020-02-05 09:51:13', '3', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('29', '28', '2', 'users', '29', '2020-02-05 07:51:13', '41', '2020-02-05 07:51:13', '2', '2020-02-05 09:51:13', '0', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('30', '28', '2', 'roles', '29', '2020-02-05 07:51:13', '41', '2020-02-05 07:51:13', '2', '2020-02-05 09:51:13', '1', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('31', '28', '2', 'permissions', '29', '2020-02-05 07:51:13', '41', '2020-02-05 07:51:13', '2', '2020-02-05 09:51:13', '2', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('32', '31', '5', 'page-edit', '25', '2020-02-05 07:51:13', '41', '2020-02-05 07:51:13', '2', '2020-02-05 09:51:13', '2', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('34', '31', '5', 'page-delete', '25', '2020-02-05 07:51:13', '41', '2020-02-05 07:51:13', '2', '2020-02-05 09:51:13', '3', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('35', '31', '5', 'page-move', '25', '2020-02-05 07:51:13', '41', '2020-02-05 07:51:13', '2', '2020-02-05 09:51:13', '4', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('36', '31', '5', 'page-view', '25', '2020-02-05 07:51:13', '41', '2020-02-05 07:51:13', '2', '2020-02-05 09:51:13', '0', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('37', '30', '4', 'guest', '25', '2020-02-05 07:51:13', '41', '2020-02-05 07:51:13', '2', '2020-02-05 09:51:13', '0', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('38', '30', '4', 'superuser', '25', '2020-02-05 07:51:13', '41', '2020-02-05 07:51:13', '2', '2020-02-05 09:51:13', '1', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('41', '29', '3', 'admin', '1', '2021-01-21 09:41:16', '40', '2020-02-05 07:51:13', '2', '2020-02-05 09:51:13', '0', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('40', '29', '3', 'guest', '25', '2020-02-05 07:51:13', '41', '2020-02-05 07:51:13', '2', '2020-02-05 09:51:13', '1', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('50', '31', '5', 'page-sort', '25', '2020-02-05 07:51:13', '41', '2020-02-05 07:51:13', '41', '2020-02-05 09:51:13', '5', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('51', '31', '5', 'page-template', '25', '2020-02-05 07:51:13', '41', '2020-02-05 07:51:13', '41', '2020-02-05 09:51:13', '6', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('52', '31', '5', 'user-admin', '25', '2020-02-05 07:51:13', '41', '2020-02-05 07:51:13', '41', '2020-02-05 09:51:13', '10', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('53', '31', '5', 'profile-edit', '1', '2020-02-05 07:51:13', '41', '2020-02-05 07:51:13', '41', '2020-02-05 09:51:13', '13', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('54', '31', '5', 'page-lock', '1', '2020-02-05 07:51:13', '41', '2020-02-05 07:51:13', '41', '2020-02-05 09:51:13', '8', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('300', '3', '2', 'search', '1045', '2020-02-05 07:51:13', '41', '2020-02-05 07:51:13', '2', '2020-02-05 09:51:13', '6', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('301', '3', '2', 'trash', '1047', '2020-02-05 07:51:13', '41', '2020-02-05 07:51:13', '2', '2020-02-05 09:51:13', '6', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('302', '3', '2', 'link', '1041', '2020-02-05 07:51:13', '41', '2020-02-05 07:51:13', '2', '2020-02-05 09:51:13', '7', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('303', '3', '2', 'image', '1041', '2020-02-05 07:51:13', '41', '2020-02-05 07:51:13', '2', '2020-02-05 09:51:13', '8', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('304', '2', '2', 'profile', '1025', '2020-02-05 07:51:13', '41', '2020-02-05 07:51:13', '41', '2020-02-05 09:51:13', '5', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1000', '1', '26', 'search', '1025', '2020-04-15 07:34:23', '41', '2020-02-05 07:51:13', '2', '2020-02-05 09:51:13', '9', 'suche', '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1001', '1', '29', 'about', '1', '2020-04-22 16:16:58', '41', '2020-02-05 07:51:13', '2', '2020-02-05 09:51:13', '5', 'uber', '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1002', '1001', '29', 'manufactory', '1', '2021-03-07 13:02:23', '41', '2020-02-05 07:51:13', '2', '2020-02-05 09:51:13', '0', 'manufaktur', '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1024', '1001', '29', 'team', '1', '2020-06-06 14:25:41', '41', '2020-04-15 07:33:35', '41', '2020-04-15 11:33:37', '2', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1004', '1001', '29', 'history', '1', '2020-04-15 07:33:21', '41', '2020-02-05 07:51:13', '2', '2020-02-05 09:51:13', '1', 'geschichte', '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1005', '1', '34', 'site-map', '1025', '2020-04-15 07:34:27', '41', '2020-02-05 07:51:13', '2', '2020-02-05 09:51:13', '8', 'sitemap', '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1006', '31', '5', 'page-lister', '1', '2020-02-05 07:51:13', '40', '2020-02-05 07:51:13', '40', '2020-02-05 09:51:13', '9', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1007', '3', '2', 'lister', '1', '2020-02-05 07:51:13', '40', '2020-02-05 07:51:13', '40', '2020-02-05 09:51:13', '9', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1009', '22', '2', 'languages', '16', '2020-02-05 07:51:13', '41', '2020-02-05 07:51:13', '41', '2020-02-05 09:51:13', '2', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1010', '1009', '43', 'default', '16', '2020-06-06 14:28:18', '41', '2020-02-05 07:51:13', '41', '2020-02-05 09:51:13', '0', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1011', '22', '2', 'language-translator', '1040', '2020-02-05 07:51:13', '41', '2020-02-05 07:51:13', '41', '2020-02-05 09:51:13', '3', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1012', '1009', '43', 'de', '1', '2020-06-06 14:28:01', '41', '2020-02-05 07:51:13', '41', '2020-02-05 09:51:13', '1', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1122', '1103', '52', '1592069321-1275-1', '1', '2020-06-13 15:29:14', '41', '2020-06-13 15:28:41', '41', '2020-06-13 17:28:57', '2', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1015', '3', '2', 'recent-pages', '1', '2020-02-05 07:51:29', '40', '2020-02-05 07:51:29', '40', '2020-02-05 09:51:29', '10', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1016', '31', '5', 'page-edit-recent', '1', '2020-02-05 07:51:29', '40', '2020-02-05 07:51:29', '40', '2020-02-05 09:51:29', '10', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1017', '22', '2', 'logs', '1', '2020-02-05 07:51:35', '40', '2020-02-05 07:51:35', '40', '2020-02-05 09:51:35', '4', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1018', '31', '5', 'logs-view', '1', '2020-02-05 07:51:35', '40', '2020-02-05 07:51:35', '40', '2020-02-05 09:51:35', '11', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1019', '31', '5', 'logs-edit', '1', '2020-02-05 07:51:35', '40', '2020-02-05 07:51:35', '40', '2020-02-05 09:51:35', '12', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1020', '1', '48', 'boards', '1025', '2021-03-07 10:22:45', '41', '2020-04-15 07:31:08', '41', '2020-04-25 00:47:59', '3', 'boards', '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1021', '7', '48', '1021.1.2_boards', '8193', '2020-04-24 22:02:32', '41', '2020-04-15 07:31:23', '41', '2020-04-15 11:31:43', '2', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1022', '1021', '48', 'twin-tips', '8193', '2020-04-22 16:05:54', '41', '2020-04-15 07:31:36', '41', '2020-04-15 11:31:46', '1', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1023', '1021', '48', 'skimboards', '8193', '2020-04-22 10:23:53', '41', '2020-04-15 07:31:57', '41', '2020-04-15 11:31:59', '2', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1025', '1', '29', 'blog', '2049', '2021-03-07 11:00:33', '41', '2020-04-15 07:33:54', '41', '2021-03-07 10:53:13', '6', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1026', '1', '54', 'contact', '1', '2020-06-06 14:05:18', '41', '2020-04-15 07:34:51', '41', '2020-04-15 11:34:53', '7', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1027', '2', '2', 'repeaters', '1036', '2020-04-20 04:38:23', '41', '2020-04-20 04:38:23', '41', '2020-04-20 08:38:23', '6', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1028', '1027', '2', 'for-field-104', '17', '2020-04-20 04:47:53', '41', '2020-04-20 04:47:53', '41', '2020-04-20 08:47:53', '0', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1029', '1028', '2', 'for-page-1', '17', '2020-04-20 05:24:05', '41', '2020-04-20 05:24:05', '41', '2020-04-20 09:24:05', '0', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1030', '1029', '44', '1587365554-6473-1', '3073', '2020-04-20 05:53:33', '41', '2020-04-20 05:52:34', '41', NULL, '0', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1031', '1027', '2', 'for-field-108', '17', '2020-04-20 05:54:54', '41', '2020-04-20 05:54:54', '41', '2020-04-20 09:54:54', '1', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1032', '1031', '2', 'for-page-1', '17', '2020-04-20 05:55:51', '41', '2020-04-20 05:55:51', '41', '2020-04-20 09:55:51', '0', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1033', '1027', '2', 'for-field-111', '17', '2020-04-20 05:57:54', '41', '2020-04-20 05:57:54', '41', '2020-04-20 09:57:54', '2', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1034', '1033', '2', 'for-page-1', '17', '2020-04-20 06:07:14', '41', '2020-04-20 06:07:14', '41', '2020-04-20 10:07:14', '0', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1035', '1032', '45', '1587366473-433-1', '1', '2020-05-26 12:42:58', '41', '2020-04-20 06:07:53', '41', '2020-04-20 10:08:31', '0', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1036', '1032', '45', '1587366487-1834-1', '1', '2020-05-26 12:42:58', '41', '2020-04-20 06:08:07', '41', '2020-04-20 10:08:31', '1', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1037', '1032', '45', '1587366498-3997-1', '1', '2020-05-26 12:42:58', '41', '2020-04-20 06:08:18', '41', '2020-04-20 10:08:31', '2', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1038', '1032', '45', '1587366504-9452-1', '1', '2020-05-26 12:42:58', '41', '2020-04-20 06:08:24', '41', '2020-04-20 10:08:31', '3', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1043', '1028', '2', 'for-page-1025', '17', '2020-04-20 07:00:38', '41', '2020-04-20 07:00:38', '41', '2020-04-20 11:00:38', '1', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1039', '1034', '46', '1587366539-3996-1', '1', '2020-06-13 15:43:24', '41', '2020-04-20 06:08:59', '41', '2020-04-20 10:09:30', '0', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1040', '1034', '46', '1587366554-4292-1', '1', '2020-06-13 15:43:24', '41', '2020-04-20 06:09:14', '41', '2020-04-20 10:09:30', '1', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1041', '1034', '46', '1587366559-4722-1', '1', '2020-06-13 15:43:24', '41', '2020-04-20 06:09:19', '41', '2020-04-20 10:09:30', '2', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1042', '1034', '46', '1587366564-8884-1', '1', '2020-06-13 15:44:27', '41', '2020-04-20 06:09:24', '41', '2020-04-20 10:09:30', '3', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1044', '1028', '2', 'for-page-1001', '17', '2020-04-20 07:00:49', '41', '2020-04-20 07:00:49', '41', '2020-04-20 11:00:49', '2', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1045', '1044', '44', '1587369833-5524-1', '1', '2020-04-20 07:17:51', '41', '2020-04-20 07:03:53', '41', '2020-04-20 11:04:24', '0', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1048', '22', '2', 'db-backups', '1', '2020-04-20 07:47:22', '41', '2020-04-20 07:47:22', '41', '2020-04-20 11:47:22', '5', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1046', '1044', '44', '1587370386-2849-1', '1', '2020-04-20 07:19:24', '41', '2020-04-20 07:13:06', '41', '2020-04-20 11:13:30', '1', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1047', '1044', '44', '1587370388-9256-1', '3073', '2020-04-20 07:13:08', '41', '2020-04-20 07:13:08', '41', NULL, '2', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1049', '31', '5', 'db-backup', '1', '2020-04-20 07:47:22', '41', '2020-04-20 07:47:22', '41', '2020-04-20 11:47:22', '13', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1050', '7', '29', '1050.1020.0_boards', '8193', '2020-04-24 20:47:50', '41', '2020-04-20 08:04:25', '41', '2020-04-20 12:04:37', '0', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1051', '1028', '2', 'for-page-1050', '17', '2020-04-20 08:04:25', '41', '2020-04-20 08:04:25', '41', '2020-04-20 12:04:25', '3', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1052', '1022', '47', 'freestyler', '8193', '2020-04-23 10:59:40', '41', '2020-04-20 08:47:39', '41', '2020-04-22 13:58:53', '0', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1053', '1028', '2', 'for-page-1052', '17', '2020-04-20 08:47:39', '41', '2020-04-20 08:47:39', '41', '2020-04-20 12:47:39', '4', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1054', '1021', '48', 'waveboards', '8193', '2020-04-22 10:24:01', '41', '2020-04-22 10:22:55', '41', '2020-04-22 14:24:01', '3', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1055', '1028', '2', 'for-page-1054', '17', '2020-04-22 10:22:55', '41', '2020-04-22 10:22:55', '41', '2020-04-22 14:22:55', '5', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1056', '1028', '2', 'for-page-1022', '17', '2020-04-22 10:23:38', '41', '2020-04-22 10:23:38', '41', '2020-04-22 14:23:38', '6', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1057', '1028', '2', 'for-page-1023', '17', '2020-04-22 10:23:49', '41', '2020-04-22 10:23:49', '41', '2020-04-22 14:23:49', '7', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1058', '1023', '47', 'testboard', '8193', '2020-04-22 10:24:25', '41', '2020-04-22 10:24:16', '41', '2020-04-22 14:24:25', '0', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1059', '1028', '2', 'for-page-1058', '17', '2020-04-22 10:24:16', '41', '2020-04-22 10:24:16', '41', '2020-04-22 14:24:16', '8', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1060', '1054', '47', 'beaver', '8193', '2020-04-22 10:24:40', '41', '2020-04-22 10:24:37', '41', '2020-04-22 14:24:40', '0', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1061', '1028', '2', 'for-page-1060', '17', '2020-04-22 10:24:37', '41', '2020-04-22 10:24:37', '41', '2020-04-22 14:24:37', '9', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1062', '1028', '2', 'for-page-1021', '17', '2020-04-22 10:25:42', '41', '2020-04-22 10:25:42', '41', '2020-04-22 14:25:42', '10', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1063', '1022', '47', 'rote-rakete', '8193', '2020-04-22 16:24:09', '41', '2020-04-22 10:36:59', '41', '2020-04-22 14:37:04', '1', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1064', '1028', '2', 'for-page-1063', '17', '2020-04-22 10:36:59', '41', '2020-04-22 10:36:59', '41', '2020-04-22 14:36:59', '11', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1065', '1022', '47', 'flitzer', '8193', '2020-04-22 16:24:56', '41', '2020-04-22 10:37:17', '41', '2020-04-22 14:37:19', '2', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1066', '1028', '2', 'for-page-1065', '17', '2020-04-22 10:37:17', '41', '2020-04-22 10:37:17', '41', '2020-04-22 14:37:17', '12', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1067', '3', '2', 'croppable-image-3', '1025', '2020-04-22 10:50:40', '41', '2020-04-22 10:50:40', '41', '2020-04-22 14:50:40', '10', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1068', '31', '5', 'croppable-image-3', '1', '2020-04-22 10:50:40', '41', '2020-04-22 10:50:40', '41', '2020-04-22 14:50:40', '14', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1069', '22', '2', 'processdiagnostics', '1', '2020-04-22 10:53:13', '41', '2020-04-22 10:53:13', '41', '2020-04-22 14:53:13', '6', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1070', '1062', '44', '1587576401-1901-1', '1', '2020-04-22 16:28:21', '41', '2020-04-22 16:26:41', '41', '2020-04-22 20:27:25', '0', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1071', '1062', '44', '1587576517-5102-1', '1', '2020-04-22 16:29:26', '41', '2020-04-22 16:28:37', '41', '2020-04-22 20:29:02', '1', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1072', '1028', '2', 'for-page-1020', '17', '2020-04-22 17:02:34', '41', '2020-04-22 17:02:34', '41', '2020-04-22 21:02:34', '13', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1073', '22', '2', 'snipwire', '1', '2020-04-24 20:03:58', '41', '2020-04-24 20:03:58', '41', '2020-04-25 00:03:58', '7', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1074', '1073', '49', 'custom-cart-fields', '1040', '2020-04-24 20:03:58', '41', '2020-04-24 20:03:58', '41', '2020-04-25 00:03:58', '0', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1075', '31', '5', 'snipwire-dashboard', '1', '2020-04-24 20:03:58', '41', '2020-04-24 20:03:58', '41', '2020-04-25 00:03:58', '15', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1076', '1020', '50', 'freestyler', '1', '2021-03-07 10:24:56', '41', '2020-04-24 20:04:40', '41', '2020-04-25 00:04:40', '0', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1077', '1076', '51', 'freerider-lady', '1', '2021-03-07 10:24:54', '41', '2020-04-24 20:04:40', '41', '2020-04-25 00:04:40', '1', 'freerider-lady', '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1078', '7', '51', '1078.1076.1_festish-wet-warmer', '8193', '2020-04-24 20:50:07', '41', '2020-04-24 20:04:40', '41', '2020-04-25 00:04:40', '1', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1079', '7', '51', '1079.1076.2_axolotl-juicer', '8193', '2020-04-24 20:50:11', '41', '2020-04-24 20:04:40', '41', '2020-04-25 00:04:40', '2', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1080', '7', '51', '1080.1076.3_freestyler', '10241', '2020-04-24 20:50:15', '41', '2020-04-24 20:42:27', '41', NULL, '3', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1081', '7', '29', '1081.1.11_shoping', '10241', '2020-04-24 20:47:43', '41', '2020-04-24 20:47:39', '41', NULL, '11', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1082', '1028', '2', 'for-page-1081', '17', '2020-04-24 20:47:39', '41', '2020-04-24 20:47:39', '41', '2020-04-25 00:47:39', '14', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1083', '1076', '51', 'lightwind-plan-b', '1', '2021-02-01 07:52:06', '41', '2020-04-24 20:53:07', '41', '2020-04-25 00:53:12', '0', 'leichtwind-plan-b', '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1085', '1027', '2', 'for-field-152', '17', '2020-04-28 04:19:31', '41', '2020-04-28 04:19:31', '41', '2020-04-28 08:19:31', '3', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1086', '1027', '2', 'for-field-153', '17', '2020-04-28 04:21:54', '41', '2020-04-28 04:21:54', '41', '2020-04-28 08:21:54', '4', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1087', '1086', '2', 'for-page-1077', '17', '2020-04-28 04:22:45', '41', '2020-04-28 04:22:45', '41', '2020-04-28 08:22:45', '0', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1088', '1087', '53', '1588051372-1268-1', '1', '2020-06-13 15:34:31', '41', '2020-04-28 04:22:52', '41', '2020-04-28 08:25:05', '0', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1089', '1085', '2', 'for-page-1088', '17', '2020-04-28 04:22:52', '41', '2020-04-28 04:22:52', '41', '2020-04-28 08:22:52', '0', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1090', '1089', '52', '1588051375-5055-1', '1', '2020-06-13 15:33:41', '41', '2020-04-28 04:22:55', '41', '2020-04-28 08:25:05', '0', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1091', '1089', '52', '1588051493-3907-1', '1', '2020-06-13 15:33:41', '41', '2020-04-28 04:24:53', '41', '2020-04-28 08:25:05', '1', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1092', '1089', '52', '1588051498-7915-1', '1', '2020-06-13 15:33:41', '41', '2020-04-28 04:24:58', '41', '2020-04-28 08:25:05', '2', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1133', '1115', '53', '1592069847-7351-1', '1', '2021-02-01 07:51:10', '41', '2020-06-13 15:37:27', '41', '2020-06-13 17:38:37', '0', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1142', '1076', '51', 'special-edition', '1', '2021-02-01 07:51:47', '41', '2020-06-13 15:39:35', '41', '2020-06-13 17:40:51', '2', 'spezial-edition', '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1143', '1086', '2', 'for-page-1142', '17', '2020-06-13 15:39:35', '41', '2020-06-13 15:39:35', '41', '2020-06-13 17:39:35', '5', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1101', '1086', '2', 'for-page-1076', '17', '2020-05-07 03:49:50', '41', '2020-05-07 03:49:50', '41', '2020-05-07 07:49:50', '1', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1102', '1101', '53', '1588826993-8588-1', '1', '2020-06-13 15:29:14', '41', '2020-05-07 03:49:53', '41', '2020-05-07 07:50:35', '0', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1103', '1085', '2', 'for-page-1102', '17', '2020-05-07 03:49:53', '41', '2020-05-07 03:49:53', '41', '2020-05-07 07:49:53', '3', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1104', '1103', '52', '1588827003-58-1', '1', '2020-06-13 15:28:57', '41', '2020-05-07 03:50:03', '41', '2020-05-07 07:50:35', '0', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1105', '1103', '52', '1588827028-1931-1', '1', '2020-06-13 15:28:57', '41', '2020-05-07 03:50:28', '41', '2020-05-07 07:50:35', '1', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1124', '1089', '52', '1592069584-5722-1', '1', '2020-06-13 15:34:31', '41', '2020-06-13 15:33:04', '41', '2020-06-13 17:33:41', '3', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1125', '1089', '52', '1592069597-7585-1', '1', '2020-06-13 15:34:31', '41', '2020-06-13 15:33:17', '41', '2020-06-13 17:33:41', '4', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1126', '1089', '52', '1592069609-2397-1', '1', '2020-06-13 15:34:31', '41', '2020-06-13 15:33:29', '41', '2020-06-13 17:33:41', '5', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1110', '1020', '50', 'skimboards', '1', '2020-05-27 05:01:46', '41', '2020-05-26 11:22:12', '41', '2020-05-26 13:22:36', '1', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1111', '1086', '2', 'for-page-1110', '17', '2020-05-26 11:22:12', '41', '2020-05-26 11:22:12', '41', '2020-05-26 13:22:12', '2', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1112', '1020', '50', 'waveboards', '1', '2020-05-27 05:01:48', '41', '2020-05-26 11:35:57', '41', '2020-05-26 13:37:01', '2', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1113', '1086', '2', 'for-page-1112', '17', '2020-05-26 11:35:57', '41', '2020-05-26 11:35:57', '41', '2020-05-26 13:35:57', '3', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1114', '1032', '45', '1590504161-5765-1', '1', '2020-05-27 04:52:21', '41', '2020-05-26 12:42:41', '41', '2020-05-26 14:42:58', '4', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1115', '1086', '2', 'for-page-1083', '17', '2020-05-27 06:14:41', '41', '2020-05-27 06:14:41', '41', '2020-05-27 08:14:41', '4', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1116', '1', '29', 'impressum', '1025', '2021-03-07 10:24:18', '41', '2020-06-06 13:46:23', '41', '2020-06-06 15:46:32', '10', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1117', '1028', '2', 'for-page-1116', '17', '2020-06-06 13:46:23', '41', '2020-06-06 13:46:23', '41', '2020-06-06 15:46:23', '15', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1118', '1028', '2', 'for-page-1026', '17', '2020-06-06 13:56:17', '41', '2020-06-06 13:56:17', '41', '2020-06-06 15:56:17', '16', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1119', '1028', '2', 'for-page-1024', '17', '2020-06-06 14:17:46', '41', '2020-06-06 14:17:46', '41', '2020-06-06 16:17:46', '17', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1120', '1119', '44', '1591460326-9931-1', '1', '2020-06-06 14:25:26', '41', '2020-06-06 14:18:47', '41', '2020-06-06 16:19:23', '0', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1121', '1119', '44', '1591460597-8222-1', '1', '2020-06-06 14:25:41', '41', '2020-06-06 14:23:17', '41', '2020-06-06 16:23:43', '1', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1123', '1103', '52', '1592069328-0751-1', '1', '2020-06-13 15:29:14', '41', '2020-06-13 15:28:48', '41', '2020-06-13 17:28:57', '3', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1134', '1085', '2', 'for-page-1133', '17', '2020-06-13 15:37:27', '41', '2020-06-13 15:37:27', '41', '2020-06-13 17:37:27', '3', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1135', '1134', '52', '1592069852-034-1', '1', '2020-06-13 15:38:37', '41', '2020-06-13 15:37:32', '41', '2020-06-13 17:38:37', '2', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1136', '1134', '52', '1592069861-8238-1', '1', '2020-06-13 15:38:37', '41', '2020-06-13 15:37:41', '41', '2020-06-13 17:38:37', '3', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1137', '1134', '52', '1592069874-1114-1', '1', '2020-06-13 15:38:37', '41', '2020-06-13 15:37:54', '41', '2020-06-13 17:38:37', '4', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1138', '1134', '52', '1592069884-5484-1', '1', '2020-06-13 15:38:37', '41', '2020-06-13 15:38:04', '41', '2020-06-13 17:38:37', '5', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1139', '1134', '52', '1592069891-3994-1', '1', '2020-06-13 15:38:37', '41', '2020-06-13 15:38:11', '41', '2020-06-13 17:38:37', '6', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1140', '1134', '52', '1592069898-578-1', '1', '2020-06-13 15:38:37', '41', '2020-06-13 15:38:18', '41', '2020-06-13 17:38:37', '7', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1141', '1134', '52', '1592069909-5548-1', '1', '2020-06-13 15:38:37', '41', '2020-06-13 15:38:29', '41', '2020-06-13 17:38:37', '8', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1144', '1076', '51', 'freerider-explorer', '2177', '2021-02-01 07:49:04', '41', '2021-02-01 07:48:16', '41', NULL, '3', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1145', '1086', '2', 'for-page-1144', '17', '2021-02-01 07:48:16', '41', '2021-02-01 07:48:16', '41', '2021-02-01 07:48:16', '6', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1146', '1076', '51', 'chrome-edition', '2177', '2021-02-01 07:52:37', '41', '2021-02-01 07:52:30', '41', NULL, '4', 'chrom-edition', '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1147', '1086', '2', 'for-page-1146', '17', '2021-02-01 07:52:30', '41', '2021-02-01 07:52:30', '41', '2021-02-01 07:52:30', '7', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1148', '1076', '51', 'big-air-challenger', '2177', '2021-02-01 07:52:57', '41', '2021-02-01 07:52:54', '41', NULL, '5', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1149', '1086', '2', 'for-page-1148', '17', '2021-02-01 07:52:54', '41', '2021-02-01 07:52:54', '41', '2021-02-01 07:52:54', '8', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1150', '1076', '51', 'freestyler', '2177', '2021-02-01 07:53:16', '41', '2021-02-01 07:53:14', '41', NULL, '6', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1151', '1086', '2', 'for-page-1150', '17', '2021-02-01 07:53:14', '41', '2021-02-01 07:53:14', '41', '2021-02-01 07:53:14', '9', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1152', '1', '50', 'twintips', '1', '2021-03-07 14:45:11', '41', '2021-03-07 10:20:38', '41', '2021-03-07 10:20:44', '0', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1153', '1086', '2', 'for-page-1152', '17', '2021-03-07 10:20:38', '41', '2021-03-07 10:20:38', '41', '2021-03-07 10:20:38', '10', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1154', '1', '29', 'customs', '2049', '2021-03-07 11:54:04', '41', '2021-03-07 10:23:20', '41', '2021-03-07 10:23:22', '1', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1195', '1194', '44', '1615117253-3058-1', '1', '2021-03-07 11:42:59', '41', '2021-03-07 11:40:53', '41', '2021-03-07 11:41:07', '0', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1156', '1', '48', 'shop', '1', '2021-03-07 10:37:58', '41', '2021-03-07 10:23:51', '41', '2021-03-07 10:23:57', '2', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1177', '1156', '50', 'stock-boards', '1', '2021-03-07 10:37:22', '41', '2021-03-07 10:36:32', '41', '2021-03-07 10:36:36', '0', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1158', '1152', '51', 'freerider', '1', '2021-03-07 14:38:09', '41', '2021-03-07 10:25:41', '41', '2021-03-07 10:27:38', '1', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1159', '1086', '2', 'for-page-1158', '17', '2021-03-07 10:25:41', '41', '2021-03-07 10:25:41', '41', '2021-03-07 10:25:41', '13', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1160', '1152', '51', 'freerider-lady', '1', '2021-03-07 12:11:25', '41', '2021-03-07 10:28:44', '41', '2021-03-07 10:29:04', '2', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1161', '1086', '2', 'for-page-1160', '17', '2021-03-07 10:28:44', '41', '2021-03-07 10:28:44', '41', '2021-03-07 10:28:44', '14', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1162', '1152', '51', 'lightwind-plan-b', '1', '2021-03-07 12:13:17', '41', '2021-03-07 10:30:04', '41', '2021-03-07 10:30:13', '3', 'leichtwind-plan-b', '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1163', '1086', '2', 'for-page-1162', '17', '2021-03-07 10:30:04', '41', '2021-03-07 10:30:04', '41', '2021-03-07 10:30:04', '15', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1164', '1152', '51', 'special-edition', '1', '2021-03-07 11:44:09', '41', '2021-03-07 10:30:40', '41', '2021-03-07 10:30:49', '4', 'spezial-edition', '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1165', '1086', '2', 'for-page-1164', '17', '2021-03-07 10:30:40', '41', '2021-03-07 10:30:40', '41', '2021-03-07 10:30:40', '16', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1166', '1152', '51', 'chrome-edition', '1', '2021-03-07 11:43:48', '41', '2021-03-07 10:31:10', '41', '2021-03-07 10:31:19', '5', 'chrom-edition', '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1167', '1086', '2', 'for-page-1166', '17', '2021-03-07 10:31:10', '41', '2021-03-07 10:31:10', '41', '2021-03-07 10:31:10', '17', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1168', '1152', '51', 'big-air-challenger', '2049', '2021-03-07 11:38:42', '41', '2021-03-07 10:31:49', '41', '2021-03-07 10:32:17', '6', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1169', '1086', '2', 'for-page-1168', '17', '2021-03-07 10:31:49', '41', '2021-03-07 10:31:49', '41', '2021-03-07 10:31:49', '18', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1170', '1152', '51', 'freestyler', '2049', '2021-03-07 11:38:45', '41', '2021-03-07 10:32:40', '41', '2021-03-07 10:32:49', '7', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1171', '1086', '2', 'for-page-1170', '17', '2021-03-07 10:32:40', '41', '2021-03-07 10:32:40', '41', '2021-03-07 10:32:40', '19', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1172', '7', '51', '1172.1154.0_waveboard', '8193', '2021-03-07 11:45:11', '41', '2021-03-07 10:33:34', '41', '2021-03-07 10:33:42', '0', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1173', '1086', '2', 'for-page-1172', '17', '2021-03-07 10:33:34', '41', '2021-03-07 10:33:34', '41', '2021-03-07 10:33:34', '20', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1174', '7', '51', '1174.1154.1_skimboard', '8193', '2021-03-07 11:45:12', '41', '2021-03-07 10:33:53', '41', '2021-03-07 10:34:02', '1', '1174.1154.1_skimboard', '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1175', '1086', '2', 'for-page-1174', '17', '2021-03-07 10:33:53', '41', '2021-03-07 10:33:53', '41', '2021-03-07 10:33:53', '21', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1176', '1028', '2', 'for-page-1156', '17', '2021-03-07 10:36:01', '41', '2021-03-07 10:36:01', '41', '2021-03-07 10:36:01', '18', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1182', '1177', '51', 'freestyler', '1', '2021-03-07 10:37:45', '41', '2021-03-07 10:37:36', '41', '2021-03-07 10:37:45', '0', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1179', '7', '50', '1179.1177.0_muster-stock-board', '10241', '2021-03-07 10:37:14', '41', '2021-03-07 10:37:00', '41', NULL, '0', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1180', '1086', '2', 'for-page-1179', '17', '2021-03-07 10:37:00', '41', '2021-03-07 10:37:00', '41', '2021-03-07 10:37:00', '21', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1181', '1086', '2', 'for-page-1177', '17', '2021-03-07 10:37:22', '41', '2021-03-07 10:37:22', '41', '2021-03-07 10:37:22', '22', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1183', '1086', '2', 'for-page-1182', '17', '2021-03-07 10:37:36', '41', '2021-03-07 10:37:36', '41', '2021-03-07 10:37:36', '23', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1184', '1156', '50', 'equipment', '2049', '2021-03-07 11:08:34', '41', '2021-03-07 10:38:34', '41', '2021-03-07 10:38:36', '1', 'zubehor', '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1185', '1086', '2', 'for-page-1184', '17', '2021-03-07 10:38:34', '41', '2021-03-07 10:38:34', '41', '2021-03-07 10:38:34', '24', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1186', '1184', '51', 'straps', '1', '2021-03-07 10:39:03', '41', '2021-03-07 10:38:45', '41', '2021-03-07 10:39:03', '0', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1187', '1086', '2', 'for-page-1186', '17', '2021-03-07 10:38:45', '41', '2021-03-07 10:38:45', '41', '2021-03-07 10:38:45', '25', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1188', '1156', '50', 'promotion', '1', '2021-03-07 10:39:36', '41', '2021-03-07 10:39:25', '41', '2021-03-07 10:39:36', '2', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1189', '1086', '2', 'for-page-1188', '17', '2021-03-07 10:39:25', '41', '2021-03-07 10:39:25', '41', '2021-03-07 10:39:25', '26', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1190', '1', '29', 'testpoint', '1', '2021-03-07 10:52:44', '41', '2021-03-07 10:45:13', '41', '2021-03-07 10:45:16', '4', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1191', '1028', '2', 'for-page-1190', '17', '2021-03-07 10:45:13', '41', '2021-03-07 10:45:13', '41', '2021-03-07 10:45:13', '19', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1192', '1152', '55', 'gallery', '1', '2021-03-07 11:07:39', '41', '2021-03-07 10:55:59', '41', '2021-03-07 10:56:12', '0', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1194', '1028', '2', 'for-page-1154', '17', '2021-03-07 11:40:29', '41', '2021-03-07 11:40:29', '41', '2021-03-07 11:40:29', '20', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1196', '1194', '44', '1615117363-2913-1', '1', '2021-03-07 11:49:22', '41', '2021-03-07 11:42:43', '41', '2021-03-07 11:42:59', '1', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1216', '1085', '2', 'for-page-1215', '17', '2021-03-07 12:10:21', '41', '2021-03-07 12:10:21', '41', '2021-03-07 12:10:21', '5', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1217', '1216', '52', '1615119028-0927-1', '1', '2021-03-07 12:11:25', '41', '2021-03-07 12:10:28', '41', '2021-03-07 12:11:25', '2', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1200', '1159', '53', '1615118283-1115-1', '1', '2021-03-07 14:38:09', '41', '2021-03-07 11:58:03', '41', '2021-03-07 11:58:34', '0', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1201', '1085', '2', 'for-page-1200', '17', '2021-03-07 11:58:03', '41', '2021-03-07 11:58:03', '41', '2021-03-07 11:58:03', '4', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1202', '1201', '52', '1615118290-575-1', '1', '2021-03-07 12:00:02', '41', '2021-03-07 11:58:10', '41', '2021-03-07 11:58:34', '0', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1203', '1201', '52', '1615118307-3685-1c', '3073', '2021-03-07 13:58:27', '41', '2021-03-07 13:58:27', '41', NULL, '2', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1210', '1159', '53', '1615118649-951-1', '1', '2021-03-07 14:38:09', '41', '2021-03-07 12:04:09', '41', '2021-03-07 12:05:20', '1', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1204', '1201', '52', '1615118345-7731-1c', '1', '2021-03-07 12:01:13', '41', '2021-03-07 13:59:05', '41', '2021-03-07 12:00:02', '2', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1205', '1201', '52', '1615118358-2323-1c', '1', '2021-03-07 12:01:13', '41', '2021-03-07 13:59:18', '41', '2021-03-07 12:00:02', '3', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1206', '1201', '52', '1615118369-9061-1', '1', '2021-03-07 12:01:13', '41', '2021-03-07 11:59:29', '41', '2021-03-07 12:00:02', '4', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1207', '1201', '52', '1615118378-0127-1', '1', '2021-03-07 12:01:13', '41', '2021-03-07 11:59:38', '41', '2021-03-07 12:00:02', '5', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1209', '1201', '52', '1615118482-8039-1', '1', '2021-03-07 14:38:09', '41', '2021-03-07 12:01:22', '41', '2021-03-07 12:01:33', '6', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1211', '1085', '2', 'for-page-1210', '17', '2021-03-07 12:04:09', '41', '2021-03-07 12:04:09', '41', '2021-03-07 12:04:09', '5', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1212', '1211', '52', '1615118653-3658-1', '1', '2021-03-07 12:06:44', '41', '2021-03-07 12:04:13', '41', '2021-03-07 12:05:20', '0', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1213', '1211', '52', '1615118683-7655-1', '1', '2021-03-07 14:38:09', '41', '2021-03-07 12:04:43', '41', '2021-03-07 12:05:20', '1', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1214', '1211', '52', '1615118698-535-1', '1', '2021-03-07 12:08:02', '41', '2021-03-07 12:04:58', '41', '2021-03-07 12:05:20', '2', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1215', '1161', '53', '1615119021-8129-1', '1', '2021-03-07 12:11:25', '41', '2021-03-07 12:10:21', '41', '2021-03-07 12:11:25', '2', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1218', '1216', '52', '1615119036-8924-1', '1', '2021-03-07 12:11:25', '41', '2021-03-07 12:10:36', '41', '2021-03-07 12:11:25', '3', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1219', '1216', '52', '1615119061-6015-1', '1', '2021-03-07 12:11:25', '41', '2021-03-07 12:11:01', '41', '2021-03-07 12:11:25', '4', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1220', '1163', '53', '1615119135-792-1', '1', '2021-03-07 12:13:17', '41', '2021-03-07 12:12:15', '41', '2021-03-07 12:13:17', '2', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1221', '1085', '2', 'for-page-1220', '17', '2021-03-07 12:12:15', '41', '2021-03-07 12:12:15', '41', '2021-03-07 12:12:15', '6', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1222', '1221', '52', '1615119143-7236-1', '1', '2021-03-07 12:13:17', '41', '2021-03-07 12:12:23', '41', '2021-03-07 12:13:17', '2', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1223', '1163', '53', '1615119154-0203-1', '1', '2021-03-07 12:13:17', '41', '2021-03-07 12:12:34', '41', '2021-03-07 12:13:17', '3', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1224', '1085', '2', 'for-page-1223', '17', '2021-03-07 12:12:34', '41', '2021-03-07 12:12:34', '41', '2021-03-07 12:12:34', '7', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1225', '1224', '52', '1615119156-7904-1', '1', '2021-03-07 12:13:17', '41', '2021-03-07 12:12:36', '41', '2021-03-07 12:13:17', '2', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1226', '1224', '52', '1615119177-7176-1', '1', '2021-03-07 12:13:17', '41', '2021-03-07 12:12:57', '41', '2021-03-07 12:13:17', '3', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1227', '1027', '2', 'for-field-158', '17', '2021-03-07 12:23:39', '41', '2021-03-07 12:23:39', '41', '2021-03-07 12:23:39', '5', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1228', '1227', '2', 'for-page-1', '17', '2021-03-07 12:24:44', '41', '2021-03-07 12:24:44', '41', '2021-03-07 12:24:44', '0', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1229', '1228', '56', '1615119890-5588-1', '1', '2021-03-07 13:01:43', '41', '2021-03-07 12:24:50', '41', '2021-03-07 12:25:23', '0', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1230', '1228', '56', '1615120300-1529-1', '1', '2021-03-07 13:00:34', '41', '2021-03-07 12:31:40', '41', '2021-03-07 12:32:25', '1', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1231', '1228', '56', '1615121130-066-1', '1', '2021-03-07 13:06:27', '41', '2021-03-07 12:45:30', '41', '2021-03-07 12:45:39', '2', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1253', '1252', '58', '1615123764-5698-1', '1', '2021-03-07 13:34:50', '41', '2021-03-07 13:29:24', '41', '2021-03-07 13:29:43', '0', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1234', '1028', '2', 'for-page-1002', '17', '2021-03-07 13:02:15', '41', '2021-03-07 13:02:15', '41', '2021-03-07 13:02:15', '21', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1251', '1244', '57', '1615123757-6252-1', '1', '2021-03-07 14:45:11', '41', '2021-03-07 13:29:17', '41', '2021-03-07 13:29:43', '0', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1252', '1243', '2', 'for-page-1251', '17', '2021-03-07 13:29:17', '41', '2021-03-07 13:29:17', '41', '2021-03-07 13:29:17', '0', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1255', '1252', '58', '1615124045-4354-1', '1', '2021-03-07 14:41:52', '41', '2021-03-07 13:34:05', '41', '2021-03-07 13:34:50', '2', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1242', '1027', '2', 'for-field-160', '17', '2021-03-07 13:22:56', '41', '2021-03-07 13:22:56', '41', '2021-03-07 13:22:56', '6', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1256', '1252', '58', '1615124060-898-1', '1', '2021-03-07 14:41:52', '41', '2021-03-07 13:34:20', '41', '2021-03-07 13:34:50', '3', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1243', '1027', '2', 'for-field-161', '17', '2021-03-07 13:24:16', '41', '2021-03-07 13:24:16', '41', '2021-03-07 13:24:16', '7', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1244', '1242', '2', 'for-page-1152', '17', '2021-03-07 13:28:33', '41', '2021-03-07 13:28:33', '41', '2021-03-07 13:28:33', '0', NULL, '0');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1254', '1252', '58', '1615123772-1523-1', '1', '2021-03-07 14:39:39', '41', '2021-03-07 13:29:32', '41', '2021-03-07 13:29:43', '1', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1257', '1252', '58', '1615124073-5257-1', '1', '2021-03-07 14:41:52', '41', '2021-03-07 13:34:33', '41', '2021-03-07 13:34:50', '4', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1258', '1252', '58', '1615128122-0414-1', '1', '2021-03-07 14:45:11', '41', '2021-03-07 14:42:02', '41', '2021-03-07 14:45:11', '7', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1259', '1252', '58', '1615128146-7475-1', '1', '2021-03-07 14:45:11', '41', '2021-03-07 14:42:26', '41', '2021-03-07 14:45:11', '8', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1260', '1252', '58', '1615128172-4389-1', '1', '2021-03-07 14:45:11', '41', '2021-03-07 14:42:52', '41', '2021-03-07 14:45:11', '9', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1261', '1252', '58', '1615128199-3437-1', '1', '2021-03-07 14:45:11', '41', '2021-03-07 14:43:19', '41', '2021-03-07 14:45:11', '10', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1262', '1252', '58', '1615128230-1215-1', '1', '2021-03-07 14:45:11', '41', '2021-03-07 14:43:50', '41', '2021-03-07 14:45:11', '11', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1263', '1252', '58', '1615128263-5436-1', '1', '2021-03-07 14:45:11', '41', '2021-03-07 14:44:23', '41', '2021-03-07 14:45:11', '12', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1012`, `status1012`) VALUES('1264', '1252', '58', '1615128289-2231-1', '1', '2021-03-07 14:45:11', '41', '2021-03-07 14:44:49', '41', '2021-03-07 14:45:11', '13', NULL, '1');

DROP TABLE IF EXISTS `pages_access`;
CREATE TABLE `pages_access` (
  `pages_id` int NOT NULL,
  `templates_id` int NOT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`pages_id`),
  KEY `templates_id` (`templates_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('37', '2', '2020-02-05 07:51:13');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('38', '2', '2020-02-05 07:51:13');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('32', '2', '2020-02-05 07:51:13');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('34', '2', '2020-02-05 07:51:13');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('35', '2', '2020-02-05 07:51:13');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('36', '2', '2020-02-05 07:51:13');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('50', '2', '2020-02-05 07:51:13');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('51', '2', '2020-02-05 07:51:13');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('52', '2', '2020-02-05 07:51:13');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('53', '2', '2020-02-05 07:51:13');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('54', '2', '2020-02-05 07:51:13');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1006', '2', '2020-02-05 07:51:13');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1010', '2', '2020-02-05 07:51:13');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1012', '2', '2020-02-05 07:51:13');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1122', '2', '2020-06-13 15:28:41');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1016', '2', '2020-02-05 07:51:29');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1018', '2', '2020-02-05 07:51:35');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1019', '2', '2020-02-05 07:51:35');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1020', '1', '2020-04-15 07:31:08');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1021', '2', '2020-04-24 22:02:32');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1068', '2', '2020-04-22 10:50:40');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1065', '2', '2020-04-24 22:02:32');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1024', '1', '2020-04-15 07:33:35');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1025', '1', '2020-04-15 07:33:54');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1026', '1', '2020-04-15 07:34:51');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1030', '2', '2020-04-20 05:52:34');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1035', '2', '2020-04-20 06:07:53');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1036', '2', '2020-04-20 06:08:07');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1037', '2', '2020-04-20 06:08:18');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1038', '2', '2020-04-20 06:08:24');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1039', '2', '2020-04-20 06:08:59');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1040', '2', '2020-04-20 06:09:14');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1041', '2', '2020-04-20 06:09:19');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1042', '2', '2020-04-20 06:09:24');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1045', '2', '2020-04-20 07:03:53');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1046', '2', '2020-04-20 07:13:06');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1047', '2', '2020-04-20 07:13:08');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1049', '2', '2020-04-20 07:47:22');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1050', '2', '2020-04-24 20:47:50');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1071', '2', '2020-04-22 16:28:37');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1063', '2', '2020-04-24 22:02:32');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1070', '2', '2020-04-22 16:26:41');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1074', '2', '2020-04-24 20:03:58');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1075', '2', '2020-04-24 20:03:58');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1078', '2', '2020-04-24 20:50:07');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1083', '1', '2020-04-24 20:53:07');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1080', '2', '2020-04-24 20:50:15');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1079', '2', '2020-04-24 20:50:11');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1081', '2', '2020-04-24 20:47:43');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1052', '2', '2020-04-24 22:02:32');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1058', '2', '2020-04-24 22:02:32');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1060', '2', '2020-04-24 22:02:32');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1022', '2', '2020-04-24 22:02:32');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1023', '2', '2020-04-24 22:02:32');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1054', '2', '2020-04-24 22:02:32');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1088', '2', '2020-04-28 04:22:52');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1090', '2', '2020-04-28 04:22:55');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1091', '2', '2020-04-28 04:24:53');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1092', '2', '2020-04-28 04:24:58');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1133', '2', '2020-06-13 15:37:27');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1142', '1', '2020-06-13 15:39:35');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1102', '2', '2020-05-07 03:49:53');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1104', '2', '2020-05-07 03:50:03');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1105', '2', '2020-05-07 03:50:28');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1124', '2', '2020-06-13 15:33:04');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1126', '2', '2020-06-13 15:33:29');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1125', '2', '2020-06-13 15:33:17');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1110', '1', '2020-05-26 11:22:13');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1112', '1', '2020-05-26 11:35:57');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1114', '2', '2020-05-26 12:42:41');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1116', '1', '2020-06-06 13:46:23');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1120', '2', '2020-06-06 14:18:47');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1121', '2', '2020-06-06 14:23:17');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1123', '2', '2020-06-13 15:28:48');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1135', '2', '2020-06-13 15:37:32');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1136', '2', '2020-06-13 15:37:41');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1137', '2', '2020-06-13 15:37:54');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1138', '2', '2020-06-13 15:38:04');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1139', '2', '2020-06-13 15:38:11');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1140', '2', '2020-06-13 15:38:18');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1141', '2', '2020-06-13 15:38:29');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1144', '1', '2021-02-01 07:48:16');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1146', '1', '2021-02-01 07:52:30');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1148', '1', '2021-02-01 07:52:55');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1150', '1', '2021-02-01 07:53:14');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1152', '1', '2021-03-07 10:20:38');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1154', '1', '2021-03-07 10:23:20');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1156', '1', '2021-03-07 10:23:51');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1158', '1', '2021-03-07 10:25:41');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1160', '1', '2021-03-07 10:28:44');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1162', '1', '2021-03-07 10:30:04');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1164', '1', '2021-03-07 10:30:40');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1166', '1', '2021-03-07 10:31:10');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1168', '1', '2021-03-07 10:31:49');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1170', '1', '2021-03-07 10:32:40');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1196', '2', '2021-03-07 11:42:43');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1195', '2', '2021-03-07 11:40:53');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1177', '1', '2021-03-07 10:36:32');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1179', '2', '2021-03-07 10:37:14');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1182', '1', '2021-03-07 10:37:36');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1184', '1', '2021-03-07 10:38:34');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1186', '1', '2021-03-07 10:38:45');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1188', '1', '2021-03-07 10:39:25');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1190', '1', '2021-03-07 10:45:13');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1192', '1', '2021-03-07 10:55:59');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1172', '2', '2021-03-07 11:45:11');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1174', '2', '2021-03-07 11:45:12');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1215', '2', '2021-03-07 12:10:21');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1217', '2', '2021-03-07 12:10:28');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1200', '2', '2021-03-07 11:58:03');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1202', '2', '2021-03-07 11:58:10');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1203', '2', '2021-03-07 11:58:27');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1204', '2', '2021-03-07 11:59:05');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1205', '2', '2021-03-07 11:59:18');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1206', '2', '2021-03-07 11:59:29');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1207', '2', '2021-03-07 11:59:38');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1209', '2', '2021-03-07 12:01:22');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1210', '2', '2021-03-07 12:04:09');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1212', '2', '2021-03-07 12:04:13');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1213', '2', '2021-03-07 12:04:43');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1214', '2', '2021-03-07 12:04:58');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1218', '2', '2021-03-07 12:10:36');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1219', '2', '2021-03-07 12:11:01');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1220', '2', '2021-03-07 12:12:15');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1222', '2', '2021-03-07 12:12:23');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1223', '2', '2021-03-07 12:12:34');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1225', '2', '2021-03-07 12:12:36');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1226', '2', '2021-03-07 12:12:57');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1229', '2', '2021-03-07 12:24:50');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1230', '2', '2021-03-07 12:31:40');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1231', '2', '2021-03-07 12:45:30');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1256', '2', '2021-03-07 13:34:20');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1251', '2', '2021-03-07 13:29:17');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1254', '2', '2021-03-07 13:29:32');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1255', '2', '2021-03-07 13:34:05');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1253', '2', '2021-03-07 13:29:24');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1257', '2', '2021-03-07 13:34:33');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1258', '2', '2021-03-07 14:42:02');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1259', '2', '2021-03-07 14:42:26');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1260', '2', '2021-03-07 14:42:52');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1261', '2', '2021-03-07 14:43:19');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1262', '2', '2021-03-07 14:43:50');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1263', '2', '2021-03-07 14:44:23');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1264', '2', '2021-03-07 14:44:49');

DROP TABLE IF EXISTS `pages_parents`;
CREATE TABLE `pages_parents` (
  `pages_id` int unsigned NOT NULL,
  `parents_id` int unsigned NOT NULL,
  PRIMARY KEY (`pages_id`,`parents_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('3', '2');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('22', '2');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('28', '2');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('29', '2');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('29', '28');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('30', '2');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('30', '28');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('31', '2');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('31', '28');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1009', '2');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1009', '22');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1021', '7');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1022', '7');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1022', '1021');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1023', '7');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1023', '1021');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1027', '2');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1028', '2');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1028', '1027');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1029', '2');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1029', '1027');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1029', '1028');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1031', '2');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1031', '1027');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1032', '2');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1032', '1027');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1032', '1031');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1033', '2');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1033', '1027');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1034', '2');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1034', '1027');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1034', '1033');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1044', '2');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1044', '1027');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1044', '1028');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1054', '7');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1054', '1021');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1062', '2');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1062', '1027');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1062', '1028');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1073', '2');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1073', '22');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1076', '1020');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1085', '2');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1085', '1027');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1086', '2');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1086', '1027');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1087', '2');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1087', '1027');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1087', '1086');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1089', '2');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1089', '1027');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1089', '1085');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1101', '2');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1101', '1027');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1101', '1086');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1103', '2');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1103', '1027');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1103', '1085');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1115', '2');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1115', '1027');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1115', '1086');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1119', '2');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1119', '1027');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1119', '1028');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1134', '2');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1134', '1027');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1134', '1085');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1153', '2');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1153', '1027');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1153', '1086');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1159', '2');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1159', '1027');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1159', '1086');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1161', '2');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1161', '1027');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1161', '1086');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1163', '2');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1163', '1027');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1163', '1086');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1177', '1156');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1184', '1156');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1194', '2');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1194', '1027');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1194', '1028');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1201', '2');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1201', '1027');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1201', '1085');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1211', '2');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1211', '1027');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1211', '1085');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1216', '2');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1216', '1027');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1216', '1085');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1221', '2');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1221', '1027');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1221', '1085');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1224', '2');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1224', '1027');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1224', '1085');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1227', '2');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1227', '1027');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1228', '2');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1228', '1027');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1228', '1227');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1242', '2');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1242', '1027');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1243', '2');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1243', '1027');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1244', '2');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1244', '1027');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1244', '1242');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1252', '2');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1252', '1027');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1252', '1243');

DROP TABLE IF EXISTS `pages_sortfields`;
CREATE TABLE `pages_sortfields` (
  `pages_id` int unsigned NOT NULL DEFAULT '0',
  `sortfield` varchar(20) NOT NULL DEFAULT '',
  PRIMARY KEY (`pages_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


DROP TABLE IF EXISTS `session_login_throttle`;
CREATE TABLE `session_login_throttle` (
  `name` varchar(128) NOT NULL,
  `attempts` int unsigned NOT NULL DEFAULT '0',
  `last_attempt` int unsigned NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `session_login_throttle` (`name`, `attempts`, `last_attempt`) VALUES('admin', '1', '1615112353');

DROP TABLE IF EXISTS `templates`;
CREATE TABLE `templates` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(250) CHARACTER SET ascii COLLATE ascii_general_ci NOT NULL,
  `fieldgroups_id` int unsigned NOT NULL DEFAULT '0',
  `flags` int NOT NULL DEFAULT '0',
  `cache_time` mediumint NOT NULL DEFAULT '0',
  `data` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `fieldgroups_id` (`fieldgroups_id`)
) ENGINE=MyISAM AUTO_INCREMENT=59 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `templates` (`id`, `name`, `fieldgroups_id`, `flags`, `cache_time`, `data`) VALUES('2', 'admin', '2', '8', '0', '{\"useRoles\":1,\"parentTemplates\":[2],\"allowPageNum\":1,\"redirectLogin\":23,\"slashUrls\":1,\"noGlobal\":1,\"compile\":3,\"modified\":1611219107,\"ns\":\"ProcessWire\"}');
INSERT INTO `templates` (`id`, `name`, `fieldgroups_id`, `flags`, `cache_time`, `data`) VALUES('3', 'user', '3', '8', '0', '{\"useRoles\":1,\"noChildren\":1,\"parentTemplates\":[2],\"slashUrls\":1,\"pageClass\":\"User\",\"noGlobal\":1,\"noMove\":1,\"noTrash\":1,\"noSettings\":1,\"noChangeTemplate\":1,\"nameContentTab\":1}');
INSERT INTO `templates` (`id`, `name`, `fieldgroups_id`, `flags`, `cache_time`, `data`) VALUES('4', 'role', '4', '8', '0', '{\"noChildren\":1,\"parentTemplates\":[2],\"slashUrls\":1,\"pageClass\":\"Role\",\"noGlobal\":1,\"noMove\":1,\"noTrash\":1,\"noSettings\":1,\"noChangeTemplate\":1,\"nameContentTab\":1}');
INSERT INTO `templates` (`id`, `name`, `fieldgroups_id`, `flags`, `cache_time`, `data`) VALUES('5', 'permission', '5', '8', '0', '{\"noChildren\":1,\"parentTemplates\":[2],\"slashUrls\":1,\"guestSearchable\":1,\"pageClass\":\"Permission\",\"noGlobal\":1,\"noMove\":1,\"noTrash\":1,\"noSettings\":1,\"noChangeTemplate\":1,\"nameContentTab\":1}');
INSERT INTO `templates` (`id`, `name`, `fieldgroups_id`, `flags`, `cache_time`, `data`) VALUES('1', 'home', '1', '0', '0', '{\"useRoles\":1,\"noParents\":1,\"slashUrls\":1,\"compile\":3,\"label\":\"Home\",\"modified\":1615121588,\"ns\":\"ProcessWire\",\"label1012\":\"Zuhause\",\"roles\":[37]}');
INSERT INTO `templates` (`id`, `name`, `fieldgroups_id`, `flags`, `cache_time`, `data`) VALUES('29', 'basic-page', '83', '0', '0', '{\"slashUrls\":1,\"compile\":3,\"label\":\"Basic Page\",\"modified\":1615117815,\"ns\":\"ProcessWire\",\"label1012\":\"Grund Seite\"}');
INSERT INTO `templates` (`id`, `name`, `fieldgroups_id`, `flags`, `cache_time`, `data`) VALUES('26', 'search', '80', '0', '0', '{\"noChildren\":1,\"noParents\":1,\"allowPageNum\":1,\"slashUrls\":1,\"compile\":3,\"label\":\"Search\",\"modified\":1611219107,\"ns\":\"ProcessWire\",\"label1012\":\"Suche\"}');
INSERT INTO `templates` (`id`, `name`, `fieldgroups_id`, `flags`, `cache_time`, `data`) VALUES('34', 'sitemap', '88', '0', '0', '{\"noChildren\":1,\"noParents\":1,\"redirectLogin\":23,\"slashUrls\":1,\"compile\":3,\"label\":\"Site Map\",\"modified\":1611219107,\"ns\":\"ProcessWire\",\"label1012\":\"Sitemap\"}');
INSERT INTO `templates` (`id`, `name`, `fieldgroups_id`, `flags`, `cache_time`, `data`) VALUES('43', 'language', '97', '8', '0', '{\"parentTemplates\":[2],\"slashUrls\":1,\"pageClass\":\"Language\",\"pageLabelField\":\"name\",\"noGlobal\":1,\"noMove\":1,\"noTrash\":1,\"noChangeTemplate\":1,\"noUnpublish\":1,\"nameContentTab\":1,\"modified\":1409651146}');
INSERT INTO `templates` (`id`, `name`, `fieldgroups_id`, `flags`, `cache_time`, `data`) VALUES('44', 'repeater_text_fields', '98', '8', '0', '{\"noChildren\":1,\"noParents\":1,\"slashUrls\":1,\"pageClass\":\"RepeaterPage\",\"noGlobal\":1,\"compile\":3,\"modified\":1587361673}');
INSERT INTO `templates` (`id`, `name`, `fieldgroups_id`, `flags`, `cache_time`, `data`) VALUES('45', 'repeater_simple_textblocks', '99', '8', '0', '{\"noChildren\":1,\"noParents\":1,\"slashUrls\":1,\"pageClass\":\"RepeaterPage\",\"noGlobal\":1,\"compile\":3,\"modified\":1587365694}');
INSERT INTO `templates` (`id`, `name`, `fieldgroups_id`, `flags`, `cache_time`, `data`) VALUES('46', 'repeater_repeater_why', '100', '8', '0', '{\"noChildren\":1,\"noParents\":1,\"slashUrls\":1,\"pageClass\":\"RepeaterPage\",\"noGlobal\":1,\"compile\":3,\"modified\":1587365874}');
INSERT INTO `templates` (`id`, `name`, `fieldgroups_id`, `flags`, `cache_time`, `data`) VALUES('47', 'board-page', '101', '0', '0', '{\"slashUrls\":1,\"compile\":3,\"modified\":1615116811,\"ns\":\"ProcessWire\"}');
INSERT INTO `templates` (`id`, `name`, `fieldgroups_id`, `flags`, `cache_time`, `data`) VALUES('48', 'board-overview-page', '102', '0', '0', '{\"slashUrls\":1,\"compile\":3,\"modified\":1611219107,\"ns\":\"ProcessWire\"}');
INSERT INTO `templates` (`id`, `name`, `fieldgroups_id`, `flags`, `cache_time`, `data`) VALUES('49', 'snipcart-cart', '103', '0', '0', '{\"noChildren\":1,\"noParents\":1,\"slashUrls\":1,\"pageLabelField\":\"fa-cog title\",\"compile\":3,\"label\":\"Snipcart Cart (System)\",\"tags\":\"Snipcart\",\"modified\":1587762238}');
INSERT INTO `templates` (`id`, `name`, `fieldgroups_id`, `flags`, `cache_time`, `data`) VALUES('50', 'snipcart-shop', '104', '0', '0', '{\"childTemplates\":[51,47,55],\"slashUrls\":1,\"pageLabelField\":\"fa-tags title\",\"compile\":3,\"label\":\"Snipcart Shop\",\"tags\":\"Snipcart\",\"modified\":1615123706,\"ns\":\"ProcessWire\"}');
INSERT INTO `templates` (`id`, `name`, `fieldgroups_id`, `flags`, `cache_time`, `data`) VALUES('51', 'snipcart-product', '105', '0', '0', '{\"noChildren\":1,\"parentTemplates\":[50],\"slashUrls\":1,\"pageLabelField\":\"fa-tag title\",\"compile\":3,\"label\":\"Snipcart Product\",\"tags\":\"Snipcart\",\"modified\":1615128330,\"ns\":\"ProcessWire\"}');
INSERT INTO `templates` (`id`, `name`, `fieldgroups_id`, `flags`, `cache_time`, `data`) VALUES('52', 'repeater_prod_variant_item', '106', '8', '0', '{\"noChildren\":1,\"noParents\":1,\"slashUrls\":1,\"pageClass\":\"RepeaterPage\",\"noGlobal\":1,\"compile\":3,\"modified\":1588051251}');
INSERT INTO `templates` (`id`, `name`, `fieldgroups_id`, `flags`, `cache_time`, `data`) VALUES('53', 'repeater_prod_variants', '107', '8', '0', '{\"noChildren\":1,\"noParents\":1,\"slashUrls\":1,\"pageClass\":\"RepeaterPage\",\"noGlobal\":1,\"compile\":3,\"modified\":1588051314}');
INSERT INTO `templates` (`id`, `name`, `fieldgroups_id`, `flags`, `cache_time`, `data`) VALUES('54', 'basic-page-twoes', '108', '0', '0', '{\"slashUrls\":1,\"compile\":3,\"modified\":1611219107,\"ns\":\"ProcessWire\"}');
INSERT INTO `templates` (`id`, `name`, `fieldgroups_id`, `flags`, `cache_time`, `data`) VALUES('55', 'gallery', '109', '0', '0', '{\"slashUrls\":1,\"compile\":3,\"modified\":1615115223,\"ns\":\"ProcessWire\"}');
INSERT INTO `templates` (`id`, `name`, `fieldgroups_id`, `flags`, `cache_time`, `data`) VALUES('56', 'repeater_bigLinks', '110', '8', '0', '{\"noChildren\":1,\"noParents\":1,\"slashUrls\":1,\"pageClass\":\"RepeaterPage\",\"noGlobal\":1,\"compile\":3,\"modified\":1615119819}');
INSERT INTO `templates` (`id`, `name`, `fieldgroups_id`, `flags`, `cache_time`, `data`) VALUES('57', 'repeater_prod_custom_variants', '111', '8', '0', '{\"noChildren\":1,\"noParents\":1,\"slashUrls\":1,\"pageClass\":\"RepeaterPage\",\"noGlobal\":1,\"compile\":3,\"modified\":1615123376}');
INSERT INTO `templates` (`id`, `name`, `fieldgroups_id`, `flags`, `cache_time`, `data`) VALUES('58', 'repeater_prod_custom_variant_item', '112', '8', '0', '{\"noChildren\":1,\"noParents\":1,\"slashUrls\":1,\"pageClass\":\"RepeaterPage\",\"noGlobal\":1,\"compile\":3,\"modified\":1615123456}');

DROP TABLE IF EXISTS `textformatter_video_embed`;
CREATE TABLE `textformatter_video_embed` (
  `video_id` varchar(128) NOT NULL,
  `embed_code` varchar(1024) NOT NULL DEFAULT '',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`video_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `textformatter_video_embed` (`video_id`, `embed_code`, `created`) VALUES('16evq_vySAc', '<iframe width=\"640\" height=\"360\" src=\"https://www.youtube.com/embed/16evq_vySAc?feature=oembed\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>', '2020-04-23 10:36:12');

# --- /WireDatabaseBackup {"numTables":85,"numCreateTables":85,"numInserts":1919,"numSeconds":0}