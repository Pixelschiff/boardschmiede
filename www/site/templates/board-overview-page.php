<?php

namespace ProcessWire;

$content = '
<section class="container p-5">
	<div class="row">
		<div class="col-md-12">
			' . $page->body . '
		</div>
	</div>
</section>
';
$sections = '';

$sections .= renderBoardOverview($page->children, null, 0);

if ($page->text_fields->count > 0) {
	$sections .= '
	<section class="bg--dark">
		<div class="container-fluid p-5">';
	$blockIndex = 0;
	foreach ($page->text_fields as $text_block) {
		$alignClass = $blockIndex % 2 ? 'flex-row-reverse' :
			'';

		$textAlignClass = !$blockIndex % 2 ? 'text-right' :
			'';
		$sections .= '
				<div class="row p-5 ' . $alignClass . '">
					<div class="col-md-6 ' . $textAlignClass . '">
						<h3>
						' . $text_block->headline . '
						</h3>	
						<p>
						' . $text_block->textblock . '
						</p>
					</div>
					<div class="col-md-6">
						<img src="' . $text_block->image->url . '" />
					</div>
				</div>
				';
		$blockIndex++;
	}
	$sections .= '
		</div>
	</sections>
	';
}
