<?php namespace ProcessWire;
if($page->body) {

	$content = '
	<section class="container p-5">
		<div class="row">
			<div class="col-md-12">
				'.$page->body.'
			</div>
		</div>
	</section>
	';
}

$sections = '<section class="container">'.renderGallery($page->images).'</section>';

if($page->body_2) {

	$sections .= '
	<section class="container p-5">
		<div class="row">
			<div class="col-md-12">
				'.$page->body_2.'
			</div>
		</div>
	</section>
	';
}