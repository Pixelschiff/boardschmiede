<?php

namespace ProcessWire;

function renderVariants($variants)
{
	if (!$variants) return '';
	$out = '';

	foreach ($variants as $variant) {
		$options = '';

		foreach ($variant->prod_variant_item as $variantItem) {
			if ($variantItem->images->first()) {
				$imageRefSmall = $variantItem->images->first()->height(64)->url();
				$imageRefBig = $variantItem->images->first()->height(470)->url();
			} else {
				$imageRefSmall = '';
				$imageRefBig = '';
			}

			$options .= '
                <option rel="js_variant_' . $variant->id . '" 
					data-small="' . $imageRefSmall . '"
					data-big="' . $imageRefBig . '">
                    ' . $variantItem->title . '
                    <small>
                    (+' . number_format($variantItem->price, 2, ',', '.') . '€)
                    </small>
                </option>
            ';
		}
		$out .= '
        <div class="form-group">
            <label for="variant_' . $variant->id . '">' . $variant->title . '</label>
            <select class="form-control target js_variant" id="variant_' . $variant->id . '" name="' . $variant->title . '">
                ' . $options . '
            </select>
        </div>
        ';
	}

	return $out;
}

function renderCustomVariants($variants)
{
	if (!$variants) return '';
	$out = '';
	foreach ($variants as $variant) {
		$options = '';
		$i = 0;

		foreach ($variant->prod_custom_variant_item as $variantItem) {
			if ($variantItem->images->first()) {
				$imageRefSmall = $variantItem->images->first()->height(64)->url();
				$imageRefBig = $variantItem->images->first()->height(470)->url();
			} else {
				$imageRefSmall = '';
				$imageRefBig = '';
			}


			$options .= '
			<a class="dropdown-item" href="#">
				<label class="d-flex ColorBubble__Label ' . ($i == 0 ? 'active' : '') . ' js_variant_' . $variant->id . '" for="variant_' . $variant->id . '-' . $variantItem->id . '" data-toggle="tooltip" data-placement="top" title="' . $variantItem->title . '">
					<span class="ColorBubble mr-1" style="background-color: ' . $variantItem->color . '"></span>

					<span class="pl-2 pr-2">
					' . $variantItem->title . '
					</span>
					
					<input 
						rel="js_variant_' . $variant->id . '" 
						data-small="' . $imageRefSmall . '"
						data-big="' . $imageRefBig . '"
						type="radio" ' . ($i == 0 ? 'checked' : '') . ' 
						class="radio js_color" id="variant_' . $variant->id . '-' . $variantItem->id . '" 
						name="variant_' . $variant->id . '" 
						value="' . $variantItem->title . ' (+' . number_format($variantItem->price, 2, ',', '.') . '€)">
					
					<small>
					(+' . number_format($variantItem->price, 2, ',', '.') . '€)
					</small>
				</label>
			</a>
            ';
			$i++;
		}
		$out .= '
		<div class="BoardVariant js_boardVariant mb-3">
			<label>' . $variant->title . '</label>

			<button class="btn btn-secondary dropdown-toggle js_variant_' . $variant->id . '_button d-flex btn-block  align-center align-items-center" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-expanded="false">
				<span class="ColorBubble mr-1" style="background-color: ' . $variant->prod_custom_variant_item->first()->color . '"></span>

				<span class="pl-2 pr-2">
				' . $variant->prod_custom_variant_item->first()->title . '
				</span>
				
				<small>
				(+' . number_format($variant->prod_custom_variant_item->first()->price, 2, ',', '.') . '€)
				</small>
			</button>

			<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
				' . $options . '
			</div>
		</div>
        ';
	}

	return $out;
}

$options = array(
	'label' => ukIcon('cart') . ' ' . __('Add to cart'),
	'class' => 'btn btn-primary',
	'attr' => array('aria-label' => __('Add item to cart')),
);
$product = page();

$globaleVariants = $page->parent->prod_variants;
$customVariants = $page->parent->prod_custom_variants;

$customVariantsPage = $page->prod_custom_variants;
$variants = $page->prod_variants;

class VariantImage
{
	public function getCustomVariantImages($customVariants)
	{
		$customVariantImages = array();

		foreach ($customVariants as $customVariant) {
			$vItem = new \stdClass();
			$vItem->images = [];

			foreach ($customVariant->prod_custom_variant_item as $variantItem) {
				if (count($variantItem->images)) {
					$vItem->images[$variantItem->id] = $variantItem->images;
				}
			}
			if (count($vItem->images)) {
				$customVariantImages[$customVariant->id] = $vItem;
			}
		}
		return $customVariantImages;
	}

	public function getVariantImages($customVariants)
	{
		$customVariantImages = array();

		foreach ($customVariants as $customVariant) {
			$vItem = new \stdClass();
			$vItem->images = [];

			foreach ($customVariant->prod_variant_item as $variantItem) {
				if (count($variantItem->images)) {
					$vItem->images[$variantItem->id] = $variantItem->images;
				}
			}
			if (count($vItem->images)) {
				$customVariantImages[$customVariant->id] = $vItem;
			}
		}
		return $customVariantImages;
	}

	public function getFirstImage($variantImages)
	{
		if (!count($variantImages)) {
			return;
		}
		$customVariantImages = array_values($variantImages)[0]->images;
		return array_values($customVariantImages)[0]->first()->url();
	}
}

$variantImage = new VariantImage;

$content = '';
$sections = '';
$sections .= '
        <section class="container pt-5">
		<div class="row justify-content-between">
			<div class="col-12 text-center pb-5">
				<h1>' . $page->title . '</h1>
				<h3>' . $page->subheadline . '</h1>
			</div>
			<div class="col-12 p-0">
				<div class="row no-gutters Gallery">
				
				<div class="col-md-8 pl-5 pr-5">';
$customVariantImageItems = $customVariantsPage ? $customVariantsPage : $customVariants;
$variantsImageItems = $variants ? $variants : $globaleVariants;

$variantImages = $variantImage->getCustomVariantImages($customVariantImageItems);
$generalVariantImages = $variantImage->getVariantImages($variantsImageItems);

$variantImages = array_merge($variantImages, $generalVariantImages);

$firstVariantImage = $variantImage->getFirstImage($variantImages);
$initialImageURL = $firstVariantImage ? $firstVariantImage : $page->product_images->first()->width(169)->url();

if ($variantImages) {
	$sections .= renderVariantsGallery($variantImages);
} else {
	$sections .= renderGallery($page->product_images);
}

$sections .= '
				</div>
				
				<div class="col-md-4">
					<div class="board__configurator">
						<div class="p-5">
							
							<form class="pb-2" method="POST">
								' . renderCustomVariants($customVariants) . '
								' . renderCustomVariants($customVariantsPage) . '
								' . renderVariants($globaleVariants) . '
								' . renderVariants($variants) . '
	
								<input id="addToCart" name="addToCart" type="hidden" value="1"/>
								<input id="price_base" name="price_base" type="hidden" value="' . $page->price . '"/>
								<input id="price_variants" name="price_variants" type="hidden" value="0"/>
								<input id="price_variants2" name="price_variants2" type="hidden" value="0"/>
								<input id="itemID" name="itemID" type="hidden" value="' . $page->id . '"/>
								<input id="orderItemID" name="orderItemID" type="hidden" value="' . rand() . '"/>
								<input id="priceTotal" name="priceTotal" type="hidden" value=""/>
								<input id="previewImage" name="previewImage" type="hidden" value="' . $initialImageURL . '"/>
								
								<div class="row pb-3">
									<div class="col-md-12">
										<small>
											' . __("Price inclusive 21% TAX") . '
										</small>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<button class="btn btn-primary">
										' . __("ADD TO CART") . '
										</button>
									</div>
									<div class="col-md-6 text-right">
										<h4 id="price"></h4>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
				</div>
			</div>

			<div class="col-md-12">
				
				' . $page->body . '
				' . $page->board_features . '
				' . $page->video_url . '
			</div>

			
		</section>
';


if ($page->text_fields && $page->text_fields->count > 0) {
	$sections .= '
	<section class="bg--white">
		<div class="container-fluid p-5">';
	$blockIndex = 0;
	foreach ($page->text_fields as $text_block) {
		$alignClass = $blockIndex % 2 ? 'flex-row-reverse' :
			'';

		$textAlignClass = !$blockIndex % 2 ? 'text-right' :
			'';
		$sections .= '
				<div class="row p-5 ' . $alignClass . '">
					<div class="col-md-6 ' . $textAlignClass . '">
						<h3>
						' . $text_block->headline . '
						</h3>	
						<p>
						' . $text_block->textblock . '
						</p>
					</div>
					<div class="col-md-6">
						<img src="' . $text_block->image->url . '" />
					</div>
				</div>
				';
		$blockIndex++;
	}
	$sections .= '
		</div>
	</sections>
	';
}

$sections .= '
<div class="pt-5 pb-0">
    <section class="container-fluid p-0">
        
            
        <!--
            <svg id="wave" class="waves__wave waves__wave--top" style="transform:rotate(180deg); transition: 0.3s" viewBox="0 0 1440 100" version="1.1" xmlns="http://www.w3.org/2000/svg"><path style="transform:translate(0, 0px); opacity:1" d="M0,30L120,40L240,20L360,90L480,90L600,50L720,40L840,90L960,10L1080,30L1200,80L1320,0L1440,10L1560,20L1680,0L1800,0L1920,60L2040,90L2160,90L2280,40L2400,20L2520,30L2640,60L2760,40L2880,60L2880,100L2760,100L2640,100L2520,100L2400,100L2280,100L2160,100L2040,100L1920,100L1800,100L1680,100L1560,100L1440,100L1320,100L1200,100L1080,100L960,100L840,100L720,100L600,100L480,100L360,100L240,100L120,100L0,100Z"></path></svg>
            <svg id="wave" class="waves__wave waves__wave--bottom" style="transform:rotate(180deg); transition: 0.3s" viewBox="0 0 1440 100" version="1.1" xmlns="http://www.w3.org/2000/svg"><path style="transform:translate(0, 0px); opacity:1" d="M0,30L120,40L240,20L360,90L480,90L600,50L720,40L840,90L960,10L1080,30L1200,80L1320,0L1440,10L1560,20L1680,0L1800,0L1920,60L2040,90L2160,90L2280,40L2400,20L2520,30L2640,60L2760,40L2880,60L2880,100L2760,100L2640,100L2520,100L2400,100L2280,100L2160,100L2040,100L1920,100L1800,100L1680,100L1560,100L1440,100L1320,100L1200,100L1080,100L960,100L840,100L720,100L600,100L480,100L360,100L240,100L120,100L0,100Z"></path></svg>
-->
        ' . renderTriptichon($page->triptichon_images) . '
    </section>
</div>
';
