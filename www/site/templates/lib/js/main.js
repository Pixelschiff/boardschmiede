$(document).ready(function () {
  let navigation_on = false;

  window.onscroll = function () {
    scrollFunction();
  };    

  $('.slick-slider').slickLightbox({
    src: 'rel',
    itemSelector: '.slick-big'
  });

  function scrollFunction() {
    navigation_on = $(".navbar--mega").hasClass("navbar--mega--visible");
    if (
      (document.body.scrollTop > 80 ||
        document.documentElement.scrollTop > 80) &&
      !navigation_on
    ) {
      $(".navbar").addClass("navbar--scrolled");
      $(".navbar-brand--light").addClass("d-none");
      $(".navbar-brand--dark").removeClass("d-none");
    } else {
      $(".navbar").removeClass("navbar--scrolled");
      $(".navbar-brand--light").removeClass("d-none");
      $(".navbar-brand--dark").addClass("d-none");
    }
  }

  $(".js__showNav").on("click", () => {
    navigation_on = $(".navbar--mega").hasClass("navbar--mega--visible");
    if (navigation_on) {
      $("html,body").scrollTop(0);
    }
    $(".navbar--mega").toggleClass("navbar--mega--visible");
    $(".navbar--mega").addClass("navbar--mega--initiated");
  });

  $('.nav-item--children a').on('click', e => {
    return;
    if($(e.currentTarget).hasClass('selected')) {
      return true;
    };

    $('a').removeClass('selected');
    $(e.currentTarget).addClass('selected');

    $('.subnav').hide();

    $('#'+e.currentTarget.rel).show();
    return false;
  });

  $(function () {
    $('[data-toggle="tooltip"]').tooltip();
  });

  

  $('.js__slideshow').not('.slick-initialized').slick({
    dots: true,
    dotsClass: 'dotty',
    infinite: true,
    speed: 2000,
    fade: true,
    cssEase: 'linear',
    lazyLoad: 'ondemand',
    autoplay: true,
    pauseOnFocus: false,
    pauseOnHover: false,
    autoplaySpeed: 4000,
    dots: false,
    prevArrow: false,
    nextArrow: false
  });

  var slidesToShow = 6;

  $(".slick-slider").not('.slick-initialized').slick({
    infinite: false,
    speed: 500,
    asNavFor: ".slick-thumbs",
  });

  $(".slick-thumbs").not('.slick-initialized').slick({
    infinite: false,
    speed: 500,
    slidesToShow: slidesToShow,
    slidesToScroll: 1,
    asNavFor: ".slick-slider",
    focusOnSelect: true,
    arrows: false,
    centerMode: true,
  });

  // Remove active class from all thumbnail slides
  $(".slick-thumbs .slick-slide").removeClass("slick-active");

  // Set active class to first thumbnail slides
  $(".slick-thumbs .slick-slide").eq(0).addClass("slick-active");

  // On before slide change match active thumbnail to current slide
  $(".slider").on(
    "beforeChange",
    function (event, slick, currentSlide, nextSlide) {
      var mySlideNumber = nextSlide;
      $(".slick-thumbs .slick-slide").removeClass("slick-active");
      $(".slick-thumbs .slick-slide")
        .eq(mySlideNumber)
        .addClass("slick-active");
    }
  );

  adjustPrice();

  $(".js_color").change(function () {
    $currentVariant = $(this).attr("rel");
    $variantRefBig = $(this).data("big");

    $("." + $currentVariant).removeClass("active");
    $(this).parent("label").addClass("active");

    const variantHTML = $("." + $currentVariant + ".active");
    variantHTML.find(".redio").remove();
    console.log("variantHTML", variantHTML);

    $("." + $currentVariant + "_button").html(variantHTML.html());

    console.log("$currentVariant", $currentVariant);

    if ($variantRefBig) {
      changePreviewImage($variantRefBig);
    }

    adjustPrice();
  });

  $(".js_variant").change(function () {
    $currentVariant = $("option:selected", this).attr("rel");
    $variantRefBig = $("option:selected", this).data("big");

    console.log("$currentVariant", $currentVariant);

    if ($variantRefBig) {
      changePreviewImage($variantRefBig);
    }

    adjustPrice();
  });

  function changePreviewImage($variantRefBig) {
    console.log("changePreviewImage");
    const slide = $('.slick-slider figure[data-id="' + $variantRefBig + '"]');
    const index = slide.data("slick-index");
    $(".slick-slider").slick("goTo", index);
    $("#previewImage").val($variantRefBig);
  }

  $("select")
    .change(function () {
      adjustPrice();
    })
    .change();

  function adjustPrice() {
    var priceBase = Number($("#price_base").val());
    var prices = [];
    var pricesCustomVariants = [];
    var str = "";

    $(".js_color:checked").each(function () {
      priceOfCustomVariants = 0;
      currentPrice = this.value
        .match(/\(([^)]+)\)/g)[0]
        .replace(/[,]/g, ".")
        .replace(/[()€]/g, "");
      pricesCustomVariants.push(currentPrice);

      pricesCustomVariants.forEach((price) => {
        priceOfCustomVariants = Number(priceOfCustomVariants) + Number(price);
        $("#price_variants2").val(priceOfCustomVariants);
      });

      str += "<li>" + this.value + "</li>";
    });

    $("select option:selected").each(function () {
      priceOfOptions = 0;
      currentPrice = this.value
        .match(/\(([^)]+)\)/g)[0]
        .replace(/[,]/g, ".")
        .replace(/[()€]/g, "");
      prices.push(currentPrice);

      prices.forEach((price) => {
        priceOfOptions = Number(priceOfOptions) + Number(price);
        $("#price_variants").val(priceOfOptions);
      });

      str += "<li>" + $(this).text() + "</li>";
    });

    var total =
      Number(
        Number($("#price_variants").val()) + Number($("#price_variants2").val())
      ) + priceBase;
    var totalString = total.toLocaleString("de-DE", {
      style: "currency",
      currency: "EUR",
    });

    $("#priceTotal").val(total);
    $("#price").text(totalString);
  }

  // Boardselecter
  const boardSelecterEle = $(".js__board__selecter li");

  boardSelecterEle.on("click", (ele) => {
    $(boardSelecterEle).removeClass("board__selecter--active");
    $(ele.currentTarget).toggleClass("board__selecter--active");
    displaySelectedBoard(ele.currentTarget);
  });
});

function displaySelectedBoard(board) {
  const boardRel = board.attributes.rel.value;
  $(".board__selecter__images__image").removeClass(
    "board__selecter__images__image--active"
  );
  $("img[data-id=" + boardRel + "]").addClass(
    "board__selecter__images__image--active"
  );
}
