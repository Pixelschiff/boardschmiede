<?php

namespace ProcessWire;

$content = '';

$sections = '<section class="container"><div class="row">';

if ($page->shop_showTable) {

	$boardTitle = $title ? '<div class="col-md-12 text-center pb-5"><h2>' . $title . '</h2></div>' : '';
	$summaryBody = $page->summary ? '<div class="col-md-12 text-center pb-5"><p>' . $page->summary . '</p></div>' : '';

	$table = $boardTitle . $summaryBody . '
			<div class="col-12 pb-10">
				<div class="board__table__wrapper">
				
				<table class="board__table">
					<colgroup>
						<col style="width:auto" class="d-none d-md-table-cell">
						<col style="width:27%">
						<col style="width:27%">
						<col style="width:27%">
					</colgroup>  
					<thead>
						<tr>
							<th class="d-none d-md-table-cell">
							
							</th>';
	foreach ($page->children as $boardPage) {
		if ($boardPage->board_cover) {
			$table .= '
		<th>
			<a href="' . $boardPage->url . '">
				<img class="mb-3" src="' . $boardPage->board_cover->width(222)->url() . '" />
			</a>
			<h3>
			' . $boardPage->title . '
			</h3>
		</th>';
		}
	}


	$table .= '
</tr>
</thead>
<tbody>
<tr>
<td class="d-none d-md-table-cell">
	<h4>
	Allround
	</h4>
</td>';
	foreach ($page->children as $boardPage) {
		$table .= '
		<td>
		<small class="d-md-none d-flex">
			Allround
		</small>
			' . renderRating($boardPage->board_spec_allround->title) . '
		</td>';
	}


	$table .= '
</tr>

<tr>
<td class="d-none d-md-table-cell">
	<h4>
	Big Air
	</h4>
</td>';
	foreach ($page->children as $boardPage) {
		$table .= '
		<td>
		<small class="d-md-none d-flex">
			Big Air
		</small>
			' . renderRating($boardPage->board_spec_bigair->title) . '
		</td>';
	}


	$table .= '
</tr>

<tr>
<td class="d-none d-md-table-cell">
	<h4>
	Freestyle
	</h4>
</td>';
	foreach ($page->children as $boardPage) {
		$table .= '
		<td>
		<small class="d-md-none d-flex">
			Freestyle
		</small>
		' . renderRating($boardPage->board_spec_freestyle->title) . '
		</td>';
	}


	$table .= '
</tr>

<tr>
<td class="d-none d-md-table-cell">
	<h4>
	Control
	</h4>
</td>';
	foreach ($page->children as $boardPage) {
		$table .= '
		<td>
		<small class="d-md-none d-flex">
			Control
		</small>
			' . renderRating($boardPage->board_spec_control->title) . '
		</td>';
	}


	$table .= '
</tr>
<tr>
<td class="d-none d-md-table-cell">
	<h4>
	Gliding
	</h4>
</td>';
	foreach ($page->children as $boardPage) {
		$table .= '
		<td>
		<small class="d-md-none d-flex">
			Gliding
		</small>
			' . renderRating($boardPage->board_spec_gliding->title) . '
		</td>';
	}


	$table .= '
</tr>

<tr>
<td class="d-none d-md-table-cell">
	<h4>
	Flex
	</h4>
</td>';
	foreach ($page->children as $boardPage) {
		$table .= '
		<td>
		<small class="d-md-none d-flex">
		Flex
		</small>
			' . renderRating($boardPage->board_spec_flex->title) . '
		</td>';
	}


	$table .= '
</tr>

<tr>
<td class="d-none d-md-table-cell">
	
	</td>';
	foreach ($page->children as $boardPage) {
		$table .= '
		<th class="text-left pt-5">
			<a href="' . $boardPage->url . '" class="btn btn-primary">CHECK IT OUT!</a>
		</th>';
	}


	$table .= '
</td>
</tr>
					</tbody>
					</div>
				</table>
			</div>
';

	$sections .= $table;
} else {
	$sections .= renderBoardOverview($page->children->find('template!=gallery'), $page->title, 2, $page->summary);
}

$sections .= '</div></section>';

if ($page->image) {
	$sections .= '
	<section>
	' . renderFullImage($page->image) . '
	</section>';
}

if ($page->body) {
	$sections .= '
	<section class="container pt-10">
		<div class="row">
			<div class="col-md-12">
				' . $page->body . '
			</div>
		</div>
	</section>
	';
}


if ($page->teaer_image) {
	$sections .= '
	<section class="teaser--auto spikes">
	' . renderTeaser($page) . '
	</section>';
}

if ($page->text_fields && $page->text_fields->count > 0) {
	$sections .= '
	<section class="bg--dark">
		<div class="container-fluid p-md-5">';
	$blockIndex = 0;
	foreach ($page->text_fields as $text_block) {
		$alignClass = $blockIndex % 2 ? 'flex-row-reverse' :
			'';

		$textAlignClass = $blockIndex % 2 == 0 ? 'text-md-right' : '';
		#$textAlignClass = 'text-center';
		$sections .= '
				<div class="row p-md-5 ' . $alignClass . '">
					<div class="col-md-6 ' . $textAlignClass . '">
						<h3>
						' . $text_block->headline . '
						</h3>	
						<p>
						' . $text_block->textblock . '
						</p>
					</div>
					<div class="col-md-6">
						<img loading="lazy" src="' . $text_block->image->url . '" />
					</div>
				</div>
				';
		$blockIndex++;
	}
	$sections .= '
		</div>
	</sections>
	';
}
