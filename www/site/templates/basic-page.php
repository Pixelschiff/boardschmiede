<?php

namespace ProcessWire;

$content = '';

if ($page->body) {
	$content .= '
	<section class="container p-5">
		<div class="row">
			<div class="col-md-12">
				' . $page->body . '
			</div>
		</div>
	</section>
	';
}

$sections = '';

if ($page->text_fields->count > 0) {
	$sections .= '
	<section>
		<div class="container-fluid p-5">';
	$blockIndex = 0;
	foreach ($page->text_fields as $text_block) {
		$blockImage = '';
		if ($text_block->image && $text_block->image->url) {
			$blockImage = '<img src="' . $text_block->image->url . '" />';
		}
		$alignClass = $blockIndex % 2 ? 'flex-row-reverse' :
			'';

		$textAlignClass = !$blockIndex % 2 ? 'text-right' :
			'';
			$text_button = '';
			if($text_block->button) {
				$text_button = '<a href="'.$text_block->button->url.'" class="btn btn-primary mt-3">'.$text_block->button_label.'</a>';
			}
		$sections .= '
				<div class="row p-5 ' . $alignClass . '">
					<div class="col-md-6 ' . $textAlignClass . '">
						<h3>
						' . $text_block->headline . '
						</h3>	
						<p>
						' . $text_block->textblock . '
						</p>
						'.$text_button.'
					</div>
					<div class="col-md-6">
						' . $blockImage . '
					</div>
				</div>
				';
		$blockIndex++;
	}
	$sections .= '
		</div>
	</sections>
	';
}

$whyBlocks = '';
$whyBlocks0 = [];
$whyBlockIndex = 0;
if ($page->repeater_why) {
	foreach ($page->repeater_why as $whyBlock) {
		$whyClass = 'text-right';
		$whyFlex = 'flex-row';
		$whyImg = '';
		$whyBg = 'bg--grey';

		if ($whyBlockIndex % 2 == 0) {
			$whyClass = 'text-left';
			$whyFlex = 'flex-row-reverse';
			$whyBg = 'bg--white';
		}

		if ($whyBlock->image_why) {
			$whyImg = '<img class="img-fluid w-100" src="' . $whyBlock->image_why->url . '"/>';
		}

		$whyClass = 'text-center';

		array_push($whyBlocks0, '
		<div class="row no-gutters pt-5 ' . $whyFlex . ' ' . $whyBg . ' h-50">
			<div class="col-md-12 pb-5">
				<div class="' . $whyClass . '">
					<h3 class="heading h1">' . $whyBlock->headline . '</h3>
					<p>' . $whyBlock->textblock . '</p>
				</div>
			</div>
			
			<div class="col-md-12">
				<div class="' . $whyClass . '">
					' . $whyImg . '
				</div>
			</div>
		</div>
		');

		$whyBlockIndex++;
	}

	foreach ($whyBlocks0 as $whyBlock0) {
		$sections .= $whyBlock0;
	}
}

$sections .= '
<section class="container--cards cards">
    <div class="row no-gutters">
    ' . renderTeaser($page) . '
    </div>
</section>';
