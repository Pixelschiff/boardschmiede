<?php namespace ProcessWire;

$content = '
<section class="container p-5">
            <div class="row">
                <div class="col-md-6">
                    '.$page->body.'
				</div>
				<div class="col-md-6">
					<h3>WHERE TO FIND US</h3>
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2402.7823433264607!2d5.426005115824332!3d52.970329779901974!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47c8ea6363694651%3A0x6da3a09641a10148!2sSuders%C3%A9leane%207%2C%208711%20GX%20Workum%2C%20Niederlande!5e0!3m2!1sde!2see!4v1591459337419!5m2!1sde!2see" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
				</div>
            </div>
        </section>
';
$sections = '';
if($page->text_fields->count > 0) {
	$sections .= '
	<section>
		<div class="container-fluid p-5">';
			$blockIndex = 0;
			foreach ($page->text_fields as $text_block) {
				$alignClass = $blockIndex % 2 ? 'flex-row-reverse' : 
				'';

				$textAlignClass = !$blockIndex % 2 ? 'text-right' : 
				'';
				$sections .= '
				<div class="row p-5 '.$alignClass.'">
					<div class="col-md-6 '.$textAlignClass.'">
						<h3>
						'.$text_block->headline.'
						</h3>	
						<p>
						'.$text_block->textblock.'
						</p>
					</div>
					<div class="col-md-6">
						<img src="'.$text_block->image->url.'" />
					</div>
				</div>
				';
				$blockIndex++;
			}
			$sections .= '
		</div>
	</sections>
	';
}