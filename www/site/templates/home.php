<?php

namespace ProcessWire;

$content = '';

function renderHomeBoardNavi($boardPages)
{
    $out = '';

    $i = 1;
    foreach ($boardPages as $boardPage) {
        $active = $i == 1 ? 'board__selecter--active' : '';
        $out .= '
        <li class="' . $active . '" rel="board-' . $i . '">
            <a class="board__selecter__link" href="javascript:void(0);">
                ' . $boardPage->title . '
            </a>
            <div class="board__selecter__info">
                <p>' . $boardPage->product_subtitle . '</p>
                <a href="' . $boardPage->url . '" class="btn Button">
                    Check it out
                </a>
            </div>
        </li>
        ';
        $i++;
    }

    return $out;
}

function renderHomeBoardNaviImages($boardPages)
{
    $out = '';

    $i = 1;
    foreach ($boardPages as $boardPage) {
        if ($boardPage->board_cover) {
            $active = $i == 1 ? 'board__selecter__images__image--active' : '';
            $out .= '
            <img class="board__selecter__images__image ' . $active . '" data-id="board-' . $i . '" src="' . $boardPage->board_cover->url() . '"  />
            ';
            $i++;
        }
    }

    return $out;
}

$sections = '
<div class="pt-5">
    <section class="container-fluid container--420 pb-5">
        <div class="row">
            <div class="col-md-5">
                <h2 class="display-4">
                    PICK YOUR BOARD
                </h2>
                <ul class="board__selecter js__board__selecter">
                ' . renderHomeBoardNavi($pages->find('template=snipcart-product, parent=/twintips|/surf')) . '
                </ul>
            </div>
            <div class="col-md-7 text-center board__selecter__images">
            ' . renderHomeBoardNaviImages($pages->find('template=snipcart-product, parent=/twintips|/surf')) . '
            </div>
        </div>
    </section>
</div>
';

$sections .= '
<div class="kritzel">
    <section class="container-fluid p-0">
        ' . renderTriptichon($page->triptichon_images) . '
    </section>
</div>
';

$sections .= '
<section class="teaser--100 spikes">
' . renderTeaser($page) . '
</section>';

/*
three
$sections .= "
<div>
    <section class='container-fluid p-0'>
        <div class='pageOption'>
            <a href='#' class='option' data-inf='photo'>
                <img src='" . $config->urls->templates . "/lib/img/001.jpg'>
            </a>
            <a href='#' class='option' data-inf='cinema'>
                <img src='" . $config->urls->templates . "/lib/img/003.jpg'>
            </a>
        </div>

    </section>
</div>
";
*/