<?php

namespace ProcessWire;

function receiveBg($page)
{
	/*
	if($page->template == 'snipcart-shop' || $page->template == 'snipcart-product') {
		return 'bg--2';
	}
	*/
	// return $page->page_color ? 'bg--'.$page->page_color : '';
	return 'bg--1';
}

function renderTriptichon($images)
{
	$out = '';

	foreach ($images as $tripImage) {
		$out .= '
        <div class="col-md-4 moods__card" style="background-image:url(' . $tripImage->url() . ')"></div>
        ';
	}

	return '<div class="row no-gutters moods waves">' . $out . '</div>';
}

function sendNewOrderMail($p, $orderMailPage, $customerMail = true)
{
	// Always set content-type when sending HTML email
	$headers = "MIME-Version: 1.0" . "\r\n";
	$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

	// More headers
	$headers .= 'From: <info@boardschmiede.com>' . "\r\n";

	/*
	$m = wire('modules')->get('WireMailSendGrid');
	if ($customerMail) {
		$m->setTemplateId('d-c56faf8f8d7442138084530ba88fd269');
		$m->to($email = $p->customer_email, $name = $p->customer_name, $substitutions = null);
		$m->setDynamicTemplateData("yourdata", __("Your data"));
		$m->setDynamicTemplateData("deliverydata", __("Delivery data"));
		$m->setDynamicTemplateData("displayname", $p->customer_name);
		$m->setDynamicTemplateData("message", $orderMailPage->email_text_top);
		$m->setDynamicTemplateData("messagetwo", $orderMailPage->email_text_bottom);
		wire('log')->message('Ordermail: Customer - ' . $p->customer_email);
	} else {
		$m->setTemplateId('d-7bfc275edc634c5ba6c091f0e6227388');
		$m->to($email = $orderMailPage->mail_admin, $name = 'Thilo', $substitutions = null);
		$m->setDynamicTemplateData("yourdata", __("Customer data"));
		$m->setDynamicTemplateData("deliverydata", __("Delivery data"));
		$m->setDynamicTemplateData("displayname", "Thilo!");
		$m->setDynamicTemplateData("message", __("A new order is coming in."));
		wire('log')->message('Ordermail: Admin - ' . $orderMailPage->mail_admin);
	}

	$m->setDynamicTemplateData("firstname", $p->customer_name);
	$m->setDynamicTemplateData("order", $p->order_text);
	$m->setDynamicTemplateData("price", 'Total amount: ' . number_format($p->order_total, 2, ',', '.') . '€');
	$m->setDynamicTemplateData("lastname", $p->customer_lastname);
	$m->setDynamicTemplateData("street", $p->customer_street);
	$m->setDynamicTemplateData("zipcode", $p->customer_zipcode);
	$m->setDynamicTemplateData("city", $p->customer_city);
	$m->setDynamicTemplateData("country", $p->customer_country);
	$m->setDynamicTemplateData("dstreet", $p->order_delivery_street);
	$m->setDynamicTemplateData("dzipcode", $p->order_delivery_zipcode);
	$m->setDynamicTemplateData("dcity", $p->order_delivery_city);
	$m->setDynamicTemplateData("dcountry", $p->order_delivery_country);
	$m->setDynamicTemplateData("wish", $p->order_wish);
	$m->setDynamicTemplateData("date", date('d.m.Y'));
	$m->setDynamicTemplateData("orderid", $p->name);
	
	$m->subject = 'New order';
	
	return $m->___send();
	*/

	if ($customerMail) {
		$to = $p->customer_email;
	} else {
		$to = $orderMailPage->mail_admin;
	}

	$subject = "Your Order";
	$totalPrice = number_format($p->order_total, 2, ',', '.');
	$date = date('d.m.Y');

	$message = "
	<html>
	<head>
	<title>New Order</title>
	</head>
	<body>
	<p>
	$orderMailPage->email_text_top
	</p>
	<table>
	
	<tr>
		<td>
			<strong>
				Email:
			</strong>
		</td>
		<td>$p->customer_email</td>
	</tr>
	<tr>
		<td>
			<strong>
				Name:
			</strong>
		</td>
		<td>$p->customer_name, $p->customer_lastname</td>
	</tr>
	<tr>
		<td>
			<strong>
				Street:
			</strong>
		</td>
		<td>$p->customer_street</td>
	</tr>
	<tr>
		<td>
			<strong>
				Zipcode:
			</strong>
		</td>
		<td>$p->customer_zipcode</td>
	</tr>
	<tr>
		<td>
			<strong>
				City:
			</strong>
		</td>
		<td>$p->customer_city</td>
	</tr>
	<tr>
		<td>
			<strong>
				Country:
			</strong>
		</td>
		<td>$p->customer_country</td>
	</tr>

	<tr>
		<td colspan='2'>
			<hr/>
		</td>
	</tr>

	<tr>
		<td>
			<strong>
				Delivery Street:
			</strong>
		</td>
		<td>$p->order_delivery_street</td>
	</tr>
	<tr>
		<td>
			<strong>
				Delivery Country:
			</strong>
		</td>
		<td>$p->order_delivery_zipcode</td>
	</tr>
	<tr>
		<td>
			<strong>
				Delivery Country:
			</strong>
		</td>
		<td>$p->order_delivery_city</td>
	</tr>
	<tr>
		<td>
			<strong>
				Delivery Country:
			</strong>
		</td>
		<td>$p->order_delivery_country</td>
	</tr>

	<tr>
		<td>
			<strong>
				Order:
			</strong>
		</td>
		<td>$p->order_text</td>
	</tr>
	<tr>
		<td>
			<strong>
				Price Total:
			</strong>
		</td>
		<td>$totalPrice</td>
	</tr>

	<tr>
		<td>
			<strong>
				Message:
			</strong>
		</td>
		<td>$p->order_wish</td>
	</tr>
	<tr>
		<td>
			<strong>
				Date:
			</strong>
		</td>
		<td>$date</td>
	</tr>
	<tr>
		<td>
			<strong>
				OrderID:
			</strong>
		</td>
		<td>$p->name</td>
	</tr>
	</table>
	<p>
	$orderMailPage->email_text_bottom
	</p>
	</body>
	</html>
	";

	return mail($to, $subject, $message, $headers);
}

/**
 * /site/templates/_func.php
 * 
 * Example of shared functions used by template files
 *
 * This file is currently included by _init.php 
 * 
 * FUN FACT: This file is identical to the one in the NON-multi-language
 * version of this site profile (site-default). In fact, it's rare that
 * one has to think about languages when developing a multi-language 
 * site in ProcessWire. 
 *
 */

/**
 * Given a group of pages, render a simple <ul> navigation
 *
 * This is here to demonstrate an example of a simple shared function.
 * Usage is completely optional.
 *
 * @param PageArray $items
 * @return string
 *
 */
function renderNav(PageArray $items)
{

	// $out is where we store the markup we are creating in this function
	$out = '';

	// cycle through all the items
	foreach ($items as $item) {

		// render markup for each navigation item as an <li>
		if ($item->id == wire('page')->id) {
			// if current item is the same as the page being viewed, add a "current" class to it
			$out .= "<li class='current'>";
		} else {
			// otherwise just a regular list item
			$out .= "<li>";
		}

		// markup for the link
		$out .= "<a href='$item->url'>$item->title</a> ";

		// if the item has summary text, include that too
		if ($item->summary) $out .= "<div class='summary'>$item->summary</div>";

		// close the list item
		$out .= "</li>";
	}

	// if output was generated above, wrap it in a <ul>
	if ($out) $out = "<ul class='nav'>$out</ul>\n";

	// return the markup we generated above
	return $out;
}


function renderNavTree($items)
{
	if ($items instanceof Page) $items = array($items);
	if (!count($items)) return;
	$maxLevel = 2;
	$out = '';

	foreach ($items as $item) {
		$level = $item->parents->count();

		if ($level > $maxLevel) {
			return;
		}

		if ($item->template == 'blog-item') {
			return;
		}

		$childClass = $item->hasChildren() ? 'nav-item nav-item--children ' : 'nav-item';

		if ($item->id == wire('page')->id) {
			$out .= "<li class='$childClass'>";
		} else {
			$out .= "<li class='$childClass'>";
		}

		$activeClass = $item->id == wire('page')->id ? 'active ' : '';
		$out .= "<a href='$item->url' class='$activeClass' rel='js__child-$item->id'>$item->title</a>";

		/*
		if ($item->hasChildren() && $item->template != 'blog' && $level < $maxLevel) {
			$out .= "<ul class='sub-menu'>";
			$out .= renderNavTree($item->children);
			$out .= "</ul>";
		}
		*/

		$out .= "</li>";
	}
	return $out;
}

function renderSubnavTree($items)
{
	$out = '';

	return '';

	foreach ($items as $item) {
		if ($item->hasChildren()) {
			$childs = '';
			foreach ($item->children as $child) {
				$childs .= '
				<li>
					<a href="' . $child->url . '">
					' . $child->title . '
					</a>
				</li>';
			}

			$out .= '
			<div class="subnav" id="js__child-' . $item->id . '">
				<ul>
					<li>
						<a href="' . $item->url . '">
						' . $item->title . '
						</a>
					</li>
					' . $childs . '
				</ul>
			</div>';
		}
	}

	return $out;
}

function renderTeaser($page)
{
	$out = '';
	$teaserImage = null;
	if ($page->teaer_image) {
		$teaserImage = $page->teaer_image ? $page->teaer_image->url : null;
	}

	if ($teaserImage) {
		$teaserHeadline = $page->teaser_headline ? '<h2>' . $page->teaser_headline . '</h2>' : '';
		$teaserText = $page->teaser_text ? '<h4 class="card-title ShopCategory__CardText">' . $page->teaser_text . '</h4>' : '';

		$out = '
            <a href="' . ($page->teaser_link ? $page->teaser_link->url : '#') . '" class="Teaser card bg-dark text-white Teaser__Link">
                <img loading="lazy" src="' . $teaserImage . '" class="card-img">
                <div class="card-img-overlay Teaser__Overlay">
                    <div class="Teaser__Overlay__Text text-center">
                        ' . $teaserHeadline . '
                        ' . $teaserText . '
                        ' . $page->teaser_linktext . '
                    </div>
                </div>
            </a>
        ';
	}
	return $out;
}

function renderFullImage($image)
{
	$out = '';

	$out = '
		<img loading="lazy" src="' . $image->url . '" class="card-img">
	';
	return $out;
}

function renderBoardOverview($boards, $title, $level = 0, $summary = null)
{
	if ($boards->count() == 0) return;
	$out = '';
	$boardTitle = $title ? '<div class="col-md-12 text-center"><h2>' . $title . '</h2></div>' : '';
	$summaryBody = $summary ? '<div class="col-md-12 text-center py-3"><p>' . $summary . '</p></div>' : '';

	if ($level > 0) {
		$out .= $boardTitle . $summaryBody;
		foreach ($boards as $board) {
			$image = $board->product_images->first();
			if ($image) {
				$image = '
				<a class="card--shop" href="' . $board->url . '">
					<img class="card-img" loading="lazy" src="' . $image->height(440)->url . '" />
				</a>';
			}
			$out .= '
			<div class="col-md-4 text-center">
					' . $image . '
					<h4>
						' . $board->title . '
					</h4>
					<p>
					' . $board->board_style . '
					</p>
					<a href="' . $board->url . '" class="btn">BUY NOW!</a>
			</div>
			';
		}
	} else {
		$out .= '<section class="container">';
		foreach ($boards as $board) {
			$out .= '
			<div class="row">
				<div class="col-md-12 text-center">
					' . $boardTitle . '
					' . $board->body . '
				</div>
			</div>
			<div class="row pb-10">
				' . renderBoardOverview($board->children, $board->title, count($board->parents), $board->summary) . '
			</div>
			';
		}
		$out .= '</section>';
	}

	return $out;
}

function renderBreadCrumbs($page)
{
	$out = "<nav aria-label='breadcrumb' class='breadcrumbs'><ol class='breadcrumb'>";

	$parents = $page->parents;

	foreach ($parents as $parent) {
		$url = $parent->url;
		$out .= "<li class='breadcrumb-item'><a href='$url'>{$parent->title}</a></li>\n";
	}

	$out .= "<li class='breadcrumb-item active' aria-current='page'>{$page->title}</li>\n";

	$out .= "</ol></nav>";

	return $out;
}

function renderGallery($images)
{
	$out = '';
	$imageset = '';
	$thumbset = '';

	foreach ($images as $image) {
		$imageset .= '
	  	<figure class="slick-big noVariant" rel="' . $image->url() . '">
			<img src="' . $image->height(470)->url() . '">
		</figure>
	  ';

		$thumbset .= '
	  	<figure>
			<img src="' . $image->height(64)->url() . '">
		</figure>
	  ';
	}

	$out .= '
	<div class="Gallery">
		<div class="slick-slider">
			' . $imageset . '
		</div>
		<div class="slick-thumbs">
			' . $thumbset . '
		</div>
	</div>
	';
	return $out;
}

function renderVariantsGallery($variants)
{
	$out = '';
	$imageset = '';
	$thumbset = '';

	foreach ($variants as $variantKey => $variantValue) {
		if (count($variantValue->images)) {
			foreach ($variantValue->images as $variantImages) {
				foreach ($variantImages as $variantImage) {
					$imageset .= '
						<figure  class="slick-big variants" data-id="' . $variantImage->height(470)->url() . '"  rel="' . $variantImage->url() . '">
							<img src="' . $variantImage->height(470)->url() . '">
						</figure>
					';

					$thumbset .= '
						<figure data-id="' . $variantImage->height(64)->url() . '">
							<img src="' . $variantImage->height(64)->url() . '">
						</figure>
					';
				}
			}
		}
	}

	$out .= '
	<div class="Gallery js_customVariantsGallery">
		<div class="slick-slider">
			' . $imageset . '
		</div>
		<div class="slick-thumbs">
			' . $thumbset . '
		</div>
	</div>
	';
	return $out;
}

function renderRating($rating)
{
	$out = '';
	$a = 0;
	while ($a < intval($rating)) {
		$out .= '<i class="rating rating--full"></i>';
		$a++;
	}

	for ($i = 6; $i > $a; $i--) {
		$out .= '<i class="rating rating--empty"></i>';
	}
	return $out;
}
