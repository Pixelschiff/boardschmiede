<?php namespace ProcessWire;

$out = '
<section class="container-fluid pb-5 bg--dark blog">
    <div class="row text-center">
        <div class="col-md-12 p-5">
            <h3>BLOG</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <a href="#" class="card bg-dark text-white">
                <img src="https://picsum.photos/500?1" class="card-img" alt="...">
                <div class="card-img-overlay">
                    <h5 class="card-title">Custom Blog Post</h5>
                    <span class="badge badge-secondary">today</span>
                </div>
            </a>
        </div>
        <div class="col-md-3">
            <a href="#" class="card bg-dark text-white">
                <img src="https://picsum.photos/500?2" class="card-img" alt="...">

                <div class="card-img-overlay">
                    <h5 class="card-title">Just an awesome weekend at the beach with those great boards</h5>
                    <span class="badge badge-secondary">yesterday</span>
                </div>
            </a>
        </div>
        <div class="col-md-3">
            <a href="#" class="card bg-dark text-white">
                <img src="https://picsum.photos/500?3" class="card-img" alt="...">

                <div class="card-img-overlay">
                    <h5 class="card-title">My job is done</h5>
                    <span class="badge badge-secondary">01.01.2020</span>
                </div>
            </a>
        </div>
        <div class="col-md-3">
            <a href="#" class="card bg-dark text-white">
                <img src="https://picsum.photos/500?4" class="card-img" alt="...">

                <div class="card-img-overlay">
                    <h5 class="card-title">Aloha 😁</h5>
                    <span class="badge badge-secondary">22.01.2020</span>
                </div>
            </a>
        </div>
    </div>
    <div class="row text-center pt-5">
        <div class="col-md-12">
            <a href="#">More posts...</a>
        </div>
    </div>
</section>
';

echo $out;

?>