<?php

namespace ProcessWire;


function renderCart($input)
{
    $items = '';
    $itemsClean = '';
    $out = '';

    if ($input->post('addToCart') == '1') {
        $item = array();

        foreach ($input->post as $key => $value) {
            $item = array_merge($item, [$key => $value]);
        }

        if (isset($_SESSION['cart'])) {
            array_push($_SESSION['cart'], $item);
        } else {
            $_SESSION['cart'] = array();
            array_push($_SESSION['cart'], $item);
        }
    }
    /*
    $_SESSION['cart'] = null;
    */

    if ($input->post('removeItem') == '1') {
        $index = 0;

        foreach ($_SESSION['cart'] as $itKey => $itValue) {
            if (array_search($input->post('orderItemID'), $itValue)) {
                $found = $itKey;
                unset($_SESSION['cart'][$itKey]);
            }
            $index++;
        }
    }

    $priceTotal = '';

    if (isset($_SESSION['cart'])) {

        foreach ($_SESSION['cart'] as $cartItem) {
            $productOverviewPage = wire('pages')->get($cartItem['itemID'])->parent();
            $item = wire('pages')->get($cartItem['itemID']);

            $cartItems = '';
            $itemsClean .= $item->title . '<br/>';

            foreach ($cartItem as $itemKey => $itemValue) {
                if (
                    $itemKey != 'addToCart'
                    && $itemKey != 'price_base'
                    && $itemKey != 'price_variants'
                    && $itemKey != 'price_variants2'
                    && $itemKey != 'itemID'
                    && $itemKey != 'orderItemID'
                    && $itemKey != 'previewImage'
                ) {
                    if ($itemKey == 'priceTotal') {
                        $cartItems .= '<span class="Cart__Item">Price: <b>' . (number_format(floatval($itemValue), 2, ',', '.')) . '€</b></span>';
                        $itemsClean .= 'Price: ' . (number_format(floatval($itemValue), 2, ',', '.')) . '€<br/>';
                        $priceTotal = floatval($priceTotal) + floatval($itemValue);
                    } else {
                        if (substr($itemKey, 0, 8) === "variant_") {
                            $cleanKey = str_replace('variant_', '', $itemKey);
                            $variantTitle =
                                $productOverviewPage->prod_custom_variants->get('id=' . $cleanKey . '')
                                ? $productOverviewPage->prod_custom_variants->get('id=' . $cleanKey . '')->title
                                : $item->prod_custom_variants->get('id=' . $cleanKey . '')->title;
                            $cartItems .= '<span class="Cart__Item">' . $variantTitle . ': <small>' . $itemValue . '</small></span>';

                            $itemsClean .= $variantTitle . ': ' . $itemValue . '<br/>';
                        } else {
                            $cartItems .= '<span class="Cart__Item">' . $itemKey . ': <small>' . $itemValue . '</small></span>';
                            $itemsClean .= $itemKey . ': ' . $itemValue . '<br/>';
                        }
                    }
                }
            }
            $itemsClean .= '<hr/>';

            $cartBox = '
                <div class="row">
                    <div class="col-12">
                        ' . $item->title . '
                    </div>
                    <div class="col-3">
                        <img src="' . $cartItem['previewImage'] . '" alt="' . $item->title . '" />
                    </div>
                    <div class="col-9 d-flex flex-column">
                        ' . $cartItems . '
                        <div class="text-right">
                            <form method="POST">
                                <input type="hidden" name="orderItemID" value="' . $cartItem["orderItemID"] . '" />
                                <input type="hidden" name="removeItem" value="1" />
        
                                <button class="btn btn-link btn-text" type="submit">
                                ' . __('Remove') . '
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            ';

            $items .= '<li>' . $cartBox . '</li>';
        };
    }

    if ($input->post('submitItemID') && $_SESSION['cart']) {
        $orderNumber = date('Ymd') . '-' . wire('pages')->find("template=order")->count();
        $p = new Page();
        $p->template = 'order';
        $p->parent = wire('pages')->get(1282);
        $p->name = 'order_' . $orderNumber;
        $p->title = 'order_' . $orderNumber;

        $p->customer_name = $input->post('name');
        $p->customer_lastname = $input->post('lastname');
        $p->customer_street = $input->post('street');
        $p->customer_zipcode = $input->post('zipcode');
        $p->customer_city = $input->post('city');
        $p->customer_country = $input->post('country');
        $p->customer_email = $input->post('email');
        $p->customer_phone = $input->post('phone');
        $p->order_wish = $input->post('wishes');

        $p->order_delivery_street = $input->post('street_delivery') | $input->post('street');
        $p->order_delivery_zipcode = $input->post('zipcode_delivery') | $input->post('zipcode');
        $p->order_delivery_city = $input->post('city_delivery') | $input->post('city');
        $p->order_delivery_country = $input->post('country_delivery') | $input->post('country');
        $p->order_total = $priceTotal;

        $p->order_text = $itemsClean;
        $p->save();
        $_SESSION['cart'] = null;

        if (sendNewOrderMail($p, wire('pages')->get('/orders/'))) {
            wire('log')->message('Order Customer: Mail sent');
        };

        if (sendNewOrderMail($p, wire('pages')->get('/orders/'), false)) {
            wire('log')->message('Order Admin: Mail sent');
        };
    }

    if (isset($_SESSION['cart']) && count($_SESSION['cart']) > 0) {

        $customerForm = '
        <form method="post" action="' . wire('pages')->get('/thank-you')->url . '">
        <h5>' . __("Customer data") . '</h5>
        <div class="row">
            <div class="col-12 col-md-6">
                <div class="input-group mb-3">
                    <input type="text" class="form-control" placeholder="' . __("Name") . '" name="name" required>
                </div>
            </div>
            <div class="col-12 col-md-6">
                <div class="input-group mb-3">
                    <input type="text" class="form-control" placeholder="' . __("Lastname") . '" name="lastname" required>
                </div>
            </div>
            <div class="col-12 col-md-6">
                <div class="input-group mb-3">
                    <input type="text" class="form-control" placeholder="' . __("Email") . '" name="email" required>
                </div>
            </div>
            <div class="col-12 col-md-6">
                <div class="input-group mb-3">
                    <input type="text" class="form-control" placeholder="' . __("Telephone") . '" name="phone">
                </div>
            </div>
    
            <div class="col-12">
                <h5>Rechnungsadresse:</h5>
            </div>
            <div class="col-12 col-md-6">
                <div class="input-group mb-3">
                    <input type="text" class="form-control" placeholder="' . __("Street") . '" name="street" required>
                </div>
            </div>
            <div class="col-12 col-md-6">
                <div class="input-group mb-3">
                    <input type="text" class="form-control" placeholder="' . __("Zipcode") . '" name="zipcode" required>
                </div>
            </div>
            <div class="col-12 col-md-6">
                <div class="input-group mb-3">
                    <input type="text" class="form-control" placeholder="' . __("City") . '" name="city" required>
                </div>
            </div>
            <div class="col-12 col-md-6">
                <div class="input-group mb-3">
                    <input type="text" class="form-control" placeholder="' . __("Country") . '" name="country" required>
                </div>
            </div>
    
            <div class="col-12">
                <h5>Lieferadresse:</h5>
            </div>
            <div class="col-12 col-md-6">
                <div class="input-group mb-3">
                    <input type="text" class="form-control" placeholder="' . __("Street") . '" name="street_delivery">
                </div>
            </div>
            <div class="col-12 col-md-6">
                <div class="input-group mb-3">
                    <input type="text" class="form-control" placeholder="' . __("Zipcode") . '" name="zipcode_delivery">
                </div>
            </div>
            <div class="col-12 col-md-6">
                <div class="input-group mb-3">
                    <input type="text" class="form-control" placeholder="' . __("City") . '" name="city_delivery">
                </div>
            </div>
            <div class="col-12 col-md-6">
                <div class="input-group mb-3">
                    <input type="text" class="form-control" placeholder="' . __("Country") . '" name="country_delivery">
                </div>
            </div>
    
            <div class="col-12">
                <h5>Bemerkungen:</h5>
            </div>
            <div class="col-12">
                <div class="input-group mb-3">
                    <textarea class="form-control" id="wishes" name="wishes" rows="3"></textarea>
                </div>
            </div>
        </div>

        <div class="row pb-3">
            <div class="col-md-12">
                <small>
                    ' . __("Price inclusive 21% TAX") . '
                </small>
            </div>
        </div>

        <div class="row pb-10 pb-md-0">
            <div class="col-md-12">
                <div class="d-flex justify-content-between">
                    <h5>
                        Total: ' . (number_format($priceTotal, 2, ',', '.')) . '€
                    </h5>
                    <div>
                        <input type="hidden" name="submitItemID" value="' . $cartItem["orderItemID"] . '" />
                        <button class="btn btn-primary btn-md" type="submit">' . __("Order now!") . '</button>
                    </div>
                </div>
            </div>
        </div>
    
    </form>
        ';

        $out = '
        <ul class="navbar-nav ml-auto">
            <li class="nav-item snipcart-summary jRight">
                <a href="#" class="nav-link" data-toggle="collapse" data-target="#shoppingCart">
                    <div class="d-flex">
                        <span class="material-icons">
                        shopping_cart
                        </span>
                        <span class="uk-badge uk-text-middle" aria-label="Items in cart">
                            ' . count($_SESSION['cart']) . '
                        </span>
                        <small class="ml-2 uk-text-middle d-none d-md-flex" aria-label="Total">
                            (' . (number_format($priceTotal, 2, ',', '.')) . '€)
                        </small>
                    </div>
                </a>
            </li>
        </ul>
        ';

        $out .= '
        <div class="card Cart collapse" id="shoppingCart">
            <button class="Cart__Close" data-toggle="collapse" data-target="#shoppingCart">
                <span class="material-icons">
                    close
                </span>
            </button>
            <div class="card-body">
                
                <div class="row">
                    <div class="col-md-6">
                        <h4 class="card-title">' . __("Your cart") . '</h4>
                        <p class="card-text">
                            <ul class="Cart__List">
                                ' . $items . '
                            </ul>
                        </p>
                    </div>
                    <div class="col-md-6 Cart__Customer">
                        ' . (count($_SESSION['cart']) > 0 ? $customerForm : '') . '
                    </div>
                </div>
            </div>
        </div>';
    }


    return $out;
}
