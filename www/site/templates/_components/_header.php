<?php

namespace ProcessWire;

$headerImages = null;

if ($page->coverimage && $page->coverimage->count() > 0) {
    foreach ($page->coverimage as $coverimage) {
        $headerImages .= '<div><img class="header__slider__image" src="' . $coverimage->url . '" /></div>';
    }
} else if ($page->parent->coverimage->count() > 0) {
    foreach ($page->parent->coverimage as $coverimage) {
        $headerImages .= '<div><img class="header__slider__image" src="' . $coverimage->url . '" /></div>';
    }
} else if ($page->parent->parent->parent->coverimage && $page->parent->parent->parent->coverimage->count() > 0) {
    foreach ($page->parent->parent->coverimage as $coverimage) {
        $headerImages .= '<div><img class="header__slider__image" src="' . $coverimage->url . '" /></div>';
    }
}

if (true || $headerImages && !$page->hide_header && $page->template != 'home') {
    echo '
    <section class="header js__slideshow ' . $coverClass . '">
        ' . $headerImages . '
    </section>
    ';
} else {
    echo '
    <section class="header ' . $coverClass . '">
        <video class="header__video" poster="' . $headerImages . '" autoplay playsinline muted loop>
            <source src="' . $config->urls->templates . 'lib/clips/loop_teaser_1.mp4" type="video/mp4">
        </video>
    </section>
    ';
};
