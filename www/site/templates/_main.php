<?php

namespace ProcessWire; ?>
<!DOCTYPE html>
<html lang="<?php echo _x('en', 'HTML language code'); ?>">

<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title><?php echo $title; ?></title>
    <link href="//fonts.googleapis.com/css?family=Lusitana:400,700|Quattrocento:400,700" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="<?php echo $config->urls->templates ?>lib/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo $config->urls->templates ?>lib/slick/slick.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo $config->urls->templates ?>lib/slick/slick-theme.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo $config->urls->templates ?>lib/css/styles.css?v=4.0" />
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.1/umd/popper.min.js" integrity="sha512-ubuT8Z88WxezgSqf3RLuNi5lmjstiJcyezx34yIU2gAHonIi27Na7atqzUZCOoY4CExaoFumzOsFQ2Ch+I/HCw==" crossorigin="anonymous"></script>
    <script type='text/javascript' src="<?php echo $config->urls->templates ?>lib/js/jquery.min.js"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.css">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-lightbox/0.2.12/slick-lightbox.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/slick-lightbox/0.2.12/slick-lightbox.css" rel="stylesheet" />
    <script type='text/javascript' src="<?php echo $config->urls->templates ?>lib/js/bootstrap.min.js"></script>
    <script type='text/javascript' src="<?php echo $config->urls->templates ?>lib/slick/slick.min.js"></script>
    <script src="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.min.js"></script>
    <script type='text/javascript' src="<?php echo $config->urls->templates ?>lib/js/main.js?v=4.4"></script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css" integrity="sha512-Fo3rlrZj/k7ujTnHg4CGR2D7kSs0v4LLanw2qksYuRlEzO+tcaEPQogQ0KaoGN26/zrn20ImR1DfuLWnOo7aBA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <?php

    foreach ($languages as $language) {
        if (!$page->viewable($language)) continue;
        $url = $page->localHttpUrl($language);
        $hreflang = $homepage->getLanguageValue($language, 'name');
        echo "\n\t<link rel='alternate' hreflang='$hreflang' href='$url' />";
    }

    echo $page->seo_maestro ? $page->seo_maestro->render() : '';

    ?>
    <script id="mcjs">
        ! function(c, h, i, m, p) {
            m = c.createElement(h), p = c.getElementsByTagName(h)[0], m.async = 1, m.src = i, p.parentNode.insertBefore(m, p)
        }(document, "script", "https://chimpstatic.com/mcjs-connected/js/users/e6587550a2a78449d4a97261a/5c42e48df5a67d4af06ab63de.js");
    </script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-7RHQBE8LFQ"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'G-7RHQBE8LFQ');
    </script>
</head>
<?php
$bgClass = receiveBg($page);
$noCoverClass = (!$page->coverimage && !$page->parent->coverimage && !$page->parent->parent->coverimage || $page->template == 'gallery') ? 'noCover' : '';
?>

<body class="<?= $bgClass ?> <?= $noCoverClass ?>">
    <?php
    include('./_components/_navigation.php');

    $coverClass = '';
    if ($page->template == 'home' || $page->large_header) {
        $coverClass = 'header--home';
    }
    include('./_components/_header.php');
    ?>

    <main>
        <?php
        if ($page->template != 'home') {
            echo '<section class="container">';
            echo renderBreadCrumbs($page);
            echo '</section>';
        }
        ?>
        <?= $content ?>
        <?= $sections ?>

        <?php
        //include_once("./_components/_blog.php");
        ?>
    </main>

    <footer class="container py-5">

        <div class="row Newsletter__Row">
            <div class="col-12 col-md-6 text-center pr-md-5 pb-5 pb-md-0">
                <div>
                    <h3 class="pb-3">GET IN TOUCH:</h3>
                    <a href="https://www.facebook.com/boardschmiede.de" class="mr-2" target="_blank">
                        <i class="fab fa-facebook fa-2x"></i>
                    </a>
                    <a href="https://www.instagram.com/boardschmiede/" target="_blank">
                        <i class="fab fa-instagram fa-2x"></i>
                    </a>
                </div>
            </div>

            <div class="col-12 col-md-6 pl-md-5 text-center border-left-line">
                <?php include('./_components/_newsletter.php'); ?>
            </div>

        </div>

        <div class="row">

            <div class="col-md-12 text-center pt-5">
                <img class="disp--dark mr-3" src="<?= $config->urls->templates ?>lib/img/PayPal.svg.png" height="22" />
                <img class="disp--dark mr-3" src="<?= $config->urls->templates ?>lib/img/MasterCard_Logo.svg.png" height="22" />
                <img class="disp--dark" src="<?= $config->urls->templates ?>lib/img/Visa_Inc._logo.svg.png" height="22" />
            </div>

            <div class="col-md-12 mt-5 text-center">
                <small>
                    &copy; <?= date('Y') ?> Boardschmiede.com - <a class="link--white" href="/impressum" rel="nofollow">Impressum</a>
                </small>
            </div>

        </div>
    </footer>
    <script id="__bs_script__">
        //<![CDATA[
        document.write("<script async src='http://HOST:8080/browser-sync/browser-sync-client.js?v=2.26.7'><\/script>".replace("HOST", location.hostname));
        //]]>
    </script>

    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
    <script type="text/javascript">
        $('.slider-for').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            fade: true,
            asNavFor: '.slider-nav',
        });
        $('.slider-nav').slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            asNavFor: '.slider-for',
            centerMode: false,
            focusOnSelect: true,
            infinite: true,
            vertical: true,
            variableHeight: true
        });
    </script>

</body>

</html>