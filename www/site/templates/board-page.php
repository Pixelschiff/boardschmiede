<?php namespace ProcessWire;

$content = ''; 


$sections = '';
$sections .= '
		<section class="container pt-5">
		<div class="row pb-5">
			<div class="col-md-12">
				<h1>
				'.$page->title.'
				</h1>
				<h5>
					'.$page->parent->title.'
				</h5>
			</div>
		</div>
		<div class="row pb-5">
			<div class="col-md-4">
				<img src="'.$page->board_images->first->url.'" />
			</div>
			<div class="col-md-4">
				'.$page->body.'
			</div>
			<div class="col-md-4">
				<div class="card">
					<div class="card-body">
					'.$page->board_features.'
					<form class="pb-2">
						<div class="form-group">
							<label for="boardsize">Choose a size</label>
							<select class="form-control" id="boardsize">
							<option>135x39cm</option>
							<option>140x44cm</option>
							</select>
						</div>
	
						<div class="form-group">
							<label for="bindings">Choose a binding</label>
							<select class="form-control" id="bindings">
							<option>Boardschmiede Binding</option>
							<option>No binding</option>
							</select>
						</div>
	
						<div class="form-group">
							<label for="fins">Choose a set of fins</label>
							<select class="form-control" id="fins">
							<option>Freestyle 4.5</option>
							<option>Wakestyle 2.25</option>
							</select>
						</div>
					</form>
					<a href="#" class="btn btn-primary">BUY NOW!</a>
					
					</div>
				</div>
			</div>
		</div>
		</section>
';

if($page->video_url) {
	$sections .= '
		<section class="container">
		<div class="row p-5">
			<div class="col-md-12">
			'.$page->video_url.'
			</div>
		</div>
		</section>
	';
}

if($page->text_fields->count > 0) {
	$sections .= '
	<section class="bg--white">
		<div class="container-fluid p-5">';
			$blockIndex = 0;
			foreach ($page->text_fields as $text_block) {
				$alignClass = $blockIndex%2 ? 'flex-row-reverse' : 
				'';

				$textAlignClass = !$blockIndex%2 ? 'text-right' : 
				'';
				$sections .= '
				<div class="row p-5 '.$alignClass.'">
					<div class="col-md-6 '.$textAlignClass.'">
						<h3>
						'.$text_block->headline.'
						</h3>	
						<p>
						'.$text_block->textblock.'
						</p>
					</div>
					<div class="col-md-6">
						<img src="'.$text_block->image->url.'" />
					</div>
				</div>
				';
				$blockIndex++;
			}
			$sections .= '
		</div>
	</sections>
	';
}