const bsync = require('browser-sync');
const gulp = require('gulp');
const sass = require('gulp-sass');
const postcss = require("gulp-postcss");
const autoprefixer = require('autoprefixer');


function sync(done) {
    bsync.init({
        files: [
            'www/**/*.php',
            'src/scss/**/*.scss',
            'src/js/**/*.js'
        ],
        host: '0.0.0.0',
        proxy: {
            target: "localhost:8001",
        },
        ghostMode: false,
        notify: true
    });
    done();
}

function styles() {
    const plugins = [
        autoprefixer({cascade: false}),
        require('cssnano')({
            preset: 'default',
        }),
      ];
    return gulp.src(['./src/scss/**/*.scss'])
        .pipe(sass({ outputStyle: 'expanded' }).on('error', sass.logError))
        .pipe(postcss(plugins))
        .pipe(gulp.dest('./www/site/templates/lib/css'))
        .pipe(bsync.stream());
}

function nodeJs() {
    return gulp.src(['node_modules/popper.js/dist/umd/popper.min.js', 'node_modules/lightbox2/dist/js/lightbox.min.js'])
        .pipe(gulp.dest("./www/site/templates/lib/js"));
};

function nodeCss() {
    return gulp.src(['node_modules/lightbox2/dist/css/lightbox.min.css'])
        .pipe(gulp.dest("./www/site/templates/lib/css"));
};

function nodeImg() {
    return gulp.src(['node_modules/lightbox2/dist/images/*'])
        .pipe(gulp.dest("./www/site/templates/lib/images"));
};

function watchFiles() {
    gulp.watch('./src/scss/**/*.scss', styles);
}

gulp.task('default', gulp.series(sync,nodeJs,nodeCss,nodeImg, styles, watchFiles));
gulp.task('css', gulp.series(styles));